var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
   // mix.sass('app.scss');

    mix.copy('resources/assets/bootstrap','public/bootstrap');
    mix.copy('resources/assets/bootstrap-extension','public/bootstrap-extension');

    mix.copy('resources/assets/css','public/css');
    mix.copy('resources/assets/images','public/images');
    mix.copy('resources/assets/jquery','public/jquery');
    mix.copy('resources/assets/less','public/less');
    mix.copy('resources/assets/js','public/js');
    mix.copy('resources/assets/morrisjs','public/morrisjs');
    mix.copy('resources/assets/peity','public/peity');
    mix.copy('resources/assets/sidebar-nav','public/sidebar-nav');
    mix.copy('resources/assets/styleswitcher','public/styleswitcher');
    mix.copy('resources/assets/angular','public/angular');
    mix.copy('resources/assets/tablesaw-master','public/tablesaw-master');

    mix.scripts(['../angular/angularscript.js','../angular/service/*.js','../angular/controller/*.js'],'public/angular/app.js');

});

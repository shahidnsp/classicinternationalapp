<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application pages
    |--------------------------------------------------------------------------
    |
    | All application pages must listed here for user information and
    | permission management
    |
    */

    'pages' =>[
        [
            'title'=>'Dashboard',
            'name'=>'Dashboard',
            'route'=>'/',
            'group'=>'Dashboard',
            'type'=>'Main',
            'alias'=>'dashboard'
        ],
        [
            'title'=>'Account Head',
            'name'=>'Account Head(Main Account)',
            'route'=>'main-account-account-head',
            'group'=>'Main Account',
            'type'=>'Main',
            'alias'=>'mainaccounthead'
        ],
        [
            'title'=>'Income Register',
            'name'=>'Income Register(Main Account)',
            'route'=>'main-account-income',
            'group'=>'Main Account',
            'type'=>'Main',
            'alias'=>'mainincome'
        ],
        [
            'title'=>'Expense Register',
            'name'=>'Expense Register(Main Account)',
            'route'=>'main-account-expense',
            'group'=>'Main Account',
            'type'=>'Main',
            'alias'=>'mainexpense'
        ],
        [
            'title'=>'Bill Book',
            'name'=>'Bill Book(Main Account)',
            'route'=>'main-account-billbook',
            'group'=>'Main Account',
            'type'=>'Main',
            'alias'=>'mainbillbook'
        ],
        [
            'title'=>'Day Book',
            'name'=>'Day Book(Main Account)',
            'route'=>'main-account-daybook',
            'group'=>'Main Account',
            'type'=>'Main',
            'alias'=>'maindaybook'
        ],
        [
            'title'=>'Designation',
            'name'=>'Designation(Main Account)',
            'route'=>'showdesignation',
            'group'=>'Main Account',
            'type'=>'Sub',
            'alias'=>'designation'
        ],
        [
            'title'=>'Employee Register',
            'name'=>'Employee Register(Main Account)',
            'route'=>'showemployee',
            'group'=>'Main Account',
            'type'=>'Sub',
            'alias'=>'employee'
        ],
        [
            'title'=>'Attendance',
            'name'=>'Attendance(Main Account)',
            'route'=>'showattendance',
            'group'=>'Main Account',
            'type'=>'Sub',
            'alias'=>'attendance'
        ],
        [
            'title'=>'Account Head',
            'name'=>'Account Head(Courier Account)',
            'route'=>'courier-account-account-head',
            'group'=>'Courier Account',
            'type'=>'Main',
            'alias'=>'courieraccounthead'
        ],
        [
            'title'=>'Income Register',
            'name'=>'Income Register(Courier Account)',
            'route'=>'courier-account-income',
            'group'=>'Courier Account',
            'type'=>'Main',
            'alias'=>'courierincome'
        ],
        [
            'title'=>'Expense Register',
            'name'=>'Expense Register(Courier Account)',
            'route'=>'courier-account-expense',
            'group'=>'Courier Account',
            'type'=>'Main',
            'alias'=>'courierexpense'
        ],
        [
            'title'=>'Day Book',
            'name'=>'Day Book(Courier Account)',
            'route'=>'courier-account-daybook',
            'group'=>'Courier Account',
            'type'=>'Main',
            'alias'=>'courierdaybook'
        ],
        [
            'title'=>'Courier Register',
            'name'=>'Courier Register(Courier Account)',
            'route'=>'showcourier',
            'group'=>'Courier Account',
            'type'=>'Main',
            'alias'=>'courier'
        ],
        [
            'title'=>'Account Head',
            'name'=>'Account Head(Nurse Account)',
            'route'=>'nurse-account-account-head',
            'group'=>'Nurse Account',
            'type'=>'Main',
            'alias'=>'nurseaccounthead'
        ],
        [
            'title'=>'Income Register',
            'name'=>'Income Register(Nurse Account)',
            'route'=>'nurse-account-income',
            'group'=>'Nurse Account',
            'type'=>'Main',
            'alias'=>'nurseincome'
        ],
        [
            'title'=>'Expense Register',
            'name'=>'Expense Register(Nurse Account)',
            'route'=>'nurse-account-expense',
            'group'=>'Nurse Account',
            'type'=>'Main',
            'alias'=>'nurseexpense'
        ],
        [
            'title'=>'Day Book',
            'name'=>'Day Book(Nurse Account)',
            'route'=>'nurse-account-daybook',
            'group'=>'Nurse Account',
            'type'=>'Main',
            'alias'=>'nursedaybook'
        ],
        [
            'title'=>'Batch',
            'name'=>'Batch(Nurse Account)',
            'route'=>'showbatch',
            'group'=>'Nurse Account',
            'type'=>'Sub',
            'alias'=>'batch'
        ],
        [
            'title'=>'Hospitals',
            'name'=>'Hospitals(Nurse Account)',
            'route'=>'showhospital',
            'group'=>'Nurse Account',
            'type'=>'Sub',
            'alias'=>'nursehospital'
        ],
        [
            'title'=>'Nurse Register',
            'name'=>'Nurse Register(Nurse Account)',
            'route'=>'shownurse',
            'group'=>'Nurse Account',
            'type'=>'Sub',
            'alias'=>'nurse'
        ],
        [
            'title'=>'Account Head',
            'name'=>'Account Head(Rent Account)',
            'route'=>'rent-account-account-head',
            'group'=>'Rent Account',
            'type'=>'Main',
            'alias'=>'rentaccounthead'
        ],
        [
            'title'=>'Income Register',
            'name'=>'Income Register(Rent Account)',
            'route'=>'rent-account-income',
            'group'=>'Rent Account',
            'type'=>'Main',
            'alias'=>'rentincome'
        ],
        [
            'title'=>'Expense Register',
            'name'=>'Expense Register(Rent Account)',
            'route'=>'rent-account-expense',
            'group'=>'Rent Account',
            'type'=>'Main',
            'alias'=>'rentexpense'
        ],
        [
            'title'=>'Day Book',
            'name'=>'Day Book(Rent Account)',
            'route'=>'rent-account-daybook',
            'group'=>'Rent Account',
            'type'=>'Main',
            'alias'=>'rentdaybook'
        ],
        [
            'title'=>'Rent Register',
            'name'=>'Rent Register(Rent Account)',
            'route'=>'showrent',
            'group'=>'Rent Account',
            'type'=>'Main',
            'alias'=>'rent'
        ],
        [
            'title'=>'Rent Payment',
            'name'=>'Rent Payment(Rent Account)',
            'route'=>'showrentcollection',
            'group'=>'Rent Account',
            'type'=>'Main',
            'alias'=>'rentcollection'
        ],
        [
            'title'=>'Rent Chart',
            'name'=>'Rent Chart(Rent Account)',
            'route'=>'rentchart',
            'group'=>'Rent Account',
            'type'=>'Main',
            'alias'=>'rentchart'
        ],
        [
            'title'=>'Account Head',
            'name'=>'Account Head(Transportation)',
            'route'=>'transportation-account-account-head',
            'group'=>'Transportation',
            'type'=>'Main',
            'alias'=>'transportationaccounthead'
        ],
        [
            'title'=>'Income Register',
            'name'=>'Income Register(Transportation)',
            'route'=>'transportation-account-income',
            'group'=>'Transportation',
            'type'=>'Main',
            'alias'=>'transportationincome'
        ],
        [
            'title'=>'Expense Register',
            'name'=>'Expense Register(Transportation)',
            'route'=>'transportation-account-expense',
            'group'=>'Transportation',
            'type'=>'Main',
            'alias'=>'transportationexpense'
        ],
        [
            'title'=>'Day Book',
            'name'=>'Day Book(Transportation)',
            'route'=>'transportation-account-daybook',
            'group'=>'Transportation',
            'type'=>'Main',
            'alias'=>'transportationdaybook'
        ],
        [
            'title'=>'Residence Area',
            'name'=>'Residence Area(Transportation)',
            'route'=>'showresidence',
            'group'=>'Transportation',
            'type'=>'Sub',
            'alias'=>'residencearea'
        ],
        [
            'title'=>'Hospitals',
            'name'=>'Hospitals(Transportation)',
            'route'=>'showhospital',
            'group'=>'Transportation',
            'type'=>'Sub',
            'alias'=>'transportationhospital'
        ],
        [
            'title'=>'Drivers',
            'name'=>'Drivers(Transportation)',
            'route'=>'showdriver',
            'group'=>'Transportation',
            'type'=>'Sub',
            'alias'=>'driver'
        ],
        [
            'title'=>'Candidate Register',
            'name'=>'Candidate Register(Transportation)',
            'route'=>'showcandidate',
            'group'=>'Transportation',
            'type'=>'Sub',
            'alias'=>'candidate'
        ],
        [
            'title'=>'Fair Register',
            'name'=>'Fair Register(Transportation)',
            'route'=>'fair_register_report_first',
            'group'=>'Transportation',
            'type'=>'Main',
            'alias'=>'fair'
        ],
        [
            'title'=>'Duty Chart',
            'name'=>'Duty Chart(Transportation)',
            'route'=>'dutychart',
            'group'=>'Transportation',
            'type'=>'Main',
            'alias'=>'dutychart'
        ],
        /*[
            'title'=>'Fair Register Report 1',
            'name'=>'Fair Register Report 1(Transportation)',
            'route'=>'fair_register_report_first',
            'group'=>'Transportation',
            'type'=>'Main',
            'alias'=>'fair_register_report_first'
        ],*/
        [
            'title'=>'Fair Register Report 2',
            'name'=>'Fair Register Report 2(Transportation)',
            'route'=>'fair_register_report_second',
            'group'=>'Transportation',
            'type'=>'Main',
            'alias'=>'fair_register_report_second'
        ],
        [
            'title'=>'Salary Advance',
            'name'=>'Salary Advance(Salary)',
            'route'=>'showsalaryadvance',
            'group'=>'Salary',
            'type'=>'Main',
            'alias'=>'salaryadvance'
        ],
        [
            'title'=>'Salary Register',
            'name'=>'Salary Register(Salary)',
            'route'=>'showsalary',
            'group'=>'Salary',
            'type'=>'Main',
            'alias'=>'salary'
        ],
        [
            'title'=>'Salary Chart',
            'name'=>'Salary Chart(Salary)',
            'route'=>'salarychart',
            'group'=>'Salary',
            'type'=>'Main',
            'alias'=>'salarychart'
        ],
        [
            'title'=>'Day Book',
            'name'=>'Day Book(Day Book)',
            'route'=>'overalldaybook',
            'group'=>'Daybook',
            'type'=>'Main',
            'alias'=>'daybook'
        ],
        [
            'title'=>'Income Register',
            'name'=>'Income Register(Day Book)',
            'route'=>'overallincome',
            'group'=>'Daybook',
            'type'=>'Main',
            'alias'=>'overallincome'
        ],
        [
            'title'=>'Expense Register',
            'name'=>'Expense Register(Day Book)',
            'route'=>'overallexpense',
            'group'=>'Daybook',
            'type'=>'Main',
            'alias'=>'overallexpense'
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => 'http://localhost',

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    //'timezone' => 'UTC',
    'timezone' => 'Asia/Kuwait',
   // 'timezone' => 'Asia/Kolkata',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY', 'SomeRandomString'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */

    'log' => env('APP_LOG', 'single'),

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Foundation\Providers\ArtisanServiceProvider::class,
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Routing\ControllerServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,
        Illuminate\Html\HtmlServiceProvider::class,
        Maatwebsite\Excel\ExcelServiceProvider::class,
        Kim\Activity\ActivityServiceProvider::class,
        Barryvdh\DomPDF\ServiceProvider::class,

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App'       => Illuminate\Support\Facades\App::class,
        'Artisan'   => Illuminate\Support\Facades\Artisan::class,
        'Auth'      => Illuminate\Support\Facades\Auth::class,
        'Blade'     => Illuminate\Support\Facades\Blade::class,
        'Bus'       => Illuminate\Support\Facades\Bus::class,
        'Cache'     => Illuminate\Support\Facades\Cache::class,
        'Config'    => Illuminate\Support\Facades\Config::class,
        'Cookie'    => Illuminate\Support\Facades\Cookie::class,
        'Crypt'     => Illuminate\Support\Facades\Crypt::class,
        'DB'        => Illuminate\Support\Facades\DB::class,
        'Eloquent'  => Illuminate\Database\Eloquent\Model::class,
        'Event'     => Illuminate\Support\Facades\Event::class,
        'File'      => Illuminate\Support\Facades\File::class,
        'Gate'      => Illuminate\Support\Facades\Gate::class,
        'Hash'      => Illuminate\Support\Facades\Hash::class,
        'Input'     => Illuminate\Support\Facades\Input::class,
        'Lang'      => Illuminate\Support\Facades\Lang::class,
        'Log'       => Illuminate\Support\Facades\Log::class,
        'Mail'      => Illuminate\Support\Facades\Mail::class,
        'Password'  => Illuminate\Support\Facades\Password::class,
        'Queue'     => Illuminate\Support\Facades\Queue::class,
        'Redirect'  => Illuminate\Support\Facades\Redirect::class,
        'Redis'     => Illuminate\Support\Facades\Redis::class,
        'Request'   => Illuminate\Support\Facades\Request::class,
        'Response'  => Illuminate\Support\Facades\Response::class,
        'Route'     => Illuminate\Support\Facades\Route::class,
        'Schema'    => Illuminate\Support\Facades\Schema::class,
        'Session'   => Illuminate\Support\Facades\Session::class,
        'Storage'   => Illuminate\Support\Facades\Storage::class,
        'URL'       => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View'      => Illuminate\Support\Facades\View::class,
        'Form'      => Illuminate\Html\FormFacade::class,
        'Html'      => Illuminate\Html\HtmlFacade::class,
        'Excel' => Maatwebsite\Excel\Facades\Excel::class,
        'Activity' => Kim\Activity\ActivityFacade::class,
        'PDF' => Barryvdh\DomPDF\Facade::class,
    ],

];

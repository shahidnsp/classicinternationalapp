/**
 * Created by Shahid Neermunda on 12/05/15.
 */

angular.module('FairCollectionService',[]).factory('FairCollection',['$resource',
    function($resource){
        return $resource('/api/faircollection/:faircollectionId',{
            faircollectionId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 19/5/15.
 */

angular.module('SalaryService',[]).factory('Salary',['$resource',
    function($resource){
        return $resource('/api/salaries/:salaryId',{
            salaryId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
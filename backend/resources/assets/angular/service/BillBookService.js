/**
 * Created by Shahid Neermunda on 8/5/15.
 */

angular.module('BillBookService',[]).factory('BillBook',['$resource',
    function($resource){
        return $resource('/api/bill/:billId',{
            billId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
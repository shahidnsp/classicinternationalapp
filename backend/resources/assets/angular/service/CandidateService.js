/**
 * Created by Shahid Neermunda on 9/5/15.
 */

angular.module('CandidateService',[]).factory('Candidate',['$resource',
    function($resource){
        return $resource('/api/transportation/:transportationId',{
            transportationId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
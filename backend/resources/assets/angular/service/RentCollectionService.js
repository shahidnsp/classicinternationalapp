/**
 * Created by Shahid Neermunda on 12/05/15.
 */

angular.module('RentCollectionService',[]).factory('RentCollection',['$resource',
    function($resource){
        return $resource('/api/rentcollection/:rentcollectionId',{
            rentcollectionId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
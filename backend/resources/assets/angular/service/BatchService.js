/**
 * Created by Shahid Neermunda on 8/5/15.
 */

angular.module('BatchService',[]).factory('Batch',['$resource',
    function($resource){
        return $resource('/api/batch/:batchId',{
            batchId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
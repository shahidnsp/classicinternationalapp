/**
 * Created by Shahid Neermunda on 8/5/15.
 */

angular.module('NurseService',[]).factory('Nurse',['$resource',
    function($resource){
        return $resource('/api/nurse/:nurseId',{
            nurseId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
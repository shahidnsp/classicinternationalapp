/**
 * Created by Shahid Neermunda on 5/7/15.
 */

angular.module('AttendanceService',[]).factory('Attendance',['$resource',
    function($resource){
        return $resource('/api/attendance/:attendanceId',{
            attendanceId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
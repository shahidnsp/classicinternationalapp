app.controller('BatchController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,Batch,UserInfo){
    $scope.batchedit = false;
    $scope.newbatch = new Batch();
    $scope.curBatch = {};

    $scope.batches=[];
    function loadBatch() {
        Batch.query(function (head) {
            $scope.batches = head;
        });
    }loadBatch();

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Batch(Nurse Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

       // console.log(data);
    });




    $scope.newBatch = function () {
        $scope.batchedit = true;
        $scope.newbatch = new Batch();
        $scope.curBatch = {};
    };
    $scope.editBatch = function (thisHead) {
        $scope.batchedit = true;
        $scope.curBatch =  thisHead;
        $scope.newbatch = new Batch();
        $scope.newbatch = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addBatch = function () {
        $scope.newbatch.default=0;
        if ($scope.curBatch.id) {
            $http.put('/api/batch/'+$scope.curBatch.id, $scope.newbatch).
                success(function(data, status)
                {
                    loadBatch();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Batch updated Successfully');
                });
        } else{
            $scope.newbatch.$save(function(account){
                loadBatch();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Batch saved Successfully');
            });
        }
        $scope.batchedit = false;
    };
    $scope.deleteBatch = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/batch/'+item.id).
                success(function(data, status)
                {
                    loadBatch();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Batch removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelBatch = function () {
        $scope.batchedit = false;
        $scope.newbatch =  new Batch();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Batch Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeBatchStatus/'+id).
                success(function(data, status)
                {
                    loadBatch();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Batch Status Changed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.open=function(data,ev){
        $http.get('/api/batch/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/batch',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {

        $scope.batch=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

});
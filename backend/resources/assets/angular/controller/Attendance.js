app.controller('AttendanceController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,Employee,UserInfo,Attendance){
    $scope.headedit = false;
    $scope.newattendance = new Attendance();
    $scope.curAttendance = {};

    $scope.attendances=[];

    function loadAttendance() {
        Attendance.query(function (attendance) {
            $scope.attendances = attendance;
        });
    }loadAttendance();

    $scope.employees=[];
    $http.get('/api/getEmployees').
        success(function(data, status)
        {
            $scope.employees = data;
        });

    $scope.types = [
        { value: 'income', name: 'Income' },
        { value: 'expense', name: 'Expense' }
    ];

    // $scope.numPerPage=5;

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Attendance(Main Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });



    $scope.newAttendance = function () {
        $scope.headedit = true;
        $scope.newattendance = new Attendance();
        $scope.newattendance.date = new Date();
        $scope.curAttendance = {};
    };
    $scope.editAttendance = function (thisHead) {
        $scope.headedit = true;
        $scope.curAttendance =  thisHead;
        $scope.newattendance = new Attendance();
        $scope.newattendance = angular.copy(thisHead);
        $scope.newattendance.date = new Date(thisHead.date);
        $anchorScroll();
    };
    $scope.addAttendance = function () {
        $scope.newattendance.group='Main';
        $scope.newattendance.default=0;
        if ($scope.curAttendance.id) {
            $http.put('/api/attendance/'+$scope.curAttendance.id, $scope.newattendance).
                success(function(data, status)
                {
                    //$window.location.reload();
                    loadAttendance();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Attendance Head updated Successfully');
                });
        } else{
            $scope.newattendance.$save(function(attendance){
                //$window.location.reload();
                // $window.location.href=$window.location.href;
                $scope.attendances.push(attendance);
                loadAttendance();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Attendance Head Saved Successfully');

            });
        }
        $scope.headedit = false;
    };
    $scope.deleteAttendance = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/attendance/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    if(data.default===true) {
                        ngNotify.set('Attendance Head Deleted Successfully');
                    }else{
                        ngNotify.set(' Attendance Head Set has DEFAULT...Not DELETABLE');
                    }
                    loadAttendance();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelAttendance = function () {
        $scope.headedit = false;
        $scope.newattendance =  new Attendance();
    };
    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);
    $scope.employee_id='All';
    $scope.search=function(fromDate,toDate,employee_id){
        Attendance.query({fromDate:fromDate,toDate:toDate,employee_id:employee_id},function (attendance) {
            $scope.attendances = attendance;
        });
    };
    

});
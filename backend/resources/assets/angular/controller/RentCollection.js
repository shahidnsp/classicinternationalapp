app.controller('RentCollectionController', function($scope,$http,$filter,$window,$location,$log,$timeout,$mdDialog,$anchorScroll,ngNotify,UserInfo,RentCollection){
    $scope.rentedit = false;
    $scope.newrent = new RentCollection();
    $scope.curRentCollection = {};
    $scope.rent_id = 'All';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.rentcollections =[];
    function loadRentCollection() {
        RentCollection.query(function (head) {
            $scope.rentcollections = head;
            calculateTotal(head);
            //console.log(head);
        });
    }loadRentCollection();

    $scope.search=function(fromDate,toDate,rent_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_rent_collection', {params:{fromDate:from,toDate:to,rent_id:rent_id}}).
            success(function(data, status)
            {
                $scope.rentcollections = data;
                calculateTotal(data);
            });
    };
    $scope.total=0;
    function calculateTotal(rents){
        var length=rents.length;
        var sum=0;
        for(var i=0;i<length;i++){
            sum+=parseFloat(rents[i].amount);
        }
        $scope.total=sum;
    }

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Rent Payment(Rent Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });



    $scope.newRentCollection = function () {
        $scope.rentedit = true;
        $scope.newrent = new RentCollection();
        $scope.newrent.date=new Date();
        $scope.curRentCollection = {};

    };
    $scope.editRentCollection = function (thisHead) {
        $scope.rentedit = true;
        $scope.curRentCollection =  thisHead;
        $scope.newrent = new RentCollection();
        $scope.newrent = angular.copy(thisHead);
        $scope.newrent.date=new Date(thisHead.date);
        $anchorScroll();
    };
    $scope.addRentCollection = function () {
        $scope.newrent.rent_id=$scope.selectedItem.id;
        $scope.newrent.date=new Date($scope.newrent.date);
        if ($scope.curRentCollection.id) {
            $http.put('/api/rentcollection/'+$scope.curRentCollection.id, $scope.newrent).
                success(function(data, status)
                {
                    loadRentCollection();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                });
        } else{
            $scope.newrent.$save(function(account){
                loadRentCollection();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Your Transaction Saved Successfully');
            });
        }
        $scope.rentedit = false;
    };
    $scope.deleteRentCollection = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/rentcollection/'+item.id).
                success(function(data, status)
                {
                    loadRentCollection();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelRentCollection = function () {
        $scope.rentedit = false;
        $scope.newrent =  new RentCollection();
    };




    $scope.simulateQuery = false;
    $scope.isDisabled    = false;

    // list of `state` value/display objects
    $scope.states        = [];loadAll();
    $scope.querySearch   = querySearch;
    $scope.selectedItemChange = selectedItemChange;
    $scope.searchTextChange   = searchTextChange;

    $scope.newState = newState;

    function newState(state) {
        alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
        var results = query ? $scope.states.filter( createFilterFor(query) ) : $scope.states,
            deferred;
        if ($scope.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    function searchTextChange(text) {
        //$log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
        //$log.info('Item changed to ' + JSON.stringify(item));

    }


    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
        $scope.data1=[];
        $http.get('/api/getRent/').
            success(function(data, status)
            {
                $scope.states=data;
            });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {

        var lowercaseQuery =query; //angular.lowercase(query);

        return function filterFn(state) {
            return (state.value.indexOf(lowercaseQuery) === 0);
        };

    }

    $scope.openRent=function(data,ev){
        $http.get('/api/rent/'+data.rent_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogRentController,
                    templateUrl: $window.location.origin+'/template/rent',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogRentController($scope, $mdDialog,data) {

        $scope.rent=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }


    $scope.export=function(fromDate,toDate,rent_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        var req = {
            method: 'POST',
            url: '/api/excelExportRentCollection',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,rent_id:rent_id}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='RentCollectionReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };
});
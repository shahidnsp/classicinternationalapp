app.controller('NurseController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,Nurse){
    $scope.nurseedit = false;
    $scope.newnurse = new Nurse();
    $scope.curNurse = {};

    $scope.nurses=[];
    function loadNurse() {
        Nurse.query(function (head) {
            $scope.nurses = head;
        });
    }loadNurse();

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Nurse Register(Nurse Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });

    $scope.batches=[];
    $scope.hospitals=[];

    function loadBatch(){
        $http.get('/api/getBatch').
            success(function(data, status)
            {
                $scope.batches=data;
            });
    }loadBatch();

    function loadHospital(){
        $http.get('/api/getHospital').
            success(function(data, status)
            {
                $scope.hospitals=data;
            });
    }loadHospital();



    $scope.newNurse = function () {
        $scope.nurseedit = true;
        $scope.newnurse = new Nurse();
        $scope.curNurse = {};
    };
    $scope.editNurse = function (thisHead) {
        $scope.nurseedit = true;
        $scope.curNurse =  thisHead;
        $scope.newnurse = new Nurse();
        $scope.newnurse = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addNurse = function () {
        $scope.newnurse.default=0;
        if ($scope.curNurse.id) {
            $http.put('/api/nurse/'+$scope.curNurse.id, $scope.newnurse).
                success(function(data, status)
                {
                    loadNurse();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Nurse updated Successfully');
                });
        } else{
            $scope.newnurse.$save(function(account){
                loadNurse();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Nurse saved Successfully');
            });
        }
        $scope.nurseedit = false;
    };
    $scope.deleteNurse = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/nurse/'+item.id).
                success(function(data, status)
                {
                    loadNurse();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Nurse removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelNurse = function () {
        $scope.nurseedit = false;
        $scope.newnurse =  new Nurse();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Nurse Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeNurseStatus/'+id).
                success(function(data, status)
                {
                    loadNurse();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Nurse Status Changed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };


    $scope.openHospital=function(data,ev){
        $http.get('/api/hospital/'+data.hospital_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogHospitalController,
                    templateUrl: $window.location.origin+'/template/hospital',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogHospitalController($scope, $mdDialog,data) {
        $scope.hospital=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openBatch=function(data,ev){
        $http.get('/api/batch/'+data.nursebatch_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/batch',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {

        $scope.batch=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openNurse=function(data,ev){
        $http.get('/api/nurse/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogNurseController,
                    templateUrl: $window.location.origin+'/template/nurse',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogNurseController($scope, $mdDialog,$window,data) {

        $scope.nurse=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

});
app.controller('ResidenceController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,Residence){
    $scope.residenceedit = false;
    $scope.newresidence = new Residence();
    $scope.curResidence = {};

    $scope.residences=[];
    function loadResidence() {
        Residence.query(function (head) {
            $scope.residences = head;
        });
    }loadResidence();

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Residence Area(Transportation)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });




    $scope.newResidence = function () {
        $scope.residenceedit = true;
        $scope.newresidence = new Residence();
        $scope.curResidence = {};
    };
    $scope.editResidence = function (thisHead) {
        $scope.residenceedit = true;
        $scope.curResidence =  thisHead;
        $scope.newresidence = new Residence();
        $scope.newresidence = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addResidence = function () {
        $scope.newresidence.default=0;
        if ($scope.curResidence.id) {
            $http.put('/api/residence/'+$scope.curResidence.id, $scope.newresidence).
                success(function(data, status)
                {
                    loadResidence();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Residence updated Successfully');
                });
        } else{
            $scope.newresidence.$save(function(account){
                loadResidence();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Residence saved Successfully');
            });
        }
        $scope.residenceedit = false;
    };
    $scope.deleteResidence = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/residence/'+item.id).
                success(function(data, status)
                {
                    loadResidence();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Residence removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelResidence = function () {
        $scope.residenceedit = false;
        $scope.newresidence =  new Residence();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Residence Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeResidenceStatus/'+id).
                success(function(data, status)
                {
                    loadResidence();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Residence Status Changed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.openResidence=function(data,ev){
        $http.get('/api/residence/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogResidenceController,
                    templateUrl: $window.location.origin+'/template/residence',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogResidenceController($scope, $mdDialog,data) {
        $scope.residence=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

});
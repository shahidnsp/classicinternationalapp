app.controller('RentAccountAccountHeadController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,AccountHead){
    $scope.headedit = false;
    $scope.newaccount = new AccountHead();
    $scope.curAccount = {};

    //$scope.numPerPage=5;
    $scope.accountheads=[];
    function loadAccountHead() {
        AccountHead.query({group: 'Rent'}, function (head) {
            $scope.accountheads = head;
        });
    }loadAccountHead();

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Account Head(Rent Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });



    $scope.newAccountHead = function () {
        $scope.headedit = true;
        $scope.newaccount = new AccountHead();
        $scope.curAccount = {};
    };
    $scope.editAccountHeadt = function (thisHead) {
        $scope.headedit = true;
        $scope.curAccount =  thisHead;
        $scope.newaccount = new AccountHead();
        $scope.newaccount = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addAccountHead = function () {
        $scope.newaccount.group='Rent';
        $scope.newaccount.default=0;
        if ($scope.curAccount.id) {
            $http.put('/api/accounthead/'+$scope.curAccount.id, $scope.newaccount).
                success(function(data, status)
                {
                    loadAccountHead();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Account Head updated Successfully');
                });
        } else{
            $scope.newaccount.$save(function(account){
                loadAccountHead();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Account Head Saved Successfully');
            });
        }
        $scope.headedit = false;
    };
    $scope.deleteAccountHead = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/accounthead/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    if(data.default===true) {
                        ngNotify.set('Account Head Deleted Successfully');
                    }else{
                        ngNotify.set(' Account Head Set has DEFAULT...Not DELETABLE');
                    }
                    loadAccountHead();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelAccountHead = function () {
        $scope.headedit = false;
        $scope.newaccount =  new AccountHead();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Account Head Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeAccountHeadStatus/'+id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Account Head Status Changed Successfully');

                    loadAccountHead();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

});
/**
 * Created by psybo on 18/5/17.
 */
app.controller('SalarychartController', function($scope,$http,$window,$mdDialog,UserInfo){

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Salary Chart(Salary)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });


    $scope.openSalaryChart=function(data,ev){
        $mdDialog.show({
            controller: DialogDutyController,
            templateUrl: $window.location.origin+'/template/salarychart',
            parent: angular.element(document.querySelector('#popup')),
            targetEvent: ev,
            clickOutsideToClose:true,
            locals: {
                data: data
            }
        })
            .then(function(answer) {
                //$scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                //$scope.status = 'You cancelled the dialog.';
            });

    };

    function DialogDutyController($scope, $mdDialog,$window,data) {

        $scope.basepath=$window.location.origin;
        $scope.salarychart=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }



});
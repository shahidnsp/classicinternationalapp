/**
 * Created by psybo on 18/5/17.
 */
app.controller('FairRegisterReport1Controller', function($scope,$http,$filter,$window,$mdDialog,$location,ngNotify,UserInfo){

    $scope.driver_id='All';
    var makeDate = new Date();
    makeDate = new Date(makeDate.setMonth(makeDate.getMonth() - 1));
    $scope.month=makeDate;
    $scope.showProgress=false;

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Fair Register(Transportation)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });

    $scope.account_id = 'All';

    $scope.duties=[];
    $scope.getDutyChart=function(){
        $scope.showProgress=true;
        $scope.reports=[];

        $http.post('/api/fairregisterreportjson1',{month:$scope.month,driver_id:$scope.driver_id}).
            success(function(data, status)
            {
                $scope.reports=data;
                $scope.showProgress=false;
            });
    };



    $scope.showMonth=function(date){
        var d = new Date(date);
        var month = new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";
        var curmonth=d.getMonth();
        var premonth=0;
        var nextmonth=0;
        $scope.curMonth=month[curmonth];
        if(curmonth==0)
            premonth=11;
        else
            premonth=curmonth-1;

        if(curmonth==11)
            nextmonth=0;
        else
            nextmonth=curmonth+1;

        $scope.preMonth=month[premonth];
        $scope.nextMonth=month[nextmonth];
    };

    $scope.showMonth($scope.month);



    $scope.saveFair=function(fairs,month,ev){

        var date = $filter("date")(Date.parse(month), 'yyyy-MM-dd');
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to Save These Fair ?')
            .textContent('Are you sure to Save These Fair.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $scope.showProgress=true;
            $http.post('/api/savefairregister',{fairs:fairs,date:date}).
                success(function(data, status)
                {
                    console.log(data);
                    $scope.showProgress=false;
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Fairs Saved Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.openResidence=function(data,ev){
        $http.get('/api/residence/'+data.residence_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogResidenceController,
                    templateUrl: $window.location.origin+'/template/residence',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogResidenceController($scope, $mdDialog,data) {
        $scope.residence=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openHospital=function(data,ev){
        $http.get('/api/hospital/'+data.hospital_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogHospitalController,
                    templateUrl: $window.location.origin+'/template/hospital',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogHospitalController($scope, $mdDialog,data) {
        $scope.hospital=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openEmployee=function(data,ev){
        $http.get('/api/employee/'+data.employee_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/employee',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {

        $scope.basepath=$window.location.origin;
        $scope.employee=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openDutyChart=function(data,residence,hospital,driver,ev){
        $mdDialog.show({
            controller: DialogDutyController,
            templateUrl: $window.location.origin+'/template/dutychart',
            parent: angular.element(document.querySelector('#popup')),
            targetEvent: ev,
            clickOutsideToClose:true,
            locals: {
                data: data,
                residence:residence,
                hospital:hospital,
                driver:driver
            }
        })
            .then(function(answer) {
                //$scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                //$scope.status = 'You cancelled the dialog.';
            });

    };

    function DialogDutyController($scope, $mdDialog,$window,data,residence,hospital,driver) {

        $scope.dutychart=data;
        $scope.residence=residence;
        $scope.hospital=hospital;
        $scope.driver=driver;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }


});
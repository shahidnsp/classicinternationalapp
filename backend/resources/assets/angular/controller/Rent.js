app.controller('RentController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,Rent){
    $scope.rentedit = false;
    $scope.newrent = new Rent();
    $scope.curRent = {};

    $scope.rents=[];
    function loadRent() {
        Rent.query(function (head) {
            $scope.rents = head;
        });
    }loadRent();

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Rent Register(Rent Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });



    $scope.newRent = function () {
        $scope.rentedit = true;
        $scope.newrent = new Rent();
        $scope.curRent = {};
    };
    $scope.editRent = function (thisHead) {
        $scope.rentedit = true;
        $scope.curRent =  thisHead;
        $scope.newrent = new Rent();
        $scope.newrent = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addRent = function () {
        $scope.newrent.default=0;
        if ($scope.curRent.id) {
            $http.put('/api/rent/'+$scope.curRent.id, $scope.newrent).
                success(function(data, status)
                {
                    loadRent();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Rent updated Successfully');
                });
        } else{
            $scope.newrent.$save(function(account){
                loadRent();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Rent saved Successfully');
            });
        }
        $scope.rentedit = false;
    };
    $scope.deleteRent = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/rent/'+item.id).
                success(function(data, status)
                {
                    loadRent();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Rent removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelRent = function () {
        $scope.rentedit = false;
        $scope.newrent =  new Rent();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Rent Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeRentStatus/'+id).
                success(function(data, status)
                {
                    loadRent();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Rent Status Changed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.openRent=function(data,ev){
        $http.get('/api/rent/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogRentController,
                    templateUrl: $window.location.origin+'/template/rent',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogRentController($scope, $mdDialog,data) {

        $scope.rent=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

});
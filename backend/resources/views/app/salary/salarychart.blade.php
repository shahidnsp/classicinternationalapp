@extends('layout.main')
@include('app.partial.message')
@section('content')
  <!--
        ...................SALARY......................................
  -->

  <div class="row bg-title">
          <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h4 class="page-title">Salary-Salary Chart</h4> </div>
          <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
              <ol class="breadcrumb">
                  <li><a href="/">Accounts</a></li>
                  <li class="active">Salary Chart</li>
              </ol>
          </div>
          <!-- /.col-lg-12 -->
      </div>

  <md-content  id="popupContainer" ng-controller="SalarychartController" ng-cloak>
       <div ng-show="permission">
        <div class="row">
             <div class="col-md-12">
                    <div class="box">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="white-box">
                                    <div class="col-sm-12">
                                        <h3 class="box-title m-b-0">Salary Chart</h3>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <form class="form-horizontal" method="get" action="{{url('api/salarychart')}}">
                                                    <div class="col-md-3">
                                                        <select name="year" class="form-control pagiantion">
                                                            @foreach($years as $year)
                                                                <option value="{{$year->year}}" @if($year->year == $selected) selected @endif>{{$year->year}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="form-inline form-group ">
                                                        <label class="control-label" for="driver">Employee &nbsp;&nbsp; </label>
                                                        <select id="employee" name="employee_id" class="form-control col-sm-8">
                                                            <option value="">All</option>
                                                            @foreach($emps as $emp)
                                                                <option value="{{$emp->id}}" @if($emp->id == $employee_id) selected @endif>{{$emp->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <md-button type="submit" class="md-raised md-primary"> <md-icon>note_add</md-icon>Generate</md-button>
                                                    </div>
                                                </form>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-inline form-group " style="float: right;">
                                                    <label for="filter-list">Search </label>
                                                    <input type="text" class="form-control" ng-model="filterlist" placeholder="Search">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                             <h2 class="text-primary" style="text-decoration: underline;">Salary Chart of the Year {{$selected}}</h2>
                                        </div>
                                    </div>

                                    <table id="myTable" class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                        <thead>
                                            <tr>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">SlNo</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Employee Name</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Basic Salary</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="1">Jan</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Feb</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Mar</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Apr</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">May</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">Jun</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="7">Jul</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="8">Aug</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="9">Sep</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="10">Oct</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="11">Nov</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="12">Dec</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($employees as $key => $employee)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td class="title"><a ng-click="openSalaryChart({{$employee}},$event);" href="javascript:void(0)">{{$employee->name}}</a></td>
                                                <td>{{$employee->basicpay}}</td>
                                                @if($employee->jan=='NPD')
                                                    <td class="text-danger">{{$employee->jan}}</td>
                                                @else
                                                    <td class="text-success">{{$employee->jan}}</td>
                                                @endif
                                                @if($employee->feb=='NPD')
                                                    <td class="text-danger">{{$employee->feb}}</td>
                                                @else
                                                    <td class="text-success">{{$employee->feb}}</td>
                                                @endif
                                                @if($employee->mar=='NPD')
                                                    <td class="text-danger">{{$employee->mar}}</td>
                                                @else
                                                    <td class="text-success">{{$employee->mar}}</td>
                                                @endif
                                                @if($employee->apr=='NPD')
                                                    <td class="text-danger">{{$employee->apr}}</td>
                                                @else
                                                    <td class="text-success">{{$employee->apr}}</td>
                                                @endif
                                                @if($employee->may=='NPD')
                                                    <td class="text-danger">{{$employee->may}}</td>
                                                @else
                                                    <td class="text-success">{{$employee->may}}</td>
                                                @endif
                                                @if($employee->jun=='NPD')
                                                    <td class="text-danger">{{$employee->jun}}</td>
                                                @else
                                                    <td class="text-success">{{$employee->jun}}</td>
                                                @endif
                                                @if($employee->jul=='NPD')
                                                    <td class="text-danger">{{$employee->jul}}</td>
                                                @else
                                                    <td class="text-success">{{$employee->jul}}</td>
                                                @endif
                                                @if($employee->aug=='NPD')
                                                    <td class="text-danger">{{$employee->aug}}</td>
                                                @else
                                                    <td class="text-success">{{$employee->aug}}</td>
                                                @endif
                                                @if($employee->sep=='NPD')
                                                    <td class="text-danger">{{$employee->sep}}</td>
                                                @else
                                                    <td class="text-success">{{$employee->sep}}</td>
                                                @endif
                                                @if($employee->oct=='NPD')
                                                    <td class="text-danger">{{$employee->oct}}</td>
                                                @else
                                                    <td class="text-success">{{$employee->oct}}</td>
                                                @endif
                                                @if($employee->nov=='NPD')
                                                    <td class="text-danger">{{$employee->nov}}</td>
                                                @else
                                                    <td class="text-success">{{$employee->nov}}</td>
                                                @endif
                                                @if($employee->dec=='NPD')
                                                    <td class="text-danger">{{$employee->dec}}</td>
                                                @else
                                                    <td class="text-success">{{$employee->dec}}</td>
                                                @endif

                                            </tr>
                                            @endforeach
                                            <tr style="color: #000099;font-size: large;">
                                                <td>Total</td><td></td><td>{{$employees->sum('basicpay')}}</td>
                                                <td>{{$employees->sum('jan')}}</td>
                                                <td>{{$employees->sum('feb')}}</td>
                                                <td>{{$employees->sum('mar')}}</td>
                                                <td>{{$employees->sum('apr')}}</td>
                                                <td>{{$employees->sum('may')}}</td>
                                                <td>{{$employees->sum('jun')}}</td>
                                                <td>{{$employees->sum('jul')}}</td>
                                                <td>{{$employees->sum('aug')}}</td>
                                                <td>{{$employees->sum('sep')}}</td>
                                                <td>{{$employees->sum('oct')}}</td>
                                                <td>{{$employees->sum('nov')}}</td>
                                                <td>{{$employees->sum('dec')}}</td>
                                            </tr>

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>

                    <br/><br/>
             </div>
        </div>
        </div>
        <div ng-hide="permission">
            <div  layout="row" layout-padding layout-wrap layout-fill>
                <div md-whiteframe="3" class="padded alert">
                    <strong>So...Sorry!</strong> You have no Permission to View This Page.
                </div>
            </div>
        </div>
  </md-content>


@section('scripts')

@stop

@stop
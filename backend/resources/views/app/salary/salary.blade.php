@extends('layout.main')
@include('app.partial.message')
@section('content')
  <!--
        ...................SALARY ACCOUNT......................................
  -->

   <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Salary-Salary Register</h4> </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
            <ol class="breadcrumb">
                <li><a href="/">Accounts</a></li>
                <li class="active">Salary Register</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
   <section id="content" role="content"  layout="column" layout-padding md-scroll-y style="overflow: auto;">
      <md-content id="popupContainer"  ng-controller="SalaryController" class="md-whiteframe-z2" ng-cloak>
        <div ng-show="permission">
        <div class="row">

             <div class="col-md-12">
                    <div class="box">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="row pull-right">
                                <md-button ng-if="user.permissions.salary.write =='true'" ng-hide="salaryedit" class="md-raised md-small md-primary" ng-click="newSalary();"> <md-icon>note_add</md-icon> Add Salary</md-button>
                            </div>
                            <br/>
                            <form ng-show="salaryedit" class="form-horizontal"  ng-submit="addSalary();">
                                <h3>New Salary</h3><br>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                       <label class="control-label1">Employee Name</label>
                                       <md-select ng-model="newsalary.employee_id" ng-change="getBasicPayAndAdvance(newsalary.employee_id);" name="employee_id" required="">
                                         @foreach($employees as $employee)
                                         <md-option value="{{$employee->id}}">{{$employee->name}}</md-option>
                                         @endforeach
                                       </md-select>
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                      <mdp-date-picker name="fromDate" mdp-placeholder="Date" ng-change="getBasicPayAndAdvance(newsalary.employee_id);"  mdp-format="DD/MM/YYYY"  ng-model="newsalary.date"></mdp-date-picker>
                                    </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                       <label class="control-label1">Basic Pay</label>
                                       <input type="number" ng-change="changeSalary();" ng-model="newsalary.basic" required="">
                                    </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                       <label class="control-label1">Advance</label>
                                       <input type="number" ng-change="changeSalary();" ng-model="newsalary.advance" required="">
                                    </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                       <label class="control-label1">Allowance</label>
                                       <input type="number" ng-change="changeSalary();" ng-model="newsalary.allowance" required="">
                                    </md-input-container>
                                </div>
                                <div  layout-gt-xs="row">
                                   <h5 style="color: #ff562d;">Total [[leave]] Leave and [[half]] Half Leave in Last Month</h5>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                       <label class="control-label1">Leave Salary</label>
                                       <input type="number" ng-change="changeSalary();" ng-model="newsalary.leavesalary" required="">
                                    </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                       <label class="control-label1">Total Salary</label>
                                       <input type="number" ng-model="newsalary.total" required="">
                                    </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label>Remark</label>
                                     <textarea ng-model="newsalary.remark" md-maxlength="250" rows="5" md-select-on-focus></textarea>
                                   </md-input-container>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-12 text-right">
                                    <md-button class="md-raised btn-default pull-right" ng-click="cancelSalary();"> <md-icon>cancel</md-icon> Cancel</md-button>
                                    <md-button type="submit" class="md-raised  md-primary  pull-right" > <md-icon>note_add</md-icon>Save</md-button>
                                  </div>
                                </div>
                                <hr>
                            </form>
                            </div>
                        </div>
                    </div>

                    <div  class="box">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="white-box" style="background: transparent;">
                                    <div class="col-sm-12">
                                        <h3 class="box-title m-b-0">Salary List</h3>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Show
                                                    <select ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);" class="form-control pagiantion">
                                                    </select>
                                                    entries
                                                </label>
                                            </div>

                                            <div class="col-md-8 ">
                                                <div class="form-inline form-group " style="float: right;">
                                                    <label for="filter-list">Search </label>
                                                    <input type="text" class="form-control" ng-model="filterlist" placeholder="Search">
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row text-center">
                                        <section  class="form-horizontal col-sm-12">
                                            <div class="row">
                                               <label class="col-sm-1 control-label">From</label>
                                              <div class="col-md-2">
                                                  <div class="input-group">
                                                      <input type="text" class="form-control" datepicker-popup="dd-MM-yyyy" ng-model="fromDate" is-open="expensepicker" show-button-bar="false" show-weeks="false" readonly>
                                                      <span class="input-group-btn">
                                                        <button type="button" class="btn btn-default" ng-click="expensepicker=true"><i class="fa fa-calendar"></i></button>
                                                      </span>
                                                  </div>
                                              </div>
                                              <label class="col-sm-1 control-label">To</label>
                                              <div class="col-md-2">
                                                  <div class="input-group">
                                                      <input type="text" class="form-control" datepicker-popup="dd-MM-yyyy" ng-model="toDate" is-open="expensepickerto" show-button-bar="false" show-weeks="false" readonly>
                                                      <span class="input-group-btn">
                                                        <button type="button" class="btn btn-default" ng-click="expensepickerto=true"><i class="fa fa-calendar"></i></button>
                                                      </span>
                                                  </div>
                                              </div>
                                              <label class="col-sm-1 control-label">Employee</label>
                                                <div class="col-md-2">
                                                    <select ng-model="employee_id" class="form-control" required="">
                                                       <option value="All">All</option>
                                                       @foreach($employees as $employee)
                                                       <option value="{{$employee->id}}">{{$employee->name}}</option>
                                                        @endforeach
                                                   </select>
                                                </div>
                                                <div class="col-md-3">
                                                      <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                           <md-button ng-click="search(fromDate,toDate,employee_id);" class="md-primary md-raised pull-left"> <md-icon>search</md-icon> Search</md-button>
                                                           <md-button ng-click="export(fromDate,toDate,employee_id);"  class="md-accent md-raised"> <md-icon>speaker_notes</md-icon> Export</md-button>
                                                      </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>

                                    <table id="myTable" class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                        <thead>
                                            <tr>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Employee</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="2">Date</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Total</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Basic Pay(+)</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Advance(-)</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Allowance(+)</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Leave Salary(-)</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">Remark</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="7">Created_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="8">Updated_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="9">User</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="salary in listCount  = (salaries | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                                                <td class="title"><a ng-click="open(salary,$event);" href="javascript:void(0)">[[salary.employee.name]]</a></td>
                                                <td>[[salary.date]]</td>
                                                <td class="text-danger">[[salary.total]] KD</td>
                                                <td>[[salary.basic]] KD</td>
                                                <td>[[salary.advance]] KD</td>
                                                <td>[[salary.allowance]] KD</td>
                                                <td>[[salary.leavesalary]] KD</td>
                                                <td>[[salary.remark]]</td>
                                                <td>[[salary.created_at]]</td>
                                                <td>[[salary.updated_at]]</td>
                                                <td>[[salary.user.name]]</td>
                                                <td>
                                                    <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                         <md-button ng-if="user.permissions.salary.edit =='true'" class="md-primary md-small" ng-click="editSalary(salary);"> <md-icon>mode_edit</md-icon></md-button>
                                                         <md-button ng-if="user.permissions.salary.delete =='true'" class="md-warn md-small" ng-click="deleteSalary(salary,$event);"><md-icon>delete</md-icon></md-button>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="text-danger">
                                                <td colspan="2">Total</td><td>[[total]] KD</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                     <div class="clearfix pull-left" ng-show="fairs.length > numPerPage">
                                   <pagination
                                       ng-model="currentPage"
                                       total-items="listCount.length"
                                       max-size="maxSize"
                                       items-per-page="numPerPage"
                                       boundary-links="true"
                                       class="pagination-sm pull-right"
                                       previous-text="&lsaquo;"
                                       next-text="&rsaquo;"
                                       first-text="&laquo;"
                                       last-text="&raquo;"
                                       ></pagination>
                               </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br/><br/>
             </div>
        </div>
        </div>
        <div ng-hide="permission">
            <div  layout="row" layout-padding layout-wrap layout-fill>
                <div md-whiteframe="3" class="padded alert">
                    <strong>So...Sorry!</strong> You have no Permission to View This Page.
                </div>
            </div>
        </div>
  </md-content>
   </section>

@section('scripts')

@stop

@stop
@extends('layout.main')
@include('app.partial.message')
@section('content')
  <!--
        ...................USER MANAGEMENT......................................
  -->
   <div class="row bg-title">
          <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h4 class="page-title">User-Profile</h4> </div>
          <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
              <ol class="breadcrumb">
                  <li><a href="/">Accounts</a></li>
                  <li class="active">Profile</li>
              </ol>
          </div>
          <!-- /.col-lg-12 -->
      </div>

  <md-content id="popupContainer" ng-controller="UserController" ng-cloak>
        <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="white-box">
                <div class="user-bg" >
                    <div class="overlay-box" >
                        <div class="user-content" >
                            <a href="javascript:void(0)"><img src="{{url('images/'.\Illuminate\Support\Facades\Auth::user()->photo)}}" class="thumb-lg img-circle" alt="img"></a>
                            <h4 class="text-white">{{$profile->name}}</h4>
                            <h5 class="text-white">{{$profile->email}}</h5>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-8 col-xs-12">
            <md-content>
                  <md-tabs md-dynamic-height md-border-bottom>
                    {{-- user Profile --}}
                    <md-tab label="Profile">
                      <md-content class="md-padding">
                          <md-card style="width: 100%;" md-theme="dark-purple" md-theme-watch>
                              <md-card-title>
                                  <md-card-title-text>
                                      <span class="md-headline">{{$profile->name}}</span>
                                      <span class="md-subhead" style="color:#00796B;">---{{$profile->username}}---</span>
                                      <span class="md-subhead">Eamil: {{$profile->email}}</span>
                                      @if($profile->type=='a')
                                        <span class="md-subhead">Account Type: Administrator</span>
                                      @elseif($profile->type=='u')
                                        <span class="md-subhead">Account Type: User</span>
                                      @endif
                                  </md-card-title-text>
                                  <md-card-title-media>
                                      <div class="md-media-sm card-media"></div>
                                  </md-card-title-media>
                                   <md-card-actions layout="row" layout-align="center">
                                       <md-button md-no-ink class="md-primary">Last Login: {{$profile->lastLogin}}</md-button>
                                       <md-button md-no-ink class="md-warn">Logout</md-button>
                                   </md-card-actions>
                              </md-card-title>

                          </md-card>
                      </md-content>
                    </md-tab>
                    <md-tab label="Access Privilege">
                      <md-content class="md-padding">
                             <?php
                                $pages=\App\Http\Controllers\Auth\UserInfoController::getPermission();
                            ?>
                            <div class="col-sm-12">
                                <div class="row">
                                <div class="col-sm-6">
                                    <md-card>
                                          <md-card-title>
                                              <md-card-title-text>
                                                    <div class="r-icon-stats"> <i class="ti-key bg-megna"></i>
                                                       <div class="bodystate">
                                                           <h4>Main Account</h4> <span class="text-muted">Permission</span> </div>
                                                   </div>
                                              </md-card-title-text>
                                          </md-card-title>
                                          <md-card-content>
                                            <ol>
                                                @foreach($pages['menu'] as $page)
                                                    @if($page['group']=='Main Account')
                                                       <li>{{$page['title']}}</li>
                                                    @endif
                                                @endforeach
                                            </ol>
                                          </md-card-content>

                                    </md-card>
                                </div>
                                <div class="col-sm-6">
                                    <md-card>
                                          <md-card-title>
                                              <md-card-title-text>
                                                    <div class="r-icon-stats"> <i class="ti-key bg-megna"></i>
                                                       <div class="bodystate">
                                                           <h4>Courier Account</h4> <span class="text-muted">Permission</span> </div>
                                                   </div>
                                              </md-card-title-text>
                                          </md-card-title>
                                          <md-card-content>
                                              <ol>
                                                  @foreach($pages['menu'] as $page)
                                                      @if($page['group']=='Courier Account')
                                                         <li>{{$page['title']}}</li>
                                                      @endif
                                                  @endforeach
                                              </ol>
                                          </md-card-content>
                                    </md-card>
                                </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="row">
                                <div class="col-sm-6">
                                    <md-card>
                                          <md-card-title>
                                              <md-card-title-text>
                                                    <div class="r-icon-stats"> <i class="ti-key bg-megna"></i>
                                                       <div class="bodystate">
                                                           <h4>Nurse Account</h4> <span class="text-muted">Permission</span> </div>
                                                   </div>
                                              </md-card-title-text>
                                          </md-card-title>
                                          <md-card-content>
                                            <ol>
                                                @foreach($pages['menu'] as $page)
                                                    @if($page['group']=='Nurse Account')
                                                       <li>{{$page['title']}}</li>
                                                    @endif
                                                @endforeach
                                            </ol>
                                          </md-card-content>

                                    </md-card>
                                </div>
                                <div class="col-sm-6">
                                    <md-card>
                                          <md-card-title>
                                              <md-card-title-text>
                                                    <div class="r-icon-stats"> <i class="ti-key bg-megna"></i>
                                                       <div class="bodystate">
                                                           <h4>Rent Account</h4> <span class="text-muted">Permission</span> </div>
                                                   </div>
                                              </md-card-title-text>
                                          </md-card-title>
                                          <md-card-content>
                                              <ol>
                                                  @foreach($pages['menu'] as $page)
                                                      @if($page['group']=='Rent Account')
                                                         <li>{{$page['title']}}</li>
                                                      @endif
                                                  @endforeach
                                              </ol>
                                          </md-card-content>
                                    </md-card>
                                </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="row">
                                <div class="col-sm-6">
                                    <md-card>
                                          <md-card-title>
                                              <md-card-title-text>
                                                    <div class="r-icon-stats"> <i class="ti-key bg-megna"></i>
                                                       <div class="bodystate">
                                                           <h4>Transportation</h4> <span class="text-muted">Permission</span> </div>
                                                   </div>
                                              </md-card-title-text>
                                          </md-card-title>
                                          <md-card-content>
                                            <ol>
                                                @foreach($pages['menu'] as $page)
                                                    @if($page['group']=='Transportation')
                                                       <li>{{$page['title']}}</li>
                                                    @endif
                                                @endforeach
                                            </ol>
                                          </md-card-content>

                                    </md-card>
                                </div>
                                <div class="col-sm-6">
                                    <md-card>
                                          <md-card-title>
                                              <md-card-title-text>
                                                    <div class="r-icon-stats"> <i class="ti-key bg-megna"></i>
                                                       <div class="bodystate">
                                                           <h4>Salary</h4> <span class="text-muted">Permission</span> </div>
                                                   </div>
                                              </md-card-title-text>
                                          </md-card-title>
                                          <md-card-content>
                                              <ol>
                                                  @foreach($pages['menu'] as $page)
                                                      @if($page['group']=='Salary')
                                                         <li>{{$page['title']}}</li>
                                                      @endif
                                                  @endforeach
                                              </ol>
                                          </md-card-content>
                                    </md-card>
                                </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="row">
                                <div class="col-sm-6">
                                    <md-card>
                                          <md-card-title>
                                              <md-card-title-text>
                                                    <div class="r-icon-stats"> <i class="ti-key bg-megna"></i>
                                                       <div class="bodystate">
                                                           <h4>Day Book</h4> <span class="text-muted">Permission</span> </div>
                                                   </div>
                                              </md-card-title-text>
                                          </md-card-title>
                                          <md-card-content>
                                            <ol>
                                                @foreach($pages['menu'] as $page)
                                                    @if($page['group']=='Daybook')
                                                       <li>{{$page['title']}}</li>
                                                    @endif
                                                @endforeach
                                            </ol>
                                          </md-card-content>

                                    </md-card>
                                </div>
                                <div class="col-sm-6">

                                </div>
                                </div>
                            </div>
                      </md-content>
                    </md-tab>
                    <md-tab label="Account Settings">
                      <md-content class="md-padding">
                            <div class="row">
                                <div class="col-md-12">
                                    <form class="form-horizontal" ng-submit="updateProfile();">
                                        <div layout-gt-xs="row">
                                          <md-input-container class="md-block" flex-gt-xs>
                                            <label class="control-label1">Profile Photo</label>
                                            <input type="file" ng-model="photo" accept="image/*" image-Reader multiple>
                                          </md-input-container>
                                        </div>
                                        <div layout-gt-xs="row">
                                          <md-input-container class="md-block" flex-gt-xs>
                                            <label class="control-label1">Name</label>
                                            <input type="text" ng-model="name" required="">
                                          </md-input-container>
                                        </div>
                                        <div layout-gt-xs="row">
                                          <md-input-container class="md-block" flex-gt-xs>
                                            <label class="control-label1">Email</label>
                                            <input type="email" ng-model="email">
                                          </md-input-container>
                                        </div>
                                        <div class="form-group">
                                          <div class="col-sm-12 text-right">
                                            <md-button type="submit" class="md-raised  md-primary  pull-right" > <md-icon>note_add</md-icon>Save</md-button>
                                          </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                      </md-content>
                    </md-tab>
                  </md-tabs>
            </md-content>
        </div>
    </div>
  </md-content>

@stop
@extends('layout.main')
@include('app.partial.message')
@section('content')
  <!--
        ...................USER MANAGEMENT......................................
  -->
    <div class="row bg-title">
          <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h4 class="page-title">User-User Management</h4> </div>
          <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
              <ol class="breadcrumb">
                  <li><a href="/">Accounts</a></li>
                  <li class="active">User Management</li>
              </ol>
          </div>
          <!-- /.col-lg-12 -->
      </div>
@if(\Illuminate\Support\Facades\Auth::user()->type=='a')
  <md-content id="popupContainer" ng-controller="UserInfoController" >
    <div class="row">
            <div ng-hide="resetPermission">
               <div class="col-md-12">
                      <div class="box">
                          <div class="row">
                              <div class="col-md-12">
                              <div class="row pull-right">
                                  <md-button ng-hide="headedit" class="md-raised md-small md-primary" ng-click="newUser();"> <md-icon>note_add</md-icon> Add User</md-button>
                              </div>
                              <br/>
                              <form ng-show="headedit" class="form-horizontal"  ng-submit="addUser();">
                                  <h3>New Account Head</h3><br>
                                  <div layout-gt-xs="row">
                                    <md-input-container class="md-block" flex-gt-xs>
                                      <label class="control-label1">Name</label>
                                      <input ng-model="newuser.name" required="">
                                    </md-input-container>
                                  </div>
                                  <div layout-gt-xs="row">
                                     <md-input-container class="md-block" flex-gt-xs>
                                       <label class="control-label1">Type</label>
                                       <md-select ng-model="newuser.type" required="">
                                         <md-option value="a">Admin</md-option>
                                         <md-option value="u">User</md-option>
                                       </md-select>
                                     </md-input-container>
                                  </div>
                                  <div layout-gt-xs="row">
                                      <md-input-container class="md-block" flex-gt-xs>
                                       <label class="control-label1">Email Address</label>
                                       <input type="email" ng-model="newuser.email">
                                     </md-input-container>
                                  </div>
                                  <div layout-gt-xs="row">
                                      <md-input-container class="md-block" flex-gt-xs>
                                        <label class="control-label1">Username</label>
                                        <input ng-model="newuser.username" ng-change="checkUser(newuser.username);" required="">
                                      </md-input-container><br/>
                                      <p style="color: red;" ng-show="available==='1'">User not available</p>
                                  </div>
                                  <div ng-show="showpassword" layout-gt-xs="row">
                                        <md-input-container class="md-block" flex-gt-xs>
                                          <label class="control-label1">Password</label>
                                          <input type="password" ng-model="newuser.password" minlength="5">
                                        </md-input-container>
                                   </div>
                                  <div class="form-group">
                                    <div class="col-sm-12 text-right">
                                      <md-button class="md-raised btn-default pull-right" ng-click="cancelUser();"> <md-icon>cancel</md-icon> Cancel</md-button>
                                      <md-button type="submit" class="md-raised  md-primary  pull-right" > <md-icon>note_add</md-icon>Save</md-button>
                                    </div>
                                  </div>
                                  <hr>
                              </form>
                              </div>
                          </div>
                      </div>


                      <div class="box">
                          <div class="row">
                              <div class="col-lg-12">
                                  <div class="white-box">
                                      <div class="col-sm-12">
                                          <h3 class="box-title m-b-0">Account Head List</h3>

                                          <div class="row">
                                              <div class="col-md-4">
                                                  <label for="">Show
                                                      <select ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);" class="form-control pagiantion">
                                                      </select>
                                                      entries
                                                  </label>
                                              </div>

                                              <div class="col-md-8 ">
                                                  <div class="form-inline form-group " style="float: right;">
                                                      <label for="filter-list">Search </label>
                                                      <input type="text" class="form-control" ng-model="filterlist" placeholder="Search">
                                                  </div>
                                              </div>
                                          </div>

                                      </div>


                                      <table id="myTable" class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                          <thead>
                                              <tr>
                                                  <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Name</th>
                                                  <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="3">Username</th>
                                                  <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Password</th>
                                                  <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Privilege</th>
                                                  <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Email</th>
                                                  <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Last Login</th>
                                                  <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">Created_at</th>
                                                  <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="7">Updated_at</th>
                                                  <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Action</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              <tr ng-repeat="user in listCount  = (users | filter:filterlist) | orderBy:'-created_at'  | pagination: currentPage : numPerPage">
                                                  <td class="title"><a href="javascript:void(0)">[[user.name]]</a></td>
                                                  <td>[[user.username]]</td>
                                                  <td><md-button class="md-warn md-small" ng-click="resetPassword(user,$event);"> reset</md-button></td>
                                                  <td><md-button class="md-warn md-small" ng-click="permissionMode(user);"> <md-icon>vpn_key</md-icon></md-button></td>
                                                  <td>[[user.email]]</td>
                                                  <td>[[user.lastLogin]]</td>
                                                  <td>[[user.created_at]]</td>
                                                  <td>[[user.updated_at]]</td>
                                                  <td>
                                                      <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                           <md-button class="md-primary md-small" ng-click="editUser(user);"> <md-icon>mode_edit</md-icon></md-button>
                                                           <md-button class="md-warn md-small" ng-click="deleteUser(user,$event);"><md-icon>delete</md-icon></md-button>
                                                      </div>
                                                  </td>
                                              </tr>

                                          </tbody>
                                      </table>
                                      <div class="clearfix pull-left" ng-show="users.length > numPerPage">
                                           <pagination
                                               ng-model="currentPage"
                                               total-items="listCount.length"
                                               max-size="maxSize"
                                               items-per-page="numPerPage"
                                               boundary-links="true"
                                               class="pagination-sm pull-right"
                                               previous-text="&lsaquo;"
                                               next-text="&rsaquo;"
                                               first-text="&laquo;"
                                               last-text="&raquo;"
                                               ></pagination>
                                       </div>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <br/><br/>
                   </div>
              </div>
          </div>


          <div ng-show="resetPermission">
              <div class="row">
                  <div class="col-md-12">
                    <div class="row">
                        <ol class="breadcrumb">
                          <li>
                            <a href="javascript:void(0)" ng-click="backToUsers($event);"><i class="fa fa-home"></i></a>
                            <span ng-click="backToUsers($event);">User:[[name]]</span>
                          </li>
                          <li>
                              <a href="javascript:void(0)" ><i class="fa fa-key"></i></a>
                              <span>Permission</span>
                          </li>
                        </ol>
                    </div>
                    <div>
                        Select the pages you want add for this user from available pages and then you can add write,edit (White color is for not allowed) permission for those pages
                    </div>
                  </div>
                  <div class="col-md-6">
                    <ul class="list-group">
                      <li class="list-group-item active">
                          Permited pages
                          <md-button class="md-raised md-small  md-primary pull-right" ng-click="savePermission($event)">Save</md-button>
                      </li>
                      <li class="list-group-item">
                        <input type="text" name="SearchDualList" class="form-control" placeholder="search" ng-model="filterMypermission" />
                      </li>
                      <li class="list-group-item" ng-repeat="mypermision in mypermisions | filter:filterMypermission">
                        [[mypermision.name]] <a href ng-click="removeToList(mypermision);"><i class="fa fa-arrow-right"></i></a>

                        <div class="btn-group btn-group-xs pull-right">
                          <label class="btn" ng-model="mypermision.write" ng-click="toggleWrite(mypermision)"  ng-class="mypermision.write === 'true'? 'btn-primary' : 'btn-default'">
                            write
                          </label>&nbsp;
                          <label class="btn" ng-model="mypermision.edit" ng-click="toggleEdit(mypermision)"  ng-class="mypermision.edit === 'true'? 'btn-success' : 'btn-default'">
                            Edit
                          </label>&nbsp;
                          <label class="btn" ng-model="mypermision.delete" ng-click="toggleDelete(mypermision)"  ng-class="mypermision.delete === 'true'? 'btn-danger' : 'btn-default'">
                              Delete
                          </label>
                        </div>

                      </li>
                    </ul>
                  </div>
                  <div class="col-md-6">
                    <ul class="list-group">
                      <li class="list-group-item active">Available pages</li>
                      <li class="list-group-item">
                        <input type="text" name="SearchDualList" class="form-control" placeholder="search" ng-model="filterPage" />
                      </li>
                      <li class="list-group-item" ng-repeat="page in pages | filter:filterPage">
                        <a href ng-click="addToList(page);"><i class="fa fa-arrow-left"></i></a> [[page.name]]
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="row text-center">
                        <md-button ng-click="savePermission($event)" class="md-raised  md-primary text-center" > <md-icon>note_add</md-icon>Update Permission</md-button>
                    </div>
                  </div>
                </div>
          </div>
    </md-content>
    @else
        Unauthorized Access
    @endif
  
  
  @section('scripts')
     <script>
     function mySearch() {
       // Declare variables
       var input, filter, table, tr, td, i;
       input = document.getElementById("myInput");
       filter = input.value.toUpperCase();
       table = document.getElementById("myTable");
       tr = table.getElementsByTagName("tr");
  
       // Loop through all table rows, and hide those who don't match the search query
       for (i = 0; i < tr.length; i++) {
         td = tr[i].getElementsByTagName("td")[0];
         if (td) {
           if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
             tr[i].style.display = "";
           } else {
             tr[i].style.display = "none";
           }
         }
       }
     }
     </script>
  @stop
  
  @stop
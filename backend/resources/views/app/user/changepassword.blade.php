@extends('layout.main')
@include('app.partial.message')
@section('content')
  <!--
        ...................USER MANAGEMENT......................................
  -->
  <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">User-Change Password</h4> </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
            <ol class="breadcrumb">
                <li><a href="/">Accounts</a></li>
                <li class="active">Change Password</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
  <md-content id="popupContainer">
  <div class="row">
           <div class="col-md-12">
                  <div class="box">
                      <div class="row">
                          <div class="col-md-12">
                              <form method="post" action="{{url('api/changepassword')}}"  class="form-horizontal" >
                                  <h3>Change Password</h3><br>
                                  <div layout-gt-xs="row">
                                    <md-input-container class="md-block" flex-gt-xs>
                                      <label class="control-label1">Current Password</label>
                                      <input type="password" name="currentpassword" required="">
                                    </md-input-container>
                                  </div>
                                  <div layout-gt-xs="row">
                                      <md-input-container class="md-block" flex-gt-xs>
                                        <label class="control-label1">New Password</label>
                                        <input id="newpassword" type="password" name="newpassword" required="">
                                      </md-input-container>
                                  </div>
                                  <div layout-gt-xs="row">
                                        <md-input-container class="md-block" flex-gt-xs>
                                          <label class="control-label1">Re-enter Password</label>
                                          <input id="repassword" onkeyup="check();" type="password" name="repassword" required="">
                                          <span id="message"></span>
                                        </md-input-container>
                                   </div>
                                  <div class="form-group">
                                    <div class="col-sm-12 text-right">
                                      <md-button type="submit" class="md-raised  md-primary  pull-right" > <md-icon>note_add</md-icon>Change</md-button>
                                    </div>
                                  </div>
                                  <hr>
                              </form>
                          </div>
                      </div>
                  </div>
           </div>
          </div>
    </md-content>
@section('scripts')
     <script>
     var check=function mySearch() {
       if(document.getElementById('newpassword').value==document.getElementById('repassword').value){
            document.getElementById('message').style.color='green';
            document.getElementById('message').innerHTML='Matching';
       }else{
             document.getElementById('message').style.color='red';
             document.getElementById('message').innerHTML='Not Matching';
       }
     }
     </script>
  @stop

  @stop
@extends('layout.main')
@include('app.partial.message')
@section('content')
  <!--
        ...................MAIN ACCOUNT-DAY BOOK......................................
  -->
  <div class="row bg-title">
          <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h4 class="page-title">MAIN ACCOUNT-Day Book</h4> </div>
          <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
              <ol class="breadcrumb">
                  <li><a href="/">Accounts</a></li>
                  <li class="active">Day Book</li>
              </ol>
          </div>
          <!-- /.col-lg-12 -->
      </div>
   <section id="content" role="content" ng-controller="MainDaybookController"  layout="column" layout-padding md-scroll-y style="overflow: auto;" ng-cloak>
      <md-content id="popupContainer"  class="md-whiteframe-z2" ng-cloak>
        <div ng-show="permission">
        <div class="row">
             <div class="col-md-12">
                    <div  class="box">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="white-box" style="background: transparent;">
                                    <div class="col-sm-12">
                                        <h3 class="box-title m-b-0">Day Book </h3>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Show
                                                    <select ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);" class="form-control pagiantion">
                                                    </select>
                                                    entries
                                                </label>
                                            </div>

                                            <div class="col-md-8 ">
                                                <div class="form-inline form-group " style="float: right;">
                                                    <label for="filter-list">Search </label>
                                                    <input type="text" class="form-control" ng-model="filterlist" placeholder="Search">
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row text-center">
                                        <section class="form-horizontal col-sm-12">
                                            <div class="row">
                                                <label class="col-sm-1 control-label">From</label>
                                                <div class="col-md-2">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" datepicker-popup="dd-MM-yyyy" ng-model="fromDate" is-open="expensepicker" show-button-bar="false" show-weeks="false" readonly>
                                                        <span class="input-group-btn">
                                                          <button type="button" class="btn btn-default" ng-click="expensepicker=true"><i class="fa fa-calendar"></i></button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <label class="col-sm-1 control-label">To</label>
                                                <div class="col-md-2">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" datepicker-popup="dd-MM-yyyy" ng-model="toDate" is-open="expensepickerto" show-button-bar="false" show-weeks="false" readonly>
                                                        <span class="input-group-btn">
                                                          <button type="button" class="btn btn-default" ng-click="expensepickerto=true"><i class="fa fa-calendar"></i></button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <select ng-model="account_id" class="form-control" required="">
                                                        <option value="All">All</option>
                                                        @foreach($accounts as $account)
                                                        <option value="{{$account->id}}">{{$account->name}}</option>
                                                         @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                      <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                           <md-button ng-click="search(fromDate,toDate,account_id);" class="md-primary md-raised pull-left"> <md-icon>search</md-icon> Search</md-button>
                                                           <md-button ng-click="export(fromDate,toDate,account_id);"  class="md-accent md-raised"> <md-icon>speaker_notes</md-icon> Export</md-button>
                                                           <md-button ng-click="exportPDF(fromDate,toDate,account_id);"  class="md-warn md-raised"> <md-icon>picture_as_pdf</md-icon> PDF</md-button>
                                                      </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="container" ng-show="showProgress">
                                                    <md-progress-linear md-mode="query"></md-progress-linear>
                                                    <div class="bottom-block">
                                                      <span>Generating Documents...Please Wait..!!!</span>
                                                    </div>
                                                  </div>
                                            </div>
                                        </section>
                                    </div>

                                    <table id="myTable" class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                        <thead>
                                            <tr>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Account Head</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Amount</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Date</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Remark</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Created_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Updated_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">User</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="daybook in listCount  = (daybooks | filter:filterlist) | orderBy:'-date'  | pagination: currentPage : numPerPage" ng-class="daybook.accounthead.type==='income'?'text-primary':'text-danger'">
                                                <td class="title"><a ng-click="open(daybook,$event);" href="javascript:void(0)">[[daybook.accounthead.name]]</a></td>
                                                <td>[[daybook.amount]] KD</td>
                                                <td>[[daybook.date | date:'dd-MMMM-yyyy']]</td>
                                                <td>[[daybook.remark]]</td>
                                                <td>[[daybook.created_at]]</td>
                                                <td>[[daybook.updated_at]]</td>
                                                <td>[[daybook.user.name]]</td>
                                            </tr>
                                            <tr class="text-danger">
                                                <td class="text-center">Total Income</td><td class="text-center">[[totalincome]] KD</td>
                                                <td class="text-center">Total Expense</td><td class="text-center">[[totalexpense]] KD</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                     <div class="clearfix pull-left" ng-show="daybooks.length > numPerPage">
                                        <pagination
                                            ng-model="currentPage"
                                            total-items="listCount.length"
                                            max-size="maxSize"
                                            items-per-page="numPerPage"
                                            boundary-links="true"
                                            class="pagination-sm pull-right"
                                            previous-text="&lsaquo;"
                                            next-text="&rsaquo;"
                                            first-text="&laquo;"
                                            last-text="&raquo;"
                                            ></pagination>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                  {{--  <div class="row">
                         <div style="width: 100%;" id="piechart_income"></div>
                       </div>--}}
                    <br/><br/>
             </div>
        </div>
        </div>
                <div ng-hide="permission">
                    <div  layout="row" layout-padding layout-wrap layout-fill>
                        <div md-whiteframe="3" class="padded alert">
                            <strong>So...Sorry!</strong> You have no Permission to View This Page.
                        </div>
                    </div>
                </div>
  </md-content>
   </section>

@section('scripts')

  {{--<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
     google.charts.load("current", {packages:["corechart"]});
     google.charts.setOnLoadCallback(drawChart);
     function drawChart() {
       var data = google.visualization.arrayToDataTable([
         ['Task', 'Income'],
         ['Income',     {{$daybooks->where('accounthead.type','income')->sum('amount')}}],
         ['Expense',    {{$daybooks->where('accounthead.type','expense')->sum('amount')}}],
       ]);

       var options = {
         title: 'Income and Expenditure',
         is3D: true,
       };

       var chart = new google.visualization.PieChart(document.getElementById('piechart_income'));
       chart.draw(data, options);
     }
  </script>--}}
@stop

@stop
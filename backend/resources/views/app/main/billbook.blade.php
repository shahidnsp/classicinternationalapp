@extends('layout.main')
@include('app.partial.message')
@section('content')
  <!--
        ...................MAIN ACCOUNT......................................
  -->
  <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Main-Bill Book</h4> </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
            <ol class="breadcrumb">
                <li><a href="/">Accounts</a></li>
                <li class="active">Bill Book</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

  <md-content id="popupContainer" ng-controller="MainAccountBillBookController" >
    <div ng-show="permission" ng-cloak>
        <div class="row">
             <div class="col-md-12">
                    <div class="box">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="row pull-right">
                                <md-button ng-if="user.permissions.mainbillbook.write =='true'" ng-hide="billedit" class="md-raised md-small md-primary" ng-click="newBillBook();"> <md-icon>note_add</md-icon> Add Bill</md-button>
                            </div>
                            <br/>
                            <form ng-show="billedit" class="form-horizontal"  ng-submit="addBillBook();">
                                <h3>New Bill</h3><br>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <mdp-date-picker name="date" mdp-placeholder="Date"   mdp-format="DD/MM/YYYY"  ng-model="newbill.date"></mdp-date-picker>
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Customer</label>
                                    <input ng-model="newbill.name" required="">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label>Address</label>
                                     <textarea ng-model="newbill.address" md-maxlength="250" rows="5" md-select-on-focus></textarea>
                                   </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Phone</label>
                                    <input ng-model="newbill.phone">
                                  </md-input-container>
                                </div>

                                <div layout="column">
                                    <table class="table table-striped table-bpurchaseed table-hover" id="dataTables-example">
                                            <thead>
                                            <tr>
                                                <th>SlNo</th>
                                                <th>Item</th>
                                                <th>Amount</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr ng-repeat="newitem in newitems">
                                                <td><div>[[$index+1]]</div></td>
                                                <td>
                                                    <div ng-hide="editingData[newitem.id]">[[newitem.name]]</div>
                                                    <div ng-show="editingData[newitem.id]"><select class="form-control" ng-model="newitem.accounthead_id" > <option ng-repeat="expense in expenses" value="[[expense.id]]" ng-selected="newitem.accounthead_id == expense.id">[[expense.name]]</option></select></div>
                                                </td>
                                                <td>
                                                    <div ng-hide="editingData[newitem.id]">[[newitem.amount]]</div>
                                                    <div ng-show="editingData[newitem.id]"><input type="text" onkeypress="return isNumberKey(event)" ng-change="addTotal();" class="form-control" ng-model="newitem.amount"></div>
                                                </td>

                                                <td ng-show="newitems.length>0">
                                                    <div ng-hide="editingData[newitem.id]" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                        <button type="button" class="btn btn-success" ng-click="editItem(newitem);">Edit
                                                        </button>
                                                        <button type="button" class="btn btn-danger" ng-click="removeItem(newitem); editmode = !editmode">Remove
                                                        </button>
                                                    </div>
                                                    <div ng-show="editingData[newitem.id]"  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                        <button type="button" class="btn btn-success" ng-click="updateItem(newitem);">Update
                                                        </button>
                                                        <button type="button" class="btn btn-danger" ng-click="removeItem(newitem); editmode = !editmode">Remove
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div layout="row">
                                        <h2>Total Amount: [[totalAmount]] KD</h2>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 text-right">
                                        <button type="button" class="btn btn-default" ng-click="addItem();">Add Item To List</button>
                                    </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-sm-12 text-right">
                                    <md-button class="md-raised btn-default pull-right" ng-click="cancelBillBook();"> <md-icon>cancel</md-icon> Cancel</md-button>
                                    <md-button type="submit" class="md-raised  md-primary  pull-right" > <md-icon>note_add</md-icon>Save and Get Invoice</md-button>
                                  </div>
                                </div>
                                <hr>
                            </form>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="white-box">
                                    <div class="col-sm-12">
                                        <h3 class="box-title m-b-0">Bill List</h3>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Show
                                                    <select ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);" class="form-control pagiantion">
                                                    </select>
                                                    entries
                                                </label>
                                            </div>

                                            <div class="col-md-8 ">
                                                <div class="form-inline form-group " style="float: right;">
                                                    <label for="filter-list">Search </label>
                                                    <input type="text" class="form-control" ng-model="filterlist" placeholder="Search">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row text-center">
                                        <section  class="form-horizontal col-sm-12">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="input-group">
                                                      <input type="text" class="form-control" datepicker-popup="dd-MM-yyyy" ng-model="fromDate" is-open="expensepicker" show-button-bar="false" show-weeks="false" readonly>
                                                      <span class="input-group-btn">
                                                        <button type="button" class="btn btn-default" ng-click="expensepicker=true"><i class="fa fa-calendar"></i></button>
                                                      </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="input-group">
                                                       <input type="text" class="form-control" datepicker-popup="dd-MM-yyyy" ng-model="toDate" is-open="expensepickerto" show-button-bar="false" show-weeks="false" readonly>
                                                       <span class="input-group-btn">
                                                         <button type="button" class="btn btn-default" ng-click="expensepickerto=true"><i class="fa fa-calendar"></i></button>
                                                       </span>
                                                   </div>
                                                </div>
                                                 <div class="col-md-2">
                                                      <md-button ng-click="search(fromDate,toDate);" class="md-primary md-raised pull-left"> <md-icon>search</md-icon> Search</md-button>
                                                </div>

                                            </div>
                                        </section>
                                    </div>

                                    <table id="myTable" class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                        <thead>
                                            <tr>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">#</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Date</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Name</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Address</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Phone</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Amount</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="7">Created_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="8">Updated_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="9">User</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <tr ng-repeat="bill in listCount  = (billbooks | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage" ng-cloak>
                                                <td>[[numPerPage *(currentPage-1)+$index+1]]</td>
                                                <td class="title">[[bill.date | date:'dd-MMMM-yyyy']]</td>
                                                <td ng-cloak>[[bill.name]]</td>
                                                <td ng-cloak>[[bill.address]]</td>
                                                <td ng-cloak>[[bill.phone]]</td>
                                                <td ng-cloak>[[bill.amount]] KD</td>
                                                <td ng-cloak>[[bill.created_at]]</td>
                                                <td ng-cloak>[[bill.updated_at]]</td>
                                                <td ng-cloak>[[bill.user.name]]</td>
                                                <td>
                                                    <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                         <md-button ng-if="user.permissions.mainbillbook.edit =='true'" class="md-primary md-small" ng-click="editBillBook(bill);"> <md-icon>mode_edit</md-icon></md-button>
                                                         <md-button ng-if="user.permissions.mainbillbook.delete =='true'" class="md-warn md-small" ng-click="deleteBillBook(bill,$event);"><md-icon>delete</md-icon></md-button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="clearfix pull-left" ng-show="billbooks.length > numPerPage">
                                        <pagination
                                            ng-model="currentPage"
                                            total-items="listCount.length"
                                            max-size="maxSize"
                                            items-per-page="numPerPage"
                                            boundary-links="true"
                                            class="pagination-sm pull-right"
                                            previous-text="&lsaquo;"
                                            next-text="&rsaquo;"
                                            first-text="&laquo;"
                                            last-text="&raquo;"
                                            ></pagination>
                                    </div>
                                     {{--{!! $accountheads->render() !!}--}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <br/><br/>
             </div>
        </div>
    </div>

    <div ng-hide="permission" ng-cloak>
        <div  layout="row" layout-padding layout-wrap layout-fill>
            <div md-whiteframe="3" class="padded alert">
                <strong>So...Sorry!</strong> You have no Permission to View This Page.
            </div>
        </div>
    </div>
  </md-content>


@section('scripts')

@stop

@stop
@extends('layout.main')
@include('app.partial.message')
@section('content')
  <!--
        ...................MAIN ACCOUNT......................................
  -->
  <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Main-Account Head</h4> </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
            <ol class="breadcrumb">
                <li><a href="/">Accounts</a></li>
                <li class="active">Account Head</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

  <md-content id="popupContainer" ng-controller="AttendanceController" >
    <div ng-show="permission" ng-cloak>
        <div class="row">
             <div class="col-md-12">
                    <div class="box">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="row pull-right">
                                <md-button ng-if="user.permissions.attendance.write =='true'" ng-hide="headedit" class="md-raised md-small md-primary" ng-click="newAttendance();"> <md-icon>note_add</md-icon> Add Account Head</md-button>
                            </div>
                            <br/>
                            <form ng-show="headedit" class="form-horizontal"  ng-submit="addAttendance();">
                                <h3>New Account Head</h3><br>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                        <mdp-date-picker name="fromDate" mdp-placeholder="Date"   mdp-format="DD/MM/YYYY"  ng-model="newattendance.date"></mdp-date-picker>
                                    </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label class="control-label1">Employee</label>
                                     <md-select ng-model="newattendance.employee_id" required="">
                                       <md-option ng-value="employee.id" ng-repeat="employee in employees">[[employee.name]]</md-option>
                                     </md-select>
                                   </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label class="control-label1">Type</label>
                                     <md-select ng-model="newattendance.type" required="">
                                       <md-option value="Present">Present</md-option>
                                       <md-option value="Absent">Absent</md-option>
                                       <md-option value="Half Day">Half Day</md-option>
                                     </md-select>
                                   </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label>Remark</label>
                                     <textarea ng-model="newattendance.remark" md-maxlength="250" rows="5" md-select-on-focus></textarea>
                                   </md-input-container>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-12 text-right">
                                    <md-button class="md-raised btn-default pull-right" ng-click="cancelAttendance();"> <md-icon>cancel</md-icon> Cancel</md-button>
                                    <md-button type="submit" class="md-raised  md-primary  pull-right" > <md-icon>note_add</md-icon>Save</md-button>
                                  </div>
                                </div>
                                <hr>
                            </form>
                            </div>
                        </div>
                    </div>



                    <div class="box">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="white-box">
                                    <div class="col-sm-12">
                                        <h3 class="box-title m-b-0">Account Head List</h3>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Show
                                                    <select ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);" class="form-control pagiantion">
                                                    </select>
                                                    entries
                                                </label>
                                            </div>

                                            <div class="col-md-8 ">
                                                <div class="form-inline form-group " style="float: right;">
                                                    <label for="filter-list">Search </label>
                                                    <input type="text" class="form-control" ng-model="filterlist" placeholder="Search">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row text-center">
                                        <section  class="form-horizontal col-sm-12">
                                            <div class="row">
                                                <label class="col-md-1 control-label">From</label>
                                                <div class="col-md-2">
                                                     <div class="input-group">
                                                        <input type="text" class="form-control" datepicker-popup="dd-MM-yyyy" ng-model="fromDate" is-open="expensepicker" show-button-bar="false" show-weeks="false" readonly>
                                                        <span class="input-group-btn">
                                                          <button type="button" class="btn btn-default" ng-click="expensepicker=true"><i class="fa fa-calendar"></i></button>
                                                        </span>
                                                     </div>
                                                </div>
                                                <label class="col-md-1 control-label">To</label>
                                                <div class="col-md-2">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" datepicker-popup="dd-MM-yyyy" ng-model="toDate" is-open="expensepickerto" show-button-bar="false" show-weeks="false" readonly>
                                                        <span class="input-group-btn">
                                                          <button type="button" class="btn btn-default" ng-click="expensepickerto=true"><i class="fa fa-calendar"></i></button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <label class="col-md-1 control-label">Employee</label>
                                                <div class="col-md-2">
                                                    <select class="form-control" ng-model="employee_id">
                                                        <option value="All">All</option>
                                                        <option ng-repeat="employee in employees" value="[[employee.id]]">[[employee.name]]</option>
                                                    </select>
                                                </div>
                                                 <div class="col-md-2">
                                                      <md-button ng-click="search(fromDate,toDate,employee_id);" class="md-primary md-raised pull-left"> <md-icon>search</md-icon> Search</md-button>
                                                </div>

                                            </div>
                                        </section>
                                    </div>

                                    <table id="myTable" class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                        <thead>
                                            <tr>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">#</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Date</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Employee</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="3">Attendance</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Remark</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Created_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">Updated_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="7">User</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <tr ng-repeat="attendance in listCount  = (attendances | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage" ng-cloak ng-class="attendance.type=='Present' ? 'present' : attendance.type=='Absent' ? 'absent' : 'halfday' ">
                                                <td>[[numPerPage *(currentPage-1)+$index+1]]</td>
                                                <td class="title">[[attendance.date | date:'dd-MMMM-yyyy']]</td>
                                                <td class="title">[[attendance.employee.name]]</td>
                                                <td ng-cloak>[[attendance.type]]</td>
                                                <td ng-cloak>[[attendance.remark]]</td>
                                                <td ng-cloak>[[attendance.created_at]]</td>
                                                <td ng-cloak>[[attendance.updated_at]]</td>
                                                <td ng-cloak>[[attendance.user.name]]</td>
                                                <td>
                                                    <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                         <md-button ng-if="user.permissions.attendance.edit =='true'" class="md-primary md-small" ng-click="editAttendance(attendance);"> <md-icon>mode_edit</md-icon></md-button>
                                                         <md-button ng-if="user.permissions.attendance.delete =='true'" class="md-warn md-small" ng-click="deleteAttendance(attendance,$event);"><md-icon>delete</md-icon></md-button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="clearfix pull-left" ng-show="attendances.length > numPerPage">
                                        <pagination
                                            ng-model="currentPage"
                                            total-items="listCount.length"
                                            max-size="maxSize"
                                            items-per-page="numPerPage"
                                            boundary-links="true"
                                            class="pagination-sm pull-right"
                                            previous-text="&lsaquo;"
                                            next-text="&rsaquo;"
                                            first-text="&laquo;"
                                            last-text="&raquo;"
                                            ></pagination>
                                    </div>
                                     {{--{!! $attendances->render() !!}--}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <br/><br/>
             </div>
        </div>
    </div>

    <div ng-hide="permission" ng-cloak>
        <div  layout="row" layout-padding layout-wrap layout-fill>
            <div md-whiteframe="3" class="padded alert">
                <strong>So...Sorry!</strong> You have no Permission to View This Page.
            </div>
        </div>
    </div>
  </md-content>


@section('scripts')

@stop

@stop
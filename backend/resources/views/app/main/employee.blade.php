@extends('layout.main')
@include('app.partial.message')
@section('content')
  <!--
        ...................MAIN ACCOUNT......................................
  -->
  <div class="row bg-title">
          <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h4 class="page-title">Main-Employee</h4> </div>
          <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
              <ol class="breadcrumb">
                  <li><a href="/">Accounts</a></li>
                  <li class="active">Employee</li>
              </ol>
          </div>
          <!-- /.col-lg-12 -->
  </div>

  <md-content id="popupContainer" ng-controller="EmployeeController" ng-cloak>
     <div ng-show="permission">
        <div class="row">
             <div class="col-md-12">
                    <div class="box">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="row pull-right">
                                <md-button ng-if="user.permissions.employee.write =='true'" ng-hide="employeeedit" class="md-raised md-small md-primary" ng-click="newEmployee();"> <md-icon>note_add</md-icon> Add Employee</md-button>
                            </div>
                            <br/>
                            <form ng-show="employeeedit" class="form-horizontal"  ng-submit="addEmployee();">
                                <h3>New Employee</h3><br>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Employee Name</label>
                                    <input ng-model="newemployee.name" required="">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Photo</label>
                                    <input type="file" ng-model="photos" accept="image/*" image-Reader multiple>
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Mobile Number</label>
                                    <input ng-model="newemployee.mobile" required="">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Whatsapp Number</label>
                                    <input ng-model="newemployee.whatsapp">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Email Address</label>
                                    <input type="email" ng-model="newemployee.email">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label>Address (Kuwait)</label>
                                     <textarea ng-model="newemployee.addresskuwait" md-maxlength="250" rows="5" md-select-on-focus></textarea>
                                   </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label>Address (India)</label>
                                     <textarea ng-model="newemployee.addressindia" md-maxlength="250" rows="5" md-select-on-focus></textarea>
                                   </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Mobile Number (India)</label>
                                    <input type="text" ng-model="newemployee.mobileindia">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label class="control-label1">Designation</label>
                                     <md-select ng-model="newemployee.designation_id" required="">
                                        <md-option ng-repeat="designation in designations" value="[[designation.id]]">[[designation.name]]</md-option>
                                     </md-select>
                                   </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Basic Pay</label>
                                    <input type="text" ng-model="newemployee.basicpay" required="">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Overtime Pay</label>
                                    <input type="text" ng-model="newemployee.overtimepay">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Passport Number</label>
                                    <input type="text" ng-model="newemployee.passportno">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Visa Number</label>
                                    <input type="text" ng-model="newemployee.visano">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Driving License Number</label>
                                    <input type="text" ng-model="newemployee.drivinglicenseno">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label>Description</label>
                                     <textarea ng-model="newemployee.remark" md-maxlength="250" rows="5" md-select-on-focus></textarea>
                                   </md-input-container>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-12 text-right">
                                    <md-button class="md-raised btn-default pull-right" ng-click="cancelEmployee();"> <md-icon>cancel</md-icon> Cancel</md-button>
                                    <md-button type="submit" class="md-raised  md-primary  pull-right" > <md-icon>note_add</md-icon>Save</md-button>
                                  </div>
                                </div>
                                <hr>
                            </form>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="white-box">
                                    <div class="col-sm-12">
                                        <h3 class="box-title m-b-0">Employee List</h3>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Show
                                                    <select ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);" class="form-control pagiantion">
                                                    </select>
                                                    entries
                                                </label>
                                            </div>

                                            <div class="col-md-8 ">
                                                <div class="form-inline form-group " style="float: right;">
                                                    <label for="filter-list">Search </label>
                                                    <input type="text" class="form-control" ng-model="filterlist" placeholder="Search">
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <table id="myTable" class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                        <thead>
                                            <tr>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">#</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Name</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="2">Photo</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Mobile</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Whatsapp</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Email</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">Address(Kuwait)</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="7">Address(India)</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="8">Mobile(India)</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="9">Designation</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="10">Basic Pay</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="11">Overtime Pay</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="12">Passport No</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="13">Visa Number</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="14">Driving Lic.No</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="15">Remark</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="16">Created_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="17">Updated_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="18">User</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="employee in listCount  = (employees | filter:filterlist) | orderBy:'-created_at'  | pagination: currentPage : numPerPage" ng-cloak>
                                                <td>[[numPerPage *(currentPage-1)+$index+1]]</td>
                                                <td class="title"><a ng-click="open(employee,$event);" href="javascript:void(0)">[[employee.name]]</a></td>
                                                <td><img src="[[basepath]]/images/[[employee.photo]]" style="width: 100px;height: 100px;" alt=""/></td>
                                                <td>[[employee.mobile]]</td>
                                                <td>[[employee.whatsapp]]</td>
                                                <td>[[employee.email]]</td>
                                                <td>[[employee.addresskuwait]]</td>
                                                <td>[[employee.addressindia]]</td>
                                                <td>[[employee.mobileindia]]</td>
                                                <td>[[employee.designation.name]]</td>
                                                <td>[[employee.basicpay]]</td>
                                                <td>[[employee.overtimepay]]</td>
                                                <td>[[employee.passportno]]</td>
                                                <td>[[employee.visano]]</td>
                                                <td>[[employee.drivinglicenseno]]</td>
                                                <td>[[employee.remark]]</td>
                                                <td>[[employee.created_at]]</td>
                                                <td>[[employee.updated_at]]</td>
                                                <td>[[employee.user.name]]</td>
                                                <td>
                                                    <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                         <md-button ng-if="user.permissions.employee.edit =='true'" class="md-primary md-small" ng-click="editEmployee(employee);"> <md-icon>mode_edit</md-icon></md-button>
                                                         <md-button ng-show="employee.active=='1'" class="md-primary md-small" ng-click="changeStaus(employee.id,$event);">Active</md-button>
                                                         <md-button ng-show="employee.active=='0'" class="md-danger md-small" ng-click="changeStaus(employee.id,$event);">In Active</md-button>
                                                         <md-button ng-if="user.permissions.employee.delete =='true'" class="md-warn md-small" ng-click="deleteEmployee(employee,$event);"><md-icon>delete</md-icon></md-button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="clearfix pull-left" ng-show="employees.length > numPerPage">
                                        <pagination
                                            ng-model="currentPage"
                                            total-items="listCount.length"
                                            max-size="maxSize"
                                            items-per-page="numPerPage"
                                            boundary-links="true"
                                            class="pagination-sm pull-right"
                                            previous-text="&lsaquo;"
                                            next-text="&rsaquo;"
                                            first-text="&laquo;"
                                            last-text="&raquo;"
                                            ></pagination>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br/><br/>
             </div>
        </div>
     </div>

    <div ng-hide="permission">
        <div  layout="row" layout-padding layout-wrap layout-fill>
            <div md-whiteframe="3" class="padded alert">
                <strong>So...Sorry!</strong> You have no Permission to View This Page.
            </div>
        </div>
    </div>
  </md-content>


@section('scripts')

@stop

@stop
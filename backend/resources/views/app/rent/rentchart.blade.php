@extends('layout.main')
@include('app.partial.message')
@section('content')
  <!--
        ...................RENT ACCOUNT......................................
  -->

  <div class="row bg-title">
          <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h4 class="page-title">Rent-Rent Chart</h4> </div>
          <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
              <ol class="breadcrumb">
                  <li><a href="/">Accounts</a></li>
                  <li class="active">Rent Chart</li>
              </ol>
          </div>
          <!-- /.col-lg-12 -->
      </div>

  <md-content id="popupContainer" ng-controller="RentchartController" ng-cloak>
      <div ng-show="permission">
        <div class="row">
             <div class="col-md-12">
                    <div class="box">

                            <div class="col-lg-12">
                                <div class="white-box">
                                    <div class="col-sm-12">
                                        <h3 class="box-title m-b-0">Rent Chart</h3>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <form class="form-horizontal" method="get" action="{{url('api/rentchart')}}">
                                                    <div class="col-md-3">
                                                        <select name="year" ng-model="year" class="form-control pagiantion">
                                                            @foreach($years as $year)
                                                                <option value="{{$year->year}}" @if($year->year == $selected) selected @endif>{{$year->year}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                      <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                           <md-button type="submit" class="md-raised md-primary"> <md-icon>note_add</md-icon>Generate</md-button>
                                                           <md-button ng-click="export(year);"  class="md-raised"> <md-icon>speaker_notes</md-icon> Export</md-button>
                                                      </div>
                                                     </div>
                                                    {{--<div class="col-md-3">--}}
                                                        {{--<md-button type="submit" class="md-raised md-primary"> <md-icon>note_add</md-icon>Generate</md-button>--}}
                                                    {{--</div>--}}
                                                </form>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-inline form-group " style="float: right;">
                                                    <label for="filter-list">Search </label>
                                                    <input type="text" class="form-control" ng-model="filterlist" placeholder="Search">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                             <h2 class="text-primary" style="text-decoration: underline;">Rent Collection Chart of the Year {{$selected}}</h2>
                                        </div>
                                    </div>

                                    <table id="myTable" class="tablesaw table-bordered table-hover table"  data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                        <thead>
                                            <tr>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">SlNo</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Building Name</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Basic Rent</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="1">Jan</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Feb</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Mar</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Apr</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">May</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">Jun</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="7">Jul</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="8">Aug</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="9">Sep</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="10">Oct</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="11">Nov</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="12">Dec</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($rents as $key => $rent)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td class="title"><a ng-click="openRentChart({{$rent}},$event);" href="javascript:void(0)">{{$rent->officename}}</a></td>
                                                <td>{{$rent->buildingrentamount}}</td>
                                                @if($rent->jan=='NPD')
                                                    <td class="text-danger">{{$rent->jan}}</td>
                                                @else
                                                    <td class="text-success">{{$rent->jan}}</td>
                                                @endif
                                                @if($rent->feb=='NPD')
                                                    <td class="text-danger">{{$rent->feb}}</td>
                                                @else
                                                    <td class="text-success">{{$rent->feb}}</td>
                                                @endif
                                                @if($rent->mar=='NPD')
                                                    <td class="text-danger">{{$rent->mar}}</td>
                                                @else
                                                    <td class="text-success">{{$rent->mar}}</td>
                                                @endif
                                                @if($rent->apr=='NPD')
                                                    <td class="text-danger">{{$rent->apr}}</td>
                                                @else
                                                    <td class="text-success">{{$rent->apr}}</td>
                                                @endif
                                                @if($rent->may=='NPD')
                                                    <td class="text-danger">{{$rent->may}}</td>
                                                @else
                                                    <td class="text-success">{{$rent->may}}</td>
                                                @endif
                                                @if($rent->jun=='NPD')
                                                    <td class="text-danger">{{$rent->jun}}</td>
                                                @else
                                                    <td class="text-success">{{$rent->jun}}</td>
                                                @endif
                                                @if($rent->jul=='NPD')
                                                    <td class="text-danger">{{$rent->jul}}</td>
                                                @else
                                                    <td class="text-success">{{$rent->jul}}</td>
                                                @endif
                                                @if($rent->aug=='NPD')
                                                    <td class="text-danger">{{$rent->aug}}</td>
                                                @else
                                                    <td class="text-success">{{$rent->aug}}</td>
                                                @endif
                                                @if($rent->sep=='NPD')
                                                    <td class="text-danger">{{$rent->sep}}</td>
                                                @else
                                                    <td class="text-success">{{$rent->sep}}</td>
                                                @endif
                                                @if($rent->oct=='NPD')
                                                    <td class="text-danger">{{$rent->oct}}</td>
                                                @else
                                                    <td class="text-success">{{$rent->oct}}</td>
                                                @endif
                                                @if($rent->nov=='NPD')
                                                    <td class="text-danger">{{$rent->nov}}</td>
                                                @else
                                                    <td class="text-success">{{$rent->nov}}</td>
                                                @endif
                                                @if($rent->dec=='NPD')
                                                    <td class="text-danger">{{$rent->dec}}</td>
                                                @else
                                                    <td class="text-success">{{$rent->dec}}</td>
                                                @endif

                                            </tr>
                                            @endforeach
                                            <tr style="color: #000099;font-size: large;">
                                                <td>Total</td><td></td><td>{{$rents->sum('buildingrentamount')}}</td>
                                                <td>{{$rents->sum('jan')}}</td>
                                                <td>{{$rents->sum('feb')}}</td>
                                                <td>{{$rents->sum('mar')}}</td>
                                                <td>{{$rents->sum('apr')}}</td>
                                                <td>{{$rents->sum('may')}}</td>
                                                <td>{{$rents->sum('jun')}}</td>
                                                <td>{{$rents->sum('jul')}}</td>
                                                <td>{{$rents->sum('aug')}}</td>
                                                <td>{{$rents->sum('sep')}}</td>
                                                <td>{{$rents->sum('oct')}}</td>
                                                <td>{{$rents->sum('nov')}}</td>
                                                <td>{{$rents->sum('dec')}}</td>
                                            </tr>

                                        </tbody>
                                    </table>

                                </div>
                            </div>

                    </div>

                    <br/><br/>
             </div>
        </div>
        </div>
        <div ng-hide="permission">
            <div  layout="row" layout-padding layout-wrap layout-fill>
                <div md-whiteframe="3" class="padded alert">
                    <strong>So...Sorry!</strong> You have no Permission to View This Page.
                </div>
            </div>
        </div>
  </md-content>


@section('scripts')

@stop

@stop
@extends('layout.main')
@include('app.partial.message')
@section('content')
  <!--
        ...................MAIN ACCOUNT......................................
  -->
<div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Rent-Rent Register</h4> </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
            <ol class="breadcrumb">
                <li><a href="/">Accounts</a></li>
                <li class="active">Rent Register</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
  <md-content id="popupContainer" ng-controller="RentController" ng-cloak>
        <div ng-show="permission">
        <div class="row">
             <div class="col-md-12">
                    <div class="box">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="row pull-right">
                                <md-button ng-if="user.permissions.rent.write =='true'" ng-hide="rentedit" class="md-raised md-small md-primary" ng-click="newRent();"> <md-icon>note_add</md-icon> Add Rent</md-button>
                            </div>
                            <br/>
                            <form ng-show="rentedit" class="form-horizontal"  ng-submit="addRent();">
                                <h3>New Rent</h3><br>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Rent Name</label>
                                    <input ng-model="newrent.officename" required="">
                                  </md-input-container>
                                </div>

                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label>Area</label>
                                     <textarea ng-model="newrent.area" md-maxlength="250" rows="5" md-select-on-focus></textarea>
                                   </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Rent Amount</label>
                                    <input ng-model="newrent.buildingrentamount" required="">
                                  </md-input-container>
                                </div>

                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Rentee Name</label>
                                    <input ng-model="newrent.buildingrentername" required="">
                                  </md-input-container>
                                </div>

                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Rentee Mobile</label>
                                    <input ng-model="newrent.buildingrentermobile" required="">
                                  </md-input-container>
                                </div>

                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label>Remark</label>
                                     <textarea ng-model="newrent.remark" md-maxlength="250" rows="5" md-select-on-focus></textarea>
                                   </md-input-container>
                                </div>

                                <div class="form-group">
                                  <div class="col-sm-12 text-right">
                                    <md-button class="md-raised btn-default pull-right" ng-click="cancelRent();"> <md-icon>cancel</md-icon> Cancel</md-button>
                                    <md-button type="submit" class="md-raised  md-primary  pull-right" > <md-icon>note_add</md-icon>Save</md-button>
                                  </div>
                                </div>
                                <hr>
                            </form>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="white-box">
                                    <div class="col-sm-12">
                                        <h3 class="box-title m-b-0">Rent List</h3>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Show
                                                    <select ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);" class="form-control pagiantion">
                                                    </select>
                                                    entries
                                                </label>
                                            </div>

                                            <div class="col-md-8 ">
                                                <div class="form-inline form-group " style="float: right;">
                                                    <label for="filter-list">Search </label>
                                                    <input type="text" class="form-control" ng-model="filterlist" placeholder="Search">
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <table id="myTable" class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                        <thead>
                                            <tr>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">#</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Office Name</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="2">Area</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Rent Amount</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Rentee Name</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Rentee Contact</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Remark</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">Created_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="7">Updated_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="8">User</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="rent in listCount  = (rents | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                                                <td>[[numPerPage *(currentPage-1)+$index+1]]</td>
                                                <td class="title"><a ng-click="openRent(rent,$event);" href="javascript:void(0)">[[rent.officename]]</a></td>
                                                <td>[[rent.area]]</td>
                                                <td>[[rent.buildingrentamount]] KD</td>
                                                <td>[[rent.buildingrentername]]</td>
                                                <td>[[rent.buildingrentermobile]]</td>
                                                <td>[[rent.remark]]</td>
                                                <td>[[rent.created_at]]</td>
                                                <td>[[rent.updated_at]]</td>
                                                <td>[[rent.user.name]]</td>
                                                <td>
                                                    <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                         <md-button ng-if="user.permissions.rent.edit =='true'" class="md-primary md-small" ng-click="editRent(rent);"> <md-icon>mode_edit</md-icon></md-button>
                                                         <md-button ng-show="rent.active=='1'" class="md-primary md-small" ng-click="changeStaus(rent.id,$event);">Active</md-button>
                                                         <md-button ng-show="rent.active=='0'" class="md-danger md-small" ng-click="changeStaus(rent.id,$event);">In Active</md-button>
                                                         <md-button ng-if="user.permissions.rent.delete =='true'" class="md-warn md-small" ng-click="deleteRent(rent,$event);"><md-icon>delete</md-icon></md-button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="clearfix pull-left" ng-show="rents.length > numPerPage">
                                       <pagination
                                           ng-model="currentPage"
                                           total-items="listCount.length"
                                           max-size="maxSize"
                                           items-per-page="numPerPage"
                                           boundary-links="true"
                                           class="pagination-sm pull-right"
                                           previous-text="&lsaquo;"
                                           next-text="&rsaquo;"
                                           first-text="&laquo;"
                                           last-text="&raquo;"
                                           ></pagination>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br/><br/>
             </div>
        </div>
         </div>
        <div ng-hide="permission">
            <div  layout="row" layout-padding layout-wrap layout-fill>
                <div md-whiteframe="3" class="padded alert">
                    <strong>So...Sorry!</strong> You have no Permission to View This Page.
                </div>
            </div>
        </div>
  </md-content>


@section('scripts')

@stop

@stop
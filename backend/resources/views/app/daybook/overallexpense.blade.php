@extends('layout.main')
@include('app.partial.message')
@section('content')
  <!--
        ...................DAY BOOK......................................
  -->
  <div class="row bg-title">
          <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h4 class="page-title">Daybook-Expense Register</h4> </div>
          <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
              <ol class="breadcrumb">
                  <li><a href="/">Accounts</a></li>
                  <li class="active">Expense Register</li>
              </ol>
          </div>
          <!-- /.col-lg-12 -->
      </div>
   <section id="content" role="content" ng-controller="OverallExpenseController"  layout="column" layout-padding md-scroll-y style="overflow: auto;" ng-cloak>
      <md-content id="popupContainer"  class="md-whiteframe-z2" ng-cloak>
       <div ng-show="permission">
        <div class="row">
             <div class="col-md-12">
                    <div  class="box">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="white-box" style="background: transparent;">
                                    <div class="col-sm-12">
                                        <h3 class="box-title m-b-0">Expense List</h3>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Show
                                                    <select ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);" class="form-control pagiantion">

                                                    </select>
                                                    entries
                                                </label>
                                            </div>

                                            <div class="col-md-8 ">
                                                <div class="form-inline form-group " style="float: right;">
                                                    <label for="filter-list">Search </label>
                                                    <input type="text" class="form-control" id="myInput" onkeyup="mySearch()" placeholder="Search">
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row text-center">
                                        <section  class="form-horizontal col-sm-12">
                                           <div class="row">
                                               <label class="col-sm-1 control-label">From</label>
                                               <div class="col-md-2">
                                                   <div class="input-group">
                                                       <input type="text" class="form-control" datepicker-popup="dd-MM-yyyy" ng-model="fromDate" is-open="expensepicker" show-button-bar="false" show-weeks="false" readonly>
                                                       <span class="input-group-btn">
                                                         <button type="button" class="btn btn-default" ng-click="expensepicker=true"><i class="fa fa-calendar"></i></button>
                                                       </span>
                                                   </div>
                                               </div>
                                               <label class="col-sm-1 control-label">To</label>
                                               <div class="col-md-2">
                                                   <div class="input-group">
                                                       <input type="text" class="form-control" datepicker-popup="dd-MM-yyyy" ng-model="toDate" is-open="expensepickerto" show-button-bar="false" show-weeks="false" readonly>
                                                       <span class="input-group-btn">
                                                         <button type="button" class="btn btn-default" ng-click="expensepickerto=true"><i class="fa fa-calendar"></i></button>
                                                       </span>
                                                   </div>
                                               </div>
                                               <label class="col-sm-1 control-label">Account</label>
                                               <div class="col-md-2">
                                                   <select ng-model="account_id" class="form-control" required="">
                                                       <option value="All">All</option>
                                                       @foreach($accounts as $account)
                                                       <option value="{{$account->id}}">{{$account->name}}</option>
                                                        @endforeach
                                                   </select>
                                               </div>
                                                <div class="col-md-3">
                                                     <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                          <md-button ng-click="search(fromDate,toDate,account_id);" class="md-primary md-raised pull-left"> <md-icon>search</md-icon> Search</md-button>
                                                          <md-button ng-click="export(fromDate,toDate,account_id);"  class="md-accent md-raised"> <md-icon>speaker_notes</md-icon> Export</md-button>
                                                     </div>
                                               </div>

                                           </div>
                                       </section>
                                    </div>

                                    <table id="myTable" class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                        <thead>
                                            <tr>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Account Head</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Amount</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Date</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Remark</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Created_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Updated_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">User</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="expense in listCount  = (expenses | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                                                <td class="title"><a ng-click="open(expense,$event);" href="javascript:void(0)">[[expense.accounthead.name]]</a></td>
                                                <td>[[expense.amount]] KD</td>
                                                <td>[[expense.date]]</td>
                                                <td>[[expense.remark]]</td>
                                                <td>[[expense.created_at]]</td>
                                                <td>[[expense.updated_at]]</td>
                                                <td>[[expense.user.name]]</td>
                                            </tr>
                                            <tr class="text-danger">
                                                <td class="text-center">Total</td><td class="text-center">[[totalexpense]]  KD</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="clearfix pull-left" ng-show="expenses.length > numPerPage">
                                        <pagination
                                            ng-model="currentPage"
                                            total-items="listCount.length"
                                            max-size="maxSize"
                                            items-per-page="numPerPage"
                                            boundary-links="true"
                                            class="pagination-sm pull-right"
                                            previous-text="&lsaquo;"
                                            next-text="&rsaquo;"
                                            first-text="&laquo;"
                                            last-text="&raquo;"
                                            ></pagination>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br/><br/>
             </div>
        </div>
        </div>
                <div ng-hide="permission">
                    <div  layout="row" layout-padding layout-wrap layout-fill>
                        <div md-whiteframe="3" class="padded alert">
                            <strong>So...Sorry!</strong> You have no Permission to View This Page.
                        </div>
                    </div>
                </div>
  </md-content>
   </section>

@section('scripts')
   <script>
   function mySearch() {
     // Declare variables
     var input, filter, table, tr, td, i;
     input = document.getElementById("myInput");
     filter = input.value.toUpperCase();
     table = document.getElementById("myTable");
     tr = table.getElementsByTagName("tr");

     // Loop through all table rows, and hide those who don't match the search query
     for (i = 0; i < tr.length; i++) {
       td = tr[i].getElementsByTagName("td")[0];
       if (td) {
         if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
           tr[i].style.display = "";
         } else {
           tr[i].style.display = "none";
         }
       }
     }
   }
   </script>
@stop

@stop
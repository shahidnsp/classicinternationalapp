@extends('layout.main')
@include('app.partial.message')
@section('content')
  <!--
        ...................MAIN ACCOUNT......................................
  -->
   <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Courier-Courier Register</h4> </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
            <ol class="breadcrumb">
                <li><a href="/">Accounts</a></li>
                <li class="active">Courier Register</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

  <md-content id="popupContainer" ng-controller="CourierController" ng-cloak>
    <div ng-show="permission">
        <div class="row">
             <div class="col-md-12">
                    <div class="box">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="row pull-right">
                                <md-button ng-if="user.permissions.courier.write =='true'" ng-hide="courieredit" class="md-raised md-small md-primary" ng-click="newCourier();"> <md-icon>note_add</md-icon> Add Courier</md-button>
                            </div>
                            <br/>
                            <form ng-show="courieredit" class="form-horizontal"  ng-submit="addCourier();">
                                <h3>New Courier</h3><br>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Courier Name</label>
                                    <input ng-model="newcourier.name" required="">
                                  </md-input-container>
                                </div>

                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label>From Address</label>
                                     <textarea ng-model="newcourier.fromaddress" md-maxlength="250" rows="5" md-select-on-focus></textarea>
                                   </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Contact Number(From)</label>
                                    <input ng-model="newcourier.frommobile" required="">
                                  </md-input-container>
                                </div>

                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label>To Address</label>
                                     <textarea ng-model="newcourier.toaddress" md-maxlength="250" rows="5" md-select-on-focus required=""></textarea>
                                   </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Contact Number(To)</label>
                                    <input ng-model="newcourier.tomobile" required="">
                                  </md-input-container>
                                </div>

                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Courier Amount</label>
                                    <input ng-model="newcourier.courieramount" required="">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Service Charge</label>
                                    <input ng-model="newcourier.servicecharge">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                       <label class="control-label1">Date</label>
                                       <md-datepicker ng-model="newcourier.date" required=""></md-datepicker>
                                    </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label>Remark</label>
                                     <textarea ng-model="newcourier.remark" md-maxlength="250" rows="5" md-select-on-focus></textarea>
                                   </md-input-container>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-12 text-right">
                                    <md-button class="md-raised btn-default pull-right" ng-click="cancelCourier();"> <md-icon>cancel</md-icon> Cancel</md-button>
                                    <md-button type="submit" class="md-raised  md-primary  pull-right" > <md-icon>note_add</md-icon>Save</md-button>
                                  </div>
                                </div>
                                <hr>
                            </form>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="white-box">
                                <div class="col-sm-12">
                                    <h3 class="box-title m-b-0">Courier List</h3>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="">Show
                                                <select ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);" class="form-control pagiantion">

                                                </select>
                                                entries
                                            </label>
                                        </div>

                                        <div class="col-md-8 ">
                                            <div class="form-inline form-group " style="float: right;">
                                                <label for="filter-list">Search </label>
                                                <input type="text" class="form-control" ng-model="filterlist" placeholder="Search">
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row text-center">
                                    <section  class="form-horizontal col-sm-12">
                                        <div class="row">
                                            <label class="col-sm-1 control-label">From</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" datepicker-popup="dd-MM-yyyy" ng-model="fromDate" is-open="expensepicker" show-button-bar="false" show-weeks="false" readonly>
                                                    <span class="input-group-btn">
                                                      <button type="button" class="btn btn-default" ng-click="expensepicker=true"><i class="fa fa-calendar"></i></button>
                                                    </span>
                                                </div>
                                            </div>
                                            <label class="col-sm-1 control-label">To</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" datepicker-popup="dd-MM-yyyy" ng-model="toDate" is-open="expensepickerto" show-button-bar="false" show-weeks="false" readonly>
                                                    <span class="input-group-btn">
                                                      <button type="button" class="btn btn-default" ng-click="expensepickerto=true"><i class="fa fa-calendar"></i></button>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                  <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                       <md-button ng-click="search(fromDate,toDate);" class="md-primary md-raised pull-left"> <md-icon>search</md-icon> Search</md-button>
                                                       <md-button ng-click="export(fromDate,toDate);"  class="md-accent md-raised"> <md-icon>speaker_notes</md-icon> Export</md-button>
                                                  </div>
                                            </div>
                                        </div>

                                    </section>
                                </div>


                                <table id="myTable" class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                    <thead>
                                        <tr>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">#</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Courier Name</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="2">Address(From)</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Contact(From)</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Address(To)</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Contact(To)</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Courier Amount</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">Service Charge</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="7">Date</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="8">Remark</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="9">Created_at</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="10">Updated_at</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="11">User</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="courier in listCount  = (couriers | filter:filterlist) | orderBy:'-created_at'  | pagination: currentPage : numPerPage">
                                            <td>[[numPerPage *(currentPage-1)+$index+1]]</td>
                                            <td class="title"><a ng-click="openCourier(courier,courier.user,$event);" href="javascript:void(0)">[[courier.name]]</a></td>
                                            <td>[[courier.fromaddress]]</td>
                                            <td>[[courier.frommobile]]</td>
                                            <td>[[courier.toaddress]]</td>
                                            <td>[[courier.tomobile]]</td>
                                            <td>[[courier.courieramount]]</td>
                                            <td>[[courier.servicecharge]]</td>
                                            <td>[[courier.date | date:'dd-MMMM-yyyy' ]]</td>
                                            <td>[[courier.remark]]</td>
                                            <td>[[courier.created_at]]</td>
                                            <td>[[courier.updated_at]]</td>
                                            <td>[[courier.user.name]]</td>
                                            <td>
                                                <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                     <md-button ng-if="user.permissions.courier.edit =='true'" class="md-primary md-small" ng-click="editCourier(courier);"> <md-icon>mode_edit</md-icon></md-button>
                                                     <md-button ng-if="user.permissions.courier.delete =='true'" class="md-warn md-small" ng-click="deleteCourier(courier,$event);"><md-icon>delete</md-icon></md-button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                  <div class="clearfix pull-left" ng-show="couriers.length > numPerPage">
                                          <pagination
                                           ng-model="currentPage"
                                           total-items="listCount.length"
                                           max-size="maxSize"
                                           items-per-page="numPerPage"
                                           boundary-links="true"
                                           class="pagination-sm pull-right"
                                           previous-text="&lsaquo;"
                                           next-text="&rsaquo;"
                                           first-text="&laquo;"
                                           last-text="&raquo;"
                                           ></pagination>
                                  </div>
                            </div>
                        </div>
                    </div>

                    <br/><br/>
             </div>
        </div>

    </div>

    <div ng-hide="permission">
        <div  layout="row" layout-padding layout-wrap layout-fill>
            <div md-whiteframe="3" class="padded alert">
                <strong>So...Sorry!</strong> You have no Permission to View This Page.
            </div>
        </div>
    </div>
  </md-content>


@section('scripts')

@stop

@stop
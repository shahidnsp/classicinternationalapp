

<md-dialog aria-label="Mango (Fruit)">
  <form ng-cloak>
    <md-toolbar>
      <div class="md-toolbar-tools">
        <h2>[[hospital.name]]</h2>
        <span flex></span>
        <md-button class="md-icon-button" ng-click="cancel()">
          <md-icon>close</md-icon>
        </md-button>
      </div>
    </md-toolbar>

    <md-dialog-content>
      <div class="md-dialog-content">
           <table class="table">
              <tr>
                <td>Residence Name:</td><td colspan="2">[[hospital.name]]</td>
              </tr>
              <tr>
                <td>Address:</td><td colspan="2">[[hospital.address]]</td>
              </tr>
              <tr>
                  <td>Phone 1:</td><td colspan="2">[[hospital.mobile1]]</td>
              </tr>
              <tr>
                  <td>Phone 2:</td><td colspan="2">[[hospital.mobile2]]</td>
              </tr>
              <tr>
                  <td>Phone 3:</td><td colspan="2">[[hospital.mobile3]]</td>
              </tr>
                <tr>
                  <td>Remark:</td><td colspan="2">[[hospital.remarks]]</td>
                </tr>
                <tr>
                    <td>Created At:[[hospital.created_at]]</td><td>Updated At:[[hospital.updated_at]]</td><td>Created By:[[hospital.user.name]]</td>
                </tr>
           </table>
      </div>
    </md-dialog-content>

    <md-dialog-actions layout="row">
      {{--<md-button href="http://en.wikipedia.org/wiki/Mango" target="_blank" md-autofocus>--}}
        {{--More on Wikipedia--}}
      {{--</md-button>--}}
      <span flex></span>
    {{--  <md-button ng-click="answer('not useful')">
       Not Useful
      </md-button>--}}
      <md-button ng-click="cancel()">
        Close
      </md-button>
    </md-dialog-actions>
  </form>
</md-dialog>


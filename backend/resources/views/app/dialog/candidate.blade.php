

<md-dialog aria-label="Mango (Fruit)">
  <form ng-cloak>
    <md-toolbar>
      <div class="md-toolbar-tools">
        <h2>[[candidate.name]]</h2>
        <span flex></span>
        <md-button class="md-icon-button" ng-click="cancel()">
          <md-icon>close</md-icon>
        </md-button>
      </div>
    </md-toolbar>

    <md-dialog-content>
      <div class="md-dialog-content">
           <table class="table">
                  <tr class="text-danger">
                    <td>Candidate Name</td><td colspan="2"><md-icon>person_pin</md-icon>[[candidate.name]]</td>
                  </tr>
                  <tr>
                    <td>Gender:</td><td colspan="2">[[candidate.gender]]</td>
                  </tr>
                  <tr>
                      <td>Mobile 1: <md-icon>phone</md-icon>[[candidate.mobile1]]</td>
                      <td>Mobile 2: <md-icon>phone</md-icon>[[candidate.mobile2]]</td>
                      <td>Mobile 3: <md-icon>phone</md-icon>[[candidate.mobile3]]</td>
                  </tr>
                  <tr>
                    <td>Wahtsapp: <md-icon>phone</md-icon> [[candidate.whatsapp]]</td><td colspan="2">Email: <md-icon>email</md-icon> [[candidate.email]]</td>
                  </tr>
                  <tr>
                    <td>Residence Area: </td><td colspan="2"><md-icon>location_on</md-icon> [[candidate.residence.name]]</td>
                  </tr>
                     <tr>
                      <td>Working Hospital: </td><td colspan="2"><a href="javascript:void(0)" ng-click="openHospital(candidate,$event)"><md-icon>location_on</md-icon> [[candidate.hospital.name]]</a></td>
                    </tr>
                     <tr>
                      <td>Duty Driver: </td><td colspan="2"><md-icon>person_pin</md-icon> [[candidate.employee.name]]</td>
                     </tr>
                    <tr>
                      <td>Description:</td><td colspan="2"> [[candidate.remark]]</td>
                    </tr>
                    <tr>
                        <td>Created At:</td><td> [[candidate.created_at]]</td>
                    </tr>
                    <tr>
                        <td>Updated At:</td><td> [[candidate.updated_at]]</td>
                    </tr>
                    <tr>
                        <td>Created By:</td><td colspan="2">[[candidate.user.name]]</td>
                    </tr>
           </table>
      </div>
    </md-dialog-content>

    <md-dialog-actions layout="row">
      {{--<md-button href="http://en.wikipedia.org/wiki/Mango" target="_blank" md-autofocus>--}}
        {{--More on Wikipedia--}}
      {{--</md-button>--}}
      <span flex></span>
    {{--  <md-button ng-click="answer('not useful')">
       Not Useful
      </md-button>--}}
      <md-button ng-click="cancel()">
        Close
      </md-button>
    </md-dialog-actions>
  </form>
</md-dialog>



<md-dialog aria-label="Mango (Fruit)">
  <form ng-cloak>
    <md-toolbar>
      <div class="md-toolbar-tools">
        <h2>[[batch.name]]</h2>
        <span flex></span>
        <md-button class="md-icon-button" ng-click="cancel()">
          <md-icon>close</md-icon>
        </md-button>
      </div>
    </md-toolbar>

    <md-dialog-content>
      <div class="md-dialog-content">
           <table class="table">
              <tr>
                <td>Batch Name:</td><td colspan="2">[[batch.name]]</td>
              </tr>
                <tr>
                  <td>Remark:</td><td colspan="2">[[batch.description]]</td>
                </tr>
                <tr>
                    <td>Created At:[[batch.created_at]]</td><td>Updated At:[[batch.updated_at]]</td><td>Created By:[[batch.user.name]]</td>
                </tr>
           </table>
      </div>
    </md-dialog-content>

    <md-dialog-actions layout="row">
      <span flex></span>
      <md-button ng-click="cancel()">
        Close
      </md-button>
    </md-dialog-actions>
  </form>
</md-dialog>


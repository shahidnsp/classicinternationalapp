

<md-dialog aria-label="Mango (Fruit)">
  <form ng-cloak>
    <md-toolbar>
      <div class="md-toolbar-tools">
        <h2>[[account.name]]</h2>
        <span flex></span>
        <md-button class="md-icon-button" ng-click="cancel()">
          <md-icon>close</md-icon>
        </md-button>
      </div>
    </md-toolbar>

    <md-dialog-content>
      <div class="md-dialog-content">
           <table class="table">
              <tr>
                <td>Account Name:</td><td colspan="2">[[account.name]]</td>
              </tr>
              <tr>
                <td>Account Type:</td><td colspan="2">[[account.type]]</td>
              </tr>
              <tr>
                  <td>Group:</td><td colspan="2">[[account.group]]</td>
              </tr>
                <tr>
                  <td>Description:</td><td colspan="2">[[account.description]]</td>
                </tr>
                <tr>
                    <td>Created At:[[account.created_at]]</td><td>Updated At:[[account.updated_at]]</td><td>Created By:[[account.user.name]]</td>
                </tr>
           </table>
      </div>
    </md-dialog-content>

    <md-dialog-actions layout="row">
      {{--<md-button href="http://en.wikipedia.org/wiki/Mango" target="_blank" md-autofocus>--}}
        {{--More on Wikipedia--}}
      {{--</md-button>--}}
      <span flex></span>
    {{--  <md-button ng-click="answer('not useful')">
       Not Useful
      </md-button>--}}
      <md-button ng-click="cancel()">
        Close
      </md-button>
    </md-dialog-actions>
  </form>
</md-dialog>


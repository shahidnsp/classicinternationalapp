

<md-dialog aria-label="Mango (Fruit)">
  <form ng-cloak>
    <md-toolbar>
      <div class="md-toolbar-tools">
        <h2>[[rentchart.officename]]</h2>
        <span flex></span>
        <md-button class="md-icon-button" ng-click="cancel()">
          <md-icon>close</md-icon>
        </md-button>
      </div>
    </md-toolbar>

    <md-dialog-content>
      <div class="md-dialog-content">
            <div class="row">
                <table class="table">
                      <tr>
                        <td colspan="2" class="text-center text-primary">Building: [[rentchart.officename]]</td>
                      </tr>
                      <tr>
                        <td class="text-center">Basic Pay: [[rentchart.buildingrentamount]]</td>
                      </tr>
               </table>
            </div>
            <div class="row">
                <table class="table">
                    <tr>
                        <th class="text-danger">Jan</th>
                        <th class="text-danger">Feb</th>
                        <th class="text-danger">Mar</th>
                        <th class="text-danger">Apr</th>
                        <th class="text-danger">May</th>
                        <th class="text-danger">Jun</th>
                    </tr>
                    <tr>
                        <td ng-class="(rentchart.jan==='NPD')  ?   'text-warning'  :   'text-primary'">[[rentchart.jan]]</td>
                        <td ng-class="(rentchart.feb==='NPD')  ?   'text-warning'  :   'text-primary'">[[rentchart.feb]]</td>
                        <td ng-class="(rentchart.mar==='NPD')  ?   'text-warning'  :   'text-primary'">[[rentchart.mar]]</td>
                        <td ng-class="(rentchart.apr==='NPD')  ?   'text-warning'  :   'text-primary'">[[rentchart.apr]]</td>
                        <td ng-class="(rentchart.may==='NPD')  ?   'text-warning'  :   'text-primary'">[[rentchart.may]]</td>
                        <td ng-class="(rentchart.jun==='NPD')  ?   'text-warning'  :   'text-primary'">[[rentchart.jun]]</td>
                    </tr>
                </table>
            </div>
            <div class="row">
                <table class="table">
                    <tr>
                        <th class="text-danger">Jul</th>
                        <th class="text-danger">Aug</th>
                        <th class="text-danger">Sep</th>
                        <th class="text-danger">Oct</th>
                        <th class="text-danger">Nov</th>
                        <th class="text-danger">Dec</th>
                    </tr>
                    <tr>
                        <td ng-class="(rentchart.jul==='NPD')  ?   'text-warning' : 'text-primary'">[[rentchart.jul]]</td>
                        <td ng-class="(rentchart.aug==='NPD')  ?   'text-warning' : 'text-primary'">[[rentchart.aug]]</td>
                        <td ng-class="(rentchart.sep==='NPD')  ?   'text-warning' : 'text-primary'">[[rentchart.sep]]</td>
                        <td ng-class="(rentchart.oct==='NPD')  ?   'text-warning' : 'text-primary'">[[rentchart.oct]]</td>
                        <td ng-class="(rentchart.nov==='NPD')  ?   'text-warning' : 'text-primary'">[[rentchart.nov]]</td>
                        <td ng-class="(rentchart.dec==='NPD')  ?   'text-warning' : 'text-primary'">[[rentchart.dec]]</td>
                    </tr>
                </table>
            </div>

      </div>
    </md-dialog-content>

    <md-dialog-actions layout="row">
      <span flex></span>
      <md-button ng-click="cancel()">
        Close
      </md-button>
    </md-dialog-actions>
  </form>
</md-dialog>


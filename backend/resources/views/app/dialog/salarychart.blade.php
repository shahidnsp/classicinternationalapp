

<md-dialog aria-label="Mango (Fruit)">
  <form ng-cloak>
    <md-toolbar>
      <div class="md-toolbar-tools">
        <h2>[[salarychart.name]]</h2>
        <span flex></span>
        <md-button class="md-icon-button" ng-click="cancel()">
          <md-icon>close</md-icon>
        </md-button>
      </div>
    </md-toolbar>

    <md-dialog-content>
      <div class="md-dialog-content">
            <div class="row">
                <table class="table">
                      <tr>
                        <td colspan="2" class="text-center"><img src="[[basepath]]/images/[[salarychart.photo]]" style="width: 100px;height: 100px;" alt=""/></td>
                      </tr>
                      <tr>
                        <td colspan="2" class="text-center text-primary">Employee: [[salarychart.name]]</td>
                      </tr>
                      <tr>
                        <td class="text-center">Basic Pay: [[salarychart.basicpay]]</td>
                      </tr>
               </table>
            </div>
            <div class="row">
                <table class="table">
                    <tr>
                        <th class="text-danger">Jan</th>
                        <th class="text-danger">Feb</th>
                        <th class="text-danger">Mar</th>
                        <th class="text-danger">Apr</th>
                        <th class="text-danger">May</th>
                        <th class="text-danger">Jun</th>
                    </tr>
                    <tr>
                        <td ng-class="(salarychart.jan==='NPD')  ?   'text-warning'  :   'text-primary'">[[salarychart.jan]]</td>
                        <td ng-class="(salarychart.feb==='NPD')  ?   'text-warning'  :   'text-primary'">[[salarychart.feb]]</td>
                        <td ng-class="(salarychart.mar==='NPD')  ?   'text-warning'  :   'text-primary'">[[salarychart.mar]]</td>
                        <td ng-class="(salarychart.apr==='NPD')  ?   'text-warning'  :   'text-primary'">[[salarychart.apr]]</td>
                        <td ng-class="(salarychart.may==='NPD')  ?   'text-warning'  :   'text-primary'">[[salarychart.may]]</td>
                        <td ng-class="(salarychart.jun==='NPD')  ?   'text-warning'  :   'text-primary'">[[salarychart.jun]]</td>
                    </tr>
                </table>
            </div>
            <div class="row">
                <table class="table">
                    <tr>
                        <th class="text-danger">Jul</th>
                        <th class="text-danger">Aug</th>
                        <th class="text-danger">Sep</th>
                        <th class="text-danger">Oct</th>
                        <th class="text-danger">Nov</th>
                        <th class="text-danger">Dec</th>
                    </tr>
                    <tr>
                        <td ng-class="(salarychart.jul==='NPD')  ?   'text-warning' : 'text-primary'">[[salarychart.jul]]</td>
                        <td ng-class="(salarychart.aug==='NPD')  ?   'text-warning' : 'text-primary'">[[salarychart.aug]]</td>
                        <td ng-class="(salarychart.sep==='NPD')  ?   'text-warning' : 'text-primary'">[[salarychart.sep]]</td>
                        <td ng-class="(salarychart.oct==='NPD')  ?   'text-warning' : 'text-primary'">[[salarychart.oct]]</td>
                        <td ng-class="(salarychart.nov==='NPD')  ?   'text-warning' : 'text-primary'">[[salarychart.nov]]</td>
                        <td ng-class="(salarychart.dec==='NPD')  ?   'text-warning' : 'text-primary'">[[salarychart.dec]]</td>
                    </tr>
                </table>
            </div>

      </div>
    </md-dialog-content>

    <md-dialog-actions layout="row">
      {{--<md-button href="http://en.wikipedia.org/wiki/Mango" target="_blank" md-autofocus>--}}
        {{--More on Wikipedia--}}
      {{--</md-button>--}}
      <span flex></span>
    {{--  <md-button ng-click="answer('not useful')">
       Not Useful
      </md-button>--}}
      <md-button ng-click="cancel()">
        Close
      </md-button>
    </md-dialog-actions>
  </form>
</md-dialog>


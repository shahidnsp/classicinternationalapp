

<md-dialog aria-label="Mango (Fruit)">
  <form ng-cloak>
    <md-toolbar>
      <div class="md-toolbar-tools">
        <h2>[[courier.name]]</h2>
        <span flex></span>
        <md-button class="md-icon-button" ng-click="cancel()">
          <md-icon>close</md-icon>
        </md-button>
      </div>
    </md-toolbar>

    <md-dialog-content>
      <div class="md-dialog-content">
           <table class="table">
                  <tr class="text-danger">
                    <td>Courier Name</td><td colspan="2"><md-icon>person_pin</md-icon>[[courier.name]]</td>
                  </tr>
                  <tr>
                      <td>Courier Date: </td><td colspan="2"><md-icon>date</md-icon>[[courier.date | date:'dd-MM-yyyy']]</td>
                   </tr>
                  <tr>
                    <td>Courier From: </td><td colspan="2"><md-icon>location_on</md-icon> [[courier.fromaddress]]<br/><br/><md-icon>phone</md-icon> [[courier.frommobile]]</td>
                  </tr>
                     <tr>
                      <td>Courier To: </td><td colspan="2"><md-icon>location_on</md-icon> [[courier.toaddress]]<br/><br/><md-icon>phone</md-icon> [[courier.tomobile]]</td>
                    </tr>
                     <tr>
                      <td>Courier Amount: </td><td colspan="2"><md-icon>trending_up</md-icon> [[courier.courieramount]]</td>
                     </tr>
                     <tr>
                       <td>Service Charge: </td><td colspan="2"><md-icon>trending_up</md-icon> [[courier.servicecharge]]</td>
                     </tr>
                    <tr>
                      <td>Description:</td><td colspan="2"> [[courier.remark]]</td>
                    </tr>
                    <tr>
                        <td>Created At:</td><td> [[courier.created_at]]</td>
                    </tr>
                    <tr>
                        <td>Updated At:</td><td> [[courier.updated_at]]</td>
                    </tr>
                    <tr>
                        <td>Created By:</td><td colspan="2">[[user.name]]</td>
                    </tr>
           </table>
      </div>
    </md-dialog-content>

    <md-dialog-actions layout="row">
      {{--<md-button href="http://en.wikipedia.org/wiki/Mango" target="_blank" md-autofocus>--}}
        {{--More on Wikipedia--}}
      {{--</md-button>--}}
      <span flex></span>
    {{--  <md-button ng-click="answer('not useful')">
       Not Useful
      </md-button>--}}
      <md-button ng-click="cancel()">
        Close
      </md-button>
    </md-dialog-actions>
  </form>
</md-dialog>




<md-dialog aria-label="Mango (Fruit)">
  <form ng-cloak>
    <md-toolbar>
      <div class="md-toolbar-tools">
        <h2>[[rent.officename]]</h2>
        <span flex></span>
        <md-button class="md-icon-button" ng-click="cancel()">
          <md-icon>close</md-icon>
        </md-button>
      </div>
    </md-toolbar>

    <md-dialog-content>
      <div class="md-dialog-content">
           <table class="table">
              <tr>
                <td>Office Name:</td><td colspan="2">[[rent.officename]]</td>
              </tr>
              <tr>
                <td>Area:</td><td colspan="2">[[rent.area]]</td>
              </tr>
              <tr>
                  <td>Rent Amount:</td><td colspan="2">[[rent.buildingrentamount]]</td>
              </tr>
              <tr>
                  <td>Rentee Name:</td><td colspan="2">[[rent.buildingrentername]]</td>
              </tr>
              <tr>
                  <td>Rentee Mobile:</td><td colspan="2">[[rent.buildingrentermobile]]</td>
              </tr>
                <tr>
                  <td>Remark:</td><td colspan="2">[[rent.remark]]</td>
                </tr>
                <tr>
                    <td>Created At:[[rent.created_at]]</td><td>Updated At:[[rent.updated_at]]</td><td>Created By:[[rent.user.name]]</td>
                </tr>
           </table>
      </div>
    </md-dialog-content>

    <md-dialog-actions layout="row">
      {{--<md-button href="http://en.wikipedia.org/wiki/Mango" target="_blank" md-autofocus>--}}
        {{--More on Wikipedia--}}
      {{--</md-button>--}}
      <span flex></span>
    {{--  <md-button ng-click="answer('not useful')">
       Not Useful
      </md-button>--}}
      <md-button ng-click="cancel()">
        Close
      </md-button>
    </md-dialog-actions>
  </form>
</md-dialog>




<md-dialog aria-label="Mango (Fruit)">
  <form ng-cloak>
    <md-toolbar>
      <div class="md-toolbar-tools">
        <h2>[[employee.name]]</h2>
        <span flex></span>
        <md-button class="md-icon-button" ng-click="cancel()">
          <md-icon>close</md-icon>
        </md-button>
      </div>
    </md-toolbar>

    <md-dialog-content>
      <div class="md-dialog-content">
           <table class="table">
                  <tr class="text-danger">
                    <td><img src="[[basepath]]/images/[[employee.photo]]" style="width: 100px;height: 100px;" alt=""/></td><td colspan="2" style="font-size: 27px;"><br/><md-icon>person_pin</md-icon>[[employee.name]]</td>
                  </tr>
                  <tr>
                    <td>Designation:</td><td colspan="2">[[employee.designation.name]]</td>
                  </tr>
                  <tr>
                      <td>Mobile: <md-icon>phone</md-icon>[[employee.mobile]]</td>
                      <td>Whatsapp: <md-icon>phone</md-icon>[[employee.whatsapp]]</td>
                  </tr>
                  <tr>
                    <td>Email:</td><td colspan="2"> <md-icon>email</md-icon> [[employee.email]]</td>
                  </tr>
                  <tr>
                    <td>Address (Kuwait)</td><td colspan="2"><md-icon>location_on</md-icon> [[employee.addresskuwait]]</td>
                  </tr>
                     <tr>
                      <td>Address (India)</td><td colspan="2"><md-icon>location_on</md-icon> [[employee.addressindia]]<br/><md-icon>phone</md-icon>[[employee.mobileindia]]</td>
                    </tr>
                     <tr>
                      <td>Passport No:</td><td colspan="2"><md-icon>assignment</md-icon> [[employee.passportno]]</td>
                     </tr>
                     <tr>
                       <td>Visa No:</td><td colspan="2"><md-icon>assignment</md-icon> [[employee.visano]]</td>
                      </tr>
                    <tr>
                     <td>Driving Lic No:</td><td colspan="2"><md-icon>assignment</md-icon> [[employee.drivinglicenseno]]</td>
                    </tr>
                    <tr class="text-success">
                     <td>Basic Pay:</td><td colspan="2"><md-icon>card_giftcard</md-icon> [[employee.basicpay]]</td>
                    </tr>
                    <tr class="text-success">
                       <td>Overtime Pay:</td><td colspan="2"><md-icon>card_giftcard</md-icon> [[employee.overtimepay]]</td>
                    </tr>
                    <tr>
                      <td>Description:</td><td colspan="2"> [[employee.remark]]</td>
                    </tr>
                    <tr>
                        <td>Created At:</td><td> [[employee.created_at]]</td>
                    </tr>
                    <tr>
                        <td>Updated At:</td><td> [[employee.updated_at]]</td>
                    </tr>
                    <tr>
                        <td>Created By:</td><td colspan="2">[[employee.user.name]]</td>
                    </tr>
           </table>
      </div>
    </md-dialog-content>

    <md-dialog-actions layout="row">
      {{--<md-button href="http://en.wikipedia.org/wiki/Mango" target="_blank" md-autofocus>--}}
        {{--More on Wikipedia--}}
      {{--</md-button>--}}
      <span flex></span>
    {{--  <md-button ng-click="answer('not useful')">
       Not Useful
      </md-button>--}}
      <md-button ng-click="cancel()">
        Close
      </md-button>
    </md-dialog-actions>
  </form>
</md-dialog>


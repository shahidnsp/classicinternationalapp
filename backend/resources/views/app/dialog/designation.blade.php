

<md-dialog aria-label="Mango (Fruit)">
  <form ng-cloak>
    <md-toolbar>
      <div class="md-toolbar-tools">
        <h2>[[designation.name]]</h2>
        <span flex></span>
        <md-button class="md-icon-button" ng-click="cancel()">
          <md-icon>close</md-icon>
        </md-button>
      </div>
    </md-toolbar>

    <md-dialog-content>
      <div class="md-dialog-content">
           <table class="table">
              <tr>
                <td>Designation Name:</td><td colspan="2">[[designation.name]]</td>
              </tr>
                <tr>
                  <td>Description:</td><td colspan="2">[[designation.description]]</td>
                </tr>
                <tr>
                    <td>Created At:[[designation.created_at]]</td><td>Updated At:[[designation.updated_at]]</td><td>Created By:[[designation.user.name]]</td>
                </tr>
           </table>
      </div>
    </md-dialog-content>

    <md-dialog-actions layout="row">
      <span flex></span>
      <md-button ng-click="cancel()">
        Close
      </md-button>
    </md-dialog-actions>
  </form>
</md-dialog>




<md-dialog aria-label="Mango (Fruit)">
  <form ng-cloak>
    <md-toolbar>
      <div class="md-toolbar-tools">
        <h2>[[nurse.name]]</h2>
        <span flex></span>
        <md-button class="md-icon-button" ng-click="cancel()">
          <md-icon>close</md-icon>
        </md-button>
      </div>
    </md-toolbar>

    <md-dialog-content>
      <div class="md-dialog-content">
           <table class="table">
                  <tr class="text-danger">
                    <td>Nurse Name</td><td colspan="2"><md-icon>person_pin</md-icon>[[nurse.name]]</td>
                  </tr>
                  <tr>
                    <td>Passport No:</td><td colspan="2">[[nurse.passportno]]</td>
                  </tr>
                  <tr>
                      <td>Mobile 1: <md-icon>phone</md-icon>[[nurse.mobileno1]]</td>
                      <td>Mobile 2: <md-icon>phone</md-icon>[[nurse.mobileno2]]</td>
                  </tr>
                  <tr>
                    <td colspan="2">Email: <md-icon>email</md-icon> [[nurse.email]]</td>
                  </tr>
                  <tr>
                    <td>Hospital: </td><td colspan="2"><md-icon>location_on</md-icon> [[nurse.hospital.name]]</td>
                  </tr>
                     <tr>
                      <td>Batch: </td><td colspan="2"><md-icon>location_on</md-icon> [[nurse.batch.name]]</td>
                    </tr>
                    <tr>
                      <td>Description:</td><td colspan="2"> [[nurse.remark]]</td>
                    </tr>
                    <tr>
                        <td>Created At:</td><td> [[nurse.created_at]]</td>
                    </tr>
                    <tr>
                        <td>Updated At:</td><td> [[nurse.updated_at]]</td>
                    </tr>
                    <tr>
                        <td>Created By:</td><td colspan="2">[[nurse.user.name]]</td>
                    </tr>
           </table>
      </div>
    </md-dialog-content>

    <md-dialog-actions layout="row">
      {{--<md-button href="http://en.wikipedia.org/wiki/Mango" target="_blank" md-autofocus>--}}
        {{--More on Wikipedia--}}
      {{--</md-button>--}}
      <span flex></span>
    {{--  <md-button ng-click="answer('not useful')">
       Not Useful
      </md-button>--}}
      <md-button ng-click="cancel()">
        Close
      </md-button>
    </md-dialog-actions>
  </form>
</md-dialog>


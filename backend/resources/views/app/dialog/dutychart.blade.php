

<md-dialog aria-label="Mango (Fruit)">
  <form ng-cloak>
    <md-toolbar>
      <div class="md-toolbar-tools">
        <h2>[[dutychart.name]]</h2>
        <span flex></span>
        <md-button class="md-icon-button" ng-click="cancel()">
          <md-icon>close</md-icon>
        </md-button>
      </div>
    </md-toolbar>

    <md-dialog-content>
      <div class="md-dialog-content">
            <div class="row">
                <table class="table">
                      <tr>
                        <td colspan="2" class="text-center text-primary">Candidate: [[dutychart.name]]</td>
                      </tr>
                      <tr>
                        <td>Mobile: [[dutychart.mobile1]]</td><td>Residence: [[residence.name]]</td>
                      </tr>
                      <tr>
                         <td>Hospital: [[hospital.name]]</td><td>Duty Driver: [[driver.name]]</td>
                      </tr>
               </table>
            </div>
            <div class="row">
                <table class="table">
                    <tr>
                        <th class="text-danger">Jan</th>
                        <th class="text-danger">Feb</th>
                        <th class="text-danger">Mar</th>
                        <th class="text-danger">Apr</th>
                        <th class="text-danger">May</th>
                        <th class="text-danger">Jun</th>
                    </tr>
                    <tr>
                        <td ng-class="(dutychart.jan==='NPD')  ?   'text-warning'  :   'text-primary'">[[dutychart.jan]]</td>
                        <td ng-class="(dutychart.feb==='NPD')  ?   'text-warning'  :   'text-primary'">[[dutychart.feb]]</td>
                        <td ng-class="(dutychart.mar==='NPD')  ?   'text-warning'  :   'text-primary'">[[dutychart.mar]]</td>
                        <td ng-class="(dutychart.apr==='NPD')  ?   'text-warning'  :   'text-primary'">[[dutychart.apr]]</td>
                        <td ng-class="(dutychart.may==='NPD')  ?   'text-warning'  :   'text-primary'">[[dutychart.may]]</td>
                        <td ng-class="(dutychart.jun==='NPD')  ?   'text-warning'  :   'text-primary'">[[dutychart.jun]]</td>
                    </tr>
                </table>
            </div>
            <div class="row">
                <table class="table">
                    <tr>
                        <th class="text-danger">Jul</th>
                        <th class="text-danger">Aug</th>
                        <th class="text-danger">Sep</th>
                        <th class="text-danger">Oct</th>
                        <th class="text-danger">Nov</th>
                        <th class="text-danger">Dec</th>
                    </tr>
                    <tr>
                        <td ng-class="(dutychart.jul==='NPD')  ?   'text-warning' : 'text-primary'">[[dutychart.jul]]</td>
                        <td ng-class="(dutychart.aug==='NPD')  ?   'text-warning' : 'text-primary'">[[dutychart.aug]]</td>
                        <td ng-class="(dutychart.sep==='NPD')  ?   'text-warning' : 'text-primary'">[[dutychart.sep]]</td>
                        <td ng-class="(dutychart.oct==='NPD')  ?   'text-warning' : 'text-primary'">[[dutychart.oct]]</td>
                        <td ng-class="(dutychart.nov==='NPD')  ?   'text-warning' : 'text-primary'">[[dutychart.nov]]</td>
                        <td ng-class="(dutychart.dec==='NPD')  ?   'text-warning' : 'text-primary'">[[dutychart.dec]]</td>
                    </tr>
                </table>
            </div>

      </div>
    </md-dialog-content>

    <md-dialog-actions layout="row">
      {{--<md-button href="http://en.wikipedia.org/wiki/Mango" target="_blank" md-autofocus>--}}
        {{--More on Wikipedia--}}
      {{--</md-button>--}}
      <span flex></span>
    {{--  <md-button ng-click="answer('not useful')">
       Not Useful
      </md-button>--}}
      <md-button ng-click="cancel()">
        Close
      </md-button>
    </md-dialog-actions>
  </form>
</md-dialog>


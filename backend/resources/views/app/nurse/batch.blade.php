@extends('layout.main')
@include('app.partial.message')
@section('content')
  <!--
        ...................NURSE ACCOUNT......................................
  -->

       <div class="row bg-title">
          <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h4 class="page-title">Nurse-Batch</h4> </div>
          <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
              <ol class="breadcrumb">
                  <li><a href="/">Accounts</a></li>
                  <li class="active">Batch</li>
              </ol>
          </div>
          <!-- /.col-lg-12 -->
      </div>

      <md-content id="popupContainer" ng-controller="BatchController" ng-cloak>
        <div ng-show="permission">
            <div class="row">
                 <div class="col-md-12">
                        <div class="box">
                            <div class="row">
                                <div class="col-md-12">
                                <div class="row pull-right">
                                    <md-button ng-if="user.permissions.batch.write =='true'" ng-hide="batchedit" class="md-raised md-small md-primary" ng-click="newBatch();"> <md-icon>note_add</md-icon> Add Batch</md-button>
                                </div>
                                <br/>
                                <form ng-show="batchedit" class="form-horizontal"  ng-submit="addBatch();">
                                    <h3>New Batch</h3><br>
                                    <div layout-gt-xs="row">
                                      <md-input-container class="md-block" flex-gt-xs>
                                        <label class="control-label1">Batch Name</label>
                                        <input ng-model="newbatch.name" required="">
                                      </md-input-container>
                                    </div>

                                    <div layout-gt-xs="row">
                                       <md-input-container class="md-block" flex-gt-xs>
                                         <label>Description</label>
                                         <textarea ng-model="newbatch.description" md-maxlength="300" rows="5" md-select-on-focus></textarea>
                                       </md-input-container>
                                    </div>
                                    <div class="form-group">
                                      <div class="col-sm-12 text-right">
                                        <md-button class="md-raised btn-default pull-right" ng-click="cancelBatch();"> <md-icon>cancel</md-icon> Cancel</md-button>
                                        <md-button type="submit" class="md-raised  md-primary  pull-right" > <md-icon>note_add</md-icon>Save</md-button>
                                      </div>
                                    </div>
                                    <hr>
                                </form>
                                </div>
                            </div>
                        </div>

                        <div class="box">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="white-box">
                                        <div class="col-sm-12">
                                            <h3 class="box-title m-b-0">Batch List</h3>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="">Show
                                                        <select ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);" class="form-control pagiantion">
                                                        </select>
                                                        entries
                                                    </label>
                                                </div>

                                                <div class="col-md-8 ">
                                                    <div class="form-inline form-group " style="float: right;">
                                                        <label for="filter-list">Search </label>
                                                        <input type="text" class="form-control" ng-model="filterlist" placeholder="Search">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                        <table id="myTable" class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                            <thead>
                                                <tr>
                                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">#</th>
                                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Batch</th>
                                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="2">Description</th>
                                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Created_at</th>
                                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Updated_at</th>
                                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">User</th>
                                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="batch in listCount  = (batches | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                                                    <td>[[numPerPage *(currentPage-1)+$index+1]]</td>
                                                    <td class="title"><a ng-click="open(batch,$event);" href="javascript:void(0)">[[batch.name]]</a></td>
                                                    <td>[[batch.description]]</td>
                                                    <td>[[batch.created_at]]</td>
                                                    <td>[[batch.updated_at]]</td>
                                                    <td>[[batch.user.name]]</td>
                                                    <td>
                                                        <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                             <md-button ng-if="user.permissions.batch.edit =='true'" class="md-primary md-small" ng-click="editBatch(batch);"> <md-icon>mode_edit</md-icon></md-button>
                                                             <md-button ng-show="batch.active=='1'" class="md-primary md-small" ng-click="changeStaus(batch.id,$event);">Active</md-button>
                                                             <md-button ng-show="batch.active=='0'" class="md-danger md-small" ng-click="changeStaus(batch.id,$event);">In Active</md-button>
                                                             <md-button ng-if="user.permissions.batch.delete =='true'" class="md-warn md-small" ng-click="deleteBatch(batch,$event);"><md-icon>delete</md-icon></md-button>
                                                        </div>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                         <div class="clearfix pull-left" ng-show="batches.length > numPerPage">
                                             <pagination
                                                 ng-model="currentPage"
                                                 total-items="listCount.length"
                                                 max-size="maxSize"
                                                 items-per-page="numPerPage"
                                                 boundary-links="true"
                                                 class="pagination-sm pull-right"
                                                 previous-text="&lsaquo;"
                                                 next-text="&rsaquo;"
                                                 first-text="&laquo;"
                                                 last-text="&raquo;"
                                                 ></pagination>
                                         </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br/><br/>
                 </div>
            </div>
            </div>

            <div ng-hide="permission">
                <div  layout="row" layout-padding layout-wrap layout-fill>
                    <div md-whiteframe="3" class="padded alert">
                        <strong>So...Sorry!</strong> You have no Permission to View This Page.
                    </div>
                </div>
            </div>
      </md-content>


@section('scripts')

@stop

@stop
@extends('layout.main')
@include('app.partial.message')
@section('content')
  <!--
        ...................NURSE ACCOUNT......................................
  -->
   <div class="row bg-title">
              <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <h4 class="page-title">Nurse-Hospital</h4> </div>
              <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
                  <ol class="breadcrumb">
                      <li><a href="/">Accounts</a></li>
                      <li class="active">Hospital</li>
                  </ol>
              </div>
              <!-- /.col-lg-12 -->
          </div>

  <md-content id="popupContainer" ng-controller="HospitalController" ng-cloak>
        <div ng-show="permission">
        <div class="row">
             <div class="col-md-12">
                    <div class="box">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="row pull-right">
                                <md-button ng-if="user.permissions.nursehospital.write =='true' || user.permissions.transportationhospital.write =='true'" ng-hide="hospitaledit" class="md-raised md-small md-primary" ng-click="newHospital();"> <md-icon>note_add</md-icon> Add Hospital</md-button>
                            </div>
                            <br/>
                            <form ng-show="hospitaledit" class="form-horizontal"  ng-submit="addHospital();">
                                <h3>New Hospital</h3><br>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Hospital Name</label>
                                    <input ng-model="newhospital.name" required="">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label>Address</label>
                                     <textarea ng-model="newhospital.address" md-maxlength="350" rows="5" md-select-on-focus required=""></textarea>
                                   </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Phone Number 1</label>
                                    <input ng-model="newhospital.mobile1">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Phone Number 2</label>
                                    <input ng-model="newhospital.mobile2">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Phone Number 3</label>
                                    <input ng-model="newhospital.mobile3">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label>Remark</label>
                                     <textarea ng-model="newhospital.remarks" md-maxlength="350" rows="5" md-select-on-focus></textarea>
                                   </md-input-container>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-12 text-right">
                                    <md-button class="md-raised btn-default pull-right" ng-click="cancelHospital();"> <md-icon>cancel</md-icon> Cancel</md-button>
                                    <md-button type="submit" class="md-raised  md-primary  pull-right" > <md-icon>note_add</md-icon>Save</md-button>
                                  </div>
                                </div>
                                <hr>
                            </form>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="white-box">
                                    <div class="col-sm-12">
                                        <h3 class="box-title m-b-0">Hospital List</h3>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Show
                                                    <select ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);" class="form-control pagiantion">
                                                    </select>
                                                    entries
                                                </label>
                                            </div>

                                            <div class="col-md-8 ">
                                                <div class="form-inline form-group " style="float: right;">
                                                    <label for="filter-list">Search </label>
                                                    <input type="text" class="form-control" ng-model="filterlist" placeholder="Search">
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <table id="myTable" class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                        <thead>
                                            <tr>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">#</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Hospital</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="2">Phone 1</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Phone 2</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Phone 3</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Remarks</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Created_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">Updated_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="7">User</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="hospital in listCount  = (hospitals | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                                                <td>[[numPerPage *(currentPage-1)+$index+1]]</td>
                                                <td class="title"><a ng-click="openHospital(hospital,$event);" href="javascript:void(0)">[[hospital.name]]</a></td>
                                                <td>[[hospital.mobile1]]</td>
                                                <td>[[hospital.mobile2]]</td>
                                                <td>[[hospital.mobile3]]</td>
                                                <td>[[hospital.remarks]]</td>
                                                <td>[[hospital.created_at]]</td>
                                                <td>[[hospital.updated_at]]</td>
                                                <td>[[hospital.user.name]]</td>
                                                <td>
                                                    <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                         <md-button ng-if="user.permissions.nursehospital.edit =='true' || user.permissions.transportationhospital.edit =='true'" class="md-primary md-small" ng-click="editHospital(hospital);"> <md-icon>mode_edit</md-icon></md-button>
                                                         <md-button ng-show="hospital.active=='1'" class="md-primary md-small" ng-click="changeStaus(hospital.id,$event);">Active</md-button>
                                                         <md-button ng-show="hospital.active=='0'" class="md-danger md-small" ng-click="changeStaus(hospital.id,$event);">In Active</md-button>
                                                         <md-button ng-if="user.permissions.nursehospital.delete =='true' || user.permissions.transportationhospital.delete =='true'" class="md-warn md-small" ng-click="deleteHospital(hospital,$event);"><md-icon>delete</md-icon></md-button>
                                                    </div>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                     <div class="clearfix pull-left" ng-show="hospitals.length > numPerPage">
                                          <pagination
                                              ng-model="currentPage"
                                              total-items="listCount.length"
                                              max-size="maxSize"
                                              items-per-page="numPerPage"
                                              boundary-links="true"
                                              class="pagination-sm pull-right"
                                              previous-text="&lsaquo;"
                                              next-text="&rsaquo;"
                                              first-text="&laquo;"
                                              last-text="&raquo;"
                                              ></pagination>
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br/><br/>
             </div>
        </div>
        </div>
        <div ng-hide="permission">
            <div  layout="row" layout-padding layout-wrap layout-fill>
                <div md-whiteframe="3" class="padded alert">
                    <strong>So...Sorry!</strong> You have no Permission to View This Page.
                </div>
            </div>
        </div>
  </md-content>


@section('scripts')

@stop

@stop
@extends('layout.main')
@include('app.partial.message')
@section('content')
  <!--
        ...................NURSE ACCOUNT......................................
  -->
   <div class="row bg-title">
              <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <h4 class="page-title">Nurse-Nurse Register</h4> </div>
              <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
                  <ol class="breadcrumb">
                      <li><a href="/">Accounts</a></li>
                      <li class="active">Nurse Register</li>
                  </ol>
              </div>
              <!-- /.col-lg-12 -->
          </div>

  <md-content id="popupContainer" ng-controller="NurseController" ng-cloak>
        <div ng-show="permission">
        <div class="row">
             <div class="col-md-12">
                    <div class="box">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="row pull-right">
                                <md-button ng-if="user.permissions.nurse.write =='true'" ng-hide="nurseedit" class="md-raised md-small md-primary" ng-click="newNurse();"> <md-icon>note_add</md-icon> Add Nurse</md-button>
                            </div>
                            <br/>
                            <form ng-show="nurseedit" class="form-horizontal"  ng-submit="addNurse();">
                                <h3>New Nurse</h3><br>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Nurse Name</label>
                                    <input ng-model="newnurse.name" required="">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Passport Number</label>
                                    <input ng-model="newnurse.passportno" required="">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Phone Number 1</label>
                                    <input ng-model="newnurse.mobileno1">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Phone Number 1</label>
                                    <input  ng-model="newnurse.mobileno2">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Email</label>
                                    <input type="email"  ng-model="newnurse.email">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label class="control-label1">Working Hospital</label>
                                     <md-select ng-model="newnurse.hospital_id" required="">
                                        <md-option ng-repeat="hospital in hospitals" value="[[hospital.id]]">[[hospital.name]]</md-option>
                                     </md-select>
                                   </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label class="control-label1">Batch</label>
                                     <md-select ng-model="newnurse.nursebatch_id" required="">
                                        <md-option ng-repeat="batch in batches" value="[[batch.id]]">[[batch.name]]</md-option>
                                     </md-select>
                                   </md-input-container>
                                </div>

                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label>Remark</label>
                                     <textarea ng-model="newnurse.remark" md-maxlength="250" rows="5" md-select-on-focus></textarea>
                                   </md-input-container>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-12 text-right">
                                    <md-button class="md-raised btn-default pull-right" ng-click="cancelNurse();"> <md-icon>cancel</md-icon> Cancel</md-button>
                                    <md-button type="submit" class="md-raised  md-primary  pull-right" > <md-icon>note_add</md-icon>Save</md-button>
                                  </div>
                                </div>
                                <hr>
                            </form>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="white-box">
                                    <div class="col-sm-12">
                                        <h3 class="box-title m-b-0">Nurse List</h3>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Show
                                                    <select ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);" class="form-control pagiantion">
                                                    </select>
                                                    entries
                                                </label>
                                            </div>

                                            <div class="col-md-8 ">
                                                <div class="form-inline form-group " style="float: right;">
                                                    <label for="filter-list">Search </label>
                                                    <input type="text" class="form-control" ng-model="filterlist" placeholder="Search">
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <table id="myTable" class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                        <thead>
                                            <tr>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">#</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Name</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="2">Passport No</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Phone 1</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Phone 2</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Email</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">Hospital</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="7">Batch</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="8">Remark</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="9">Created_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="10">Updated_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="11">User</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="nurse in listCount  = (nurses | filter:filterlist) | orderBy:'-created_at'  | pagination: currentPage : numPerPage">
                                                <td>[[numPerPage *(currentPage-1)+$index+1]]</td>
                                                <td class="title"><a ng-click="openNurse(nurse,$event);" href="javascript:void(0)">[[nurse.name]]</a></td>
                                                <td>[[nurse.passportno]]</td>
                                                <td>[[nurse.mobileno1]]</td>
                                                <td>[[nurse.mobileno2]]</td>
                                                <td>[[nurse.email]]</td>
                                                <td><a ng-click="openHospital(nurse,$event);" href="javascript:void(0)">[[nurse.hospital.name]]</a></td>
                                                <td><a ng-click="openBatch(nurse,$event);" href="javascript:void(0)">[[nurse.batch.name]]</a></td>
                                                <td>[[nurse.remark]]</td>
                                                <td>[[nurse.created_at]]</td>
                                                <td>[[nurse.updated_at]]</td>
                                                <td>[[nurse.user.name]]</td>
                                                <td>
                                                    <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                         <md-button ng-if="user.permissions.nurse.edit =='true'" class="md-primary md-small" ng-click="editNurse(nurse);"> <md-icon>mode_edit</md-icon></md-button>
                                                         <md-button ng-show="nurse.active=='1'" class="md-primary md-small" ng-click="changeStaus(nurse.id,$event);">Active</md-button>
                                                         <md-button ng-show="nurse.active=='0'" class="md-danger md-small" ng-click="changeStaus(nurse.id,$event);">In Active</md-button>
                                                         <md-button ng-if="user.permissions.nurse.delete =='true'" class="md-warn md-small" ng-click="deleteNurse(nurse,$event);"><md-icon>delete</md-icon></md-button>
                                                    </div>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                     <div class="clearfix pull-left" ng-show="nurses.length > numPerPage">
                                           <pagination
                                               ng-model="currentPage"
                                               total-items="listCount.length"
                                               max-size="maxSize"
                                               items-per-page="numPerPage"
                                               boundary-links="true"
                                               class="pagination-sm pull-right"
                                               previous-text="&lsaquo;"
                                               next-text="&rsaquo;"
                                               first-text="&laquo;"
                                               last-text="&raquo;"
                                               ></pagination>
                                       </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br/><br/>
             </div>
        </div>
        </div>
        <div ng-hide="permission">
            <div  layout="row" layout-padding layout-wrap layout-fill>
                <div md-whiteframe="3" class="padded alert">
                    <strong>So...Sorry!</strong> You have no Permission to View This Page.
                </div>
            </div>
        </div>
  </md-content>


@section('scripts')

@stop

@stop
@extends('layout.main')
@include('app.partial.message')
@section('content')

  <!--
        ...................NURSE ACCOUNT......................................
  -->
   <div class="row bg-title">
              <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <h4 class="page-title">Nurse-Income Register</h4> </div>
              <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
                  <ol class="breadcrumb">
                      <li><a href="/">Accounts</a></li>
                      <li class="active">Income Register</li>
                  </ol>
              </div>
              <!-- /.col-lg-12 -->
          </div>
   <section id="content" role="content"  layout="column" layout-padding md-scroll-y style="overflow: auto;">
      <md-content id="popupContainer"  ng-controller="NurseAccountIncomeController" class="md-whiteframe-z2" ng-cloak>
       <div ng-show="permission">
        <div class="row">

             <div class="col-md-12">
                    <div class="box">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="row pull-right">
                                <md-button ng-if="user.permissions.nurseincome.write =='true'" ng-hide="incomeedit" class="md-raised md-small md-primary" ng-click="newIncome();"> <md-icon>note_add</md-icon> Add Income</md-button>
                            </div>
                            <br/>
                            <form ng-show="incomeedit" class="form-horizontal"  ng-submit="addIncome();">
                                <h3>New Income</h3><br>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Income Name</label>
                                    <md-autocomplete required=""
                                              ng-model="newincome.ledger"
                                              ng-disabled="isDisabled"
                                              md-no-cache="noCache"
                                              md-selected-item="selectedItem"
                                              md-search-text-change="searchTextChange(searchText)"
                                              md-search-text="searchText"
                                              md-selected-item-change="selectedItemChange(item)"
                                              md-items="item in querySearch(searchText)"
                                              md-item-text="item.name"
                                              md-min-length="0"
                                              placeholder="What is your favorite US state?">
                                            <md-item-template>
                                              <span class="item-title">
                                                <span> <strong>[[item.name]]</strong></span>
                                              </span>
                                              <span class="item-metadata">
                                                <span>
                                                  ([[item.description]])
                                                </span>
                                              </span>
                                            </md-item-template>
                                            <md-not-found>
                                              No states matching "[[searchText]]" were found.
                                            </md-not-found>
                                          </md-autocomplete>
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                       <label class="control-label1">Amount</label>
                                       <input ng-model="newincome.amount" required="">
                                    </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                        <mdp-date-picker name="fromDate" mdp-placeholder="Date"   mdp-format="DD/MM/YYYY"  ng-model="newincome.date"></mdp-date-picker>
                                    </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label>Description</label>
                                     <textarea ng-model="newincome.remark" md-maxlength="250" rows="5" md-select-on-focus></textarea>
                                   </md-input-container>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-12 text-right">
                                    <md-button class="md-raised btn-default pull-right" ng-click="cancelIncome();"> <md-icon>cancel</md-icon> Cancel</md-button>
                                    <md-button type="submit" class="md-raised  md-primary  pull-right" > <md-icon>note_add</md-icon>Save</md-button>
                                  </div>
                                </div>
                                <hr>
                            </form>
                            </div>
                        </div>
                    </div>

                    <div  class="box">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="white-box" style="background: transparent;">
                                    <div class="col-sm-12">
                                        <h3 class="box-title m-b-0">Income List</h3>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Show
                                                    <select ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);" class="form-control pagiantion">
                                                    </select>
                                                    entries
                                                </label>
                                            </div>

                                            <div class="col-md-8 ">
                                                <div class="form-inline form-group " style="float: right;">
                                                    <label for="filter-list">Search </label>
                                                    <input type="text" class="form-control" ng-model="filterlist" placeholder="Search">
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row text-center">
                                        <section  class="form-horizontal col-sm-12">
                                           <div class="row">
                                               <label class="col-sm-1 control-label">From</label>
                                               <div class="col-md-2">
                                                   <div class="input-group">
                                                       <input type="text" class="form-control" datepicker-popup="dd-MM-yyyy" ng-model="fromDate" is-open="expensepicker" show-button-bar="false" show-weeks="false" readonly>
                                                       <span class="input-group-btn">
                                                         <button type="button" class="btn btn-default" ng-click="expensepicker=true"><i class="fa fa-calendar"></i></button>
                                                       </span>
                                                   </div>
                                               </div>
                                               <label class="col-sm-1 control-label">To</label>
                                               <div class="col-md-2">
                                                   <div class="input-group">
                                                       <input type="text" class="form-control" datepicker-popup="dd-MM-yyyy" ng-model="toDate" is-open="expensepickerto" show-button-bar="false" show-weeks="false" readonly>
                                                       <span class="input-group-btn">
                                                         <button type="button" class="btn btn-default" ng-click="expensepickerto=true"><i class="fa fa-calendar"></i></button>
                                                       </span>
                                                   </div>
                                               </div>
                                               <label class="col-sm-1 control-label">Account</label>
                                               <div class="col-md-2">
                                                   <select ng-model="account_id" class="form-control" required="">
                                                       <option value="All">All</option>
                                                       @foreach($accounts as $account)
                                                       <option value="{{$account->id}}">{{$account->name}}</option>
                                                        @endforeach
                                                   </select>
                                               </div>
                                                <div class="col-md-3">
                                                     <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                          <md-button ng-click="search(fromDate,toDate,account_id);" class="md-primary md-raised pull-left"> <md-icon>search</md-icon> Search</md-button>
                                                          <md-button ng-click="export(fromDate,toDate,account_id);"  class="md-accent md-raised"> <md-icon>speaker_notes</md-icon> Export</md-button>
                                                     </div>
                                               </div>

                                           </div>
                                       </section>

                                    <table id="myTable" class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                        <thead>
                                            <tr>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">#</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Account Head</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="2">Amount</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Date</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Remark</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Created_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Updated_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">User</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="income in listCount  = (incomes | filter:filterlist) | orderBy:'-date'  | pagination: currentPage : numPerPage" ng-cloak>
                                                <td>[[numPerPage *(currentPage-1)+$index+1]]</td>
                                                <td class="title"><a ng-click="open(income,$event);" href="javascript:void(0)">[[income.accounthead.name]]</a></td>
                                                <td>[[income.amount]] KD</td>
                                                <td>[[income.date | date:'dd-MMMM-yyyy']]</td>
                                                <td>[[income.remark]]</td>
                                                <td>[[income.created_at]]</td>
                                                <td>[[income.updated_at]]</td>
                                                <td>[[income.user.name]]</td>
                                                <td>
                                                    <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                         <md-button ng-if="user.permissions.nurseincome.edit =='true'" class="md-primary md-small" ng-click="editIncome(income);"> <md-icon>mode_edit</md-icon></md-button>
                                                         <md-button ng-if="user.permissions.nurseincome.delete =='true'" class="md-warn md-small" ng-click="deleteIncome(income,$event);"><md-icon>delete</md-icon></md-button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                     <div class="clearfix pull-left" ng-show="incomes.length > numPerPage">
                                          <pagination
                                              ng-model="currentPage"
                                              total-items="listCount.length"
                                              max-size="maxSize"
                                              items-per-page="numPerPage"
                                              boundary-links="true"
                                              class="pagination-sm pull-right"
                                              previous-text="&lsaquo;"
                                              next-text="&rsaquo;"
                                              first-text="&laquo;"
                                              last-text="&raquo;"
                                              ></pagination>
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br/><br/>
             </div>
        </div>
        </div>
        <div ng-hide="permission">
            <div  layout="row" layout-padding layout-wrap layout-fill>
                <div md-whiteframe="3" class="padded alert">
                    <strong>So...Sorry!</strong> You have no Permission to View This Page.
                </div>
            </div>
        </div>
        </div>
  </md-content>
   </section>

@section('scripts')

@stop

@stop
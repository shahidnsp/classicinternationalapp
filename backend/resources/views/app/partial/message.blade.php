<?php

$class = Session::get('messageClass') ? Session::get('messageClass') : 'alert alert-success';
$class = is_array($class) ? implode('', $class) : $class;

/**
 * Contains the notify plugin message.
 *
 * @var string / array
 */
$m = Session::get('message');


/**
 * Laravel errors message controller
 *
 * @var array
 */
$e = $errors->any() ? $errors->all() : '';
if($e!='')
    $e=implode($e,",");

//forgetting flash data to control app notify messages.
Session::forget('message');
Session::forget('messageClass');
Session::save();

?>
@if($m!='' || $e!='')
    <div ng-controller="MessageController"></div>
    @section('scripts')
    @parent
        <script>
            (function(app){
                app.controller('MessageController',['ngNotify',function(ngNotify) {
                     ngNotify.config({
                            theme: 'pure',
                            position: 'top',
                            duration: 3000,
                            type: 'info',
                            sticky: false,
                            button: true,
                            html: false
                        });

                  @if($m!='')
                    ngNotify.set('{!! $m !!}');
                  @endif
                  @if($e!='')
                    ngNotify.set('{!! $e !!}');
                  @endif
                }]);
            })(angular.module("myApp"));
        </script>
    @stop
@endif
@extends('layout.main')

@section('content')

<div  ng-cloak>
  <div class="row bg-title">
      <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Dashboard</h4> </div>
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
          <ol class="breadcrumb">
              <li><a href="/">Accounts</a></li>
              <li class="active">Dashboard</li>
          </ol>
      </div>
      <!-- /.col-lg-12 -->
  </div>
  <md-content>
       <div class="row">
           <div class="col-md-3 col-sm-6">
                <md-card style="width: 100%;">
                      <md-card-title>
                          <md-card-title-text>
                                <div class="r-icon-stats"> <i class="ti-user bg-megna"></i>
                                   <div class="bodystate">
                                       <h4>{{$usercount}}</h4> <span class="text-muted">Online Users</span> </div>
                               </div>
                          </md-card-title-text>
                      </md-card-title>
                </md-card>
           </div>
           <div class="col-md-3 col-sm-6">
                <md-card style="width: 100%;">
                      <md-card-title>
                          <md-card-title-text>
                              <div class="r-icon-stats"> <i class="ti-user bg-info"></i>
                                  <div class="bodystate">
                                      <h4>{{$activecandidates}}</h4> <span class="text-muted">Candidates</span> </div>
                              </div>
                          </md-card-title-text>
                      </md-card-title>
                </md-card>
           </div>
           <div class="col-md-3 col-sm-6">
                <md-card style="width: 100%;">
                      <md-card-title>
                          <md-card-title-text>
                              <div class="r-icon-stats"> <i class="ti-user bg-success"></i>
                                 <div class="bodystate">
                                     <h4>{{$activeemployees}}</h4> <span class="text-muted">Employees</span> </div>
                             </div>
                          </md-card-title-text>
                      </md-card-title>
                </md-card>
           </div>
           <div class="col-md-3 col-sm-6">
                <md-card style="width: 100%;">
                      <md-card-title>
                          <md-card-title-text>
                             <div class="r-icon-stats"> <i class="ti-user bg-inverse"></i>
                                 <div class="bodystate">
                                     <h4>{{$activenurses}}</h4> <span class="text-muted">Nurses</span> </div>
                             </div>
                          </md-card-title-text>
                      </md-card-title>
                </md-card>
           </div>
       </div>
       <br/>
       <div class="row">
                   <div class="col-md-4 col-sm-4">
                   </div>
                  <div class="col-md-4 col-sm-4">
                       <a href="/api/main-account-billbook">
                       <md-card style="width: 100%;">
                             <md-card-title>
                                 <md-card-title-text>
                                       <div class="r-icon-stats"> <i class="ti-user bg-megna"></i>
                                          <div class="bodystate">
                                            <h1 class="text-muted">Bill Books</h1>
                                          </div>
                                      </div>
                                 </md-card-title-text>
                             </md-card-title>
                       </md-card>
                       </a>
                  </div>
                  <div class="col-md-4 col-sm-4">
                  </div>
       </div>
       <br/>
       <div class="row">
          <div class="col-md-6 col-sm-6">
            <div id="piechart_income"></div>
          </div>
          <div class="col-md-6 col-sm-6">
            <div id="piechart_expense"></div>
          </div>
       </div>
       <br/><br/><br/>
       <br/><br/><br/>
       <br/><br/><br/>
       <br/><br/><br/>
       <br/><br/><br/>
       <br/><br/><br/>
       <br/><br/><br/>
  </md-content>
</div>
@section('scripts')
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Income'],
          ['Main Account',     {{$graphData['mainincome']}}],
          ['Rent Account',     {{$graphData['rentincome']}}],
          ['Courier Account',  {{$graphData['courierincome']}}],
          ['Transportation', {{$graphData['transincome']}}],
          ['Nurse Account', {{$graphData['nurseincome']}}],
        ]);

        var options = {
          title: 'My Montly Income',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_income'));
        chart.draw(data, options);
      }
   </script>

   <script type="text/javascript">
         google.charts.load("current", {packages:["corechart"]});
         google.charts.setOnLoadCallback(drawChart2);
         function drawChart2() {
           var data1 = google.visualization.arrayToDataTable([
             ['Task', 'Expense'],
             ['Main Account',     {{$graphData['mainexpense']}}],
             ['Rent Account',       {{$graphData['rentexpense']}}],
             ['Courier Account',  {{$graphData['courierexpense']}}],
             ['Transportation', {{$graphData['transexpense']}}],
             ['Nurse Account', {{$graphData['nurseexpense']}}],
           ]);

           var options1 = {
             title: 'My Montly Expense',
             is3D: true,
           };

           var chart = new google.visualization.PieChart(document.getElementById('piechart_expense'));
           chart.draw(data1, options1);
         }
      </script>
@stop
@stop
@extends('layout.main')
@include('app.partial.message')
@section('content')
  <!--
        ...................TRANSPORTATION ACCOUNT......................................
  -->

  <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Transportation- Fair Register Report</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
                <ol class="breadcrumb">
                    <li><a href="/">Accounts</a></li>
                    <li class="active">Fair Register Report</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
<section id="content" role="content" ng-controller="FairRegisterReport2Controller"  layout="column" layout-padding md-scroll-y style="overflow: auto;" ng-cloak>
  <md-content id="popupContainer">
        <div ng-show="permission">
        <div class="row">
             <div class="col-md-12">
                    <div class="box">
                            <div class="col-lg-12">
                                <div class="white-box">
                                    <div class="col-sm-12">
                                        <h3 class="box-title m-b-0">Fair Register Report 2</h3>
                                         <div>
                                            <md-radio-group ng-model="basedon" layout="row">
                                                  <md-radio-button value="Driver" class="md-primary">Driver</md-radio-button>
                                                  <md-radio-button value="Hospital"> Hospital </md-radio-button>
                                            </md-radio-group>
                                        </div>
                                        <br/>
                                        <div layout="row">
                                            <div class="col-md-8">
                                                <form class="form-horizontal" ng-submit="getDutyChart();">
                                                    <div class="col-md-4">
                                                        {{--<select name="month" ng-model="month" ng-change="showMonth(month);" class="form-control pagiantion" required="">
                                                            <option value="">Year</option>
                                                            @foreach($years as $year)
                                                                <option value="{{$year->date}}">{{$year->month}}</option>
                                                            @endforeach
                                                        </select>--}}
                                                          <div class="input-group">
                                                            <input type="text" class="form-control" datepicker-popup="MMMM-yyyy" ng-model="month" ng-change="showMonth(month);" is-open="expensepicker" show-button-bar="false" show-weeks="false" readonly>
                                                            <span class="input-group-btn">
                                                              <button type="button" class="btn btn-default" ng-click="expensepicker=true"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                          </div>
                                                    </div>
                                                    <div ng-show="basedon=='Driver'" class="col-md-4">
                                                        <div class="form-inline form-group ">
                                                        <label class="control-label" for="driver">Driver &nbsp;&nbsp; </label>
                                                        <select id="driver" name="driver_id" ng-model="driver_id" class="form-control col-sm-8" required="">
                                                            <option value="All" selected>All</option>
                                                            @foreach($drivers as $driver)
                                                                <option value="{{$driver->id}}">{{$driver->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div ng-show="basedon=='Hospital'" class="col-md-4">
                                                        <div class="form-inline form-group ">
                                                        <label class="control-label" for="driver">Hospital &nbsp;&nbsp; </label>
                                                        <select id="driver" name="driver_id" ng-model="hospital_id" class="form-control col-sm-8" required="">
                                                            <option value="All" selected>All</option>
                                                            @foreach($hospitals as $hospital)
                                                                <option value="{{$hospital->id}}">{{$hospital->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                          <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                               <md-button type="submit" class="md-raised md-primary"> <md-icon>note_add</md-icon>View</md-button>
                                                               {{--<md-button ng-click="export(year,driver_id);"  class="md-raised"> <md-icon>speaker_notes</md-icon> Export</md-button>--}}
                                                          </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-inline form-group " style="float: right;">
                                                    <label for="filter-list">Search </label>
                                                    <input type="text" class="form-control" ng-model="filterlist" placeholder="Search">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-2 text-left">
                                            <label for="">Show
                                                <select ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);" class="form-control pagiantion">
                                                </select>
                                                entries
                                            </label>
                                         </div>
                                         <div class="col-sm-8 text-center">
                                            <h2 class="text-primary" style="text-decoration: underline;">Fair Register Report [[curMonth]]</h2>
                                         </div>
                                         <div class="col-sm-2">

                                         </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <table class="table table-responsive table-bordered">
                                                <tr>
                                                    <td>Month</td><td class="text-primary text-center">[[curMonth]]</td>
                                                </tr>
                                                <tr>
                                                    <td>Total Income:</td><td class="text-danger text-center">[[totalIncome]] KD</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-sm-8"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                           <md-radio-group ng-show="month!=null" ng-change="changeStatus(status);" layout="row" ng-model="status">
                                             <md-radio-button value="All" class="md-primary">All</md-radio-button>
                                             <md-radio-button value="Paid" class="md-accent"> Paid </md-radio-button>
                                             <md-radio-button value="Not Paid" class="md-warn">Not Paid</md-radio-button>
                                           </md-radio-group>
                                        </div>
                                        <div ng-show="reports.length>0" class="col-sm-6 text-right">
                                              <md-button ng-click="exportExcel(month,driver_id,status,basedon,hospital_id);"  class="md-raised md-primary"> <md-icon>speaker_notes</md-icon> Export</md-button>
                                              <md-button ng-click="exportPDF(month,driver_id,status,basedon,hospital_id);"  class="md-raised md-accent"> <md-icon>picture_as_pdf</md-icon> PDF</md-button>
                                               <div ng-show="showProgressExport" class="row text-center">
                                                  <div class="loader"></div>
                                               </div>
                                        </div>
                                    </div>

                                    <table id="myTable" class="tablesaw table-bordered table-hover table"  data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                        <thead>
                                            <tr>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">SlNo</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Candidate</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Phone</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="2">Hospital</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Driver</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">[[curMonth]]</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="7">Remark</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="report in listCount  = (reports | filter:filterlist) | orderBy:'-name'  | pagination: currentPage : numPerPage">
                                                <td>[[numPerPage *(currentPage-1)+$index+1]]</td>
                                                <td class="title">[[report.name]]</td>
                                                <td class="title">[[report.mobile1]]</td>
                                                <td>[[report.hospital]]</td>
                                                <td>[[report.driver]]</td>
                                                 <td class="text-center" ng-class="report.curMonth=='NPD' ? 'text-danger' : 'text-primary'">[[report.curMonth]] <span ng-if="report.curMonth != 'NPD'"> KD</span></td>
                                                <td>[[report.remark]]</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="clearfix pull-left" ng-show="reports.length > numPerPage">
                                        <pagination
                                            ng-model="currentPage"
                                            total-items="listCount.length"
                                            max-size="maxSize"
                                            items-per-page="numPerPage"
                                            boundary-links="true"
                                            class="pagination-sm pull-right"
                                            previous-text="&lsaquo;"
                                            next-text="&rsaquo;"
                                            first-text="&laquo;"
                                            last-text="&raquo;"
                                            ></pagination>
                                    </div>
                                </div>
                            </div>
                    </div>

                    <div ng-show="showProgress" class="row text-center">
                        <div class="loader"></div>
                    </div>


                    <br/><br/>
             </div>
        </div>

        </div>
        <div ng-hide="permission">
            <div  layout="row" layout-padding layout-wrap layout-fill>
                <div md-whiteframe="3" class="padded alert">
                    <strong>So...Sorry!</strong> You have no Permission to View This Page.
                </div>
            </div>
        </div>

  </md-content>
</section>

@section('scripts')

@stop

@stop
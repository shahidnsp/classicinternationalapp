@extends('layout.main')
@include('app.partial.message')
@section('content')
  <!--
        ...................TRANSPORTATION ACCOUNT......................................
  -->
  <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Transportation-Candidate</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
                <ol class="breadcrumb">
                    <li><a href="/">Accounts</a></li>
                    <li class="active">Candidate</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

  <md-content id="popupContainer" ng-controller="CandidateController" ng-cloak>
        <div ng-show="permission">
        <div class="row">
             <div class="col-md-12">
                    <div class="box">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="row pull-right">
                                <md-button ng-if="user.permissions.candidate.write =='true'" ng-hide="candidateedit" class="md-raised md-small md-primary" ng-click="newCandidate();"> <md-icon>note_add</md-icon> Add Candidate</md-button>
                            </div>
                            <br/>
                            <form ng-show="candidateedit" class="form-horizontal"  ng-submit="addCandidate();">
                                <h3>New Candidate</h3><br>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Candidate Name</label>
                                    <input ng-model="newcandidate.name" required="">
                                  </md-input-container>
                                  <md-input-container class="md-block" flex-gt-xs>
                                       <label class="control-label1">Gender</label>
                                       <md-select ng-model="newcandidate.gender" required="">
                                          <md-option  value="Male">Male</md-option>
                                          <md-option  value="Female">Female</md-option>
                                       </md-select>
                                 </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label class="control-label1">Civil ID</label>
                                     <input ng-model="newcandidate.civilid">
                                   </md-input-container>
                                   <md-input-container class="md-block" flex-gt-xs>
                                       <label class="control-label1">Passport No</label>
                                       <input ng-model="newcandidate.passportno">
                                   </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Mobile Number 1</label>
                                    <input ng-model="newcandidate.mobile1" required="">
                                  </md-input-container>
                                  <md-input-container class="md-block" flex-gt-xs>
                                      <label class="control-label1">Mobile Number 2</label>
                                      <input ng-model="newcandidate.mobile2">
                                    </md-input-container>
                                </div>

                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Mobile Number 3</label>
                                    <input ng-model="newcandidate.mobile3">
                                  </md-input-container>
                                  <md-input-container class="md-block" flex-gt-xs>
                                      <label class="control-label1">Whatsapp Number</label>
                                      <input ng-model="newcandidate.whatsapp">
                                    </md-input-container>
                                </div>

                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Email Address</label>
                                    <input type="email" ng-model="newcandidate.email">
                                  </md-input-container>
                                  <md-input-container class="md-block" flex-gt-xs>
                                       <label class="control-label1">Residence Area</label>
                                       <md-select ng-model="newcandidate.residence_id" required="">
                                          <md-option ng-repeat="residence in residences" value="[[residence.id]]">[[residence.name]]</md-option>
                                       </md-select>
                                     </md-input-container>
                                </div>

                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label class="control-label1">Working Hospital</label>
                                     <md-select ng-model="newcandidate.hospital_id" required="">
                                        <md-option ng-repeat="hospital in hospitals" value="[[hospital.id]]">[[hospital.name]]</md-option>
                                     </md-select>
                                   </md-input-container>
                                   <md-input-container class="md-block" flex-gt-xs>
                                        <label class="control-label1">Duty Driver</label>
                                        <md-select ng-model="newcandidate.employee_id" required="">
                                           <md-option ng-repeat="driver in drivers" value="[[driver.id]]">[[driver.name]]</md-option>
                                        </md-select>
                                      </md-input-container>
                                </div>

                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label>Description</label>
                                     <textarea ng-model="newcandidate.remark" md-maxlength="250" rows="5" md-select-on-focus></textarea>
                                   </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label>Photo</label>
                                     <input accept="image/*" ng-file-model="newcandidate.photos" type="file" multiple/>
                                   </md-input-container>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-12 text-right">
                                    <md-button class="md-raised btn-default pull-right" ng-click="cancelCandidate();"> <md-icon>cancel</md-icon> Cancel</md-button>
                                    <md-button type="submit" class="md-raised  md-primary  pull-right" > <md-icon>note_add</md-icon>Save</md-button>
                                  </div>
                                </div>
                                <hr>
                            </form>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <div layout="row">
                            <div>
                                <md-radio-group ng-model="basedon" layout="row">
                                      <md-radio-button value="Driver" class="md-primary">Driver</md-radio-button>
                                      <md-radio-button value="Hospital"> Hospital </md-radio-button>
                                </md-radio-group>
                            </div>
                            <div ng-if="basedon=='Driver'">
                                <div layout="row" class="form-horizontal form-group col-sm-12">
                                    <label class="control-label col-sm-3">Driver</label>
                                    <div class="col-sm-7">
                                        <select ng-model="driver_id" class="form-control">
                                            <option value="All">All</option>
                                            <option ng-repeat="driver in drivers" value="[[driver.id]]">[[driver.name]]</option>
                                        </select>
                                    </div>
                                    <div layout="row">
                                        <md-button ng-click="search(driver_id,hostel_id,basedon);" class="md-raised md-small md-accent"> <md-icon>note_add</md-icon> Search</md-button>
                                        <md-button ng-click="download(driver_id,hostel_id,basedon);" class="md-raised md-small md-accent"> <md-icon>note_add</md-icon> Get PDF</md-button>
                                    </div>
                                </div>
                            </div>
                            <div ng-if="basedon=='Hospital'">
                                <div layout="row" class="form-horizontal form-group col-sm-12">
                                    <label class="control-label col-sm-3">Hospital</label>
                                    <div class="col-sm-7">
                                        <select ng-model="hostel_id" class="form-control">
                                            <option value="All">All</option>
                                            <option ng-repeat="hospital in hospitals" value="[[hospital.id]]">[[hospital.name]]</option>
                                        </select>
                                    </div>
                                    <div layout="row">
                                        <md-button ng-click="search(driver_id,hostel_id,basedon);" class="md-raised md-small md-accent"> <md-icon>note_add</md-icon> Search</md-button>
                                        <md-button ng-click="download(driver_id,hostel_id,basedon);" class="md-raised md-small md-accent"> <md-icon>note_add</md-icon> Get PDF</md-button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="white-box">
                                    <div class="col-sm-12">
                                        <h3 class="box-title m-b-0">Candidate List</h3>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Show
                                                    <select ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);" class="form-control pagiantion">
                                                    </select>
                                                    entries
                                                </label>
                                            </div>

                                            <div class="col-md-8 ">
                                                <div class="form-inline form-group " style="float: right;">
                                                    <label for="filter-list">Search </label>
                                                    <input type="text" class="form-control" ng-model="filterlist" placeholder="Search">
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <table id="myTable" class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                        <thead>
                                            <tr>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">#</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Name</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="2">Mobile 1</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Mobile 2</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Mobile 3</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Whatsapp</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">Email</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="7">Photo</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="8">Residence</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="9">Hospital</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="10">Gender</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="11">Driver</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="12">Remark</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="13">Created_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="14">Updated_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="15">User</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="candidate in listCount  = (candidates | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage" ng-cloak>
                                                 <td>[[numPerPage *(currentPage-1)+$index+1]]</td>
                                                <td class="title"><a ng-click="openCandidate(candidate,$event);" href="javascript:void(0)">[[candidate.name]]</a></td>
                                                <td>[[candidate.mobile1]]</td>
                                                <td>[[candidate.mobile2]]</td>
                                                <td>[[candidate.mobile3]]</td>
                                                <td>[[candidate.whatsapp]]</td>
                                                <td>[[candidate.email]]</td>
                                                <td><img src="[[basepath]]/images/[[candidate.photo]]" style="width: 120px;height: 120px;" alt=""/></td>
                                                <td><a ng-click="openResidence(candidate,$event);" href="javascript:void(0)">[[candidate.residence.name]]</a></td>
                                                <td><a ng-click="openHospital(candidate,$event);" href="javascript:void(0)">[[candidate.hospital.name]]</a></td>
                                                <td>[[candidate.gender]]</td>
                                                <td><a ng-click="open(candidate,$event);" href="javascript:void(0)">[[candidate.employee.name]]</a></td>
                                                <td>[[candidate.remark]]</td>
                                                <td>[[candidate.created_at]]</td>
                                                <td>[[candidate.updated_at]]</td>
                                                <td>[[candidate.user.name]]</td>
                                                <td>
                                                    <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                         <md-button ng-if="user.permissions.candidate.edit =='true'" class="md-primary md-small" ng-click="editCandidate(candidate);"> <md-icon>mode_edit</md-icon></md-button>
                                                         <md-button ng-show="candidate.active=='1'" class="md-primary md-small" ng-click="changeStaus(candidate.id,$event);">Active</md-button>
                                                         <md-button ng-show="candidate.active=='0'" class="md-danger md-small" ng-click="changeStaus(candidate.id,$event);">In Active</md-button>
                                                         <md-button class="md-danger md-small" ng-click="getProfile(candidate);">Get Profile</md-button>
                                                         <md-button ng-if="user.permissions.candidate.delete =='true'" class="md-warn md-small" ng-click="deleteCandidate(candidate,$event);"><md-icon>delete</md-icon></md-button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                     <div class="clearfix pull-left" ng-show="candidates.length > numPerPage">
                                          <pagination
                                              ng-model="currentPage"
                                              total-items="listCount.length"
                                              max-size="maxSize"
                                              items-per-page="numPerPage"
                                              boundary-links="true"
                                              class="pagination-sm pull-right"
                                              previous-text="&lsaquo;"
                                              next-text="&rsaquo;"
                                              first-text="&laquo;"
                                              last-text="&raquo;"
                                              ></pagination>
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br/><br/>
             </div>
        </div>
         </div>
        <div ng-hide="permission">
            <div  layout="row" layout-padding layout-wrap layout-fill>
                <div md-whiteframe="3" class="padded alert">
                    <strong>So...Sorry!</strong> You have no Permission to View This Page.
                </div>
            </div>
        </div>
  </md-content>


@section('scripts')

@stop

@stop
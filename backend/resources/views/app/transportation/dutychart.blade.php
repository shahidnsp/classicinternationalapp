@extends('layout.main')
@include('app.partial.message')
@section('content')
  <!--
        ...................TRANSPORTATION ACCOUNT......................................
  -->

  <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Transportation- Duty Chart</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
                <ol class="breadcrumb">
                    <li><a href="/">Accounts</a></li>
                    <li class="active">Duty Chart</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
<section id="content" role="content" ng-controller="DutychartController"  layout="column" layout-padding md-scroll-y style="overflow: auto;" ng-cloak>
  <md-content id="popupContainer">
        <div ng-show="permission">
        <div class="row">
             <div class="col-md-12">
                    <div class="box">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="white-box">
                                    <div class="col-sm-12">
                                        <h3 class="box-title m-b-0">Duty Chart</h3>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <form class="form-horizontal" ng-submit="getDutyChart();">
                                                    <div class="col-md-3">
                                                        <select name="year" ng-model="year" class="form-control pagiantion" required="">
                                                            @foreach($years as $year)
                                                                <option value="{{$year->year}}" @if($year->year == $selected) selected @endif>{{$year->year}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="form-inline form-group ">
                                                        <label class="control-label" for="driver">Driver &nbsp;&nbsp; </label>
                                                        <select id="driver" name="driver_id" ng-model="driver_id" class="form-control col-sm-8" required="">
                                                            <option value="All">All</option>
                                                            @foreach($drivers as $driver)
                                                                <option value="{{$driver->id}}" @if($driver->id == $driver_id) selected @endif>{{$driver->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                          <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                               <md-button type="submit" class="md-raised md-primary"> <md-icon>note_add</md-icon>Generate</md-button>
                                                               <md-button ng-click="export(year,driver_id);"  class="md-raised"> <md-icon>speaker_notes</md-icon> Export</md-button>
                                                          </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-inline form-group " style="float: right;">
                                                    <label for="filter-list">Search </label>
                                                    <input type="text" class="form-control" ng-model="filterlist" placeholder="Search">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-2 text-left">
                                            <label for="">Show
                                                <select ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);" class="form-control pagiantion">
                                                </select>
                                                entries
                                            </label>
                                         </div>
                                         <div class="col-sm-8 text-center">
                                            <h2 class="text-primary" style="text-decoration: underline;">Duty Chart of the Year {{$selected}}</h2>
                                         </div>
                                         <div class="col-sm-2">

                                         </div>
                                    </div>

                                    <table id="myTable" class="tablesaw table-bordered table-hover table"  data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                        <thead>
                                            <tr>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">SlNo</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Name</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="2">Mobile</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Residence</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Hospital</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Duty Driver</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">Jan</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="7">Feb</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="8">Mar</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="9">Apr</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="10">May</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="11">Jun</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="12">Jul</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="13">Aug</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="14">Sep</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="15">Oct</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="16">Nov</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="17">Dec</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="dutie in listCount  = (duties | filter:filterlist) | orderBy:'-name'  | pagination: currentPage : numPerPage">
                                                <td>[[numPerPage *(currentPage-1)+$index+1]]</td>
                                                <td class="title"><a ng-click="openDutyChart(dutie,dutie.residence,dutie.hospital,dutie.employee,$event);" href="javascript:void(0)">[[dutie.name]]</a></td>
                                                <td>[[dutie.mobile1]]</td>
                                                <td><a ng-click="openResidence(dutie,$event);" href="javascript:void(0)">[[dutie.residence.name]]</a></td>
                                                <td><a ng-click="openHospital(dutie,$event);" href="javascript:void(0)">[[dutie.hospital.name]]</a></td>
                                                <td><a ng-click="openEmployee(dutie,$event);" href="javascript:void(0)">[[dutie.employee.name]]</a></td>
                                                <td ng-class="dutie.jan=='NPD' ? 'text-danger' :'text-success'">[[dutie.jan]]</td>
                                                <td ng-class="dutie.feb=='NPD' ? 'text-danger' :'text-success'">[[dutie.feb]]</td>
                                                <td ng-class="dutie.mar=='NPD' ? 'text-danger' :'text-success'">[[dutie.mar]]</td>
                                                <td ng-class="dutie.apr=='NPD' ? 'text-danger' :'text-success'">[[dutie.apr]]</td>
                                                <td ng-class="dutie.may=='NPD' ? 'text-danger' :'text-success'">[[dutie.may]]</td>
                                                <td ng-class="dutie.jun=='NPD' ? 'text-danger' :'text-success'">[[dutie.jun]]</td>
                                                <td ng-class="dutie.jul=='NPD' ? 'text-danger' :'text-success'">[[dutie.jul]]</td>
                                                <td ng-class="dutie.aug=='NPD' ? 'text-danger' :'text-success'">[[dutie.aug]]</td>
                                                <td ng-class="dutie.sep=='NPD' ? 'text-danger' :'text-success'">[[dutie.sep]]</td>
                                                <td ng-class="dutie.oct=='NPD' ? 'text-danger' :'text-success'">[[dutie.oct]]</td>
                                                <td ng-class="dutie.nov=='NPD' ? 'text-danger' :'text-success'">[[dutie.nov]]</td>
                                                <td ng-class="dutie.dec=='NPD' ? 'text-danger' :'text-success'">[[dutie.dec]]</td>
                                            </tr>
                                            <tr style="color: #000099;font-size: large;">
                                                <td>Total</td><td></td><td></td><td></td><td></td><td></td>
                                                <td>[[jansum]]</td>
                                                <td>[[febsum]]</td>
                                                <td>[[marsum]]</td>
                                                <td>[[aprsum]]</td>
                                                <td>[[maysum]]</td>
                                                <td>[[junsum]]</td>
                                                <td>[[julsum]]</td>
                                                <td>[[augsum]]</td>
                                                <td>[[sepsum]]</td>
                                                <td>[[octsum]]</td>
                                                <td>[[novsum]]</td>
                                                <td>[[decsum]]</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="clearfix pull-left" ng-show="duties.length > numPerPage">
                                        <pagination
                                            ng-model="currentPage"
                                            total-items="listCount.length"
                                            max-size="maxSize"
                                            items-per-page="numPerPage"
                                            boundary-links="true"
                                            class="pagination-sm pull-right"
                                            previous-text="&lsaquo;"
                                            next-text="&rsaquo;"
                                            first-text="&laquo;"
                                            last-text="&raquo;"
                                            ></pagination>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div ng-show="year!=null" class="row text-center">
                          <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                               <md-button ng-click="getPaidPDF(year,driver_id);" class="md-raised md-primary"> <md-icon>speaker_notes</md-icon>Paid List</md-button>
                               <md-button ng-click="getUnpaidPDF(year,driver_id);" class="md-raised md-warn"> <md-icon>speaker_notes</md-icon> Unpaid List</md-button>
                               <md-button ng-click="getPaidAndUnpaidPDF(year,driver_id);" class="md-raised md-accent"> <md-icon>speaker_notes</md-icon> Paid/Unpaid List</md-button>
                          </div>
                    </div>
                    <div ng-show="showProgress" class="row text-center">
                        <div class="loader"></div>
                    </div>


                    <br/><br/>
             </div>
        </div>

        </div>
        <div ng-hide="permission">
            <div  layout="row" layout-padding layout-wrap layout-fill>
                <div md-whiteframe="3" class="padded alert">
                    <strong>So...Sorry!</strong> You have no Permission to View This Page.
                </div>
            </div>
        </div>

  </md-content>
</section>

@section('scripts')

@stop

@stop
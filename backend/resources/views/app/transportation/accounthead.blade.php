@extends('layout.main')
@include('app.partial.message')
@section('content')

  <!--
        ...................TRANSPORTATION ACCOUNT......................................
  -->

     <div class="row bg-title">
          <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h4 class="page-title">Transportation-Account Head</h4> </div>
          <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
              <ol class="breadcrumb">
                  <li><a href="/">Accounts</a></li>
                  <li class="active">Account Head</li>
              </ol>
          </div>
          <!-- /.col-lg-12 -->
      </div>

  <md-content id="popupContainer" ng-controller="TransportationAccountAccountHeadController" ng-cloak>
      <div ng-show="permission">
        <div class="row">
             <div class="col-md-12">
                    <div class="box">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="row pull-right">
                                <md-button ng-if="user.permissions.transportationaccounthead.write =='true'" ng-hide="headedit" class="md-raised md-small md-primary" ng-click="newAccountHead();"> <md-icon>note_add</md-icon> Add Account Head</md-button>
                            </div>
                            <br/>
                            <form ng-show="headedit" class="form-horizontal"  ng-submit="addAccountHead();">
                                <h3>New Account Head</h3><br>
                                <div layout-gt-xs="row">
                                  <md-input-container class="md-block" flex-gt-xs>
                                    <label class="control-label1">Account Head Name</label>
                                    <input ng-model="newaccount.name" required="">
                                  </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label class="control-label1">Type</label>
                                     <md-select ng-model="newaccount.type" required="">
                                       <md-option value="income">Income</md-option>
                                       <md-option value="expense">Expense</md-option>
                                     </md-select>
                                   </md-input-container>
                                </div>
                                <div layout-gt-xs="row">
                                   <md-input-container class="md-block" flex-gt-xs>
                                     <label>Description</label>
                                     <textarea ng-model="newaccount.description" md-maxlength="250" rows="5" md-select-on-focus></textarea>
                                   </md-input-container>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-12 text-right">
                                    <md-button class="md-raised btn-default pull-right" ng-click="cancelAccountHead();"> <md-icon>cancel</md-icon> Cancel</md-button>
                                    <md-button type="submit" class="md-raised  md-primary  pull-right" > <md-icon>note_add</md-icon>Save</md-button>
                                  </div>
                                </div>
                                <hr>
                            </form>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="white-box">
                                    <div class="col-sm-12">
                                        <h3 class="box-title m-b-0">Account Head List</h3>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Show
                                                    <select ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);" class="form-control pagiantion">
                                                    </select>
                                                    entries
                                                </label>
                                            </div>

                                            <div class="col-md-8 ">
                                                <div class="form-inline form-group " style="float: right;">
                                                    <label for="filter-list">Search </label>
                                                    <input type="text" class="form-control" ng-model="filterlist" placeholder="Search">
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <table id="myTable" class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                        <thead>
                                            <tr>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">#</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Account Head</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="3">Type</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Group</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Description</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Created_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Updated_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">User</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>


                                            <tr ng-repeat="accounthead in listCount  = (accountheads | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage" ng-cloak>
                                                 <td>[[numPerPage *(currentPage-1)+$index+1]]</td>
                                                <td class="title"><a ng-click="open(accounthead,$event);" href="javascript:void(0)">[[accounthead.name]]</a></td>
                                                <td ng-cloak>[[accounthead.type]]</td>
                                                <td ng-cloak>[[accounthead.group]]</td>
                                                <td ng-cloak>[[accounthead.description]]</td>
                                                <td ng-cloak>[[accounthead.created_at]]</td>
                                                <td ng-cloak>[[accounthead.updated_at]]</td>
                                                <td ng-cloak>[[accounthead.user.name]]</td>
                                                <td>
                                                    <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                         <md-button ng-if="user.permissions.transportationaccounthead.edit =='true'" class="md-primary md-small" ng-click="editAccountHeadt(accounthead);"> <md-icon>mode_edit</md-icon></md-button>
                                                         <md-button ng-show="accounthead.active=='1'" class="md-primary md-small" ng-click="changeStaus(accounthead.id,$event);">Active</md-button>
                                                         <md-button ng-show="accounthead.active=='0'" class="md-danger md-small" ng-click="changeStaus(accounthead.id,$event);">In Active</md-button>
                                                         <md-button ng-if="user.permissions.transportationaccounthead.delete =='true'" class="md-warn md-small" ng-click="deleteAccountHead(accounthead,$event);"><md-icon>delete</md-icon></md-button>
                                                    </div>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                      <div class="clearfix pull-left" ng-show="accountheads.length > numPerPage">
                                         <pagination
                                             ng-model="currentPage"
                                             total-items="listCount.length"
                                             max-size="maxSize"
                                             items-per-page="numPerPage"
                                             boundary-links="true"
                                             class="pagination-sm pull-right"
                                             previous-text="&lsaquo;"
                                             next-text="&rsaquo;"
                                             first-text="&laquo;"
                                             last-text="&raquo;"
                                             ></pagination>
                                     </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br/><br/>
             </div>
        </div>
        </div>
        <div ng-hide="permission">
            <div  layout="row" layout-padding layout-wrap layout-fill>
                <div md-whiteframe="3" class="padded alert">
                    <strong>So...Sorry!</strong> You have no Permission to View This Page.
                </div>
            </div>
        </div>
  </md-content>


@section('scripts')

@stop

@stop
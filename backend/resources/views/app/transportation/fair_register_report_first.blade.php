@extends('layout.main')
@include('app.partial.message')
@section('content')
  <!--
        ...................TRANSPORTATION ACCOUNT......................................
  -->

  <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Transportation- Fair Register Report</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
                <ol class="breadcrumb">
                    <li><a href="/">Accounts</a></li>
                    <li class="active">Fair Register Report</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
<section id="content" role="content" ng-controller="FairRegisterReport1Controller"  layout="column" layout-padding md-scroll-y style="overflow: auto;" ng-cloak>
  <md-content id="popupContainer">
        <div ng-show="permission">
        <div class="row">
             <div class="col-md-12">
                    <div class="box">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="white-box">
                                    <div class="col-sm-12">
                                        <h3 class="box-title m-b-0">Fair Register</h3>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <form class="form-horizontal" ng-submit="getDutyChart();">
                                                    <div class="col-md-4">
                                                      <div class="input-group">
                                                        <input type="text" class="form-control" datepicker-popup="MMMM-yyyy" ng-model="month" ng-change="showMonth(month);" is-open="expensepicker" show-button-bar="false" show-weeks="false" readonly>
                                                        <span class="input-group-btn">
                                                          <button type="button" class="btn btn-default" ng-click="expensepicker=true"><i class="fa fa-calendar"></i></button>
                                                        </span>
                                                      </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-inline form-group ">
                                                        <label class="control-label" for="driver">Driver &nbsp;&nbsp; </label>
                                                        <select id="driver" name="driver_id" ng-model="driver_id" class="form-control col-sm-8" required="">
                                                            <option value="All" selected>All</option>
                                                            @foreach($drivers as $driver)
                                                                <option value="{{$driver->id}}">{{$driver->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                          <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                               <md-button type="submit" class="md-raised md-primary"> <md-icon>note_add</md-icon>View</md-button>
                                                               {{--<md-button ng-click="export(year,driver_id);"  class="md-raised"> <md-icon>speaker_notes</md-icon> Export</md-button>--}}
                                                          </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-inline form-group " style="float: right;">
                                                    <label for="filter-list">Search </label>
                                                    <input type="text" class="form-control" ng-model="filterlist" placeholder="Search">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-2 text-left">
                                            <label for="">Show
                                                <select ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);" class="form-control pagiantion">
                                                </select>
                                                entries
                                            </label>
                                         </div>
                                         <div class="col-sm-8 text-center">
                                            <h2 class="text-primary" style="text-decoration: underline;">Fair Register  [[curMonth]]</h2>
                                         </div>
                                         <div class="col-sm-2">

                                         </div>
                                    </div>

                                    <table id="myTable" class="tablesaw table-bordered table-hover table"  data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                        <thead>
                                            <tr>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">SlNo</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Candidate</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="2">Hospital</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Driver</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">[[preMonth]]</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">[[curMonth]]</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">[[nextMonth]]</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="7">Remark</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="report in listCount  = (reports | filter:filterlist) | orderBy:'-name'  | pagination: currentPage : numPerPage">
                                                <td>[[numPerPage *(currentPage-1)+$index+1]]</td>
                                                <td class="title">[[report.name]]</td>
                                                <td>[[report.hospital]]</td>
                                                <td>[[report.driver]]</td>
                                                <td class="text-center" ng-class="report.preMonth=='NPD' ? 'text-danger' : 'text-primary'">[[report.preMonth]] <span ng-if="report.preMonth != 'NPD'"> KD</span></td>
                                                <td class="text-center" ng-class="report.curMonth=='NPD' ? 'text-danger' : 'text-primary'"> <input type="text" class="form-control" ng-model="report.curMonth"/></td>
                                                <td class="text-center" ng-class="report.nextMonth=='NPD' ? 'text-danger' : 'text-primary'"><input type="text" class="form-control" ng-model="report.nextMonth"/></td>
                                                <td><input type="text" class="form-control" ng-model="report.remark"/></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="clearfix pull-left" ng-show="reports.length > numPerPage">
                                        <pagination
                                            ng-model="currentPage"
                                            total-items="listCount.length"
                                            max-size="maxSize"
                                            items-per-page="numPerPage"
                                            boundary-links="true"
                                            class="pagination-sm pull-right"
                                            previous-text="&lsaquo;"
                                            next-text="&rsaquo;"
                                            first-text="&laquo;"
                                            last-text="&raquo;"
                                            ></pagination>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div ng-show="showProgress" class="row text-center">
                        <div class="loader"></div>
                    </div>
                    <div ng-show="reports.length>0" class="row text-center">
                         <md-button ng-click="saveFair(reports,month,$event);" class="col-lg-12 md-raised md-accent"> <md-icon>note_add</md-icon>Save Fair</md-button>
                    </div>

                    <br/><br/>
             </div>
        </div>

        </div>
        <div ng-hide="permission">
            <div  layout="row" layout-padding layout-wrap layout-fill>
                <div md-whiteframe="3" class="padded alert">
                    <strong>So...Sorry!</strong> You have no Permission to View This Page.
                </div>
            </div>
        </div>

  </md-content>
</section>

@section('scripts')

@stop

@stop
@extends('layout.main')
@include('app.partial.message')
@section('content')

  <!--
        ...................TRANSPORTATION ACCOUNT......................................
  -->
         <div class="row bg-title">
              <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <h4 class="page-title">Transportation-Fair Register</h4> </div>
              <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="{{URL::route('logout')}}"  class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Logout</a>
                  <ol class="breadcrumb">
                      <li><a href="/">Accounts</a></li>
                      <li class="active">Fair Register</li>
                  </ol>
              </div>
              <!-- /.col-lg-12 -->
          </div>
   <section id="content" role="content"  layout="column" layout-padding md-scroll-y style="overflow: auto;">
      <md-content id="popupContainer"  ng-controller="FairController" class="md-whiteframe-z2" ng-cloak>
        <div ng-show="permission">
        <div class="row">

             <div class="col-md-12">
                    <div class="box">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="row pull-right">
                                <md-button ng-if="user.permissions.fair.write =='true'" ng-hide="fairedit" class="md-raised md-small md-primary" ng-click="newFair();"> <md-icon>note_add</md-icon> Add Fair</md-button>
                            </div>
                            <br/>
                            <div ng-show="fairedit">
                                <div class="col-md-12" layout="row">
                                    <md-input-container class="md-block" flex-gt-xs>
                                       <mdp-date-picker name="month" mdp-placeholder="Date" ng-change="getFair(month);" required=""   mdp-format="DD/MM/YYYY"  ng-model="month"></mdp-date-picker>
                                    </md-input-container>
                                     <div ng-show="showProgress" class="row text-center">
                                        <div class="loader"></div>
                                     </div>
                                </div>
                                <table id="myTable" class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                    <thead>
                                        <tr>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">#</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Date</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Candidate</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Driver</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Amount</th>
                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Remark</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="fair in listFairs  = (newfairs | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                                            <td>[[numPerPage *(currentPage-1)+$index+1]]</td>
                                            <td class="title"><a>[[fair.date | date:'MMMM-yyyy']]</a></td>
                                            <td>
                                                <span ng-if="fair.created_at==null">[[fair.candidate]]</span>[[fair.transportation.name]]
                                                <!--{{--<md-input-container class="md-block" flex-gt-xs>
                                                     <md-select aria-label="candidate[[$index]]"  ng-model="fair.candidate_id" name="candidate_id">
                                                       @foreach($candidates as $candidate)
                                                         @if($candidate->active=='1')
                                                            <md-option value="{{$candidate->id}}">{{$candidate->name}}</md-option>
                                                         @endif
                                                       @endforeach
                                                     </md-select>
                                                </md-input-container>--}}-->
                                            </td>
                                            <td>
                                                <span ng-if="fair.created_at==null">[[fair.employee]]</span>[[fair.employee.name]]
                                                <!--{{--<md-input-container class="md-block" flex-gt-xs>--}}
                                                     {{--<md-select aria-label="driver[[$index]]" ng-model="fair.employee_id" name="employee_id">--}}
                                                       {{--@foreach($employees as $employee)--}}
                                                       {{--<md-option value="{{$employee->id}}">{{$employee->name}}</md-option>--}}
                                                       {{--@endforeach--}}
                                                     {{--</md-select>--}}
                                                {{--</md-input-container>--}}-->
                                            </td>
                                            <td>
                                                 <md-input-container class="md-block" flex-gt-xs>
                                                    <input aria-label="amount[[$index]]" ng-model="fair.amount" required="">
                                                 </md-input-container>
                                            </td>
                                            <td>
                                                 <md-input-container class="md-block" flex-gt-xs>
                                                    <input aria-label="remark[[$index]]" ng-model="fair.remark" required="">
                                                 </md-input-container>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="clearfix pull-left" ng-show="newfairs.length > numPerPage">
                                      <pagination
                                          ng-model="currentPage"
                                          total-items="listFairs.length"
                                          max-size="maxSize"
                                          items-per-page="numPerPage"
                                          boundary-links="true"
                                          class="pagination-sm pull-right"
                                          previous-text="&lsaquo;"
                                          next-text="&rsaquo;"
                                          first-text="&laquo;"
                                          last-text="&raquo;"
                                          ></pagination>
                                  </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6 text-left">
                                             <div ng-show="showProgressSave" class="row text-center">
                                                <div class="loader"></div>
                                             </div>
                                       </div>
                                      <div class="col-sm-6 text-right">
                                        <md-button class="md-raised btn-default pull-right" ng-click="cancelFair();"> <md-icon>cancel</md-icon> Cancel</md-button>
                                        <md-button ng-click="addFair(newfairs);" class="md-raised  md-primary  pull-right" > <md-icon>note_add</md-icon>Save</md-button>
                                      </div>
                                    </div>

                                </div>
                                <hr>
                            </div>
                        </div>
                    </div>

                    <div  class="box">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="white-box" style="background: transparent;">
                                    <div class="col-sm-12">
                                        <h3 class="box-title m-b-0">Fair List</h3>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Show
                                                    <select ng-model="numPerPage"  ng-init="numPerPage='5'" ng-change="changeNum(numPerPage);" class="form-control pagiantion">
                                                        <option value="5">5</option>
                                                        <option value="10">10</option>
                                                    </select>
                                                    entries
                                                </label>
                                            </div>

                                            <div class="col-md-8 ">
                                                <div class="form-inline form-group " style="float: right;">
                                                    <label for="filter-list">Search </label>
                                                    <input type="text" class="form-control" ng-model="filterlist" placeholder="Search">
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row text-center">
                                        <section  class="form-horizontal col-sm-12">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <md-input-container  flex-gt-xs>
                                                       <mdp-date-picker name="fromDate" mdp-placeholder="From"   mdp-format="DD/MM/YYYY"  ng-model="fromDate"></mdp-date-picker>
                                                    </md-input-container>
                                                </div>
                                                <input type="text" style="display: none;" name="fromDate" ng-model="fromDate"/>
                                                <div class="col-md-3">
                                                    <md-input-container  flex-gt-xs>
                                                       <mdp-date-picker name="toDate" mdp-placeholder="To"   mdp-format="DD/MM/YYYY"  ng-model="toDate"></mdp-date-picker>
                                                    </md-input-container>
                                                </div>
                                                <div class="col-md-2">
                                                    <md-input-container class="md-block" flex-gt-xs>
                                                         <label class="control-label1">Candidate</label>
                                                         <md-select ng-model="candidate_id" name="candidate_id" required="">
                                                           <md-option value="All">All</md-option>
                                                           @foreach($candidates as $candidate)
                                                           <md-option value="{{$candidate->id}}">{{$candidate->name}}</md-option>
                                                           @endforeach
                                                         </md-select>
                                                    </md-input-container>
                                                </div>
                                                <div class="col-md-4">
                                                      <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                           <md-button ng-click="search(fromDate,toDate,candidate_id);" class="md-raised pull-left"> <md-icon>search</md-icon> Search</md-button>
                                                           <md-button ng-click="export(fromDate,toDate,candidate_id);"  class="md-raised"> <md-icon>speaker_notes</md-icon> Export</md-button>
                                                      </div>
                                                </div>

                                            </div>
                                        </section>
                                    </div>

                                    <table id="myTable" class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                        <thead>
                                            <tr>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">#</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Date</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Amount</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Candidate</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Driver</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Remark</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Created_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Updated_at</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">User</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="fair in listCount  = (fairs | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                                                <td>[[numPerPage *(currentPage-1)+$index+1]]</td>
                                                <td class="title"><a>[[fair.date | date:'dd-MMMM-yyyy']]</a></td>
                                                <td>[[fair.amount]] KD</td>
                                                <td><a href="javascript:void(0)" ng-click="openCandidate(fair,$event);">[[fair.transportation.name]]</a></td>
                                                <td><a href="javascript:void(0)" ng-click="openEmployee(fair,$event);">[[fair.employee.name]]</a></td>
                                                <td>[[fair.remark]]</td>
                                                <td>[[fair.created_at]]</td>
                                                <td>[[fair.updated_at]]</td>
                                                <td>[[fair.user.name]]</td>
                                                <td>
                                                    <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                         <md-button ng-if="user.permissions.fair.edit =='true'" class="md-primary md-small" ng-click="editFair(fair);"> <md-icon>mode_edit</md-icon></md-button>
                                                         <md-button ng-if="user.permissions.fair.delete =='true'" class="md-warn md-small" ng-click="deleteFair(fair,$event);"><md-icon>delete</md-icon></md-button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                     <div class="clearfix pull-left" ng-show="fairs.length > numPerPage">
                                          <pagination
                                              ng-model="currentPage"
                                              total-items="listCount.length"
                                              max-size="maxSize"
                                              items-per-page="numPerPage"
                                              boundary-links="true"
                                              class="pagination-sm pull-right"
                                              previous-text="&lsaquo;"
                                              next-text="&rsaquo;"
                                              first-text="&laquo;"
                                              last-text="&raquo;"
                                              ></pagination>
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br/><br/>
             </div>
        </div>
        </div>
        <div ng-hide="permission">
            <div  layout="row" layout-padding layout-wrap layout-fill>
                <div md-whiteframe="3" class="padded alert">
                    <strong>So...Sorry!</strong> You have no Permission to View This Page.
                </div>
            </div>
        </div>
  </md-content>
   </section>

@section('scripts')

@stop

@stop
<!DOCTYPE html>
<html lang="en"  ng-app="myApp">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="blue">
    <meta name="description" content="">
    <meta name="author" content="Shahid Neermunda">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <title>Administrator[Authenticated User: {{\Auth::user()->name}} ] - Classical International Group</title>
    <!-- Bootstrap Core CSS -->
    {!! Html::style('/bootstrap/dist/css/bootstrap.min.css') !!}
    {!! Html::style('/bootstrap-extension/css/bootstrap-extension.css') !!}
    <!-- Menu CSS -->
    {!! Html::style('/sidebar-nav/dist/sidebar-nav.min.css') !!}
    <!-- morris CSS -->
    {!! Html::style('/morrisjs/morris.css') !!}
    <!-- animation CSS -->
    {!! Html::style('/css/animate.css') !!}
    <!-- Custom CSS -->
    {!! Html::style('/css/style.css') !!}

    {!! Html::style('/tablesaw-master/dist/tablesaw.css') !!}

    {!! Html::style('/css/mdPickers.min.css') !!}

    {{--<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/0.10.0/lodash.min.js"></script>--}}

    <!-- color CSS -->
    <?php
        $theme=\App\Helper\ThemeSettings::getTheme();
    ?>
    @if(count($theme)!=0)
        @if($theme->theme==1)
            <link href="{{url('/css/colors/default.css')}}" id="theme" rel="stylesheet">
        @elseif($theme->theme==2)
            <link href="{{url('/css/colors/green.css')}}" id="theme" rel="stylesheet">
        @elseif($theme->theme==3)
            <link href="{{url('/css/colors/gray.css')}}" id="theme" rel="stylesheet">
        @elseif($theme->theme==4)
            <link href="{{url('/css/colors/blue.css')}}" id="theme" rel="stylesheet">
        @elseif($theme->theme==5)
            <link href="{{url('/css/colors/purple.css')}}" id="theme" rel="stylesheet">
        @elseif($theme->theme==6)
            <link href="{{url('/css/colors/megna.css')}}" id="theme" rel="stylesheet">
        @elseif($theme->theme==7)
            <link href="{{url('/css/colors/default-dark.css')}}" id="theme" rel="stylesheet">
        @elseif($theme->theme==8)
            <link href="{{url('/css/colors/green-dark.css')}}" id="theme" rel="stylesheet">
        @elseif($theme->theme==9)
            <link href="{{url('/css/colors/gray-dark.css')}}" id="theme" rel="stylesheet">
        @elseif($theme->theme==10)
            <link href="{{url('/css/colors/blue-dark.css')}}" id="theme" rel="stylesheet">
        @elseif($theme->theme==11)
            <link href="{{url('/css/colors/purple-dark.css')}}" id="theme" rel="stylesheet">
        @elseif($theme->theme==12)
            <link href="{{url('/css/colors/megna-dark.css')}}" id="theme" rel="stylesheet">
        @else
            <link href="{{url('/css/colors/default.css')}}" id="theme" rel="stylesheet">
        @endif
    @else
       <link href="{{url('/css/colors/default.css')}}" id="theme" rel="stylesheet">
    @endif

    {!! Html::style('/angular/angular-material.min.css') !!}
    {!! Html::style('/angular/angular-notify.css') !!}
    {!! Html::style('/angular/icon.css') !!}
    {!! Html::style('/css/custom.css') !!}
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body id="popup" style="height: auto;" class="removetop" ng-controller="HomeController">



    <md-content>
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part"><a class="logo" href="#"><b><img src="{{url('images/eliteadmin-logo.png')}}" alt="home" /></b><span class="hidden-xs"><strong>class</strong>ic</span></a></div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
                    <li>
                        <form role="search" class="app-search hidden-xs">
                            <input type="text" placeholder="Search..." class="form-control"> <a href=""><i class="fa fa-search"></i></a> </form>
                    </li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-envelope"></i>
          <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
          </a>
                        <ul class="dropdown-menu mailbox animated bounceInDown">
                            <li>
                                <div class="drop-title">You have 4 new messages</div>
                            </li>
                            <li>
                                <div class="message-center">
                                    <a href="#">
                                        <div class="user-img"> <img src="{{url('images/users/pawandeep.jpg')}}" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>
                                    </a>
                                    <a href="#">
                                        <div class="user-img"> <img src="{{url('images/users/sonu.jpg')}}" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Sonu Nigam</h5> <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span> </div>
                                    </a>
                                    <a href="#">
                                        <div class="user-img"> <img src="{{url('images/users/arijit.jpg')}}" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Arijit Sinh</h5> <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span> </div>
                                    </a>
                                    <a href="#">
                                        <div class="user-img"> <img src="{{url('images/users/pawandeep.jpg')}}" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <a class="text-center" href="javascript:void(0);"> <strong>See all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-messages -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-note"></i>
          <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
          </a>
                        <ul class="dropdown-menu dropdown-tasks animated slideInUp">
                            <li>
                                <a href="#">
                                    <div>
                                        <p> <strong>Task 1</strong> <span class="pull-right text-muted">40% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p> <strong>Task 2</strong> <span class="pull-right text-muted">20% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"> <span class="sr-only">20% Complete</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p> <strong>Task 3</strong> <span class="pull-right text-muted">60% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%"> <span class="sr-only">60% Complete (warning)</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p> <strong>Task 4</strong> <span class="pull-right text-muted">80% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"> <span class="sr-only">80% Complete (danger)</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#"> <strong>See All Tasks</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-tasks -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="{{url('images/'.\Illuminate\Support\Facades\Auth::user()->photo)}}" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">{{\Auth::user()->name}}</b> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li><a href="{{url('api/myprofile')}}"><i class="ti-user"></i>  My Profile</a></li>
                            <li><a href="{{url('api/showpassword')}}"><i class="ti-key"></i> Change Password</a></li>
                             @if(\Illuminate\Support\Facades\Auth::user()->type=='a')
                                <li><a href="{{url('api/showusers')}}"><i class="ti-settings"></i> Account Setting</a></li>
                             @endif
                            <li><a href="{{URL::route('logout')}}"><i class="fa fa-power-off"></i>  Logout</a></li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <li class="right-side-toggle"> <a class="waves-effect waves-light" href="javascript:void(0)"><i class="ti-settings"></i></a></li>
                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <?php
                    $pages=\App\Http\Controllers\Auth\UserInfoController::getPermission();
                ?>

                <ul class="nav" id="side-menu">
                    <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                        <!-- input-group -->
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
                        <!-- /input-group -->
                    </li>
                    <li class="user-pro">
                        <a href="#" class="waves-effect"><img src="{{url('images/'.\Illuminate\Support\Facades\Auth::user()->photo)}}" alt="user-img" class="img-circle"> <span class="hide-menu">{{\Auth::user()->name}}<span class="fa arrow"></span></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{url('api/myprofile')}}"><i class="ti-user"></i> My Profile</a></li>
                            <li><a href="{{url('api/showpassword')}}"><i class="ti-key"></i> Change Password</a></li>
                            @if(\Illuminate\Support\Facades\Auth::user()->type=='a')
                            <li><a href="{{url('api/showusers')}}"><i class="ti-settings"></i> Account Setting</a></li>
                            @endif
                            <li><a href="{{URL::route('logout')}}"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </li>
                    <li class="nav-small-cap m-t-10">--- Main Menu</li>
                    <li> <a href="{{url('/dashboard')}}" class="waves-effect"><i class="ti-dashboard p-r-10"></i> <span class="hide-menu">Dashboard</span></a> </li>
                    {{--<li> <a href="javascript:void(0);" class="waves-effect"><i class="icon-envelope p-r-10"></i> <span class="hide-menu"> Mailbox <span class="fa arrow"></span><span class="label label-rouded label-danger pull-right">6</span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="inbox.html">Inbox</a></li>
                            <li> <a href="inbox-detail.html">Inbox detail</a></li>
                            <li> <a href="compose.html">Compose mail</a></li>
                        </ul>
                    </li>--}}
                    <li class="nav-small-cap m-t-10">--- Professional</li>

                    <li> <a href="javascript:void(0);" class="waves-effect"><i class="ti-calendar p-r-10"></i> <span class="hide-menu"> Main Account <span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            @foreach($pages['menu'] as $page)
                                @if($page['group']=='Main Account' && $page['type']=='Main')
                                    <li> <a href="{{url('api/'.$page['route'])}}">{{$page['title']}}</a> </li>
                                @endif
                            @endforeach
                            <li> <a href="javascript:void(0)" class="waves-effect">Employees<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    @foreach($pages['menu'] as $page)
                                        @if($page['group']=='Main Account' && $page['type']=='Sub')
                                            <li> <a href="{{url('api/'.$page['route'])}}">{{$page['title']}}</a> </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </li>
                     <li> <a href="javascript:void(0);" class="waves-effect"><i class="ti-calendar p-r-10"></i> <span class="hide-menu"> Courier Account <span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                             @foreach($pages['menu'] as $page)
                                @if($page['group']=='Courier Account' && $page['type']=='Main')
                                    <li> <a href="{{url('api/'.$page['route'])}}">{{$page['title']}}</a> </li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-md p-r-10"></i> <span class="hide-menu"> Nurse Account <span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            @foreach($pages['menu'] as $page)
                                @if($page['group']=='Nurse Account' && $page['type']=='Main')
                                    <li> <a href="{{url('api/'.$page['route'])}}">{{$page['title']}}</a> </li>
                                @endif
                            @endforeach
                            <li> <a href="javascript:void(0)" class="waves-effect">Nurse<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    @foreach($pages['menu'] as $page)
                                        @if($page['group']=='Nurse Account' && $page['type']=='Sub')
                                            <li> <a href="{{url('api/'.$page['route'])}}">{{$page['title']}}</a> </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);" class="waves-effect"><i class="icon-people p-r-10"></i> <span class="hide-menu"> Rent Account <span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            @foreach($pages['menu'] as $page)
                                @if($page['group']=='Rent Account' && $page['type']=='Main')
                                    <li> <a href="{{url('api/'.$page['route'])}}">{{$page['title']}}</a> </li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);" class="waves-effect"><i class="icon-chart p-r-10"></i> <span class="hide-menu"> Transportation <span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            @foreach($pages['menu'] as $page)
                                @if($page['group']=='Transportation' && $page['type']=='Main')
                                    <li> <a href="{{url('api/'.$page['route'])}}">{{$page['title']}}</a> </li>
                                @endif
                            @endforeach
                            <li> <a href="javascript:void(0)" class="waves-effect">Candidates<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    @foreach($pages['menu'] as $page)
                                        @if($page['group']=='Transportation' && $page['type']=='Sub')
                                            <li> <a href="{{url('api/'.$page['route'])}}">{{$page['title']}}</a> </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                            @foreach($pages['menu'] as $page)
                                @if($page['group']=='Transportation' && $page['type']=='Main1')
                                    <li> <a href="{{url('api/'.$page['route'])}}">{{$page['title']}}</a> </li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                   
                    <li class="nav-small-cap">--- Extra Components</li>
                    <li> <a href="#" class="waves-effect"><i data-icon="/" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Salary<span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            @foreach($pages['menu'] as $page)
                                @if($page['group']=='Salary' && $page['type']=='Main')
                                    <li> <a href="{{url('api/'.$page['route'])}}">{{$page['title']}}</a> </li>
                                @endif
                            @endforeach
                        </ul>
                    </li>

                    <li> <a href="#" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Day Book<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            @foreach($pages['menu'] as $page)
                                @if($page['group']=='Daybook' && $page['type']=='Main')
                                    <li> <a href="{{url('api/'.$page['route'])}}">{{$page['title']}}</a> </li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                    <li class="nav-small-cap">--- Support</li>
                    {{--<li> <a href="javascript:void(0)" class="waves-effect"><i data-icon="F" class="linea-icon linea-software fa-fw"></i> <span class="hide-menu">Multi-Level Dropdown<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="javascript:void(0)">Second Level Item</a> </li>
                            <li> <a href="javascript:void(0)">Second Level Item</a> </li>
                            <li> <a href="javascript:void(0)" class="waves-effect">Third Level <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li> <a href="javascript:void(0)">Third Level Item</a> </li>
                                    <li> <a href="javascript:void(0)">Third Level Item</a> </li>
                                    <li> <a href="javascript:void(0)">Third Level Item</a> </li>
                                    <li> <a href="javascript:void(0)">Third Level Item</a> </li>
                                </ul>
                            </li>
                        </ul>
                    </li>--}}
                    <li><a href="{{URL::route('logout')}}" class="waves-effect"><i class="icon-logout fa-fw"></i> <span class="hide-menu">Logout</span></a></li>
                    <li class="hide-menu">
                        <a href="javacript:void(0);"> <span>Progress Report</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-info" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%" role="progressbar"> <span class="sr-only">85% Complete (success)</span> </div>
                            </div> <span>Leads Report</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-danger" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100" style="width: 85%" role="progressbar"> <span class="sr-only">85% Complete (success)</span> </div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper" style="padding-bottom: 0px;">
            <md-content>
            <div class="container-fluid">


                <!-- Main Content Start -->
                <div>

                    @section('content')

                    @show
                    <md-button id="FAB" ng-click="moveToTop();" class="md-fab md-fab-bottom-right" aria-label="Add a category">
                      <md-icon>swap_vert</md-icon>
                    </md-button>
                </div>
                <!--  Main Content End -->
               
                <!-- .right-sidebar -->
                <div ng-controller="ThemeController" class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul>
                                <li><b>Layout Options</b></li>

                                <li>
                                    <div class="checkbox checkbox-success">
                                        <input id="checkbox4" ng-model="newtheme.toggle" type="checkbox" class="open-close">
                                        <label for="checkbox4"> Toggle Sidebar </label>
                                    </div>
                                </li>
                            </ul>
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li ng-click="changeTheme(1);"><a href="javascript:void(0)"  data-theme="default" class="default-theme">1</a></li>
                                <li ng-click="changeTheme(2);"><a href="javascript:void(0)"  data-theme="green" class="green-theme">2</a></li>
                                <li ng-click="changeTheme(3);"><a href="javascript:void(0)"  data-theme="gray" class="yellow-theme">3</a></li>
                                <li ng-click="changeTheme(4);"><a href="javascript:void(0)"  data-theme="blue" class="blue-theme">4</a></li>
                                <li ng-click="changeTheme(5);"><a href="javascript:void(0)"  data-theme="purple" class="purple-theme">5</a></li>
                                <li ng-click="changeTheme(6);"><a href="javascript:void(0)"  data-theme="megna" class="megna-theme working">6</a></li>
                                <li class="d-block m-t-30"><b>With Dark sidebar</b></li>
                                <li ng-click="changeTheme(7);"><a href="javascript:void(0)"  data-theme="default-dark" class="default-dark-theme">7</a></li>
                                <li ng-click="changeTheme(8);"><a href="javascript:void(0)"  data-theme="green-dark" class="green-dark-theme">8</a></li>
                                <li ng-click="changeTheme(9);"><a href="javascript:void(0)"  data-theme="gray-dark" class="yellow-dark-theme">9</a></li>
                                <li ng-click="changeTheme(10);"><a href="javascript:void(0)"  data-theme="blue-dark" class="blue-dark-theme">10</a></li>
                                <li ng-click="changeTheme(11);"><a href="javascript:void(0)"  data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li ng-click="changeTheme(12);"><a href="javascript:void(0)"  data-theme="megna-dark" class="megna-dark-theme">12</a></li>
                            </ul>
                            <div class="text-center">
                                <md-button ng-click="updateTheme();" class="md-raised  md-primary">Change Settings</md-button>
                            </div>
                            <ul class="m-t-20 chatonline">
                                <?php
                                    $onlineusers=\App\Helper\OnlineUser::getOnlineUsers();
                                ?>
                                <li><b>Online Users</b></li>
                                @foreach($onlineusers as $onlineuser)
                                <li>
                                    <a href="javascript:void(0)"><img src="{{url('images/'.$onlineuser->user->photo)}}" alt="user-img" class="img-circle"> <span> {{$onlineuser->user->name}} <small class="text-success">online</small></span></a>
                                </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.right-sidebar -->
            </div>
            <!-- /.container-fluid -->
            {{--<footer class="footer text-center"> 2017 &copy; Psybo Technologies </footer>--}}
            </md-content>
        </div>
        <!-- /#page-wrapper -->
    </div>

    <!-- /#wrapper -->
    <!-- jQuery -->
    {!! Html::script('/jquery/dist/jquery.min.js') !!}

    <!-- Bootstrap Core JavaScript -->
    {!! Html::script('/bootstrap/dist/js/tether.min.js') !!}
    {!! Html::script('/bootstrap/dist/js/bootstrap.min.js') !!}
    {!! Html::script('/bootstrap-extension/js/bootstrap-extension.min.js') !!}
    <!-- Menu Plugin JavaScript -->
    {!! Html::script('/sidebar-nav/dist/sidebar-nav.min.js') !!}
    <!--slimscroll JavaScript -->
    {!! Html::script('/js/jquery.slimscroll.js') !!}
    <!--Wave Effects -->
    {!! Html::script('/js/waves.js') !!}
    <!-- Custom Theme JavaScript -->
    {!! Html::script('/js/custom.min.js') !!}
    {!! Html::script('/js/dashboard1.js') !!}

    <!--Style Switcher -->
    {!! Html::script('/tablesaw-master/dist/tablesaw.js') !!}
    {!! Html::script('tablesaw-master/dist/tablesaw-init.js') !!}
    {!! Html::script('/styleswitcher/jQuery.style.switcher.js') !!}
    {!! Html::script('/angular/moment.min.js') !!}
    {!! Html::script('/angular/angular.min.js') !!}
    {!! Html::script('/angular/lodash.js') !!}
    {!! Html::script('/angular/ui-bootstrap-tpls.min.js') !!}
    {!! Html::script('/angular/angular-resource.min.js') !!}
    {!! Html::script('/angular/angular-animate.min.js') !!}
    {!! Html::script('/angular/angular-aria.min.js') !!}
    {!! Html::script('/angular/angular-messages.min.js') !!}
    {!! Html::script('/angular/angular-material.min.js') !!}
    {!! Html::script('/angular/angular-notify.js') !!}
    {!! Html::script('/angular/mdPickers.min.js') !!}
    {!! Html::script('/angular/app.js') !!}

    @yield('scripts')
    </md-content>
</body>

</html>
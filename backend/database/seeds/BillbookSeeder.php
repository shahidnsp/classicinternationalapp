<?php

use Illuminate\Database\Seeder;

class BillbookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $batch=factory(App\Billbook::class,10)->create();
    }
}

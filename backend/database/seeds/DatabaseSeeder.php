<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $info = $this->command;
        Model::unguard();

        \App\User::create([
            'email'=>'admin@admin.com',
            'username'=>'admin',
            'password'=>'admin',
            'name'=>'Admin',
            'type'=>'a',
            'photo'=>'user.png',
            'lastLogin'=>'test',
            'permission'=>'1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111']);

        \App\Designation::create([
            'name'=>'Driver',
            'description'=>'',
            'active'=>'1',
            'default'=>'1',
            'user_id'=>'1']);

        \App\Accounthead::create([
            'name'=>'Salary Advance',
            'type'=>'expense',
            'description'=>'',
            'group'=>'Main',
            'active'=>'1',
            'default'=>'1',
            'user_id'=>'1']);

        \App\Accounthead::create([
            'name'=>'Salary',
            'type'=>'expense',
            'description'=>'',
            'group'=>'Main',
            'active'=>'1',
            'default'=>'1',
            'user_id'=>'1']);

        \App\Accounthead::create([
            'name'=>'Courier Charge(Income)',
            'type'=>'income',
            'description'=>'',
            'group'=>'Courier',
            'active'=>'1',
            'default'=>'1',
            'user_id'=>'1']);

        \App\Accounthead::create([
            'name'=>'Courier Service Charge',
            'type'=>'expense',
            'description'=>'',
            'group'=>'Courier',
            'active'=>'1',
            'default'=>'1',
            'user_id'=>'1']);

        \App\Accounthead::create([
            'name'=>'Rent Payment',
            'type'=>'expense',
            'description'=>'',
            'group'=>'Rent',
            'active'=>'1',
            'default'=>'1',
            'user_id'=>'1']);

        \App\Accounthead::create([
            'name'=>'Fair Received',
            'type'=>'income',
            'description'=>'',
            'group'=>'Transportation',
            'active'=>'1',
            'default'=>'1',
            'user_id'=>'1']);

        $info->comment('Admin user created');
        $info->error('Username : admin Password:admin');

        $info->info('User table seeding started...');
        $this->call('UserSeeder');

        $info->info('Account Head table seeding started...');
        $this->call('AccountheadSeeder');

        $info->info('Ledger Posting table seeding started...');
        $this->call('LedgerpostingSeeder');

        $info->info('Designation table seeding started...');
        $this->call('DesignationSeeder');

        $info->info('Employee table seeding started...');
        $this->call('EmployeeSeeder');

        $info->info('Rent table seeding started...');
        $this->call('RentSeeder');

        $info->info('Nursebatche table seeding started...');
        $this->call('NursebatcheSeeder');

        $info->info('Nurse table seeding started...');
        $this->call('NurseSeeder');

        $info->info('Transportation table seeding started...');
        $this->call('TransportationSeeder');

        $info->info('Hospital table seeding started...');
        $this->call('HospitalSeeder');

        $info->info('Residence table seeding started...');
        $this->call('ResidenceSeeder');

        $info->info('Batch table seeding started...');
        $this->call('BatchSeeder');

        $info->info('Theme table seeding started...');
        $this->call('ThemeSeeder');

        $info->info('Courier table seeding started...');
        $this->call('CourierSeeder');

        $info->info('Faircollection table seeding started...');
        $this->call('FaircollectionSeeder');

        $info->info('Rentcollection table seeding started...');
        $this->call('RentcollectionSeeder');

        $info->info('Salary Advance table seeding started...');
        $this->call('SalaryadvanceSeeder');

        $info->info('Salaries table seeding started...');
        $this->call('SalarieSeeder');

        $info->info('Bill Book table seeding started...');
        $this->call('BillbookSeeder');

        $info->info('Bill Book Item table seeding started...');
        $this->call('BillbookItemSeeder');

        $info->info('Attendance table seeding started...');
        $this->call('AttendanceSeeder');


        $info->error('Seeding Completed.........');
        Model::reguard();
    }
}

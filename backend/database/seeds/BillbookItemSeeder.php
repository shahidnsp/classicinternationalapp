<?php

use Illuminate\Database\Seeder;

class BillbookItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $batch=factory(App\BillbookItem::class,20)->create();
    }
}

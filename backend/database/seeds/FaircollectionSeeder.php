<?php

use Illuminate\Database\Seeder;

class FaircollectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faircollection=factory(App\Faircollection::class,10)->create();
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLedgerpostingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ledgerpostings', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('accounthead_id');
            $table->decimal('amount',10,3);
            $table->date('date');
            $table->string('remark',600)->nullable();
            $table->integer('user_id');
            $table->integer('advance_id')->nullable();
            $table->integer('salary_id')->nullable();
            $table->integer('courier_id')->nullable();
            $table->integer('rent_id')->nullable();
            $table->integer('bill_id')->nullable();
            $table->integer('fair_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ledgerpostings');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryadvancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salaryadvances', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('employee_id');
            $table->decimal('amount',10,3);
            $table->decimal('balance',10,3);
            $table->string('remark',500);
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('salaryadvances');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillbookItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billbook_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('accounthead_id');
            $table->integer('billbook_id');
            $table->decimal('amount',10,3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('billbook_items');
    }
}

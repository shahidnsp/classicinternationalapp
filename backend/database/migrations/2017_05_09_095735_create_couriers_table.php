<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('couriers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('fromaddress',500);
            $table->string('frommobile');
            $table->string('toaddress',500);
            $table->string('tomobile');
            $table->decimal('courieramount',10,3);
            $table->decimal('servicecharge',10,3);
            $table->date('date');
            $table->string('remark',500);
            $table->integer('user_id');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('couriers');
    }
}

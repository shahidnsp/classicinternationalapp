<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountheadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accountheads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->enum('type',['income','expense']);
            $table->string('description');
            $table->enum('group',['Main','Courier','Nurse','Transportation','Rent']);
            $table->tinyInteger('active')->default(1);
            $table->tinyInteger('default');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accountheads');
    }
}

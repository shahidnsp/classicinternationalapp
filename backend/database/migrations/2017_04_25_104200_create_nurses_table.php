<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nurses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('passportno');
            $table->string('mobileno1');
            $table->string('mobileno2');
            $table->string('email');
            $table->bigInteger('hospital_id');
            $table->string('remark',800);
            $table->bigInteger('nursebatch_id');
            $table->tinyInteger('active')->default(1);
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nurses');
    }
}

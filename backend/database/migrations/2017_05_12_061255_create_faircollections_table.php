<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaircollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faircollections', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('candidate_id');
            $table->integer('employee_id');
            $table->decimal('amount',10,3);
            $table->string('amountStr');
            $table->string('remark',500);
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('faircollections');
    }
}

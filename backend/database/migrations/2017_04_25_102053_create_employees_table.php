<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('mobile');
            $table->string('whatsapp');
            $table->string('email');
            $table->string('addresskuwait',800);
            $table->string('addressindia',800);
            $table->string('mobileindia');
            $table->bigInteger('designation_id');
            $table->decimal('basicpay',10,3);
            $table->decimal('overtimepay',10,3);
            $table->string('passportno');
            $table->string('visano');
            $table->string('drivinglicenseno');
            $table->string('remark',500);
            $table->tinyInteger('active')->default(1);
            $table->integer('user_id');
            $table->string('photo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employees');
    }
}

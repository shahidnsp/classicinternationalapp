<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentcollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rentcollections', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('rent_id');
            $table->decimal('amount',10,3);
            $table->string('remark',500);
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rentcollections');
    }
}

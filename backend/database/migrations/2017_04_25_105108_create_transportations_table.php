<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransportationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transportations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('mobile1');
            $table->string('mobile2');
            $table->string('mobile3');
            $table->string('whatsapp');
            $table->string('email');
            $table->string('civilid');
            $table->string('passportno');
            $table->string('photo');
            $table->bigInteger('residence_id');
            $table->bigInteger('hospital_id');
            $table->string('gender');
            $table->bigInteger('employee_id');
            $table->string('remark',500);
            $table->tinyInteger('active')->default(1);
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transportations');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'email'=>$faker->email,
        'username'=>$faker->userName,
        'password'=>'admin',
        'name'=>$faker->firstName,
        'type'=>$faker->randomElement(['a','u']),
        'photo'=>'user.png',
        'permission'=>'1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111',
        'remember_token' => str_random(10),
    ];
});

//Accounthead Seed faker
$factory->define(App\Accounthead::class,function($faker){
    return [
        'name'=>$faker->name,
        'type'=>$faker->randomElement(['income','expense']),
        'description'=>$faker->sentence,
        'group'=>'Main',//$faker->randomElement(['Main','Courier','Nurse','Transportation','Rent']),
        'active'=>$faker->randomElement([1,0]),
        'default'=>0,
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});

//Ledger Posting Seed faker
$factory->define(App\Ledgerposting::class,function($faker){
    return [
        /*'name'=>$faker->name,
        'type'=>$faker->randomElement(['income','expense']),
        'description'=>$faker->sentence,
        'group'=>$faker->randomElement(['Main','Courier','Nurse','Transportation']),
        'default'=>0,*/
        'accounthead_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'amount'=>$faker->numberBetween(10,1000),
        'date'=> \Carbon\Carbon::createFromDate(2017,04,01)->toDateTimeString(),
        'remark'=>$faker->sentence,
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});
//Designation Seed Faker
$factory->define(App\Designation::class,function($faker){
    return [
        'name'=>$faker->name,
        'description'=>$faker->sentence,
        'active'=>$faker->randomElement([1,0]),
        'default'=>0,
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});

//Employee seed faker
$factory->define(App\Employee::class,function($faker){
    return [
        'name'=>$faker->name,
        'mobile'=>$faker->phoneNumber,
        'whatsapp'=>$faker->phoneNumber,
        'email'=>$faker->email,
        'addresskuwait'=>$faker->address,
        'addressindia'=>$faker->address,
        'mobileindia'=>$faker->phoneNumber,
        'designation_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'basicpay'=>$faker->numberBetween(10,3000),
        'overtimepay'=>$faker->numberBetween(10,1000),
        'passportno'=>$faker->phoneNumber,
        'visano'=>$faker->phoneNumber,
        'drivinglicenseno'=>$faker->phoneNumber,
        'remark'=>$faker->sentence,
        'active'=>$faker->randomElement([1,0]),
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
        'photo'=>'user.png',
    ];
});

//Rent seed faker
$factory->define(App\Rent::class,function($faker){
    return [
        'officename'=>$faker->name,
        'area'=>$faker->address,
        'buildingrentamount'=>$faker->numberBetween(10,1000),
        'remark'=>$faker->sentence,
        'buildingrentername'=>$faker->name,
        'buildingrentermobile'=>$faker->phoneNumber,
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
        'active'=>$faker->randomElement([1,0]),
    ];
});
//Nursebatche seed faker
$factory->define(App\Nursebatche::class,function($faker){
    return [
        'name'       =>$faker->name,
        'description'=>$faker->sentence,
        'date'=>\Carbon\Carbon::createFromDate(2017,04,01)->toDateTimeString(),
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});
//Nurse see faker
$factory->define(App\Nurse::class,function($faker){
    return [
        'name'         =>$faker->name,
        'passportno'   =>$faker->currencyCode,
        'mobileno1'   =>$faker->phoneNumber,
        'mobileno2'   =>$faker->phoneNumber,
        'email'       =>$faker->email,
        'hospital_id' =>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'remark'      =>$faker->sentence,
        'nursebatch_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'active'=>$faker->randomElement([1,0]),
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});

//Trnasportation seed faker

$factory->define(App\Transportation::class,function($faker){
    return [
         'name'=>$faker->name,
         'mobile1'=>$faker->phoneNumber,
         'mobile2'=>$faker->phoneNumber,
         'mobile3'=>$faker->phoneNumber,
         'whatsapp'=>$faker->phoneNumber,
         'email'=>$faker->email,
         'photo'=>'user.png',
         'residence_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
         'hospital_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
         'gender'=>$faker->randomElement(['Male','Female']),
         'employee_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
         'remark'=>$faker->sentence,
         'active'=>$faker->randomElement([1,0]),
         'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});

//hospitals seed faker
$factory->define(App\Hospital::class,function($faker){
    return [
         'name'=>$faker->name,
         'address'=>$faker->address,
         'mobile1'=>$faker->phoneNumber,
         'mobile2'=>$faker->phoneNumber,
         'mobile3'=>$faker->phoneNumber,
         'remarks'=>$faker->sentence,
         'active'=>$faker->randomElement([1,0]),
         'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});
//residences seed faker

$factory->define(App\Residence::class,function($faker){
    return [
        'name'=>$faker->name,
        'address'=>$faker->address,
        'phone'=>$faker->phoneNumber,
        'remarks'=>$faker->sentence,
        'active'=>$faker->randomElement([1,0]),
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});

//Batch seed faker

$factory->define(App\Batch::class,function($faker){
    return [
        'name'=>$faker->name,
        'description'=>$faker->sentence,
        'active'=>$faker->randomElement([1,0]),
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});

//Theme seed faker

$factory->define(App\Theme::class,function($faker){
    return [
        'user_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'toggle'=>$faker->randomElement([1,0]),
        'theme'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10,11,12]),
    ];
});
//courier seed faker
$factory->define(App\Courier::class,function($faker){
    return [
        'name'=>$faker->name,
        'fromaddress'=>$faker->address,
        'frommobile'=>$faker->phoneNumber,
        'toaddress'=>$faker->address,
        'tomobile'=>$faker->phoneNumber,
        'courieramount'=>$faker->numberBetween(10,1000),
        'servicecharge'=>$faker->numberBetween(10,1000),
        'remark'=>$faker->sentence,
        'date'=>\Carbon\Carbon::createFromDate(2017,04,01)->toDateTimeString(),
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});

//faircollections seed faker
$factory->define(App\Faircollection::class,function($faker){
    return [
        'date'=>\Carbon\Carbon::createFromDate(2017,04,01)->toDateTimeString(),
        'candidate_id'=>$faker->randomElement([1,2,3,4,5]),
        'employee_id'=>$faker->randomElement([1,2,3,4,5]),
        'amount'=>$faker->numberBetween(10,1000),
        'remark'=>$faker->sentence,
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});


$factory->define(App\Rentcollection::class,function($faker){
    return [
        'date'=>\Carbon\Carbon::createFromDate(2017,04,01)->toDateTimeString(),
        'rent_id'=>$faker->randomElement([1,2,3,4,5]),
        'amount'=>$faker->numberBetween(10,1000),
        'remark'=>$faker->sentence,
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];

});


$factory->define(App\Salaryadvance::class,function($faker){
    return [
        'date'=>\Carbon\Carbon::createFromDate(2017,04,01)->toDateTimeString(),
        'employee_id'=>$faker->randomElement([1,2,3,4,5]),
        'amount'=>$faker->numberBetween(10,1000),
        'balance'=>$faker->numberBetween(10,1000),
        'remark'=>$faker->sentence,
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];

});

$factory->define(App\Salarie::class,function($faker){
    return [
        'date'=>\Carbon\Carbon::createFromDate(2017,04,01)->toDateTimeString(),
        'employee_id'=>$faker->randomElement([1,2,3,4,5]),
        'basic'=>$faker->numberBetween(10,1000),
        'advance'=>$faker->numberBetween(10,1000),
        'allowance'=>$faker->numberBetween(10,1000),
        'leavesalary'=>$faker->numberBetween(10,1000),
        'total'=>$faker->numberBetween(10,1000),
        'remark'=>$faker->sentence,
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
    ];

});

$factory->define(App\Billbook::class,function($faker){
    return [
        'date'=>\Carbon\Carbon::createFromDate(2017,04,01)->toDateTimeString(),
        'name'=>$faker->name,
        'address'=>$faker->address,
        'phone'=>$faker->phoneNumber,
        'amount'=>$faker->numberBetween(10,1000),
    ];
});

$factory->define(App\BillbookItem::class,function($faker){
    return [
        'accounthead_id'=>$faker->randomElement([1,2,3,4,5]),
        'billbook_id'=>$faker->randomElement([1,2,3,4,5]),
        'amount'=>$faker->numberBetween(10,1000),
    ];
});

$factory->define(App\Attendance::class,function($faker){
    return [
        'employee_id'=>$faker->randomElement([1,2,3,4,5]),
        'user_id'=>$faker->randomElement([1,2,3,4,5]),
        'type'=>$faker->randomElement(['Present','Absent','Half Day']),
        'date'=>$faker->date,
        'remark'=>$faker->sentence
    ];
});
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ledgerposting extends Model
{
    protected $fillable=['accounthead_id','amount','date','remark'];

    public function accounthead(){
        return $this->belongsTo('App\Accounthead','accounthead_id');
    }
    public  function user()
    {
        return $this->belongsTo('App\user');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rentcollection extends Model
{
    protected  $fillable=['date','rent_id','amount','remark'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function rent(){
        return $this->belongsTo('App\Rent');
    }

    public static function boot()
    {
        parent::boot();

        static::deleted(function($cur){
            //FOR DELETING ADVANCE IN LEDGER POSTING
            Ledgerposting::where('rent_id',$cur->id)->delete();
        });

        static::created(function($cur){

        });

        static::updating(function($cur){


        });

        static::updated(function($cur){

        });
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: shahi
 * Date: 8/5/17
 * Time: 7:46 PM
 */
namespace App\Helper;
use App\Theme;
use Illuminate\Support\Facades\Auth;

class ThemeSettings {
    public static function getTheme()
    {
        $theme=Theme::where('user_id',Auth::id())->get()->first();
//        if($theme==null){
//            $theme['toggle']=1;
//            $theme['theme']=1;
//        }
        return $theme;
    }

} 
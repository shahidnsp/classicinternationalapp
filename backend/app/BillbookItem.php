<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillbookItem extends Model
{
    public function bill(){
        return $this->belongsTo('App\Billbook','billbook_id');
    }
    public function accounthead(){
        return $this->belongsTo('App\Accounthead');
    }
}

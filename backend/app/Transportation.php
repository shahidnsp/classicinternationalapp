<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transportation extends Model
{
    protected  $fillable=[
        'name',
        'mobile1' ,
        'mobile2' ,
        'mobile3' ,
        'whatsapp',
        'email',
        'civilid',
        'passportno',
        'residence_id',
        'hospital_id',
        'gender',
        'employee_id',
        'remark'];

    public function residence(){
        return $this->belongsTo('App\Residence');
    }

    public function hospital(){
        return $this->belongsTo('App\Hospital');
    }

    public function employee(){
        return $this->belongsTo('App\Employee');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}

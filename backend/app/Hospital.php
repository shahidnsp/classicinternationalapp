<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hospital extends Model
{
    protected  $fillable=['name','address','mobile1','mobile2','mobile3','remarks'];

    public function user(){
        return $this->belongsTo('App\User');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rent extends Model
{
    protected $fillable=[
        'officename' ,
        'area' ,
        'buildingrentamount' ,
        'remark',
        'buildingrentname' ,
        'buildingrentmobile' ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}

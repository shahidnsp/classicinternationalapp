<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'home', function () {
    //return view('welcome');
    if(\Illuminate\Support\Facades\Auth::check()) {
        return redirect('/dashboard');
    } else {
        return view('auth.login');
    }
}]);

Route::get('/test',  function () {
    /*$month=\Carbon\Carbon::now();
    $start=\Carbon\Carbon::parse($month)->startOfMonth();
    $end=\Carbon\Carbon::parse($month)->endOfMonth();

    $nextStart=\Carbon\Carbon::parse($end)->addDay(1);
    $nextEnd=\Carbon\Carbon::parse($nextStart)->endOfMonth();
    return $nextEnd;*/

    return $lastDate=\Carbon\Carbon::parse('25-10-2017 12:22:22');
});

Route::get('images/{filename}', function ($filename)
{
    $file = \Illuminate\Support\Facades\Storage::get($filename);
    return response($file, 200)->header('Content-Type', 'image/jpeg');
    //return base64_encode($file);
});

//Authentication
Route::group(['prefix' => 'user'], function () {

    Route::get('login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
    Route::post('login', ['as' => 'login', 'uses' => 'Auth\AuthController@postLogin']);

    Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);
});

Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'Auth\DashboardController@index']);

//TODO add middleware to authenticate
Route::group(['middleware'=>'auth','prefix'=>'api'],function(){
//Route::group(['prefix' => 'api'], function () {
    //Rest resources
    Route::resource('accounthead', 'Auth\AccountheadController');
    Route::resource('ledgerposting', 'Auth\LedgerpostingController');
    Route::resource('designation', 'Auth\DesignationController');
    Route::resource('employee', 'Auth\EmployeeController');
    Route::resource('rent', 'Auth\RentController');
    Route::resource('nursebatche', 'Auth\NursebatcheController');
    Route::resource('nurse', 'Auth\NurseController');
    Route::resource('transportation', 'Auth\TransportationController');
    Route::resource('hospital', 'Auth\HospitalController');
    Route::resource('residence', 'Auth\ResidenceController');
    Route::resource('batch', 'Auth\BatchController');
    Route::resource('courier', 'Auth\CourierController');
    Route::resource('user', 'Auth\UserController');
    Route::resource('faircollection', 'Auth\FaircollectionController');
    Route::resource('rentcollection', 'Auth\RentcollectionController');
    Route::resource('salaryadvance', 'Auth\SalaryadvanceController');
    Route::resource('salaries', 'Auth\SalarieController');
    Route::resource('user', 'Auth\UserController');
    Route::resource('bill', 'Auth\BillbookController');
    Route::resource('attendance', 'Auth\AttendanceController');

    Route::put('changeAccountHeadStatus/{id}', 'Auth\AccountheadController@changeStatus');
    Route::put('changeDesignationStatus/{id}', 'Auth\DesignationController@changeStatus');
    Route::put('changeEmployeeStatus/{id}', 'Auth\EmployeeController@changeStatus');
    Route::put('changeBatchStatus/{id}', 'Auth\BatchController@changeStatus');
    Route::put('changeHospitalStatus/{id}', 'Auth\HospitalController@changeStatus');
    Route::put('changeNurseStatus/{id}', 'Auth\NurseController@changeStatus');
    Route::put('changeResidenceStatus/{id}', 'Auth\ResidenceController@changeStatus');
    Route::put('changeCandidateStatus/{id}', 'Auth\TransportationController@changeStatus');
    Route::put('changeRentStatus/{id}', 'Auth\RentController@changeStatus');

    Route::get('getDesignation', 'Auth\DesignationController@getDesignation');
    Route::get('getBatch', 'Auth\BatchController@getBatch');
    Route::get('getHospital', 'Auth\HospitalController@getHospital');
    Route::get('getResidence', 'Auth\ResidenceController@getResidence');
    Route::get('getDriver', 'Auth\EmployeeController@getDriver');
    Route::get('getCandidate', 'Auth\TransportationController@getCandidate');
    Route::get('getRent', 'Auth\RentController@getRent');
    Route::get('getAccountHead/{id}', 'Auth\AccountheadController@getAccountHead');

    Route::post('changetheme', 'Auth\ThemeController@changeTheme');
    Route::post('updateprofile', 'Auth\UserController@updateMyProfile');
    Route::get('getprofile', 'Auth\UserController@getMyProfile');
    Route::get('showusers', 'Auth\UserController@showMyUsers');
    Route::get('showpassword', 'Auth\UserController@showPassword');
    Route::post('checkuser', 'Auth\UserController@checkUsername');
    Route::post('resetPassword', 'Auth\UserInfoController@resetUserPassword');
    Route::post('changepassword', 'Auth\UserInfoController@changeUserPassword');

    Route::post('getBasicpay', 'Auth\EmployeeController@getBasicPay');

    //EXPORT TO EXCEL
    Route::post('excelExportLedgerPosting', 'Auth\LedgerpostingController@excelExportLedgerPosting');
    Route::post('excelExportDaybook', 'Auth\LedgerpostingController@excelExportDaybook');
    Route::post('excelExportOverallIncome', 'Auth\LedgerpostingController@excelExportOverallIncome');
    Route::post('excelExportCourier', 'Auth\CourierController@excelExportCourier');
    Route::post('excelExportRentCollection', 'Auth\RentcollectionController@excelExportRentCollection');
    Route::post('excelExportRentChart', 'Auth\RentcollectionController@excelExportRentChart');
    Route::post('excelExportFair', 'Auth\FaircollectionController@excelExportFair');
    Route::post('excelExportDutyChart', 'Auth\TransportationController@excelExportDutyChart');
    Route::post('excelExportSalary', 'Auth\SalarieController@excelExportSalary');
    Route::post('excelExportSalaryAdvance', 'Auth\SalaryadvanceController@excelExportSalaryAdvance');
    Route::post('excelExportDaybookByGroup', 'Auth\LedgerpostingController@excelExportDaybookByGroup');
    Route::post('excelExportFairRegisterReport2', 'Auth\FaircollectionController@excelExportFairRegisterReport2');

    Route::post('pdfExportDayBook', 'Auth\LedgerpostingController@createDaybookPDF');
    Route::post('pdfExportFairRegisterReport2', 'Auth\FaircollectionController@pdfExportFairRegisterReport2');
    Route::post('pdfExportCandidate', 'Auth\TransportationController@pdfExportCandidate');
    Route::post('getMyProfile', 'Auth\TransportationController@getMyProfile');



    Route::put('getPaidAndUnpaidPDF', 'Auth\TransportationController@getPaidAndUnpaidPDF');
    Route::put('getUnpaidPDF', 'Auth\TransportationController@getUnpaidPDF');
    Route::put('getPaidPDF', 'Auth\TransportationController@getPaidPDF');


    Route::get('userinfo',['as'=>'userinfo','uses'=>'Auth\UserInfoController@UserInfoController']);
    Route::get('getAllPages/{id}',['as'=>'getAllPages','uses'=>'Auth\UserInfoController@getAllPages']);
    Route::get('getPermission',['as'=>'getPermission','uses'=>'Auth\UserInfoController@getUserPermission']);
    Route::post('changePermission',['as'=>'setPermission','uses'=>'Auth\UserInfoController@setUserPermission']);


    //Report Section Start
    Route::get('dutychart', 'Auth\TransportationController@GenerateDutyChart');
    Route::post('dutychartjson', 'Auth\TransportationController@GetMonthDutyChart');
    Route::get('rentchart', 'Auth\RentcollectionController@GenerateRentChart');
    Route::get('salarychart', 'Auth\SalarieController@GenerateSalaryChart');
    //Report Section End

    //Get Routing
    Route::get('accounthead_income', 'Auth\AccountheadController@GetAccountHeadIncome');

    Route::get('accounthead_expense', 'Auth\AccountheadController@GetAccountHeadExpense');
    Route::get('getEmployees', 'Auth\EmployeeController@getEmployees');
    Route::post('getLeave', 'Auth\AttendanceController@getLeave');

    //Page Routing
    //Main Account Start
    Route::get('main-account-account-head', 'Auth\AccountheadController@showMainAccountAccountHead');

    Route::get('main-account-income', 'Auth\LedgerpostingController@showMainAccountIncome');
    Route::get('search_main_income', 'Auth\LedgerpostingController@SearchMainIncome');

    Route::get('main-account-expense', 'Auth\LedgerpostingController@showMainAccountExpense');
    Route::get('search_main_expense', 'Auth\LedgerpostingController@SearchMainExpense');

    Route::get('main-account-billbook', 'Auth\BillbookController@showMainAccountBillBook');
    Route::get('showattendance', 'Auth\AttendanceController@showAttendance');

    Route::get('showdesignation', 'Auth\DesignationController@showDesignation');
    Route::get('showemployee', 'Auth\EmployeeController@showEmployee');
    Route::get('main-account-daybook', 'Auth\LedgerpostingController@showMainDaybook');
    Route::get('getDaybookByGroup', 'Auth\LedgerpostingController@GetDaybookByGroup');


    //Main Account End

    //Nurse Account Start
    Route::get('nurse-account-account-head', 'Auth\AccountheadController@showNurseAccountAccountHead');

    Route::get('nurse-account-income', 'Auth\LedgerpostingController@showNurseAccountIncome');
    Route::get('search_nurse_income',  'Auth\LedgerpostingController@SearchNurseIncome');

    Route::get('nurse-account-expense', 'Auth\LedgerpostingController@showNurseAccountExpense');
    Route::get('search_nurse_expense','Auth\LedgerpostingController@SearchNurseExpense');

    Route::get('showbatch', 'Auth\BatchController@showBatch');
    Route::get('showhospital', 'Auth\HospitalController@showHospital');
    Route::get('shownurse', 'Auth\NurseController@showNurse');

    Route::get('nurse-account-daybook', 'Auth\LedgerpostingController@showNurseDaybook');
    //Nurse Account End

    //Rent Account Start
    Route::get('rent-account-account-head', 'Auth\AccountheadController@showRentAccountAccountHead');

    Route::get('rent-account-income', 'Auth\LedgerpostingController@showRentAccountIncome');
    Route::get('search_rent_income','Auth\LedgerpostingController@SearchRentIncome');

    Route::get('rent-account-expense', 'Auth\LedgerpostingController@showRentAccountExpense');
    Route::get('search_rent_expense', 'Auth\LedgerpostingController@SearchRentExpense');
    Route::get('showrent', 'Auth\RentController@showRent');

    Route::get('showrentcollection', 'Auth\RentcollectionController@showRentCollector');
    Route::get('search_rent_collection', 'Auth\RentcollectionController@SearchRentCollection');

    Route::get('rent-account-daybook', 'Auth\LedgerpostingController@showRentDaybook');
    //Rent Account End

    //Transportation Account Start
    Route::get('transportation-account-account-head', 'Auth\AccountheadController@showTransportationAccountAccountHead');

    Route::get('transportation-account-income', 'Auth\LedgerpostingController@showTransportationAccountIncome');
    Route::get('search_transportation_income', 'Auth\LedgerpostingController@SearchTransportationIncome');

    Route::get('transportation-account-expense', 'Auth\LedgerpostingController@showTransportationAccountExpense');
    Route::get('search_transportation_expense', 'Auth\LedgerpostingController@SearchTransportationExpense');
    Route::get('showresidence', 'Auth\ResidenceController@showResidence');
    Route::get('showdriver', 'Auth\EmployeeController@showDriver');
    Route::get('showcandidate', 'Auth\TransportationController@showCandidate');
    Route::get('showfaircollector', 'Auth\FaircollectionController@showFairCollector');
    Route::get('search_transportation_fair', 'Auth\FaircollectionController@SearchTransportationFair');

    Route::get('transportation-account-daybook', 'Auth\LedgerpostingController@showTransportationDaybook');
    Route::post('showoreditfair', 'Auth\FaircollectionController@showOrEditFair');

    Route::get('fair_register_report_first', 'Auth\FaircollectionController@fairRegisterReportFirst');
    Route::post('fairregisterreportjson1', 'Auth\FaircollectionController@fairRegisterReportjson1');

    Route::get('fair_register_report_second', 'Auth\FaircollectionController@fairRegisterReportSecond');
    Route::post('fairregisterreportjson2', 'Auth\FaircollectionController@fairRegisterReportjson2');

    Route::post('savefairregister', 'Auth\FaircollectionController@saveFairRegister');

    //Transportation Account End

    //Courier Account Start
    Route::get('courier-account-account-head', 'Auth\AccountheadController@showCourierAccountAccountHead');

    Route::get('courier-account-income', 'Auth\LedgerpostingController@showCourierAccountIncome');
    Route::get('search_courier_income','Auth\LedgerpostingController@SearchCourierIncome');

    Route::get('courier-account-expense', 'Auth\LedgerpostingController@showCourierAccountExpense');
    Route::get('search_courier_expense', 'Auth\LedgerpostingController@SearchCourierExpense');

    Route::get('showcourier', 'Auth\CourierController@showCourier');
    Route::get('search_courier','Auth\CourierController@SearchCourier');

    Route::get('courier-account-daybook', 'Auth\LedgerpostingController@showCourierDaybook');
    //Courier Account End


    //User Management Start
    Route::get('myprofile', 'Auth\UserController@showMyProfile');
    //User Management End

    //Day Book Start
    Route::get('overallincome', 'Auth\LedgerpostingController@showOverallIncome');
    Route::get('search_overall_income','Auth\LedgerpostingController@SearchOverallIncome');

    Route::get('overallexpense', 'Auth\LedgerpostingController@showOverallExpense');
    Route::get('search_overall_expense','Auth\LedgerpostingController@SearchOverallExpense');

    Route::get('overalldaybook', 'Auth\LedgerpostingController@showOverallDaybook');
    Route::get('search_overall_daybook', 'Auth\LedgerpostingController@SearchOverallDaybook');
    //Day Book End

    //Salary Management Start
    Route::get('showsalaryadvance', 'Auth\SalaryadvanceController@showMySalaryAdvance');
    Route::get('search_salary_advance','Auth\SalaryadvanceController@SearchSalaryAdvance');

    Route::get('showsalary', 'Auth\SalarieController@showMySalary');
    Route::get('search_salary', 'Auth\SalarieController@SearchSalary');
    //Salary Management End








});

Route::get('template/{name}', ['as' => 'template', function ($name) {
    return view('app.dialog.'.$name);
}]);


// Catch all undefined routes and give to index.php to handle  .
Route::any('{undefinedRoute}', function ($undefinedRoute) {
    return view('errors.404');
})->where('undefinedRoute', '([A-z\d-\/_.]+)?');




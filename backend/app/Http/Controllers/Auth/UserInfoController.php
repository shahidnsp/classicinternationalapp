<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;
use Validator;
use Response;
use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Helper\UserHelper;
use App\User;


class UserInfoController extends Controller
{
    const ALL_PERMISSION = true;
    const ONLY_PAGE = true;

    public function UserInfoController()
    {
        $userInfo['id']        = Auth::id();
        $userInfo['name'] = Auth::user()->name;
        $userInfo['menu']      = UserHelper::pages(Auth::user()->permission);
        $userInfo['permissions'] = UserHelper::pages(Auth::user()->permission,false,true,true);
        $userInfo['isAdmin']   = Auth::user()->isAdmin();

        return $userInfo;
    }

    public static  function getPermission(){
        $userInfo['id']        = Auth::id();
        $userInfo['name'] = Auth::user()->name;
        $userInfo['menu']      = UserHelper::pages(Auth::user()->permission);
        $userInfo['permissions'] = UserHelper::pages(Auth::user()->permission,false,true,true);
        $userInfo['isAdmin']   = Auth::user()->isAdmin();

        return $userInfo;
    }


    public function resetUserPassword(Request $request)
    {
        try{
            $id = $request->input('id');
            $validator = Validator::make(['id'=>$id],['id'=>'required|numeric']);

            if($validator->passes()){
                $user = User::findOrFail($id);
                //TODO add settings for reset password
                $user->password = 'admin';
                if($user->save())
                    return $user;
            }
            else
                return Response::json(['error'=>$validator->errors()],400);
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json(['error'=>'User not found'],404);
        }

    }

    public function changeUserPassword(Request $request)
    {
        $data = $request->only('currentpassword','newpassword','repassword');
        $validator = Validator::make($data,[
            'currentpassword'=>'required',
            'newpassword'=>'required',
            'repassword'=>'required_with:newpassword|same:newpassword',
        ]);


        if($validator->passes()){
            try{

                $user = User::findOrFail(Auth::id());
            }
            catch(ModelNotFoundException $e)
            {
                return Response::json(['error'=>'Requested user not found'],404);
            }



            if($user->changePassword($data['currentpassword'],$data['newpassword'])){
                if($user->save()){
                    Auth::logout();
                    return Redirect::to('user/login');
                }
            }
            else
                return Redirect::back()->withInput()->withErrors(['error'=>'Old password does not match']);
                //return Response::json(['error'=>'Old password does not match'],400);
        }
        else
            return Redirect::back()->withInput()->withErrors($validator->errors()->all());//$validator->errors()->all();
    }

    public function getUserPermission(Request $request)
    {
        try{
            $id = $request->input('id');
            $validator = Validator::make(['id'=>$id],['id'=>'required|numeric']);

            if($validator->passes()){
                $user = User::findOrFail($id);

                $permission =
                    UserHelper::pages($user->permission,!self::ONLY_PAGE,self::ALL_PERMISSION);

                return Response::json($permission);
            }
            else
                return Response::json(['error'=>$validator->errors()],400);
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json(['error'=>'User not found'],404);
        }
    }

    public function setUserPermission(Request $request)
    {
        if($request->only('id','permission'))
        {
            $permission= UserHelper::makePermission($request->input('permission'));

            $user = User::findOrFail($request->input('id'));
            $user->permission = $permission;
            if($user->save())
                return $user;
            return Response::json(['error'=>'Server is down'],500);
        }

        return Response::json(['error'=>'bad request'],400);

        //return $request->input('permission');
    }

    public function getAllPages($id)
    {
        return $userPages = UserHelper::notPages(User::find($id)->permission);
    }
}

<?php

namespace App\Http\Controllers\Auth;
use App\Attendance;
use App\Designation;
use App\Employee;
use App\Salaryadvance;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Response;
use Validator;
use Excel;

class EmployeeController extends Controller
{

    /**
     * Validates given data for LegerPosting
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name'       =>'required',
            'mobile'      =>'required',
            //'whatspp'        =>'required|numeric',
            //'email'        =>'required',
            //'addresskuwait'        =>'required',
            //'addressindia'        =>'required',
            //'mobileindia'        =>'required|numeric',
            'designation_id'        =>'required|numeric',
            'basicpay'        =>'required|numeric',
           // 'overtimepay'        =>'required|numeric',

        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         $designation_id=$request->designation_id;
        if($designation_id==null)
            return Employee::with('user')->with('designation')->orderBy('created_at','desc')->get();
        else
            return Employee::with('user')->with('designation')->where('designation_id',$designation_id)->orderBy('created_at','desc')->get();
    }

    public function getEmployees(){
        return Employee::where('active',1)->get();
    }

    public function showEmployee(Request $request){
        return view('app.main.employee');
    }

    public function showDriver(Request $request){
        $num=5;
        if($request->num!=null)
            $num=$request->num;
        $drivers= Employee::where('designation_id',1)->orderBy('created_at','desc')->paginate($num);


        return view('app.transportation.driver',compact('drivers'));
    }

    /**
     * CHANGE STATUS OF EMPLOYEE
     */
    public function changeStatus($id){
        $emp=Employee::findOrfail($id);
        if($emp){
            if($emp->active==1)
                $emp->active=0;
            else
                $emp->active=1;
            if($emp->save()){
                return $emp;
            }

        }
        return "";
    }

    public function getDriver(){
        $hospital=Employee::where('active',1)->where('designation_id',1)->select('id','name')->orderBy('name')->get();
        return $hospital;
    }

    public function getBasicPay(Request $request){
        $employee_id=$request->employee_id;
        $pay=[];
        if($employee_id!=null){
            $employee=Employee::where('id',$employee_id)->select('basicpay')->get()->first();

            $pay['basic']=$employee->basicpay;

            $advance=Salaryadvance::where('employee_id',$employee_id)->sum('balance');
            if($advance!=null)
                $pay['advance']=$advance;
            else
                $pay['advance']=0;

        }else{
            $pay['basic']=0;
            $pay['advance']=0;
        }
        return $pay;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $employ=new Employee($request->all());
        $employ->user_id=Auth::id();
        $photos=$request->photos;
        if($photos!=null){
            foreach ($photos as $photo) {
                $employ->photo = $this->savePhoto($photo);
            }
        }else{
            $employ->photo='user.png';
        }
        if($employ->save()) {
            return $employ;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);

                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = mt_rand().time().'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);
                Storage::disk('local')->put($fileName,$data);
            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Employee::with('user')->with('designation')->findOrfail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $employ=Employee::findOrfail($id);
        $employ->fill($request->all());
        $employ->user_id=Auth::id();
        $photos=$request->photos;
        if($photos!=null){
            foreach ($photos as $photo) {
                $employ->photo = $this->savePhoto($photo);
            }
        }
        if($employ->update()) {
            return $employ;
        }
        return Response::json( ['error'=>'Server Down']
            ,400);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Employee::destroy($id)) {
            return Response::json(array('msg' => 'Employee record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    }

    public function exportEmployee(Request $request){
        $employee=Employee::orderBy('name')->get();

        if($employee!=null) {
            $employeeArray = [];
            $i = 0;
            foreach ($employee as $emp) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                $temp['Name'] = $emp->name;
                $temp['Mobile'] = $emp->mobile;
                $temp['Whatsapp No'] = $emp->whatsapp;
                $temp['Email'] = $emp->email;
                $temp['Address kuwaith'] = $emp->addresskuwait;
                $temp['Address India'] = $emp->addressindia;
                $temp['Mobile no India'] = $emp->mobileindia;
                $temp['Basic Pay'] = $emp->basicpay;
                $temp['Overtime Pay'] = $emp->overtimepay;
                $temp['Passport No'] = $emp->passportno;
                $temp['Visa No'] = $emp->visano;
                $temp['Driving Licence No'] = $emp->drivinglicenceno;
                $temp['remark'] = $emp->remark;
                if($emp->active==1)
                    $temp['Active'] = 'Active';
                else
                    $temp['Active'] = 'In Active';
                $temp['Created At'] = $emp->created_at;
                $temp['Updated At'] = $emp->updated_at;

                if ($emp->user != null)
                    $temp['Created By'] = $emp->user->name;
                else
                    $temp['Created By'] = '';
                $employeeArray[$i] = $temp;
                $i++;
            }

            Excel::create('Employee List', function ($excel) use ($employeeArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('Employee  List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('employee List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($employeeArray) {
                    $sheet->fromArray($employeeArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xls');
        }
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Ledgerposting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Courier;
use Illuminate\Support\Facades\Auth;
use Validator;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;

class CourierController extends Controller
{

    /**
     * Validates given data for Income
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name'       =>'required|max:255',
            'fromaddress'      =>'required',
            'frommobile'      =>'required',
            'toaddress'      =>'required',
            'tomobile'      =>'required',
            'courieramount'      =>'required',
            'servicecharge'      =>'required',
            'date'      =>'required|date',
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $from=Carbon::now()->startOfMonth();
        $to=Carbon::now()->endOfMonth();

        return Courier::with('user')->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date','desc')->get();
    }

    public function showCourier(Request $request){
        return view('app.courier.courier');
    }


    public function SearchCourier(Request $request){
        $from=new Carbon();$to=new Carbon();
        if($request->fromDate!=null)
            $from=$request->fromDate;
        if($request->toDate!=null)
            $to=$request->toDate;

        $date=strtotime($from);
        $from=date('Y/m/d',$date);

        //$date=strtotime($to);
        //$to=date('Y/m/d',$date);
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));

        $couriers= Courier::where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date','desc')->get();

        return $couriers;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $courier=new Courier($request->all());
        $courier->user_id=Auth::id();
        if($courier->save()){

            //CREATING LEDGER POSTING ENTRIES INCOME
            $ledger=new Ledgerposting();
            $ledger->accounthead_id=3;
            $ledger->amount=$courier->courieramount;
            $ledger->date=$courier->date;
            $ledger->remark=$courier->remark;
            $ledger->user_id=$courier->user_id;
            $ledger->courier_id=$courier->id;
            $ledger->save();

            //CREATING LEDGER POSTING ENTRIES EXPENSE
            $ledger=new Ledgerposting();
            $ledger->accounthead_id=4;
            $ledger->amount=$courier->servicecharge;
            $ledger->date=$courier->date;
            $ledger->remark=$courier->remark;
            $ledger->user_id=$courier->user_id;
            $ledger->courier_id=$courier->id;
            $ledger->save();

            return $courier;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $courier=Courier::findOrfail($id);
        $courier->fill($request->all());
        $courier->user_id=Auth::id();
        if($courier->update()) {

            Ledgerposting::where('courier_id',$courier->id)->delete();
            //CREATING LEDGER POSTING ENTRIES INCOME
            $ledger=new Ledgerposting();
            $ledger->accounthead_id=3;
            $ledger->amount=$courier->courieramount;
            $ledger->date=$courier->date;
            $ledger->remark=$courier->remark;
            $ledger->user_id=$courier->user_id;
            $ledger->courier_id=$courier->id;
            $ledger->save();

            //CREATING LEDGER POSTING ENTRIES EXPENSE
            $ledger=new Ledgerposting();
            $ledger->accounthead_id=4;
            $ledger->amount=$courier->servicecharge;
            $ledger->date=$courier->date;
            $ledger->remark=$courier->remark;
            $ledger->user_id=$courier->user_id;
            $ledger->courier_id=$courier->id;
            $ledger->save();

            return $courier;
        }
        return Response::json( ['error'=>'Server Down']
            ,400);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Courier::destroy($id)) {

            return Response::json(array('msg' => 'Courier record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    }

    public function excelExportCourier(Request $request){
        $from=Carbon::now();
        $to=Carbon::now();

        if($request->fromDate!=null)
            $from=$request->fromDate;
        if($request->toDate!=null)
            $to=$request->toDate;
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));

        $courier=Courier::where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date')->get();

        if($courier!=null) {
            $courierArray = [];
            $i = 0;
            foreach ($courier as $cour) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                $temp['Name'] = $cour->name;
                $temp['From Address'] = $cour->fromaddress;
                $temp['Phone(From)'] = $cour->frommobile;
                $temp['To Address'] = $cour->toaddress;
                $temp['Phone(To)'] = $cour->tomobile;
                $temp['Courier Amount'] = $cour->courieramount;
                $temp['Service Charge'] = $cour->servicecharge;
                $temp['Date'] = $cour->date;
                $temp['Remarks'] = $cour->remark;
                $temp['Created At'] = $cour->created_at;
                $temp['Updated At'] = $cour->updated_at;

                if ($cour->user != null)
                    $temp['Created By'] = $cour->user->name;
                else
                    $temp['Created By'] = '';
                $courierArray[$i] = $temp;
                $i++;
            }

            Excel::create('CourierList', function ($excel) use ($courierArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('courier  List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('courier List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($courierArray) {
                    $sheet->fromArray($courierArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xls');
        }
    }
}

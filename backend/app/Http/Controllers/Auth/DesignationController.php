<?php

namespace App\Http\Controllers\Auth;
use App\Designation;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use Response;
use Excel;

class DesignationController extends Controller
{

    /**
     * Validates given data for Designation
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name'       =>'required',
           // 'description'=>'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function index()
    {
        return Designation::with('user')->orderBy('created_at','desc')->get();
    }

    public function showDesignation(Request $request){
        return view('app.main.designation');
    }

    /**
     * CHANGE STATUS OF DESIGNATION
     */
    public function changeStatus($id){
        $desig=Designation::findOrfail($id);
        if($desig){
            if($desig->active==1)
                $desig->active=0;
            else
                $desig->active=1;
            if($desig->save()){
                return $desig;
            }

        }
        return "";
    }

    public function getDesignation(){
        $designations=Designation::where('active',1)->select('id','name')->orderBy('name')->get();
        return $designations;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=$this->validator($request->all());
        if($validator->fails())
        {
          return response::json($validator->errrors(),404);
        }
        $design =new Designation($request->all());
        $design->user_id=Auth::id();
        if($design->save()) {

            return $design;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Designation::with('user')->findOrfail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $design=Designation::findOrfail($id);
        $design->fill($request->all());
        $design->user_id=Auth::id();
        if($design->update()) {

            return $design;
        }
        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $desi=Designation::findOrfail($id);
        if($desi) {
            if($desi->default==0) {
                if (Designation::destroy($id)) {
                    return Response::json(array('msg' => 'Designation record deleted'));
                }
            }else
                return Response::json(array('default' => 'true'), 200);
        }
        return Response::json(array('error' => 'Record not found'), 400);
    }

    public function exportDesignation(Request $request){
        $designate=Designation::orderBy('name')->get();

        if($designate!=null) {
            $designateArray = [];
            $i = 0;
            foreach ($designate as $design) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                $temp['Name'] = $design->name;
                $temp['Description'] = $design->description;
                if($design->active==1)
                    $temp['Active'] = 'Active';
                else
                    $temp['Active'] = 'In Active';
                $temp['Created At'] = $design->created_at;
                $temp['Updated At'] = $design->updated_at;

                if ($design->user != null)
                    $temp['Created By'] = $design->user->name;
                else
                    $temp['Created By'] = '';
                $designateArray[$i] = $temp;
                $i++;
            }

            Excel::create('Designation List', function ($excel) use ($designateArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('Designation  List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('designation List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($designateArray) {
                    $sheet->fromArray($designateArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xls');
        }
    }
}

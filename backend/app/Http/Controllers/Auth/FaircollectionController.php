<?php

namespace App\Http\Controllers\Auth;
use App\Employee;
use App\Faircollection;
use App\Hospital;
use App\Ledgerposting;
use App\Transportation;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use Response;
use Excel;


class FaircollectionController extends Controller
{
    /**
     * Validates given data for Faircollection
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'date'       =>'required|date',
            // 'description'=>'required',
             'amount'=>'required',
             'candidate_id'=>'required|numeric',
             'employee_id'=>'required|numeric',
             //'user_id'=>'required|numeric',

        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $from=Carbon::now()->startOfMonth();
        $to=Carbon::now()->endOfMonth();
        return Faircollection::with('employee')->with('transportation')->with('user')->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date','desc')->where('amount','!=',0)->get();
    }

    public function showFairCollector(Request $request){
        $candidates=Transportation::select('id','name','active')->get();
        //$selectedcandidates=Transportation::where('active',1)->select('id','name')->get();
        //$employees=Employee::where('active',1)->where('designation_id',1)->select('id','name')->get();
        return view('app.transportation.fairs',compact('candidates'));
    }


    public function SearchTransportationFair(Request $request){
        $from=new Carbon();$to=new Carbon();

        if($request->fromDate!=null)
            $from=$request->fromDate;
        if($request->toDate!=null)
            $to=$request->toDate;
        if($request->candidate_id!=null)
            $candidate_id=$request->candidate_id;
        else
            $candidate_id="All";

        $date=strtotime($from);
        $from=date('Y/m/d',$date);

        //$date=strtotime($to);
        //$to=date('Y/m/d',$date);
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));

        if($candidate_id=='All') {
            $fairs = Faircollection::with('employee')->with('transportation')->with('user')->where('date', '>=', $from)->where('date', '<=', $to)->where('amount','!=',0)->orderBy('date', 'desc')->get();
        }else{
            $fairs = Faircollection::with('employee')->with('transportation')->with('user')->where('date', '>=', $from)->where('date', '<=', $to)->where('candidate_id', $candidate_id)->where('amount','!=',0)->orderBy('date', 'desc')->get();
        }

        return $fairs;
    }

    public function fairRegisterReportFirst(){
        //$qry='SELECT DISTINCT MONTH(date) as month,MONTHNAME(STR_TO_DATE(MONTH(date), "%m")) as monthname,YEAR(date) as year FROM faircollections GROUP BY year DESC';
        $qry='SELECT date,DATE_FORMAT(date, "%M-%Y") AS month FROM attendances GROUP BY month';
        $years=\Illuminate\Support\Facades\DB::select($qry);

        $drivers=Employee::where('active',1)->where('designation_id',1)->select('id','name')->get();
        return view('app.transportation.fair_register_report_first',compact('drivers','years'));
    }

    public function fairRegisterReportjson1(Request $request){
        $month=$request->month;
        $driver_id=$request->driver_id;
        $start=Carbon::parse($month)->startOfMonth();
        $end=Carbon::parse($month)->endOfMonth();

        $preEnd=Carbon::parse($start)->addDay(-1);
        $preStart=Carbon::parse($preEnd)->startOfMonth();

        $nextStart=Carbon::parse($end)->addDay(1);
        $nextEnd=Carbon::parse($nextStart)->endOfMonth();

        if($driver_id=='All') {
            $candidates=Transportation::where('active',1)->with('hospital')->with('employee')->get();
        }else{
            $candidates=Transportation::where('active',1)->where('employee_id',$driver_id)->with('hospital')->with('employee')->get();
        }
        $lists=[];
        $i=0;
        foreach($candidates as $candidate){
            $list['id']=$candidate->id;
            $list['candidate_id']=$candidate->id;
            $list['name']=$candidate->name;
            $list['hospital']=$candidate->hospital->name;
            $list['employee_id']=$candidate->employee->id;
            $list['driver']=$candidate->employee->name;

            $curSum = Faircollection::where('date', '>=', $start)->where('date', '<=', $end)->where('candidate_id', $candidate->id)->sum('amount');
            if($curSum==0)
                $list['curMonth']='NPD';
            else
                $list['curMonth']=$curSum;

            $preSum = Faircollection::where('date', '>=', $preStart)->where('date', '<=', $preEnd)->where('candidate_id', $candidate->id)->sum('amount');
            if($preSum==0)
                $list['preMonth']='NPD';
            else
                $list['preMonth']=$preSum;

            $nextSum = Faircollection::where('date', '>=', $nextStart)->where('date', '<=', $nextEnd)->where('candidate_id', $candidate->id)->sum('amount');
            if($nextSum==0)
                $list['nextMonth']='NPD';
            else
                $list['nextMonth']=$nextSum;

            $lastRemark = Faircollection::where('candidate_id', $candidate->id)->where('date', '>=', $start)->where('date', '<=', $end)->get()->first();
            if($lastRemark)
                $list['remark'] = $lastRemark->remark;
            else
                $list['remark'] ='';
            $lists[$i]=$list;
            $i=$i+1;
        }

        return $lists;
    }

    public function fairRegisterReportSecond(){
        $qry='SELECT date,DATE_FORMAT(date, "%M-%Y") AS month FROM attendances GROUP BY month';
        $years=\Illuminate\Support\Facades\DB::select($qry);

        $drivers=Employee::where('active',1)->where('designation_id',1)->select('id','name')->get();
        $hospitals=Hospital::where('active',1)->select('id','name')->get();
        return view('app.transportation.fair_register_report_second',compact('drivers','years','hospitals'));
    }

    public function fairRegisterReportjson2(Request $request){
        $month=$request->month;
        $driver_id=$request->driver_id;
        $status=$request->status;
        $basedon=$request->basedon;
        $hospital_id=$request->hospital_id;
        return $this->getReport($month,$driver_id,$status,$basedon,$hospital_id);
    }


    private function getReport($month,$driver_id,$status,$basedon,$hospital_id){
        $start=Carbon::parse($month)->startOfMonth();
        $end=Carbon::parse($month)->endOfMonth();

        if($basedon=='Driver') {
            if ($driver_id == 'All') {
                $candidates = Transportation::where('active', 1)->with('hospital')->with('employee')->get();
            } else {
                $candidates = Transportation::where('active', 1)->where('employee_id', $driver_id)->with('hospital')->with('employee')->get();
            }
        }else{
            if ($hospital_id == 'All') {
                $candidates = Transportation::where('active', 1)->with('hospital')->with('employee')->get();
            } else {
                $candidates = Transportation::where('active', 1)->where('hospital_id', $hospital_id)->with('hospital')->with('employee')->get();
            }
        }
        $lists=[];
        $i=0;
        if($status=='All') {
            foreach ($candidates as $candidate) {
                $list['id'] = $candidate->id;
                $list['name'] = $candidate->name;
                $list['mobile1'] = $candidate->mobile1;
                $list['hospital'] = $candidate->hospital->name;
                $list['driver'] = $candidate->employee->name;

                $curSum = Faircollection::where('date', '>=', $start)->where('date', '<=', $end)->where('candidate_id', $candidate->id)->sum('amount');
                if ($curSum == 0)
                    $list['curMonth'] = 'NPD';
                else
                    $list['curMonth'] = $curSum;

                $lastRemark = Faircollection::where('candidate_id', $candidate->id)->where('date', '>=', $start)->where('date', '<=', $end)->get()->first();
                if($lastRemark)
                    $list['remark'] = $lastRemark->remark;
                else
                    $list['remark'] ='';
                $lists[$i] = $list;
                $i = $i + 1;
            }
        }
        if($status=='Paid'){
            foreach ($candidates as $candidate) {
                $list['id'] = $candidate->id;
                $list['name'] = $candidate->name;
                $list['mobile1'] = $candidate->mobile1;
                $list['hospital'] = $candidate->hospital->name;
                $list['driver'] = $candidate->employee->name;

                $curSum = Faircollection::where('date', '>=', $start)->where('date', '<=', $end)->where('candidate_id', $candidate->id)->sum('amount');
                $list['curMonth'] = $curSum;

                $lastRemark = Faircollection::where('candidate_id', $candidate->id)->where('date', '>=', $start)->where('date', '<=', $end)->get()->first();
                if($lastRemark)
                    $list['remark'] = $lastRemark->remark;
                else
                    $list['remark'] ='';
                if($curSum!=0) {
                    $lists[$i] = $list;
                    $i = $i + 1;
                }
            }
        }

        if($status=='Not Paid') {
            foreach ($candidates as $candidate) {
                $list['id'] = $candidate->id;
                $list['name'] = $candidate->name;
                $list['mobile1'] = $candidate->mobile1;
                $list['hospital'] = $candidate->hospital->name;
                $list['driver'] = $candidate->employee->name;

                $curSum = Faircollection::where('date', '>=', $start)->where('date', '<=', $end)->where('candidate_id', $candidate->id)->sum('amount');
                if ($curSum == 0)
                    $list['curMonth'] = 'NPD';
                $lastRemark = Faircollection::where('candidate_id', $candidate->id)->where('date', '>=', $start)->where('date', '<=', $end)->get()->first();
                if($lastRemark)
                    $list['remark'] = $lastRemark->remark;
                else
                    $list['remark'] ='';
                if($curSum==0) {
                    $lists[$i] = $list;
                    $i = $i + 1;
                }
            }
        }

        return $lists;
    }

    public function excelExportFairRegisterReport2(Request $request){
        $month=$request->month;
        $driver_id=$request->driver_id;
        $status=$request->status;
        $basedon=$request->basedon;
        $hospital_id=$request->hospital_id;
        $reports=$this->getReport($month,$driver_id,$status,$basedon,$hospital_id);

        $time=strtotime($month);
        $monthName=date("F",$time);

        if($reports!=null) {
            $ledgersArray = [];
            $i = 0;
            foreach ($reports as $report) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                $temp['Candidate'] = $report['name'];
                $temp['Hospital'] = $report['hospital'];
                $temp['Driver'] = $report['driver'];
                $temp[$monthName] = $report['curMonth'];
                $temp['Remark'] = $report['remark'];
                $ledgersArray[$i] = $temp;
                $i++;
            }

            Excel::create('ReportList', function ($excel) use ($ledgersArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('ledgers  List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('ledgers List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($ledgersArray) {
                    $sheet->fromArray($ledgersArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xlsx');
        }
    }

    public function pdfExportFairRegisterReport2(Request $request){
        ini_set('max_execution_time', 60000) ;
        $month=$request->month;
        $driver_id=$request->driver_id;
        $status=$request->status;
        $basedon=$request->basedon;
        $hospital_id=$request->hospital_id;
        $reports=$this->getReport($month,$driver_id,$status,$basedon,$hospital_id);

        $time=strtotime($month);
        $monthName=date("F",$time);

        if($reports!=null) {
            $sum=$this->getSum($reports);
            $data='<html><head><style>table, td, th {border: 1px solid black;}</style></head><body>';
            $data=$data.'<h2 style="text-align:center;color:blue;">Fair List of Month '.$monthName.'</h2><br/>';
            $data=$data.'<table  style="width:100%;border-collapse: collapse;"><thead><tr style="text-align:center;color:blue;"><th>SlNo</th><th>Candidate</th><th>Hospital</th><th>Driver</th><th>'.$monthName.'</th><th>Remark</th></tr></thead>';
            $data=$data.'<tbody>';
            foreach($reports as $index=> $report){
                $data=$data.'<tr><td>'.($index+1).'</td><td>'.$report['name'].'</td><td>'.$report['hospital'].'</td><td>'.$report['driver'].'</td><td>'.$report['curMonth'].'</td><td>'.$report['remark'].'</td></tr>';
            }
            $data=$data.'<tr style="text-align:center;color:red;"><td colspan="4">Total</td><td>'.$sum.'</td></tr>';
            $data=$data.'</tbody>';
            $data=$data.'</table></body></html>';


            require_once('tcpdf_include.php');

            // create new PDF document
            $pdf = new TCPDF('P', PDF_UNIT, "A4", true, 'UTF-8', false);
            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Nicola Asuni');
            $pdf->SetTitle('TCPDF Example 006');
            $pdf->SetSubject('TCPDF Tutorial');
            $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(false);

            // set font
            $pdf->SetFont('times', '', 10);
            // add a page
            $pdf->AddPage();
            $pdf->writeHTML($data, true, false, true, false, '');
            $pdf->Output('report.pdf', 'I');
        }
    }

    private function getSum($reports){
        $sum=0;
        foreach($reports as $report){
            if($report['curMonth']!='NPD')
                $sum=$sum+$report['curMonth'];
        }
        return $sum;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $fair=new Faircollection($request->all());
        $fair->user_id=Auth::id();
        if($fair->save()) {
            return $fair;
        }*/

        $fairs=$request->fairs;
        foreach($fairs as $fair){
            //if($fair['amount']!=0) {
                $savefair = new Faircollection($fair);
                $savefair->save();
           // }
        }



        return 'OK';

       // return Response::json( ['error'=>'Server Down']
          //  ,400);
    }

    public function saveFairRegister(Request $request){
        $date=$request->date;
        $fairs=$request->fairs;

        $lastDate=Carbon::parse($date)->lastOfMonth()->addDays(1);

        $nextMonthFrom=$lastDate;
        $nextMonthTo=Carbon::parse($lastDate)->lastOfMonth();

        $fromDate=Carbon::parse($date)->firstOfMonth();
        $toDate=Carbon::parse($date)->lastOfMonth();

        Ledgerposting::where('date','>=',$fromDate)->where('date','<=',$toDate)->where('fair_id',33)->delete();
        Faircollection::where('date','>=',$fromDate)->where('date','<=',$toDate)->delete();

        Ledgerposting::where('date','>=',$nextMonthFrom)->where('date','<=',$nextMonthTo)->where('fair_id',33)->delete();
        Faircollection::where('date','>=',$nextMonthFrom)->where('date','<=',$nextMonthTo)->delete();


        foreach($fairs as $fair){

            if($fair['curMonth']!='NPD'){
                $savefair = new Faircollection();
                $savefair->date=$date;
                $savefair->candidate_id=$fair['candidate_id'];
                $savefair->employee_id=$fair['employee_id'];
                $savefair->amount=$fair['curMonth'];
                $savefair->remark=$fair['remark'];
                $savefair->user_id=Auth::id();
                if($savefair->save()){
                    //CREATING LEDGER POSTING ENTRIES INCOME
                    $ledger=new Ledgerposting();
                    $ledger->accounthead_id=33;
                    $ledger->amount=$fair['curMonth'];
                    $ledger->date=$date;
                    $ledger->remark=$fair['remark'];
                    $ledger->user_id=Auth::id();
                    $ledger->fair_id=$savefair->id;
                    $ledger->save();
                }
            }

            if($fair['nextMonth']!='NPD'){
                $savefair = new Faircollection();
                $savefair->date=$lastDate;
                $savefair->candidate_id=$fair['candidate_id'];
                $savefair->employee_id=$fair['employee_id'];
                $savefair->amount=$fair['nextMonth'];
                $savefair->remark=$fair['remark'];
                $savefair->user_id=Auth::id();
                if($savefair->save()){
                    //CREATING LEDGER POSTING ENTRIES INCOME
                    $ledger=new Ledgerposting();
                    $ledger->accounthead_id=33;
                    $ledger->amount=$fair['curMonth'];
                    $ledger->date=$lastDate;
                    $ledger->remark=$fair['remark'];
                    $ledger->user_id=Auth::id();
                    $ledger->fair_id=$savefair->id;
                    $ledger->save();
                }
            }
        }

        return 'Ok';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Faircollection::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*$validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $fair=Faircollection::findOrfail($id);
        $fair->fill($request->all());
        $fair->user_id=Auth::id();
        if($fair->update()) {

            return $fair;
        }
        return Response::json( ['error'=>'Server Down']
            ,400);*/

        $fairs=$request->fairs;
        foreach($fairs as $fair){
            $savefair= Faircollection::find($fair['id']);
            $savefair->fill($fair);
            $savefair->save();
        }

        return 'OK';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Faircollection::destroy($id)){

            return Response::json(array('msg'=>'Fair collection record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    }

    public function excelExportFair(Request $request){
        $fromDate=$request->fromDate;
        $toDate=$request->toDate;
        $candidate_id=$request->candidate_id;
        $toDate= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($toDate)));

        if($candidate_id=='All')
            $fairs=Faircollection::where('date','>=',$fromDate)->where('date','<=',$toDate)->orderBy('date')->get();
        else
            $fairs=Faircollection::where('candidate_id',$candidate_id)->where('date','>=',$fromDate)->where('date','<=',$toDate)->orderBy('date')->get();


        if($fairs!=null) {
            $fairsArray = [];
            $i = 0;
            foreach ($fairs as $fair) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                $temp['Date'] = $fair->date;
                if ($fair->transportation != null)
                    $temp['Candidate'] = $fair->transportation->name;
                else
                    $temp['Candidate'] = '';

                $temp['Amount'] = $fair->amount;
                if ($fair->employee != null)
                    $temp['Driver'] = $fair->employee->name;
                else
                    $temp['Driver'] = '';
                $temp['Remark'] = $fair->remark;
                $temp['Created At'] = $fair->created_at;
                $temp['Updated At'] = $fair->updated_at;

                if ($fair->user != null)
                    $temp['Created By'] = $fair->user->name;
                else
                    $temp['Created By'] = '';
                $fairsArray[$i] = $temp;

                $i++;
            }

            Excel::create('fair collection List', function ($excel) use ($fairsArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('fair collections  List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('fair collection List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($fairsArray) {
                    $sheet->fromArray($fairsArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xlsx');
        }
    }

    public function showOrEditFair(Request $request){
        $month=$request->month;
        $date=strtotime($month);
        $from=date('Y/m/d',$date);

        $first=\Carbon\Carbon::parse($from)->firstOfMonth();
        $last=\Carbon\Carbon::parse($from)->lastOfMonth();

        $fairLists=Faircollection::with('transportation')->with('employee')->where('date','>=',$first)->where('date','<=',$last)->get();
        $fairs = [];
        if(count($fairLists)>0){
            return $fairLists;
        }else {
            $candidates = Transportation::where('active', 1)->with('employee')->get();
            $i = 0;
            foreach ($candidates as $candidate) {
                $fair['candidate_id'] = $candidate->id;
                $fair['candidate'] = $candidate->name;
                $fair['employee_id'] = $candidate->employee->name;
                $fair['employee'] = $candidate->name;
                $fair['amount'] = 0;
                $fair['remark'] = '';
                $fair['date'] = $from;
                $fairs[$i] = $fair;
                $i++;
            }
        }
        return $fairs;
    }
}

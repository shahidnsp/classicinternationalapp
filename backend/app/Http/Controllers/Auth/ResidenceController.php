<?php

namespace App\Http\Controllers\Auth;
use App\Residence;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use Response;
use Excel;

class ResidenceController extends Controller
{
    /**
     * Validates given data for Residence
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name'       =>'required',
            'address'      =>'required'

        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  Residence::with('user')->orderBy('created_at','desc')->get();
    }

    public function showResidence(Request $request){
        return view('app.transportation.residence');
    }

    /**
     * CHANGE STATUS OF RESIDENCE
     */
    public function changeStatus($id){
        $residence=Residence::findOrfail($id);
        if($residence){
            if($residence->active==1)
                $residence->active=0;
            else
                $residence->active=1;
            if($residence->save()){
                return $residence;
            }

        }
        return "";
    }

    public function getResidence(){
        $hospital=Residence::where('active',1)->select('id','name')->orderBy('name')->get();
        return $hospital;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $resid=new Residence($request->all());
        $resid->user_id=Auth::id();
        if($resid->save()) {
            return $resid;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Residence::with('user')->findOrfail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $resid=Residence::findOrfail($id);
        $resid->fill($request->all());
        $resid->user_id=Auth::id();
        if($resid->update()){
            return $resid;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Residence::destroy($id)){
            return Response::json(array('msg'=>'Residence record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    }
    public function exportResidence(Request $request){
        $residence=Residence::orderBy('name')->get();

        if($residence!=null) {
            $residenceArray = [];
            $i = 0;
            foreach ($residence as $reside) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                $temp['Name'] = $reside->name;
                $temp['Address'] = $reside->address;
                $temp['phone'] = $reside->phone;
                $temp['remarks'] = $reside->remarks;
                if($reside->active==1)
                    $temp['Active'] = 'Active';
                else
                    $temp['Active'] = 'In Active';
                $temp['Created At'] = $reside->created_at;
                $temp['Updated At'] = $reside->updated_at;

                if ($reside->user != null)
                    $temp['Created By'] = $reside->user->name;
                else
                    $temp['Created By'] = '';
                $resideArray[$i] = $temp;
                $i++;
            }

            Excel::create(' Residence List', function ($excel) use ($resideArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('residence  List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('residence List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($resideArray) {
                    $sheet->fromArray($resideArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xls');
        }
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Accounthead;
use App\Employee;
use App\Ledgerposting;
use App\Nurse;
use App\Transportation;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Activity;

class DashboardController extends Controller
{
    function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usercount=$activities = Activity::users()->count();
        $activecandidates=Transportation::where('active',1)->count();
        $activeemployees=Employee::where('active',1)->count();
        $activenurses=Nurse::where('active',1)->count();

        $graphData=$this->getIncomeAndExpense();
        //$graphData['mainincome']=1000;
        return view('app.index',compact('usercount','activecandidates','activeemployees','activenurses','graphData'));
    }

    private function getIncomeAndExpense(){
        $data=[];
        //$from = Carbon::now()->startOfMonth();
        $from = '1965-01-01';
        $to = Carbon::now()->endOfMonth();

        $accountheads=Accounthead::where('group','Main')->where('type','income')->select('id')->get();
        $mainaccountincome=Ledgerposting::whereIn('accounthead_id',$accountheads)->where('date','>=',$from)->where('date','<=',$to)->sum('amount');
        if($mainaccountincome==null)
            $data['mainincome']=0;
        else
            $data['mainincome']=$mainaccountincome;

        $accountheadsmainexpense=Accounthead::where('group','Main')->where('type','expense')->select('id')->get();
        $mainaccountexpense=Ledgerposting::whereIn('accounthead_id',$accountheadsmainexpense)->where('date','>=',$from)->where('date','<=',$to)->sum('amount');
        if($mainaccountexpense==null)
            $data['mainexpense']=0;
        else
            $data['mainexpense']=$mainaccountexpense;


        $accountheadscourier=Accounthead::where('group','Courier')->where('type','income')->select('id')->get();
        $couieraccountincome=Ledgerposting::whereIn('accounthead_id',$accountheadscourier)->where('date','>=',$from)->where('date','<=',$to)->sum('amount');
        if($couieraccountincome==null)
            $data['courierincome']=0;
        else
            $data['courierincome']=$couieraccountincome;


        $accountheadscourierexpense=Accounthead::where('group','Courier')->where('type','expense')->select('id')->get();
        $courieraccountexpense=Ledgerposting::whereIn('accounthead_id',$accountheadscourierexpense)->where('date','>=',$from)->where('date','<=',$to)->sum('amount');
        if($courieraccountexpense==null)
            $data['courierexpense']=0;
        else
            $data['courierexpense']=$courieraccountexpense;


        $accountheadsnurse=Accounthead::where('group','Nurse')->where('type','income')->select('id')->get();
        $nurseaccountincome=Ledgerposting::whereIn('accounthead_id',$accountheadsnurse)->where('date','>=',$from)->where('date','<=',$to)->sum('amount');
        if($nurseaccountincome==null)
            $data['nurseincome']=0;
        else
            $data['nurseincome']=$nurseaccountincome;


        $accountheadsnurseexpense=Accounthead::where('group','Nurse')->where('type','expense')->select('id')->get();
        $nurseaccountexpense=Ledgerposting::whereIn('accounthead_id',$accountheadsnurseexpense)->where('date','>=',$from)->where('date','<=',$to)->sum('amount');
        if($nurseaccountexpense==null)
            $data['nurseexpense']=0;
        else
            $data['nurseexpense']=$nurseaccountexpense;


        $accountheadstrans=Accounthead::where('group','Transportation')->where('type','income')->select('id')->get();
        $transaccountincome=Ledgerposting::whereIn('accounthead_id',$accountheadstrans)->where('date','>=',$from)->where('date','<=',$to)->sum('amount');
        if($transaccountincome==null)
            $data['transincome']=0;
        else
            $data['transincome']=$transaccountincome;


        $accountheadstransexpense=Accounthead::where('group','Transportation')->where('type','expense')->select('id')->get();
        $transaccountexpense=Ledgerposting::whereIn('accounthead_id',$accountheadstransexpense)->where('date','>=',$from)->where('date','<=',$to)->sum('amount');
        if($transaccountexpense==null)
            $data['transexpense']=0;
        else
            $data['transexpense']=$transaccountexpense;


        $accountheadsrent=Accounthead::where('group','Rent')->where('type','income')->select('id')->get();
        $rentaccountincome=Ledgerposting::whereIn('accounthead_id',$accountheadsrent)->where('date','>=',$from)->where('date','<=',$to)->sum('amount');
        if($rentaccountincome==null)
            $data['rentincome']=0;
        else
            $data['rentincome']=$rentaccountincome;


        $accountheadsrentexpense=Accounthead::where('group','Rent')->where('type','expense')->select('id')->get();
        $rentaccountexpense=Ledgerposting::whereIn('accounthead_id',$accountheadsrentexpense)->where('date','>=',$from)->where('date','<=',$to)->sum('amount');
        if($rentaccountexpense==null)
            $data['rentexpense']=0;
        else
            $data['rentexpense']=$rentaccountexpense;


        return $data;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

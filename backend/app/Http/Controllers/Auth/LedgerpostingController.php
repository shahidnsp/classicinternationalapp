<?php

namespace App\Http\Controllers\Auth;

use App\Accounthead;
use App\Ledgerposting;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Validator;
use Response;
use Excel;

class LedgerpostingController extends Controller
{

    /**
     * Validates given data for LegerPosting
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'accounthead_id'       =>'required|numeric',
            'amount'      =>'required|numeric',
            'date'        =>'required|date',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $group=$request->group;
        $type=$request->type;

        $from=Carbon::now()->startOfMonth();
        $to=Carbon::now()->endOfMonth();

        if($group!=null && $type!=null) {
            $accountheads = Accounthead::where('group', $group)->where('type', $type)->select('id')->get();
            $ledger = Ledgerposting::with('accounthead')->with('user')->whereIn('accounthead_id', $accountheads)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date', 'desc')->get();
        }elseif($group==null && $type==null){
            $accountheads = Accounthead::select('id')->get();
            $ledger = Ledgerposting::with('accounthead')->with('user')->whereIn('accounthead_id', $accountheads)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date', 'desc')->get();
        }else{
            $accountheads = Accounthead::where('type', $type)->select('id')->get();
            $ledger = Ledgerposting::with('accounthead')->with('user')->whereIn('accounthead_id', $accountheads)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date', 'desc')->get();
        }
        return $ledger;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * MAIN ACCOUNT
     */
    public function showMainAccountIncome(Request $request){
//        $num=5;
//        if($request->num!=null)
//            $num=$request->num;
//        $accountheads=Accounthead::where('group','Main')->where('type','income')->select('id')->get();
//        $incomes= Ledgerposting::with('accounthead')->whereIn('accounthead_id',$accountheads)->orderBy('created_at','desc')->paginate($num);

        $accounts=Accounthead::where('group','Main')->where('type','income')->select('id','name')->get();
        return view('app.main.income',compact('accounts'));
    }

    public function SearchMainIncome(Request $request){
        $from=new Carbon();$to=new Carbon();
        $account_id="All";
        if($request->fromDate!=null)
            $from=$request->fromDate;
        if($request->toDate!=null)
            $to=$request->toDate;



        if($request->account_id!=null)
            $account_id=$request->account_id;
        else
            $account_id="All";

        $date=strtotime($from);
        $from=date('Y/m/d',$date);

        //$date=strtotime($to);
        //$to=date('Y/m/d',$date);
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));

        if($account_id=='All') {
            $accountheads = Accounthead::where('group', 'Main')->where('type', 'income')->select('id')->get();
            $incomes = Ledgerposting::with('accounthead')->with('user')->whereIn('accounthead_id', $accountheads)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date', 'desc')->get();
        }else{
            $incomes = Ledgerposting::with('accounthead')->with('user')->where('accounthead_id', $account_id)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date', 'desc')->get();
        }

        return $incomes;
    }

    public function showMainAccountExpense(Request $request){
//        $num=5;
//        if($request->num!=null)
//            $num=$request->num;
//        $accountheads=Accounthead::where('group','Main')->where('type','expense')->select('id')->get();
//        $expenses= Ledgerposting::with('accounthead')->whereIn('accounthead_id',$accountheads)->orderBy('created_at','desc')->paginate($num);

        $accounts=Accounthead::where('group','Main')->where('type','expense')->select('id','name')->get();

        return view('app.main.expense',compact('accounts'));
    }

    public function SearchMainExpense(Request $request){
        $from=new Carbon();$to=new Carbon();
        $account_id="All";
        if($request->fromDate!=null)
            $from=$request->fromDate;
        if($request->toDate!=null)
            $to=$request->toDate;
        if($request->account_id!=null)
            $account_id=$request->account_id;
        else
            $account_id="All";

        $date=strtotime($from);
        $from=date('Y/m/d',$date);

        //$date=strtotime($to);
        //$to=date('Y/m/d',$date);
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));

        if($account_id=='All') {
            $accountheads = Accounthead::where('group', 'Main')->where('type', 'expense')->select('id')->get();
            $expenses = Ledgerposting::with('accounthead')->with('user')->whereIn('accounthead_id', $accountheads)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date', 'desc')->get();
        }else{
            $expenses = Ledgerposting::with('accounthead')->with('user')->where('accounthead_id', $account_id)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date', 'desc')->get();
        }

        return $expenses;
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * NURSE ACCOUNT
     */
    public function showNurseAccountIncome(Request $request){

        $accounts=Accounthead::where('group','Nurse')->where('type','income')->select('id','name')->get();
        return view('app.nurse.income',compact('accounts'));
    }

    public function SearchNurseIncome(Request $request){
        $from=new Carbon();$to=new Carbon();

        if($request->fromDate!=null)
            $from=$request->fromDate;
        if($request->toDate!=null)
            $to=$request->toDate;
        if($request->account_id!=null)
            $account_id=$request->account_id;
        else
            $account_id="All";

        $date=strtotime($from);
        $from=date('Y/m/d',$date);

        $date=strtotime($to);
        //$to=date('Y/m/d',$date);
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));
        if($account_id=='All') {
            $accountheads = Accounthead::where('group', 'Nurse')->where('type', 'income')->select('id')->get();
            $incomes = Ledgerposting::with('accounthead')->with('user')->whereIn('accounthead_id', $accountheads)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date', 'desc')->get();
        }else{
            $incomes = Ledgerposting::with('accounthead')->with('user')->where('accounthead_id', $account_id)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date', 'desc')->get();
        }

        return $incomes;
    }

    public function showNurseAccountExpense(Request $request){

        $accounts=Accounthead::where('group','Nurse')->where('type','expense')->select('id','name')->get();
        return view('app.nurse.expense',compact('accounts'));
    }

    public function SearchNurseExpense(Request $request){
        $from=new Carbon();$to=new Carbon();

        if($request->fromDate!=null)
            $from=$request->fromDate;
        if($request->toDate!=null)
            $to=$request->toDate;
        if($request->account_id!=null)
            $account_id=$request->account_id;
        else
            $account_id="All";

        $date=strtotime($from);
        $from=date('Y/m/d',$date);

//        $date=strtotime($to);
//        $to=date('Y/m/d',$date);
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));
        if($account_id=='All') {
            $accountheads = Accounthead::where('group', 'Nurse')->where('type', 'expense')->select('id')->get();
            $expenses = Ledgerposting::with('accounthead')->with('user')->whereIn('accounthead_id', $accountheads)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date', 'desc')->get();
        }else{
            $expenses = Ledgerposting::with('accounthead')->with('user')->where('accounthead_id', $account_id)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date', 'desc')->get();
        }
        return $expenses;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * RENT ACCOUNT
     */
    public function showRentAccountIncome(Request $request){
        $accounts=Accounthead::where('group','Rent')->where('type','income')->select('id','name')->get();
        return view('app.rent.income',compact('accounts'));
    }

    public function SearchRentIncome(Request $request){
        $from=new Carbon();$to=new Carbon();

        if($request->fromDate!=null)
            $from=$request->fromDate;
        if($request->toDate!=null)
            $to=$request->toDate;
        if($request->account_id!=null)
            $account_id=$request->account_id;
        else
            $account_id="All";

        $date=strtotime($from);
        $from=date('Y/m/d',$date);

       // $date=strtotime($to);
       // $to=date('Y/m/d',$date);
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));
        if($account_id=='All') {
            $accountheads = Accounthead::where('group', 'Rent')->where('type', 'income')->select('id')->get();
            $incomes = Ledgerposting::with('accounthead')->with('user')->whereIn('accounthead_id', $accountheads)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('created_at', 'desc')->get();
        }else{
            $incomes = Ledgerposting::with('accounthead')->with('user')->where('accounthead_id', $account_id)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('created_at', 'desc')->get();
        }
        return $incomes;
    }

    public function showRentAccountExpense(Request $request){
        $accounts=Accounthead::where('group','Rent')->where('type','expense')->select('id','name')->get();
        return view('app.rent.expense',compact('accounts'));
    }

    public function SearchRentExpense(Request $request){
        $from=new Carbon();$to=new Carbon();

        if($request->fromDate!=null)
            $from=$request->fromDate;
        if($request->toDate!=null)
            $to=$request->toDate;

        if($request->account_id!=null)
            $account_id=$request->account_id;
        else
            $account_id="All";

        $date=strtotime($from);
        $from=date('Y/m/d',$date);

        //$date=strtotime($to);
        //$to=date('Y/m/d',$date);
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));
        if($account_id=='All') {
            $accountheads = Accounthead::where('group', 'Rent')->where('type', 'expense')->select('id')->get();
            $expenses = Ledgerposting::with('accounthead')->with('user')->whereIn('accounthead_id', $accountheads)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date', 'desc')->get();
        }else{
            $expenses = Ledgerposting::with('accounthead')->with('user')->where('accounthead_id', $account_id)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date', 'desc')->get();
        }

        return $expenses;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * TRANSPORTATION ACCOUNT
     */
    public function showTransportationAccountIncome(Request $request){
        $accounts=Accounthead::where('group','Transportation')->where('type','income')->select('id','name')->get();
        return view('app.transportation.income',compact('accounts'));
    }

    public function SearchTransportationIncome(Request $request){
        $from=new Carbon();$to=new Carbon();

        if($request->fromDate!=null)
            $from=$request->fromDate;
        if($request->toDate!=null)
            $to=$request->toDate;
        if($request->account_id!=null)
            $account_id=$request->account_id;
        else
            $account_id="All";

        $date=strtotime($from);
        $from=date('Y/m/d',$date);

        //$date=strtotime($to);
        //$to=date('Y/m/d',$date);
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));
        if($account_id=='All') {
            $accountheads = Accounthead::where('group', 'Transportation')->where('type', 'income')->select('id')->get();
            $incomes = Ledgerposting::with('accounthead')->with('user')->whereIn('accounthead_id', $accountheads)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date', 'desc')->get();
        }else{
            $incomes = Ledgerposting::with('accounthead')->with('user')->where('accounthead_id', $account_id)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date', 'desc')->get();
        }

        return $incomes;
    }

    public function showTransportationAccountExpense(Request $request){
        $accounts=Accounthead::where('group','Transportation')->where('type','expense')->select('id','name')->get();
        return view('app.transportation.expense',compact('accounts'));
    }

    public function SearchTransportationExpense(Request $request){
        $from=new Carbon();$to=new Carbon();

        if($request->fromDate!=null)
            $from=$request->fromDate;
        if($request->toDate!=null)
            $to=$request->toDate;
        if($request->account_id!=null)
            $account_id=$request->account_id;
        else
            $account_id="All";

        $date=strtotime($from);
        $from=date('Y/m/d',$date);

        //$date=strtotime($to);
        //$to=date('Y/m/d',$date);
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));
        if($account_id=='All') {
            $accountheads = Accounthead::where('group', 'Transportation')->where('type', 'expense')->select('id')->get();
            $expenses = Ledgerposting::with('accounthead')->with('user')->whereIn('accounthead_id', $accountheads)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('created_at', 'desc')->get();
        }else{
            $expenses = Ledgerposting::with('accounthead')->with('user')->where('accounthead_id', $account_id)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('created_at', 'desc')->get();
        }
        return $expenses;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * COURIER ACCOUNT
     */
    public function showCourierAccountIncome(Request $request){
        $accounts=Accounthead::where('group','Courier')->where('type','income')->select('id','name')->get();
        return view('app.courier.income',compact('accounts'));
    }

    public function SearchCourierIncome(Request $request){
        $from=new Carbon();$to=new Carbon();
        if($request->fromDate!=null)
            $from=$request->fromDate;
        if($request->toDate!=null)
            $to=$request->toDate;

        if($request->account_id!=null)
            $account_id=$request->account_id;
        else
            $account_id="All";

        $date=strtotime($from);
        $from=date('Y/m/d',$date);

        //$date=strtotime($to);
        //$to=date('Y/m/d',$date);
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));
        if($account_id=='All') {
            $accountheads = Accounthead::where('group', 'Courier')->where('type', 'income')->select('id')->get();
            $incomes = Ledgerposting::with('accounthead')->with('user')->whereIn('accounthead_id', $accountheads)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date', 'desc')->get();
        }else{
            $incomes = Ledgerposting::with('accounthead')->with('user')->where('accounthead_id', $account_id)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date', 'desc')->get();
        }

        return $incomes;
    }

    public function showCourierAccountExpense(Request $request){
        $accounts=Accounthead::where('group','Courier')->where('type','expense')->select('id','name')->get();
        return view('app.courier.expense',compact('accounts'));
    }

    public function SearchCourierExpense(Request $request){
        $from=new Carbon();$to=new Carbon();
        if($request->fromDate!=null)
            $from=$request->fromDate;
        if($request->toDate!=null)
            $to=$request->toDate;

        if($request->account_id!=null)
            $account_id=$request->account_id;
        else
            $account_id="All";

        $date=strtotime($from);
        $from=date('Y/m/d',$date);

        //$date=strtotime($to);
        //$to=date('Y/m/d',$date);
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));
        if($account_id=='All') {
            $accountheads = Accounthead::where('group', 'Courier')->where('type', 'expense')->select('id')->get();
            $expenses = Ledgerposting::with('accounthead')->with('user')->whereIn('accounthead_id', $accountheads)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('created_at', 'desc')->get();
        }else{
            $expenses = Ledgerposting::with('accounthead')->with('user')->where('accounthead_id', $account_id)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('created_at', 'desc')->get();
        }

        return $expenses;
    }

    public function showOverallIncome(Request $request){
        $accounts=Accounthead::where('type','income')->select('id','name')->get();
        return view('app.daybook.overallincome',compact('accounts'));
    }

    public function SearchOverallIncome(Request $request){
        $from=new Carbon();$to=new Carbon();

        if($request->fromDate!=null)
            $from=$request->fromDate;
        if($request->toDate!=null)
            $to=$request->toDate;
        if($request->account_id!=null)
            $account_id=$request->account_id;
        else
            $account_id="All";

        $date=strtotime($from);
        $from=date('Y/m/d',$date);

        //$date=strtotime($to);
        //$to=date('Y/m/d',$date);
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));
        if($account_id=='All') {
            $accountheads = Accounthead::where('type', 'income')->select('id')->get();
            $incomes = Ledgerposting::with('accounthead')->with('user')->whereIn('accounthead_id', $accountheads)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('created_at', 'desc')->get();
        }else{
            $incomes = Ledgerposting::with('accounthead')->with('user')->where('accounthead_id', $account_id)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('created_at', 'desc')->get();
        }

        return $incomes;
    }
    public function showOverallExpense(Request $request){
            $accounts=Accounthead::where('type','expense')->select('id','name')->get();
            return view('app.daybook.overallexpense',compact('accounts'));
    }

    public function SearchOverallExpense(Request $request){
        $from=new Carbon();$to=new Carbon();
        if($request->fromDate!=null)
            $from=$request->fromDate;
        if($request->toDate!=null)
            $to=$request->toDate;

        if($request->account_id!=null)
            $account_id=$request->account_id;
        else
            $account_id="All";

        $date=strtotime($from);
        $from=date('Y/m/d',$date);

        //$date=strtotime($to);
       // $to=date('Y/m/d',$date);
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));
        if($account_id=='All') {
            $accountheads = Accounthead::where('type', 'expense')->select('id')->get();
            $expenses = Ledgerposting::with('accounthead')->with('user')->whereIn('accounthead_id', $accountheads)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date', 'desc')->get();
        }else{
            $expenses = Ledgerposting::with('accounthead')->with('user')->where('accounthead_id', $account_id)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date', 'desc')->get();
        }

        return $expenses;
    }

    public function showOverallDaybook(Request $request){
        $accounts=Accounthead::select('id','name')->get();
        return view('app.daybook.daybook',compact('accounts'));
    }
    public function SearchOverallDaybook(Request $request){
        $from=new Carbon();$to=new Carbon();

        if($request->fromDate!=null)
            $from=$request->fromDate;
        if($request->toDate!=null)
            $to=$request->toDate;

        if($request->account_id!=null)
            $account_id=$request->account_id;
        else
            $account_id="All";

        $date=strtotime($from);
        $from=date('Y/m/d',$date);

        //$date=strtotime($to);
        //$to=date('Y/m/d',$date);
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));
        if($account_id=='All') {
            $accountheads = Accounthead::select('id')->get();
            $daybooks = Ledgerposting::with('accounthead')->with('user')->whereIn('accounthead_id', $accountheads)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('created_at', 'desc')->get();
        }else{
            $daybooks = Ledgerposting::with('accounthead')->with('user')->where('accounthead_id', $account_id)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('created_at', 'desc')->get();
        }
        return $daybooks;
    }

    public function showMainDaybook(Request $request){
        $accounts=Accounthead::where('group','Main')->select('id','name')->get();
        return view('app.main.daybook',compact('accounts'));
    }

    public function showCourierDaybook(Request $request){
        $accounts=Accounthead::where('group','Courier')->select('id','name')->get();
        return view('app.courier.daybook',compact('accounts'));
    }

    public function showNurseDaybook(Request $request){
        $accounts=Accounthead::where('group','Nurse')->select('id','name')->get();
        return view('app.nurse.daybook',compact('accounts'));
    }

    public function showTransportationDaybook(Request $request){
        $accounts=Accounthead::where('group','Transportation')->select('id','name')->get();
        return view('app.transportation.daybook',compact('accounts'));
    }

    public function showRentDaybook(Request $request){
        $accounts=Accounthead::where('group','Rent')->select('id','name')->get();
        return view('app.rent.daybook',compact('accounts'));
    }

    public function GetDaybookByGroup(Request $request){
        $from=new Carbon();$to=new Carbon();

        if($request->fromDate!=null)
            $from=$request->fromDate;
        if($request->toDate!=null)
            $to=$request->toDate;

        $group=$request->group;

        if($request->account_id!=null)
            $account_id=$request->account_id;
        else
            $account_id="All";

        $date=strtotime($from);
        $from=date('Y/m/d',$date);

        //$date=strtotime($to);
        //$to=date('Y/m/d',$date);
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));
        if($account_id=='All') {
            $accountheads = Accounthead::where('group', $group)->select('id')->get();
            $daybooks = Ledgerposting::with('accounthead')->with('user')->whereIn('accounthead_id', $accountheads)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('created_at', 'desc')->get();
        }else{
            $daybooks = Ledgerposting::with('accounthead')->with('user')->where('accounthead_id', $account_id)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('created_at', 'desc')->get();
        }
        return $daybooks;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return 'nothing';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $ledger=new Ledgerposting($request->all());
        $ledger->user_id=Auth::id();
        if($ledger->save()) {
           // \Session::put('message','Hai,'.Auth::user()->name.' Your Transaction saved successfully');
           // \Session::save();
            return $ledger;
        }


        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Ledgerposting::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $ledger=Ledgerposting::findOrfail($id);
        $ledger->fill($request->all());
        $ledger->user_id=Auth::id();
        if($ledger->update()) {
           // \Session::put('message','Hai,'.Auth::user()->name.' Your Transaction updated successfully');
           // \Session::save();
            return $ledger;
        }
        return Response::json( ['error'=>'Server Down']
            ,400);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Ledgerposting::destroy($id))
            return Response::json(array('msg'=>'Ledger Posting record deleted'));
        else
            return Response::json(array('error'=>'Record not found'),400);
    }



    public function excelExportLedgerPosting(Request $request){
        $fromDate=$request->fromDate;
        $toDate=$request->toDate;
        $account_id=$request->account_id;
        $type=$request->type;
        $group=$request->group;

        if($account_id=='All'){
            $head=Accounthead::where('group',$group)->where('type',$type)->select('id')->get();
            $ledgers=Ledgerposting::where('date','>=',$fromDate)->where('date','<=',$toDate)->whereIn('accounthead_id',$head)->orderBy('date')->get();
        }else{
            $ledgers=Ledgerposting::where('date','>=',$fromDate)->where('date','<=',$toDate)->where('accounthead_id',$account_id)->orderBy('date')->get();
        }

        if($ledgers!=null) {
            $ledgersArray = [];
            $i = 0;
            foreach ($ledgers as $ledg) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                if ($ledg->accounthead != null)
                    $temp['Account Head'] = $ledg->accounthead->name;
                else
                    $temp['Account Head'] = '';
                $temp['Amount'] = $ledg->amount;
                $temp['Date'] = $ledg->date;
                $temp['Remark'] = $ledg->remark;
                $temp['Created At'] = $ledg->created_at;
                $temp['Updated At'] = $ledg->updated_at;

                if ($ledg->user != null)
                    $temp['Created By'] = $ledg->user->name;
                else
                    $temp['Created By'] = '';
                $ledgersArray[$i] = $temp;
                $i++;
            }

            Excel::create('LedgerpostingList', function ($excel) use ($ledgersArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('ledgers  List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('ledgers List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($ledgersArray) {
                    $sheet->fromArray($ledgersArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xlsx');
        }
    }

    public function excelExportDaybook(Request $request){
        $fromDate=$request->fromDate;
        $toDate=$request->toDate;
        $account_id=$request->account_id;
        $toDate= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($toDate)));

        if($account_id=='All'){
            $head=Accounthead::select('id')->get();
            $ledgers=Ledgerposting::where('date','>=',$fromDate)->where('date','<=',$toDate)->whereIn('accounthead_id',$head)->orderBy('date')->get();
        }else{
            $ledgers=Ledgerposting::where('date','>=',$fromDate)->where('date','<=',$toDate)->where('accounthead_id',$account_id)->orderBy('date')->get();
        }

        if($ledgers!=null) {
            $ledgersArray = [];
            $i = 0;
            foreach ($ledgers as $ledg) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                if ($ledg->accounthead != null)
                    $temp['Account Head'] = $ledg->accounthead->name;
                else
                    $temp['Account Head'] = '';
                $temp['Amount'] = $ledg->amount;
                $temp['Date'] = $ledg->date;
                $temp['Remark'] = $ledg->remark;
                $temp['Created At'] = $ledg->created_at;
                $temp['Updated At'] = $ledg->updated_at;

                if ($ledg->user != null)
                    $temp['Created By'] = $ledg->user->name;
                else
                    $temp['Created By'] = '';
                $ledgersArray[$i] = $temp;
                $i++;
            }

            Excel::create('LedgerpostingList', function ($excel) use ($ledgersArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('ledgers  List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('ledgers List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($ledgersArray) {
                    $sheet->fromArray($ledgersArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xlsx');
        }
    }

    public function excelExportDaybookByGroup(Request $request){
        $fromDate=$request->fromDate;
        $toDate=$request->toDate;
        $account_id=$request->account_id;
        $group=$request->group;
        $toDate= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($toDate)));

        if($account_id=='All'){
            $head=Accounthead::where('group', $group)->select('id')->get();
            $ledgers=Ledgerposting::where('date','>=',$fromDate)->where('date','<=',$toDate)->whereIn('accounthead_id',$head)->orderBy('date')->get();
        }else{
            $ledgers=Ledgerposting::where('date','>=',$fromDate)->where('date','<=',$toDate)->where('accounthead_id',$account_id)->orderBy('date')->get();
        }

        if($ledgers!=null) {
            $ledgersArray = [];
            $i = 0;
            foreach ($ledgers as $ledg) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                if ($ledg->accounthead != null)
                    $temp['Account Head'] = $ledg->accounthead->name;
                else
                    $temp['Account Head'] = '';
                $temp['Amount'] = $ledg->amount;
                $temp['Date'] = $ledg->date;
                $temp['Remark'] = $ledg->remark;
                $temp['Created At'] = $ledg->created_at;
                $temp['Updated At'] = $ledg->updated_at;

                if ($ledg->user != null)
                    $temp['Created By'] = $ledg->user->name;
                else
                    $temp['Created By'] = '';
                $ledgersArray[$i] = $temp;
                $i++;
            }

            Excel::create('LedgerpostingList', function ($excel) use ($ledgersArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('ledgers  List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('ledgers List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($ledgersArray) {
                    $sheet->fromArray($ledgersArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xlsx');
        }
    }

    public function excelExportOverallIncome(Request $request){
        $fromDate=$request->fromDate;
        $toDate=$request->toDate;
        $account_id=$request->account_id;
        $type=$request->type;
        $toDate= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($toDate)));

        if($account_id=='All'){
            $head=Accounthead::where('type',$type)->select('id')->get();
            $ledgers=Ledgerposting::where('date','>=',$fromDate)->where('date','<=',$toDate)->whereIn('accounthead_id',$head)->orderBy('date')->get();
        }else{
            $ledgers=Ledgerposting::where('date','>=',$fromDate)->where('date','<=',$toDate)->where('accounthead_id',$account_id)->orderBy('date')->get();
        }

        if($ledgers!=null) {
            $ledgersArray = [];
            $i = 0;
            foreach ($ledgers as $ledg) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                if ($ledg->accounthead != null)
                    $temp['Account Head'] = $ledg->accounthead->name;
                else
                    $temp['Account Head'] = '';
                $temp['Amount'] = $ledg->amount;
                $temp['Date'] = $ledg->date;
                $temp['Remark'] = $ledg->remark;
                $temp['Created At'] = $ledg->created_at;
                $temp['Updated At'] = $ledg->updated_at;

                if ($ledg->user != null)
                    $temp['Created By'] = $ledg->user->name;
                else
                    $temp['Created By'] = '';
                $ledgersArray[$i] = $temp;
                $i++;
            }

            Excel::create('LedgerpostingList', function ($excel) use ($ledgersArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('ledgers  List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('ledgers List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($ledgersArray) {
                    $sheet->fromArray($ledgersArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xlsx');
        }
    }

    public function  createDaybookPDF(Request $request){

        $group='All';
        if($request->group!=null)
            $group=$request->group;

        $fromDate=$request->fromDate;
        $toDate=$request->toDate;
        $account_id=$request->account_id;

        $toDate= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($toDate)));

        $fromDateString=Carbon::parse($fromDate)->addDay(1);
        $totalIncome=0;
        $totalExpense=0;

        if($group=='All'){
            if($account_id=='All') {
                $income = \App\Accounthead::where('type', 'income')->select('id')->get();
                $ledgers = \App\Ledgerposting::where('date','>=',$fromDate)->where('date','<=',$toDate)->with('accounthead')->whereIn('accounthead_id', $income)->get();
            }else{
                $ledgers = \App\Ledgerposting::where('date','>=',$fromDate)->where('date','<=',$toDate)->with('accounthead')->where('accounthead_id', $account_id)->get();
            }
        }else{
            if($account_id=='All') {
                $income = \App\Accounthead::where('group',$group)->where('type', 'income')->select('id')->get();
                $ledgers = \App\Ledgerposting::where('date','>=',$fromDate)->where('date','<=',$toDate)->with('accounthead')->whereIn('accounthead_id', $income)->get();
            }else{
                $ledgers = \App\Ledgerposting::where('date','>=',$fromDate)->where('date','<=',$toDate)->with('accounthead')->where('accounthead_id', $account_id)->get();
            }
        }


        $totalIncome=$ledgers->sum('amount');
        $data= '<html><head><style>table, td, th {  border: 1px solid rgba(34, 36, 37, 0.91);  }  table {  border-collapse: collapse;  width: 100%;  }  th {  height: 50px;  }</style></head><body>';

        $data=$data.'<h2 style="text-align:center;">Day Book</h2>';
        $data=$data.'<h4 style="text-align:center;">'.\Carbon\Carbon::parse($fromDateString)->format('d-m-Y').' to '.\Carbon\Carbon::parse($toDate)->format('d-m-Y').'</h4><br/><h5 style="text-align:right;">'.date('d-M-Y h:m:s').'</h5><hr/>';
        $data=$data.'<h4 style="text-align:center;"><u>Income Report</u></h4><br/>';
        $data=$data.'<table width="100%" border="0.5"><thead><tr><th>SlNo</th><th>Date</th><th>Account</th><th>Amount</th><th>Narration</th></tr></thead>';
        $data=$data.'<tbody>';
        $id=1;
        foreach($ledgers as $ledger){
            $data=$data.'<tr><td>&nbsp;&nbsp;'.$id.'</td><td>&nbsp;&nbsp;'.\Carbon\Carbon::parse($ledger->date)->format('d/m/Y').'</td><td>'.$ledger->accounthead->name.'</td><td>&nbsp;'.$ledger->amount.'</td><td>'.$ledger->remark.'</td></tr>';
            $id=$id+1;
        }
        $data=$data.'<tr style="font-size:20px;"><td colspan="3">Total Income</td><td colspan="2">&nbsp;'.$totalIncome.'</td></tr>';
        $data=$data.'</tbody>';
        $data=$data.'</table><br/><br/>';

        $ledgers=null;
        if($group=='All'){
            if($account_id=='All') {
                $expense = \App\Accounthead::where('type', 'expense')->select('id')->get();
                $ledgers = \App\Ledgerposting::where('date','>=',$fromDate)->where('date','<=',$toDate)->with('accounthead')->whereIn('accounthead_id', $expense)->get();
            }else{
                $ledgers = \App\Ledgerposting::where('date','>=',$fromDate)->where('date','<=',$toDate)->with('accounthead')->where('accounthead_id', $account_id)->get();
            }
        }else{
            if($account_id=='All') {
                $expense = \App\Accounthead::where('group',$group)->where('type', 'expense')->select('id')->get();
                $ledgers = \App\Ledgerposting::where('date','>=',$fromDate)->where('date','<=',$toDate)->with('accounthead')->whereIn('accounthead_id', $expense)->get();
            }else{
                $ledgers = \App\Ledgerposting::where('date','>=',$fromDate)->where('date','<=',$toDate)->with('accounthead')->where('accounthead_id', $account_id)->get();
            }
        }
        $totalExpense=$ledgers->sum('amount');

        $data=$data.'<hr/>';
        $data=$data.'<h4 style="text-align:center;"><u>Expense Report</u></h4><br/>';
        $data=$data.'<table width="100%" border="0.5"><thead><tr><th>SlNo</th><th>Date</th><th>Account</th><th>Amount</th><th>Narration</th></tr></thead>';
        $data=$data.'<tbody>';
        $id=1;
        foreach($ledgers as $ledger){
            $data=$data.'<tr><td>&nbsp;&nbsp;'.$id.'</td><td>&nbsp;&nbsp;'.\Carbon\Carbon::parse($ledger->date)->format('d/m/Y').'</td><td>'.$ledger->accounthead->name.'</td><td>&nbsp;'.$ledger->amount.'</td><td>'.$ledger->remark.'</td></tr>';
            $id=$id+1;
        }
        $data=$data.'<tr style="font-size:20px;"><td colspan="3">Total Expense</td><td colspan="2">&nbsp;'.$totalExpense.'</td></tr>';
        $data=$data.'</tbody>';
        $data=$data.'</table>';
        $balance=($totalIncome-$totalExpense);
        $data=$data.'<div style="font-size:22px;"><hr>Total Income: '.$totalIncome.'<br/>Total Expense: '.$totalExpense.'<br/>Balance: '.$balance.'<hr></div>';
        $data=$data.'<br/><br/><h3 style="text-align:right;">Sign</h3>';
        $data=$data.'</body></html>';

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($data);
        $pdf->output();

        $dom_pdf = $pdf->getDomPDF();

        $canvas = $dom_pdf ->get_canvas();
        $canvas->page_text(0, 0, "Page {PAGE_NUM} of {PAGE_COUNT}", null, 10, array(0, 0, 0));
        //return $pdf->stream();

        //return $pdf->stream($student->name.' Application.pdf')->header('Content-Type','application/pdf');//download($student->name.' Application.pdf');
         return response($pdf->stream('Daybook'), 200)->header('Content-Type', 'application/pdf');
    }
}

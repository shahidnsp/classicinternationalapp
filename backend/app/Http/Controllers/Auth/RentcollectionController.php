<?php

namespace App\Http\Controllers\Auth;

use App\Ledgerposting;
use App\Rent;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Rentcollection;
use App\User;
use Validator;
use Response;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use Excel;

use App\Http\Controllers\Controller;

class RentcollectionController extends Controller
{

    /**
     * Validates given data for Rentcollection
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'date'       =>'required|date',
            // 'description'=>'required',
            'amount'=>'required|numeric',
            'rent_id'=>'required|numeric',
            //'user_id'=>'required|numeric',

        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $from=Carbon::now()->startOfMonth();
        $to=Carbon::now()->endOfMonth();
        return Rentcollection::with('rent')->with('user')->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date','desc')->get();
    }

    public function showRentCollector(Request $request){
        $rents=Rent::select('id','officename')->get();
        return view('app.rent.rentcollection',compact('rents'));
    }

    public function SearchRentCollection(Request $request){
        $from=new Carbon();$to=new Carbon();

        if($request->fromDate!=null)
            $from=$request->fromDate;
        if($request->toDate!=null)
            $to=$request->toDate;

        if($request->rent_id!=null)
            $rent_id=$request->rent_id;
        else
            $rent_id="All";

        $date=strtotime($from);
        $from=date('Y/m/d',$date);

        //$date=strtotime($to);
        //$to=date('Y/m/d',$date);
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));
        if($rent_id=='All') {
            $rentcollections= Rentcollection::with('rent')->with('user')->where('date','>=',$from)->where('date','<=',$to)->orderBy('date','desc')->get();
        }else{
            $rentcollections= Rentcollection::with('rent')->with('user')->where('rent_id',$rent_id)->where('date','>=',$from)->where('date','<=',$to)->orderBy('date','desc')->get();
        }

        return $rentcollections;
    }

    public function GenerateRentChart(Request $request){
        $year=$request->year;

        if($year==null){
            $now=new \DateTime();
            $year=$now->format('Y');
        }

        $selected=$year;

        $rents=$this->getRentChart($year);


        $qry='SELECT DISTINCT YEAR(date) as year FROM rentcollections GROUP BY year DESC';
        $years=\Illuminate\Support\Facades\DB::select($qry);

        //return $rents;
        return view('app.rent.rentchart',compact('rents','years','selected'));
    }

    private function getRentChart($year){
        $rents=Rent::all();

        foreach($rents as $index=>$rent){

            $fair1=Rentcollection::where('date','>=',$year.'/01/01')->where('date','<=',$year.'/01/31')->where('rent_id',$rent->id)->sum('amount');
            if($fair1!=null)
                $rent['jan']=$fair1;
            else
                $rent['jan']='NPD';

            $fair2=Rentcollection::where('date','>=',$year.'/02/01')->where('date','<=',$year.'/02/31')->where('rent_id',$rent->id)->sum('amount');
            if($fair2!=null)
                $rent['feb']=$fair2;
            else
                $rent['feb']='NPD';

            $fair3=Rentcollection::where('date','>=',$year.'/03/01')->where('date','<=',$year.'/03/31')->where('rent_id',$rent->id)->sum('amount');
            if($fair3!=null)
                $rent['mar']=$fair3;
            else
                $rent['mar']='NPD';

            $fair4=Rentcollection::where('date','>=',$year.'/04/01')->where('date','<=',$year.'/04/31')->where('rent_id',$rent->id)->sum('amount');
            if($fair4!=null)
                $rent['apr']=$fair4;
            else
                $rent['apr']='NPD';

            $fair5=Rentcollection::where('date','>=',$year.'/05/01')->where('date','<=',$year.'/05/31')->where('rent_id',$rent->id)->sum('amount');
            if($fair5!=null)
                $rent['may']=$fair5;
            else
                $rent['may']='NPD';

            $fair6=Rentcollection::where('date','>=',$year.'/06/01')->where('date','<=',$year.'/06/31')->where('rent_id',$rent->id)->sum('amount');
            if($fair6!=null)
                $rent['jun']=$fair6;
            else
                $rent['jun']='NPD';

            $fair7=Rentcollection::where('date','>=',$year.'/07/01')->where('date','<=',$year.'/07/31')->where('rent_id',$rent->id)->sum('amount');
            if($fair7!=null)
                $rent['jul']=$fair7;
            else
                $rent['jul']='NPD';

            $fair8=Rentcollection::where('date','>=',$year.'/08/01')->where('date','<=',$year.'/08/31')->where('rent_id',$rent->id)->sum('amount');
            if($fair8!=null)
                $rent['aug']=$fair8;
            else
                $rent['aug']='NPD';

            $fair9=Rentcollection::where('date','>=',$year.'/09/01')->where('date','<=',$year.'/09/31')->where('rent_id',$rent->id)->sum('amount');
            if($fair9!=null)
                $rent['sep']=$fair9;
            else
                $rent['sep']='NPD';

            $fair10=Rentcollection::where('date','>=',$year.'/10/01')->where('date','<=',$year.'/10/31')->where('rent_id',$rent->id)->sum('amount');
            if($fair10!=null)
                $rent['oct']=$fair10;
            else
                $rent['oct']='NPD';

            $fair11=Rentcollection::where('date','>=',$year.'/11/01')->where('date','<=',$year.'/11/31')->where('rent_id',$rent->id)->sum('amount');
            if($fair11!=null)
                $rent['nov']=$fair11;
            else
                $rent['nov']='NPD';

            $fair12=Rentcollection::where('date','>=',$year.'/12/01')->where('date','<=',$year.'/12/31')->where('rent_id',$rent->id)->sum('amount');
            if($fair12!=null)
                $rent['dec']=$fair12;
            else
                $rent['dec']='NPD';


            //unset($rents[$index]);
        }

        return $rents;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $rentcoll=new Rentcollection($request->all());
        $rentcoll->user_id=Auth::id();
        if($rentcoll->save()) {


            //CREATING LEDGER POSTING ENTRIES INCOME
            $ledger=new Ledgerposting();
            $ledger->accounthead_id=5;
            $ledger->amount=$rentcoll->amount;
            $ledger->date=$rentcoll->date;
            $ledger->remark=$rentcoll->remark;
            $ledger->user_id=$rentcoll->user_id;
            $ledger->rent_id=$rentcoll->id;
            $ledger->save();

            return $rentcoll;
        }


        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Rentcollection::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $rentcoll=Rentcollection::findOrfail($id);
        $rentcoll->fill($request->all());
        $rentcoll->user_id=Auth::id();
        if($rentcoll->update()) {


            //CREATING LEDGER POSTING ENTRIES INCOME
            Ledgerposting::where('rent_id',$rentcoll->id)->delete();

            $ledger=new Ledgerposting();
            $ledger->accounthead_id=5;
            $ledger->amount=$rentcoll->amount;
            $ledger->date=$rentcoll->date;
            $ledger->remark=$rentcoll->remark;
            $ledger->user_id=$rentcoll->user_id;
            $ledger->rent_id=$rentcoll->id;
            $ledger->save();

            return $rentcoll;
        }
        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Rentcollection::destroy($id)){
            return Response::json(array('msg'=>'rent collection record deleted'));
        }
        else
         return Response::json(array('error'=>'Record not found'),400);

    }

    public function excelExportRentCollection(Request $request){

        $fromDate=$request->fromDate;
        $toDate=$request->toDate;
        $rent_id=$request->rent_id;
        $toDate= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($toDate)));
        if($rent_id=='All')
            $rents=Rentcollection::where('date','>=',$fromDate)->where('date','<=',$toDate)->orderBy('date')->get();
        else
            $rents=Rentcollection::where('rent_id',$rent_id)->where('date','>=',$fromDate)->where('date','<=',$toDate)->orderBy('date')->get();

        if($rents!=null) {
            $rentsArray = [];
            $i = 0;
            foreach ($rents as $rent) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                $temp['Date'] = $rent->date;
                if ($rent->rent != null)
                    $temp['Office'] = $rent->rent->officename;
                else
                    $temp['Office'] = '';
                $temp['Amount'] = $rent->amount;
                $temp['Remark'] = $rent->remark;
                $temp['Created At'] = $rent->created_at;
                $temp['Updated At'] = $rent->updated_at;

                if ($rent->user != null)
                    $temp['Created By'] = $rent->user->name;
                else
                    $temp['Created By'] = '';
                $rentsArray[$i] = $temp;

                $i++;
            }

            Excel::create('rentcollectionList', function ($excel) use ($rentsArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('rent collections  List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('rent collection List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($rentsArray) {
                    $sheet->fromArray($rentsArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xls');
        }
    }

    public function excelExportRentChart(Request $request){
        $year=$request->year;

        if($year==null){
            $now=new \DateTime();
            $year=$now->format('Y');
        }

        $selected=$year;

        $rents=$this->getRentChart($year);

        if($rents!=null) {
            $rentsArray = [];
            $i = 0;
            foreach ($rents as $rent) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                $temp['Building/Office Name'] = $rent->officename;
                $temp['Basic Rent'] = $rent->buildingrentamount;
                $temp['Jan'] = $rent->jan;
                $temp['Feb'] = $rent->feb;
                $temp['Mar'] = $rent->mar;
                $temp['Apr'] = $rent->apr;
                $temp['May'] = $rent->may;
                $temp['Jun'] = $rent->jun;
                $temp['Jul'] = $rent->jul;
                $temp['Aug'] = $rent->aug;
                $temp['Sep'] = $rent->sep;
                $temp['Oct'] = $rent->oct;
                $temp['Nov'] = $rent->nov;
                $temp['Dec'] = $rent->dec;

                $rentsArray[$i] = $temp;

                $i++;
            }

            Excel::create('rentcollectionList', function ($excel) use ($rentsArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('rent collections  List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('rent collection List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($rentsArray) {
                    $sheet->fromArray($rentsArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xls');
        }

    }
}

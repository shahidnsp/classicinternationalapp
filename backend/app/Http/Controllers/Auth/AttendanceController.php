<?php

namespace App\Http\Controllers\Auth;

use App\Attendance;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;
use Auth;

class AttendanceController extends Controller
{
    /**
     * Validates given data for Income
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'date'       =>'required',
            'type'     =>'required',
            'employee_id'      =>'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fromDate=$request->fromDate;
        $toDate=$request->toDate;
        $toDate= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($toDate)));
        $employee_id=$request->employee_id;
        if($fromDate==null || $toDate==null || $employee_id==null) {
            $fromDate=Carbon::now()->startOfMonth();
            $toDate=Carbon::now()->endOfMonth();
            return Attendance::where('date','>=',$fromDate)->where('date','<=',$toDate)->with('employee')->with('user')->get();
        }else{
            if($employee_id=='All'){
                return Attendance::where('date','>=',$fromDate)->where('date','<=',$toDate)->with('employee')->with('user')->get();
            }else{
                return Attendance::where('date','>=',$fromDate)->where('date','<=',$toDate)->where('employee_id',$employee_id)->with('employee')->with('user')->get();
            }
        }
    }

    public function showAttendance(){
        return view('app.main.attendance');
    }

    public function getLeave(Request $request){
        $employee_id=$request->employee_id;
        $date=$request->date;
        $first=\Carbon\Carbon::parse($date)->startOfMonth()->subMonth()->toDateString();
        $last=\Carbon\Carbon::parse($date)->subMonth()->endOfMonth()->toDateString();

        $leave=Attendance::where('date','>=',$first)->where('date','<=',$last)->where('employee_id',$employee_id)->where('type','Absent')->count();
        $half=Attendance::where('date','>=',$first)->where('date','<=',$last)->where('employee_id',$employee_id)->where('type','Absent')->count();
        $attendance['leave']=$leave;
        $attendance['half']=$half;

        return $attendance;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $attendance=new Attendance($request->all());
        $attendance->user_id=Auth::id();
        if($attendance->save()){
            return $attendance;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $attendance=Attendance::find($id);
        $attendance->fill($request->all());
        $attendance->user_id=Auth::id();
        if($attendance->save()){
            return $attendance;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Attendance::destroy($id)){
            return Response::json(array('msg'=>'Attendance  record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    }
}

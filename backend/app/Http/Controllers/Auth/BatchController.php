<?php

namespace App\Http\Controllers\Auth;

use App\Batch;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use Response;
use Excel;

class BatchController extends Controller
{
    /**
     * Validates given data for Batch
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name'       =>'required',
            // 'description'=>'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Batch::with('user')->orderBy('created_at','desc')->get();
    }

    public function showBatch(Request $request){
        return view('app.nurse.batch');
    }

    /**
     * CHANGE STATUS OF BATCH
     */
    public function changeStatus($id){
        $batch=Batch::findOrfail($id);
        if($batch){
            if($batch->active==1)
                $batch->active=0;
            else
                $batch->active=1;
            if($batch->save()){
                return $batch;
            }

        }
        return "";
    }

    public function getBatch(){
        $designations=Batch::where('active',1)->select('id','name')->orderBy('name')->get();
        return $designations;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=$this->validator($request->all());
        if($validator->fails())
        {
            return response::json($validator->errrors(),404);
        }
        $batch =new Batch($request->all());
        $batch->user_id=Auth::id();
        if($batch->save()) {
            return $batch;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Batch::with('user')->findOrfail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $batch=Batch::findOrfail($id);
        $batch->fill($request->all());
        $batch->user_id=Auth::id();
        if($batch->update()) {
            return $batch;
        }
        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Batch::destroy($id)) {
            return Response::json(array('msg' => 'Batch record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    }

    public function exportBatch(Request $request){

        $batches=Batch::orderBy('name')->get();

        if($batches!=null) {
            $batchesArray = [];
            $i = 0;
            foreach ($batches as $batch) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                $temp['Batch Name'] = $batch->name;
                $temp['Description'] = $batch->description;
                if($batch->active==1)
                    $temp['Active'] = 'Active';
                else
                    $temp['Active'] = 'In Active';
                $temp['Created At'] = $batch->created_at;
                $temp['Updated At'] = $batch->updated_at;

                if ($batch->user != null)
                    $temp['Created By'] = $batch->user->name;
                else
                    $temp['Created By'] = '';
                $batchesArray[$i] = $temp;
                $i++;
            }

            Excel::create('BatchList', function ($excel) use ($batchesArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('Batch List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('Batch List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($batchesArray) {
                    $sheet->fromArray($batchesArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xls');
        }
    }
}

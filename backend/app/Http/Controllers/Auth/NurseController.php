<?php

namespace App\Http\Controllers\Auth;

use App\Nurse;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use Response;
use Excel;

class NurseController extends Controller
{

    /**
     * Validates given data for Nurse
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name'       =>'required',
            'passportno'      =>'required',
            //'mobileno1'        =>'required|numeric',
           // 'mobileno2'        =>'required|numeric',
           // 'email'        =>'required',
            'hospital_id'=>'required|numeric',
            'nursebatch_id'=>'required|numeric',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Nurse::with('user')->with('batch')->with('hospital')->orderBy('created_at','desc')->get();
    }

    public function showNurse(Request $request){
        return view('app.nurse.nurse');
    }
    /**
     * CHANGE STATUS OF NURSE
     */
    public function changeStatus($id){
        $nurse=Nurse::findOrfail($id);
        if($nurse){
            if($nurse->active==1)
                $nurse->active=0;
            else
                $nurse->active=1;
            if($nurse->save()){
                return $nurse;
            }
        }
        return "";
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $nurse=new Nurse($request->all());
        $nurse->user_id=Auth::id();
        if($nurse->save()){
            return $nurse;
        }


        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Nurse::with('user')->with('hospital')->with('batch')->findOrfail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $nurse=Nurse::findOrfail($id);
        $nurse->fill($request->all());
        if($nurse->update()){
            return $nurse;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if(Nurse::destroy($id)){
            return Response::json(array('msg'=>'Nurse record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    }
    public function exportNurse(Request $request){
        $nurse=Nurse::orderBy('name')->get();

        if($nurse!=null) {
            $nurseArray = [];
            $i = 0;
            foreach ($nurse as $nurs) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                $temp['Name'] = $nurs->name;
                $temp['Passport No'] = $nurs->passportno;
                $temp['Mobile  No1'] = $nurs->mobileno1;
                $temp['Mobile  No2'] = $nurs->mobileno2;
                $temp['Email'] = $nurs->email;
                $temp['remark'] = $nurs->remark;
                if($nurs->active==1)
                    $temp['Active'] = 'Active';
                else
                    $temp['Active'] = 'In Active';
                $temp['Created At'] = $nurs->created_at;
                $temp['Updated At'] = $nurs->updated_at;

                if ($nurs->user != null)
                    $temp['Created By'] = $nurs->user->name;
                else
                    $temp['Created By'] = '';
                $nurseArray[$i] = $temp;
                $i++;
            }

            Excel::create('Nurse List', function ($excel) use ($nurseArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('nurse  List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('nurse List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($nurseArray) {
                    $sheet->fromArray($nurseArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xls');
        }
    }
}

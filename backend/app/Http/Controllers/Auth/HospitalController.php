<?php

namespace App\Http\Controllers\Auth;
use App\Hospital;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use Response;
use Excel;

class HospitalController extends Controller
{

    /**
     * Validates given data for LedgerPosting
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name'       =>'required',
            'address'      =>'required',
           // 'mobile1'        =>'required|numeric',
           // 'mobile2'        =>'required|numeric',
            //'mobile3'        =>'required|numeric',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Hospital::with('user')->orderBy('created_at','desc')->get();
    }

    public function showHospital(Request $request){
        return view('app.nurse.hospital');
    }

    /**
     * CHANGE STATUS OF HOSPITAL
     */
    public function changeStatus($id){
        $hospital=Hospital::findOrfail($id);
        if($hospital){
            if($hospital->active==1)
                $hospital->active=0;
            else
                $hospital->active=1;
            if($hospital->save()){
                return $hospital;
            }

        }
        return "";
    }
    public function getHospital(){
        $hospital=Hospital::where('active',1)->select('id','name')->orderBy('name')->get();
        return $hospital;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $hosp=new Hospital($request->all());
        $hosp->user_id=Auth::id();
        if($hosp->save()) {
            return $hosp;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Hospital::with('user')->findOrfail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $hosp=Hospital::findOrfail($id);
        $hosp->fill($request->all());
        $hosp->user_id=Auth::id();
        if($hosp->update()) {
            return $hosp;
        }
        return Response::json( ['error'=>'Server Down']
            ,400);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Hospital::destroy($id)) {
            return Response::json(array('msg' => 'hospital record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    }

    public function exportHospital(Request $request){
        $hospital=Hospital::orderBy('name')->get();

        if($hospital!=null) {
            $hospitalArray = [];
            $i = 0;
            foreach ($hospital as $hosp) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                $temp['name'] = $hosp->name;
                $temp['Address'] = $hosp->address;
                $temp['mobile no 1'] = $hosp->mobile1;
                $temp['mobile no 2'] = $hosp->mobile2;
                $temp['mobile no 3'] = $hosp->mobile3;
                $temp['remarks'] = $hosp->remarks;

                if($hosp->active==1)
                    $temp['Active'] = 'Active';
                else
                    $temp['Active'] = 'In Active';
                $temp['Created At'] = $hosp->created_at;
                $temp['Updated At'] = $hosp->updated_at;

                if ($hosp->user != null)
                    $temp['Created By'] = $hosp->user->name;
                else
                    $temp['Created By'] = '';
                $hospitalArray[$i] = $temp;
                $i++;
            }

            Excel::create('Hospitals List', function ($excel) use ($hospitalArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('hospital  List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('hospital List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($hospitalArray) {
                    $sheet->fromArray($hospitalArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xls');
        }
    }
}

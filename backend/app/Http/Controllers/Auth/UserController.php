<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Response;
use Validator;

class UserController extends Controller
{
    /**
     * Validates given data for Residence
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name'       =>'required',
            'username'      =>'required|unique:users,username',
            'password'      =>'required',
            'type'      =>'required',
        ]);
    }
    protected function validatorupdate(array $data)
    {
        return Validator::make($data,[
            'name'       =>'required',
            'username'      =>'required|unique:users,username',
            //'password'      =>'required',
            'type'      =>'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::where('id','!=',\Auth::id())->where('type','!=','m')->get();
    }

    public function showMyProfile(){
        $profile=Auth::user();
        return view('app.user.profile',compact('profile'));
    }
    public function showMyUsers(Request $request){
        $num=5;
        if($request->num!=null)
            $num=$request->num;

        $users=User::where('id','!=',Auth::id())->where('id','!=','m')->orderBy('created_at','desc')->paginate($num);
        return view('app.user.users',compact('users'));
    }
    public function showPassword(){
        return view('app.user.changepassword');
    }

    public function checkUsername(Request $request){
        $username=$request->username;
        $user=User::where('username',$username)->get()->first();
        if($user!=null){
            return Response::json( ['found'=>'Found']);
        }
        return Response::json( ['found'=>'Available']);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user=Auth::user();
        if($user->type=='a'){
            $validator = $this->validator($request->all());
            if($validator->fails()){
                return Response::json( $validator->errors()
                    ,400);
            }

            $user=new User();
            $user->name=$request->name;
            $user->username=$request->username;
            $user->email=$request->email;
            $user->type=$request->type;
            $user->password=$request->password;
            $user->photo='user.png';
            if($request->type=='a')
                $user->permission='1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111';
            else
                $user->permission='1111,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000';
            if($user->save()) {
                return $user;
            }

            return Response::json( ['error'=>'Server Down']
                ,400);
        }
        return Response::json( ['error'=>'Unauthorized access']
            ,400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user=Auth::user();
        if($user->type=='a'){
            $validator = $this->validatorupdate($request->all());
            if($validator->fails()){
                return Response::json( $validator->errors()
                    ,400);
            }

            $user=User::findOrfail($id);
            if($user) {
                $user->name = $request->name;
                $user->username = $request->username;
                $user->email = $request->email;
                $user->type = $request->type;
               // $user->password = $request->password;
                //$user->photo = 'user.png';
                //$user->permission = '111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111';
                if ($user->save()) {
                    return $user;
                }
            }

            return Response::json( ['error'=>'Server Down']
                ,400);
        }
        return Response::json( ['error'=>'Unauthorized access']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->type=='a') {
            if (User::destroy($id)) {
                return Response::json(array('msg' => 'User record deleted'));
            } else
                return Response::json(array('error' => 'Record not found'), 400);
        }
        return Response::json(array('error' => 'Unauthorized access'), 400);
    }

    public function updateMyProfile(Request $request){
        $name=$request->name;
        $email=$request->email;
        $photos=$request->photos;
        $user=User::findOrfail(Auth::id());
        if($user){
            $user->name=$name;
            $user->email=$email;
            if($photos!=null){
                foreach ($photos as $photo) {
                    $user->photo = $this->savePhoto($photo);
                }
            }
            if($user->save())
                return $user;
        }
    }

    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);

                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = mt_rand().time().'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);
                Storage::disk('local')->put($fileName,$data);
            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }



    public function getMyProfile(){
       return Auth::user();
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Accounthead;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use Response;
use Excel;

class AccountheadController extends Controller
{

    /**
     * Validates given data for Income
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name'       =>'required|max:255',
            //'type'     =>'required|numeric|max:999999999999999999',
            'group'      =>'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $group=$request->group;
        $accountheads= Accounthead::where('group',$group)->with('user')->orderBy('created_at','desc')->get();

        return $accountheads;
    }

    public function getAccountHead($id){
        $accountheads= Accounthead::where('id',$id)->get()->first();
        //$accountheads['value']=strtolower($accountheads->name);
        return $accountheads;
    }

    public function  showMainAccountAccountHead(Request $request)
    {
        /*$num=5;
        if($request->num!=null)
            $num=$request->num;
        $accountheads= Accounthead::where('group','Main')->orderBy('created_at','desc')->paginate($num);


        return view('app.main.accounthead',compact('accountheads'));*/
        return view('app.main.accounthead');
    }

    public function  showNurseAccountAccountHead(Request $request)
    {
//        $num=5;
//        if($request->num!=null)
//            $num=$request->num;
//        $accountheads= Accounthead::where('group','Nurse')->orderBy('created_at','desc')->paginate($num);

        return view('app.nurse.accounthead');//,compact('accountheads'));
    }

    public function  showRentAccountAccountHead(Request $request)
    {
        /*$num=5;
        if($request->num!=null)
            $num=$request->num;
        $accountheads= Accounthead::where('group','Rent')->orderBy('created_at','desc')->paginate($num);*/


        return view('app.rent.accounthead');//,compact('accountheads'));
    }

    public function  showTransportationAccountAccountHead(Request $request)
    {
//        $num=5;
//        if($request->num!=null)
//            $num=$request->num;
//        $accountheads= Accounthead::where('group','Transportation')->orderBy('created_at','desc')->paginate($num);


        return view('app.transportation.accounthead');//,compact('accountheads'));
    }

    public function  showCourierAccountAccountHead(Request $request)
    {
//        $num=5;
//        if($request->num!=null)
//            $num=$request->num;
//        $accountheads= Accounthead::where('group','Courier')->orderBy('created_at','desc')->paginate($num);


        return view('app.courier.accounthead');//,compact('accountheads'));
    }

    /**
     * @param Request $request
     * @return mixed
     * GET INCOME ACCOUNT HEAD
     */
    public function  GetAccountHeadIncome(Request $request)
    {
        $head='';
        if($request->head!=null)
            $head=$request->head;
        $accountheads= Accounthead::where('group',$head)->where('type','income')->where('active',1)->orderBy('name')->get();
        foreach($accountheads as $accounthead)
            $accounthead['value']=strtolower($accounthead->name);
        return $accountheads;
    }
    /**
     * @param Request $request
     * @return mixed
     * GET INCOME ACCOUNT HEAD
     */
    public function  GetAccountHeadExpense(Request $request)
    {
        $head='';
        if($request->head!=null)
            $head=$request->head;
        $accountheads= Accounthead::where('group',$head)->where('type','expense')->where('active',1)->orderBy('name')->get();
        foreach($accountheads as $accounthead)
            $accounthead['value']=strtolower($accounthead->name);
        return $accountheads;
    }

    /**
     * CHANGE STATUS OF ACCOUNT HEAD
     */
    public function changeStatus($id){
        $head=Accounthead::findOrfail($id);
        if($head){
            if($head->active==1)
                $head->active=0;
            else
                $head->active=1;
            if($head->save())
                return $head;
        }
        return "";
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $head=new Accounthead($request->all());
        $head->user_id=Auth::id();
        if($head->save()){
           // \Session::put('message','Hai,'.Auth::user()->name.' Your Transaction saved successfully');
           // \Session::save();
            return $head;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Accounthead::with('user')->findOrfail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }
        $head=Accounthead::find($id);
        $head->fill($request->all());
        $head->user_id=Auth::id();
        if($head->save()) {
           // \Session::put('message','Hai,'.Auth::user()->name.' Your Transaction updated successfully');
           // \Session::save();
            return $head;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO cascade delete
        $head=Accounthead::findOrfail($id);
        if($head) {
            if($head->default!=1) {
                if (Accounthead::destroy($id)) {
                  //  \Session::put('message', 'Hai,' . Auth::user()->name . ' Account Head Removed successfully');
                 //   \Session::save();
                    return Response::json(array('msg' => 'Account record deleted'));
                } else
                    return Response::json(array('error' => 'Records not found'), 400);
            }else{
               // \Session::put('message', 'Hai,' . Auth::user()->name . ' Account Head Set has DEFAULT...Not DELETABLE');
               // \Session::save();
                return Response::json(array('default' => 'true'), 200);
            }
        }
        return Response::json(array('error' => 'Records not found'), 400);
    }

    public function exportAccountHead(Request $request){
        $accountheads=Accounthead::orderBy('name')->get();

        if($accountheads!=null) {
            $accountHeadArray = [];
            $i = 0;
            foreach ($accountheads as $accounthead) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                $temp['Account Name'] = $accounthead->name;
                $temp['Type'] = $accounthead->type;
                $temp['Description'] = $accounthead->description;
                $temp['Group'] = $accounthead->group;
                if($accounthead->active==1)
                    $temp['Active'] = 'Active';
                else
                    $temp['Active'] = 'In Active';
                $temp['Created At'] = $accounthead->created_at;
                $temp['Updated At'] = $accounthead->updated_at;

                if ($accounthead->user != null)
                    $temp['Created By'] = $accounthead->user->name;
                else
                    $temp['Created By'] = '';
                $accountHeadArray[$i] = $temp;
                $i++;
            }

            Excel::create('AccountHeadList', function ($excel) use ($accountHeadArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('Account Head List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('Account Head List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($accountHeadArray) {
                    $sheet->fromArray($accountHeadArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xls');
        }
    }


}

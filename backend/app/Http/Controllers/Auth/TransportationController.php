<?php

namespace App\Http\Controllers\Auth;
use App\Employee;
use App\Faircollection;
use App\Transportation;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Validator;
use Response;
use Excel;
use Storage;

class TransportationController extends Controller
{


    /**
     * Validates given data for Transportation
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name'       =>'required',
            'mobile1'      =>'required',
            'residence_id'        =>'required|numeric',
            'hospital_id'        =>'required|numeric',
            'gender'        =>'required',
            'employee_id'        =>'required|numeric',

        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $basedon=$request->basedon;
        $driver_id=$request->driver_id;
        $hostel_id=$request->hostel_id;

        if($basedon!=null){
            if($basedon=='Driver'){
                if($driver_id=='All')
                    return Transportation::with('residence')->with('hospital')->with('employee')->with('user')->orderBy('created_at','desc')->get();
                else
                    return Transportation::where('employee_id',$driver_id)->with('residence')->with('hospital')->with('employee')->with('user')->orderBy('created_at','desc')->get();
            }else{
                if($hostel_id=='All')
                    return Transportation::with('residence')->with('hospital')->with('employee')->with('user')->orderBy('created_at','desc')->get();
                else
                    return Transportation::where('hospital_id',$hostel_id)->with('residence')->with('hospital')->with('employee')->with('user')->orderBy('created_at','desc')->get();
            }
        }

        return Transportation::with('residence')->with('hospital')->with('employee')->with('user')->orderBy('created_at','desc')->get();
    }

    public function pdfExportCandidate(Request $request){
        $basedon=$request->basedon;
        $driver_id=$request->driver_id;
        $hostel_id=$request->hostel_id;

        $candidates=null;
        if($basedon!=null){
            if($basedon=='Driver'){
                if($driver_id=='All')
                    $candidates= Transportation::with('residence')->with('hospital')->with('employee')->with('user')->orderBy('created_at','desc')->get();
                else
                    $candidates= Transportation::where('employee_id',$driver_id)->with('residence')->with('hospital')->with('employee')->with('user')->orderBy('created_at','desc')->get();
            }else{
                if($hostel_id=='All')
                    $candidates= Transportation::with('residence')->with('hospital')->with('employee')->with('user')->orderBy('created_at','desc')->get();
                else
                    $candidates= Transportation::where('hospital_id',$hostel_id)->with('residence')->with('hospital')->with('employee')->with('user')->orderBy('created_at','desc')->get();
            }


            $data= '<html><head><style>table, td, th {  border: 1px solid rgba(34, 36, 37, 0.91);  }  table {  border-collapse: collapse;  width: 100%;  }  th {  height: 50px;  }</style></head><body>';
            $data=$data.'<h4 style="text-align:center;"><u>Candidates List</u></h4><br/>';
            $data=$data.'<table width="100%" border="0.5"><thead><tr><th>SlNo</th><th>Name</th><th>Mobile</th><th>Hospital</th><th>Driver</th><th>Remark</th></tr></thead>';
            $data=$data.'<tbody>';
            $id=1;
            foreach($candidates as $candidate){
                $data=$data.'<tr><td>&nbsp;&nbsp;'.$id.'</td><td>&nbsp;&nbsp;'.$candidate->name.'</td><td>'.$candidate->mobile1.'</td><td>&nbsp;'.$candidate->hospital->name.'</td><td>'.$candidate->employee->name.'</td><td>'.$candidate->remark.'</td></tr>';
                $id=$id+1;
            }
            $data=$data.'</tbody>';
            $data=$data.'</table><br/><br/>';
            $data=$data.'</body></html>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($data);
            $pdf->output();

            //return $pdf->stream($student->name.' Application.pdf')->header('Content-Type','application/pdf');//download($student->name.' Application.pdf');
            return response($pdf->stream('Daybook'), 200)->header('Content-Type', 'application/pdf');
        }
    }

    public function showCandidate(Request $request){
        return view('app.transportation.candidate');
    }

    public function GenerateDutyChart(Request $request){
        $year=$request->year;
        $driver_id=$request->driver_id;
        if($year==null){
            $now=new \DateTime();
            $year=$now->format('Y');
        }

        $selected=$year;

        //$duties=$this->getDutyChart($year,$driver_id);

        $qry='SELECT DISTINCT YEAR(date) as year FROM faircollections GROUP BY year DESC';
        $years=\Illuminate\Support\Facades\DB::select($qry);

        $drivers= Employee::where('designation_id',1)->orderBy('name','desc')->get();

        return view('app.transportation.dutychart',compact('years','drivers','selected','driver_id'));
    }

    public function GetMonthDutyChart(Request $request){
        ini_set('max_execution_time', 180); //3 minutes
        $year=$request->year;
        $driver_id=$request->driver_id;
        if($year==null){
            $now=new \DateTime();
            $year=$now->format('Y');
        }


        $duties=$this->getDutyChart($year,$driver_id);

        return $duties;
    }

    public function getPaidAndUnpaidPDF(Request $request){
        ini_set('max_execution_time', 60000) ;
        $year=$request->year;
        $driver_id=$request->driver_id;
        if($year==null){
            $now=new \DateTime();
            $year=$now->format('Y');
        }


        $duties=$this->getDutyChart($year,$driver_id);

        $data='<html><head><style>table, td, th {border: 1px solid black;}</style></head><body>';
        $data=$data.'<h2 style="text-align:center;color:blue;">Duty Chart List of '.$year.'</h2><br/>';
        $data=$data.'<table  style="width:100%;border-collapse: collapse;"><thead><tr style="text-align:center;color:blue;"><th>SlNo</th><th>Name</th><th>Mobile</th><th>Residence</th><th>Hospital</th><th>Duty Driver</th><th>JAN</th><th>FEB</th><th>MAR</th><th>APR</th><th>MAY</th><th>JUN</th><th>JUL</th><th>AUG</th><th>SEP</th><th>OCT</th><th>NOV</th><th>DEC</th></tr></thead>';
        $data=$data.'<tbody>';
        foreach($duties as $index=> $duty){
            $data=$data.'<tr><td>'.($index+1).'</td><td>'.$duty->name.'</td><td>'.$duty->mobile1.'</td><td>'.$duty->residence->name.'</td><td>'.$duty->hospital->name.'</td><td>'.$duty->employee->name.'</td><td>'.$duty->jan.'</td><td>'.$duty->feb.'</td><td>'.$duty->mar.'</td><td>'.$duty->apr.'</td><td>'.$duty->may.'</td><td>'.$duty->jun.'</td><td>'.$duty->jul.'</td><td>'.$duty->aug.'</td><td>'.$duty->sep.'</td><td>'.$duty->oct.'</td><td>'.$duty->nov.'</td><td>'.$duty->dec.'</td></tr>';
        }
        $data=$data.'<tr style="text-align:center;color:red;"><td colspan="6">Total</td><td>'.$duties->sum('jan').'</td><td>'.$duties->sum('feb').'</td><td>'.$duties->sum('mar').'</td><td>'.$duties->sum('apr').'</td><td>'.$duties->sum('may').'</td><td>'.$duties->sum('jun').'</td><td>'.$duties->sum('jul').'</td><td>'.$duties->sum('aug').'</td><td>'.$duties->sum('sep').'</td><td>'.$duties->sum('oct').'</td><td>'.$duties->sum('nov').'</td><td>'.$duties->sum('dec').'</td></tr>';
        $data=$data.'</tbody>';
        $data=$data.'</table></body></html>';

        /*$pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($data);
        $pdf->setPaper('A4', 'landscape');
        $pdf->output();

        //return $pdf->stream();
        return response($pdf->stream('Daybook'), 200)->header('Content-Type', 'application/pdf');*/

        require_once('tcpdf_include.php');

        // create new PDF document
        $pdf = new TCPDF('L', PDF_UNIT, "A4", true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('TCPDF Example 006');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set font
        $pdf->SetFont('times', '', 12);
        // add a page
        $pdf->AddPage();
        $pdf->writeHTML($data, true, false, true, false, '');
        $pdf->Output('HallticketList.pdf', 'I');
    }


    public function getMyProfile(Request $request){

        $id=$request->id;
        ini_set('max_execution_time', 60000) ;

        $candaidate=Transportation::findorfail($id);
        $file = \Illuminate\Support\Facades\Storage::get($candaidate->photo);
        $header=base64_encode($file);
        $data= '<html><head><style>table, td, th {border: none;}</style></head><body>';
        $data=$data.'<h1 style="text-align:center;color:blue;">'.$candaidate->name.'</h1><br/>';
        $data=$data.'<table><tr style="text-align:right;border:none;"><td colspan="2"><img style="height:100px;width:100px;" src="data:image/png;base64,'.$header.'"/></td></tr>';
        $data=$data.'<tr><td>Name</td><td> : '.$candaidate->name.'</td></tr>';
        $data=$data.'<tr><td>Gender</td><td> : '.$candaidate->gender.'</td></tr>';
        $data=$data.'<tr><td>Mobile 1</td><td> : '.$candaidate->mobile1.'</td></tr>';
        $data=$data.'<tr><td>Mobile 2</td><td> : '.$candaidate->mobile2.'</td></tr>';
        $data=$data.'<tr><td>Mobile 3</td><td> : '.$candaidate->mobile3.'</td></tr>';
        $data=$data.'<tr><td>Whatsapp</td><td> : '.$candaidate->whatsapp.'</td></tr>';
        $data=$data.'<tr><td>Email</td><td> : '.$candaidate->email.'</td></tr>';
        $data=$data.'<tr><td>Residence Area</td><td> : '.$candaidate->residence->name.'</td></tr>';
        $data=$data.'<tr><td>Working Hospital</td><td> : '.$candaidate->hospital->name.'</td></tr>';
        $data=$data.'<tr><td>Duty Driver</td><td> : '.$candaidate->employee->name.'</td></tr>';
        $data=$data.'</table></body></html>';


        require_once('tcpdf_include.php');

        // create new PDF document
        $pdf = new TCPDF('P', PDF_UNIT, "A4", true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('TCPDF Example 006');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set font
        $pdf->SetFont('times', '', 18);
        // add a page
        $pdf->AddPage();
        $pdf->writeHTML($data, true, false, true, false, '');
        $pdf->Output('HallticketList.pdf', 'I');
    }

    public function getUnpaidPDF(Request $request){
        ini_set('max_execution_time', 60000) ;
        $year=$request->year;
        $driver_id=$request->driver_id;
        if($year==null){
            $now=new \DateTime();
            $year=$now->format('Y');
        }


        $duties=$this->getDutyChart($year,$driver_id);

        $data='<html><head><style>table, td, th {border: 1px solid black;}</style></head><body>';
        $data=$data.'<h2 style="text-align:center;color:blue;">Duty Chart List of '.$year.'</h2><br/>';
        $data=$data.'<table  style="width:100%;border-collapse: collapse;"><thead><tr style="text-align:center;color:blue;"><th>SlNo</th><th>Name</th><th>Mobile</th><th>Residence</th><th>Hospital</th><th>Duty Driver</th><th>JAN</th><th>FEB</th><th>MAR</th><th>APR</th><th>MAY</th><th>JUN</th><th>JUL</th><th>AUG</th><th>SEP</th><th>OCT</th><th>NOV</th><th>DEC</th></tr></thead>';
        $data=$data.'<tbody>';
        foreach($duties as $index=> $duty){
            $jan=' - ';
            $feb=' - ';
            $mar=' - ';
            $apr=' - ';
            $may=' - ';
            $jun=' - ';
            $jul=' - ';
            $aug=' - ';
            $sep=' - ';
            $oct=' - ';
            $nov=' - ';
            $dec=' - ';

            if($duty->jan=='NPD')
                $jan='NPD';

            if($duty->feb=='NPD')
                $feb='NPD';

            if($duty->mar=='NPD')
                $mar='NPD';

            if($duty->apr=='NPD')
                $apr='NPD';

            if($duty->may=='NPD')
                $may='NPD';

            if($duty->jun=='NPD')
                $jun='NPD';

            if($duty->jul=='NPD')
                $jul='NPD';

            if($duty->aug=='NPD')
                $aug='NPD';

            if($duty->sep=='NPD')
                $sep='NPD';

            if($duty->oct=='NPD')
                $oct='NPD';

            if($duty->nov=='NPD')
                $nov='NPD';

            if($duty->dec=='NPD')
                $dec='NPD';



            $data=$data.'<tr><td>'.($index+1).'</td><td>'.$duty->name.'</td><td>'.$duty->mobile1.'</td><td>'.$duty->residence->name.'</td><td>'.$duty->hospital->name.'</td><td>'.$duty->employee->name.'</td><td>'.$jan.'</td><td>'.$feb.'</td><td>'.$mar.'</td><td>'.$apr.'</td><td>'.$may.'</td><td>'.$jun.'</td><td>'.$jul.'</td><td>'.$aug.'</td><td>'.$sep.'</td><td>'.$oct.'</td><td>'.$nov.'</td><td>'.$dec.'</td></tr>';
        }
       // $data=$data.'<tr style="text-align:center;color:red;"><td colspan="6">Total</td><td>'.$duties->sum('jan').'</td><td>'.$duties->sum('feb').'</td><td>'.$duties->sum('mar').'</td><td>'.$duties->sum('apr').'</td><td>'.$duties->sum('may').'</td><td>'.$duties->sum('jun').'</td><td>'.$duties->sum('jul').'</td><td>'.$duties->sum('aug').'</td><td>'.$duties->sum('sep').'</td><td>'.$duties->sum('oct').'</td><td>'.$duties->sum('nov').'</td><td>'.$duties->sum('dec').'</td></tr>';
        /*$data=$data.'</tbody>';
        $data=$data.'</table></body></html>';
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($data);
        $pdf->setPaper('A4', 'landscape');
        $pdf->output();

        //return $pdf->stream();
        return response($pdf->stream('Daybook'), 200)->header('Content-Type', 'application/pdf');*/
        require_once('tcpdf_include.php');

        // create new PDF document
        $pdf = new TCPDF('L', PDF_UNIT, "A4", true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('TCPDF Example 006');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set font
        $pdf->SetFont('times', '', 10);
        // add a page
        $pdf->AddPage();
        $pdf->writeHTML($data, true, false, true, false, '');
        $pdf->Output('HallticketList.pdf', 'I');

    }

    public function getPaidPDF(Request $request){
        ini_set('max_execution_time', 60000) ;
        $year=$request->year;
        //$year='2017';
        $driver_id=$request->driver_id;
        //$driver_id='9';
        if($year==null){
            $now=new \DateTime();
            $year=$now->format('Y');
        }


        $duties=$this->getDutyChart($year,$driver_id);

        $data='<html><head><style>table, td, th {border: 1px solid black;}</style></head><body>';
        $data=$data.'<h2 style="text-align:center;color:blue;">Duty Chart List of '.$year.'</h2><br/>';
        $data=$data.'<table  style="width:100%;border-collapse: collapse;"><thead><tr style="text-align:center;color:blue;"><th>SlNo</th><th>Name</th><th>Mobile</th><th>Residence</th><th>Hospital</th><th>Duty Driver</th><th>JAN</th><th>FEB</th><th>MAR</th><th>APR</th><th>MAY</th><th>JUN</th><th>JUL</th><th>AUG</th><th>SEP</th><th>OCT</th><th>NOV</th><th>DEC</th></tr></thead>';
        $data=$data.'<tbody>';
        foreach($duties as $index=> $duty){
            $jan=' - ';
            $feb=' - ';
            $mar=' - ';
            $apr=' - ';
            $may=' - ';
            $jun=' - ';
            $jul=' - ';
            $aug=' - ';
            $sep=' - ';
            $oct=' - ';
            $nov=' - ';
            $dec=' - ';

            if($duty->jan!='NPD')
                $jan=$duty->jan;

            if($duty->feb!='NPD')
                $feb=$duty->feb;

            if($duty->mar!='NPD')
                $mar=$duty->mar;

            if($duty->apr!='NPD')
                $apr=$duty->apr;

            if($duty->may!='NPD')
                $may=$duty->may;

            if($duty->jun!='NPD')
                $jun=$duty->jun;

            if($duty->jul!='NPD')
                $jul=$duty->jul;

            if($duty->aug!='NPD')
                $aug=$duty->aug;

            if($duty->sep!='NPD')
                $sep=$duty->sep;

            if($duty->oct!='NPD')
                $oct=$duty->oct;

            if($duty->nov!='NPD')
                $nov=$duty->nov;

            if($duty->dec!='NPD')
                $dec=$duty->dec;



            $data=$data.'<tr><td>'.($index+1).'</td><td>'.$duty->name.'</td><td>'.$duty->mobile1.'</td><td>'.$duty->residence->name.'</td><td>'.$duty->hospital->name.'</td><td>'.$duty->employee->name.'</td><td>'.$jan.'</td><td>'.$feb.'</td><td>'.$mar.'</td><td>'.$apr.'</td><td>'.$may.'</td><td>'.$jun.'</td><td>'.$jul.'</td><td>'.$aug.'</td><td>'.$sep.'</td><td>'.$oct.'</td><td>'.$nov.'</td><td>'.$dec.'</td></tr>';
        }
        $data=$data.'<tr style="text-align:center;color:red;"><td colspan="6">Total</td><td>'.$duties->sum('jan').'</td><td>'.$duties->sum('feb').'</td><td>'.$duties->sum('mar').'</td><td>'.$duties->sum('apr').'</td><td>'.$duties->sum('may').'</td><td>'.$duties->sum('jun').'</td><td>'.$duties->sum('jul').'</td><td>'.$duties->sum('aug').'</td><td>'.$duties->sum('sep').'</td><td>'.$duties->sum('oct').'</td><td>'.$duties->sum('nov').'</td><td>'.$duties->sum('dec').'</td></tr>';
        $data=$data.'</tbody>';
        $data=$data.'</table></body></html>';

       /* $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($data);
        $pdf->setPaper('A4', 'landscape');
        $pdf->output();

        //return $pdf->stream();
        return response($pdf->stream('Daybook'), 200)->header('Content-Type', 'application/pdf');*/


        require_once('tcpdf_include.php');

        // create new PDF document
        $pdf = new TCPDF('L', PDF_UNIT, "A4", true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('TCPDF Example 006');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set font
        $pdf->SetFont('times', '', 10);
        // add a page
        $pdf->AddPage();
        $pdf->writeHTML($data, true, false, true, false, '');
        $pdf->Output('HallticketList.pdf', 'I');
    }

    private function getDutyChart($year,$driver_id){
        ini_set('max_execution_time', 180); //3 minutes
        if($driver_id=='All')
            $duties=Transportation::with('residence')->with('hospital')->with('employee')->get();
        else
            $duties=Transportation::where('employee_id',$driver_id)->with('residence')->with('hospital')->with('employee')->get();

        foreach($duties as $dutie){
            $fair1=Faircollection::where('date','>=',$year.'/01/01')->where('date','<=',$year.'/01/31')->where('candidate_id',$dutie->id)->sum('amount');
            if($fair1!=null)
                $dutie['jan']=$fair1;
            else
                $dutie['jan']='NPD';

            $fair2=Faircollection::where('date','>=',$year.'/02/01')->where('date','<=',$year.'/02/31')->where('candidate_id',$dutie->id)->sum('amount');
            if($fair2!=null)
                $dutie['feb']=$fair2;
            else
                $dutie['feb']='NPD';

            $fair3=Faircollection::where('date','>=',$year.'/03/01')->where('date','<=',$year.'/03/31')->where('candidate_id',$dutie->id)->sum('amount');
            if($fair3!=null)
                $dutie['mar']=$fair3;
            else
                $dutie['mar']='NPD';

            $fair4=Faircollection::where('date','>=',$year.'/04/01')->where('date','<=',$year.'/04/31')->where('candidate_id',$dutie->id)->sum('amount');
            if($fair4!=null)
                $dutie['apr']=$fair4;
            else
                $dutie['apr']='NPD';

            $fair5=Faircollection::where('date','>=',$year.'/05/01')->where('date','<=',$year.'/05/31')->where('candidate_id',$dutie->id)->sum('amount');
            if($fair5!=null)
                $dutie['may']=$fair5;
            else
                $dutie['may']='NPD';

            $fair6=Faircollection::where('date','>=',$year.'/06/01')->where('date','<=',$year.'/06/31')->where('candidate_id',$dutie->id)->sum('amount');
            if($fair6!=null)
                $dutie['jun']=$fair6;
            else
                $dutie['jun']='NPD';

            $fair7=Faircollection::where('date','>=',$year.'/07/01')->where('date','<=',$year.'/07/31')->where('candidate_id',$dutie->id)->sum('amount');
            if($fair7!=null)
                $dutie['jul']=$fair7;
            else
                $dutie['jul']='NPD';

            $fair8=Faircollection::where('date','>=',$year.'/08/01')->where('date','<=',$year.'/08/31')->where('candidate_id',$dutie->id)->sum('amount');
            if($fair8!=null)
                $dutie['aug']=$fair8;
            else
                $dutie['aug']='NPD';

            $fair9=Faircollection::where('date','>=',$year.'/09/01')->where('date','<=',$year.'/09/31')->where('candidate_id',$dutie->id)->sum('amount');
            if($fair9!=null)
                $dutie['sep']=$fair9;
            else
                $dutie['sep']='NPD';

            $fair10=Faircollection::where('date','>=',$year.'/10/01')->where('date','<=',$year.'/10/31')->where('candidate_id',$dutie->id)->sum('amount');
            if($fair10!=null)
                $dutie['oct']=$fair10;
            else
                $dutie['oct']='NPD';

            $fair11=Faircollection::where('date','>=',$year.'/11/01')->where('date','<=',$year.'/11/31')->where('candidate_id',$dutie->id)->sum('amount');
            if($fair11!=null)
                $dutie['nov']=$fair11;
            else
                $dutie['nov']='NPD';

            $fair12=Faircollection::where('date','>=',$year.'/12/01')->where('date','<=',$year.'/12/31')->where('candidate_id',$dutie->id)->sum('amount');
            if($fair12!=null)
                $dutie['dec']=$fair12;
            else
                $dutie['dec']='NPD';
        }

        return $duties;
    }


    /**
     * CHANGE STATUS OF TRANSPORTATION
     */
    public function changeStatus($id){
        $candidate=Transportation::findOrfail($id);
        if($candidate){
            if($candidate->active==1)
                $candidate->active=0;
            else
                $candidate->active=1;
            if($candidate->save()){
                return $candidate;
            }

        }
        return "";
    }

    public function getCandidate(){
        $transportations=Transportation::where('active',1)->select('id','name','mobile1','remark','employee_id')->orderBy('name')->get();
        foreach($transportations as $transportation)
            $transportation['value']=strtolower($transportation->name);
        return $transportations;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $trans=new Transportation($request->all());
        $trans->user_id=Auth::id();
        $photos=$request->photos;
        if($photos!=null){
            foreach ($photos as $photo) {
                $trans->photo = $this->savePhoto($photo);
            }
        }else{
            $trans->photo='user.png';
        }
        if($trans->save()) {
            return $trans;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }


    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);

                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = mt_rand().time().'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);
                Storage::disk('local')->put($fileName,$data);
            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Transportation::with('user')->with('residence')->with('hospital')->with('employee')->findOrfail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $trans=Transportation::findOrfail($id);
        $trans->fill($request->all());
        $trans->user_id=Auth::id();
        $photos=$request->photos;
        if($photos!=null){
            foreach ($photos as $photo) {
                $trans->photo = $this->savePhoto($photo);
            }
        }
        if($trans->update()) {
            return $trans;
        }
        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Transportation::destroy($id)) {
            return Response::json(array('msg' => 'Transportation record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    }

    public function exportTransportation(Request $request){
        ini_set('max_execution_time', 180); //3 minutes
        $trans=Transportation::orderBy('name')->get();

        if($trans!=null) {
            $transArray = [];
            $i = 0;
            foreach ($trans as $tran) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                $temp['name'] = $tran->name;
                $temp['Mobile 1'] = $tran->mobile1;
                $temp['Mobile 2'] = $tran->mobile2;
                $temp['Mobile 3'] = $tran->mobile3;
                $temp['Whatsapp No'] = $tran->whatsapp;
                $temp['email'] = $tran->email;
                $temp['Gender'] = $tran->gender;
                $temp['remark'] = $tran->remark;
                if($tran->active==1)
                    $temp['Active'] = 'Active';
                else
                    $temp['Active'] = 'In Active';
                $temp['Created At'] = $tran->created_at;
                $temp['Updated At'] = $tran->updated_at;

                if ($tran->user != null)
                    $temp['Created By'] = $tran->user->name;
                else
                    $temp['Created By'] = '';
                $transArray[$i] = $temp;
                $i++;
            }

            Excel::create('transportation List', function ($excel) use ($transArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('transportation  List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('transportation List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($transArray) {
                    $sheet->fromArray($transArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xls');
        }
    }

    public function excelExportDutyChart(Request $request){

        ini_set('max_execution_time', 180); //3 minutes
        $year=$request->year;
        $driver_id=$request->driver_id;
        if($year==null){
            $now=new \DateTime();
            $year=$now->format('Y');
        }

        $selected=$year;

        $duties=$this->getDutyChart($year,$driver_id);

        if($duties!=null) {
            $rentsArray = [];
            $i = 0;
            foreach ($duties as $dutie) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                $temp['Name'] = $dutie->name;
                $temp['Mobile'] = $dutie->mobile1;
                if($dutie->residence!=null)
                    $temp['Residence'] = $dutie->residence->name;
                else
                    $temp['Residence'] = '';
                if($dutie->hospital!=null)
                    $temp['Hospital'] = $dutie->hospital->name;
                else
                    $temp['Hospital'] = $dutie->hospital->name;
                if($dutie->employee!=null)
                    $temp['Duty Driver'] = $dutie->employee->name;
                else
                    $temp['Duty Driver'] ='';
                $temp['Jan'] = $dutie->jan;
                $temp['Feb'] = $dutie->feb;
                $temp['Mar'] = $dutie->mar;
                $temp['Apr'] = $dutie->apr;
                $temp['May'] = $dutie->may;
                $temp['Jun'] = $dutie->jun;
                $temp['Jul'] = $dutie->jul;
                $temp['Aug'] = $dutie->aug;
                $temp['Sep'] = $dutie->sep;
                $temp['Oct'] = $dutie->oct;
                $temp['Nov'] = $dutie->nov;
                $temp['Dec'] = $dutie->dec;

                $rentsArray[$i] = $temp;

                $i++;
            }

            Excel::create('rentcollectionList', function ($excel) use ($rentsArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('rent collections  List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('rent collection List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($rentsArray) {
                    $sheet->fromArray($rentsArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xls');
        }

    }
}
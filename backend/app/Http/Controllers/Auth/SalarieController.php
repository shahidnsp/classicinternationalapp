<?php

namespace App\Http\Controllers\Auth;

use App\Employee;
use App\Ledgerposting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Salarie;
use App\User;
use Validator;
use Response;
use Illuminate\Support\Facades\Auth;
use Excel;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SalarieController extends Controller
{

    /**
     * Validates given data for salaries
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'date'       =>'required|date',
            'employee_id'=>'required',
            'basic'=>'required',
            'advance'=>'required',
            'allowance'=>'required',
            'leavesalary'=>'required',
            'total'=>'required',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $from=Carbon::now()->startOfMonth();
        $to=Carbon::now()->endOfMonth();
        return Salarie::with('employee')->with('user')->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date','desc')->get();
    }

    public function  showMySalary(Request $request){
        $employees=Employee::where('active',1)->select('id','name')->get();
        return view('app.salary.salary',compact('employees'));
    }

    public function SearchSalary(Request $request){
       $from=new Carbon();$to=new Carbon();
        $employee_id="All";

        if($request->fromDate!=null)
            $from=$request->fromDate;
        if($request->toDate!=null)
            $to=$request->toDate;

        if($request->employee_id!=null)
            $employee_id=$request->employee_id;
        else
            $employee_id="All";

        $date=strtotime($from);
        $from=date('Y/m/d',$date);

        //$date=strtotime($to);
        //$to=date('Y/m/d',$date);
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));

        if($employee_id=='All') {
            $salaries= Salarie::with('employee')->with('user')->orderBy('date','desc')->where('date', '>=', $from)->where('date', '<=', $to)->get();
        }else{
            $salaries= Salarie::with('employee')->with('user')->where('employee_id', $employee_id)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date','desc')->get();
        }

        return $salaries;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors()
                , 400);
        }

        $salaries = new Salarie($request->all());
        $salaries->user_id = Auth::id();
        if ($salaries->save()) {

            //CREATING LEDGER POSTING ENTRIES
            $ledger=new Ledgerposting();
            $ledger->accounthead_id=2;
            $ledger->amount=$salaries->total;
            $ledger->date=$salaries->date;
            $ledger->remark=$salaries->remark;
            $ledger->user_id=$salaries->user_id;
            $ledger->salary_id=$salaries->id;
            $ledger->save();

            return $salaries;
        }
        return Response::json( ['error'=>'Server Down']
            ,400);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Salarie::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $salaries=Salarie::findOrfail($id);
        $salaries->fill($request->all());
        $salaries->user_id=Auth::id();
        if($salaries->update()) {

            //CREATING LEDGER POSTING ENTRIES
            Ledgerposting::where('salary_id',$salaries->id)->delete();

            $ledger=new Ledgerposting();
            $ledger->accounthead_id=2;
            $ledger->amount=$salaries->total;
            $ledger->date=$salaries->date;
            $ledger->remark=$salaries->remark;
            $ledger->user_id=$salaries->user_id;
            $ledger->salary_id=$salaries->id;
            $ledger->save();

            return $salaries;
        }
        return Response::json( ['error'=>'Server Down']
            ,400);
    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Salarie::destroy($id)){
            return Response::json(array('msg'=>'salaries  record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    }

    public function GenerateSalaryChart(Request $request){
        $year=$request->year;
        $employee_id=$request->employee_id;

        if($year==null){
            $now=new \DateTime();
            $year=$now->format('Y');
        }

        $selected=$year;

        if($employee_id==null)
            $employees=Employee::all();
        else
            $employees=Employee::where('id',$employee_id)->get();

        if($employees!=null) {
            foreach ($employees as $index => $employee) {

                $sal1 = Salarie::where('date', '>=', $year . '/01/01')->where('date', '<=', $year . '/01/31')->where('employee_id', $employee->id)->sum('total');
                $adv1 = Salarie::where('date', '>=', $year . '/01/01')->where('date', '<=', $year . '/01/31')->where('employee_id', $employee->id)->sum('advance');
                if ($sal1 != null)
                    $employee['jan'] = $sal1 + $adv1;
                else
                    $employee['jan'] = 'NPD';

                $sal2 = Salarie::where('date', '>=', $year . '/02/01')->where('date', '<=', $year . '/02/31')->where('employee_id', $employee->id)->sum('total');
                $adv2 = Salarie::where('date', '>=', $year . '/02/01')->where('date', '<=', $year . '/02/31')->where('employee_id', $employee->id)->sum('advance');
                if ($sal2 != null)
                    $employee['feb'] = $sal2 + $adv2;
                else
                    $employee['feb'] = 'NPD';

                $sal3 = Salarie::where('date', '>=', $year . '/03/01')->where('date', '<=', $year . '/03/31')->where('employee_id', $employee->id)->sum('total');
                $adv3 = Salarie::where('date', '>=', $year . '/03/01')->where('date', '<=', $year . '/03/31')->where('employee_id', $employee->id)->sum('advance');
                if ($sal3 != null)
                    $employee['mar'] = $sal3 + $adv3;
                else
                    $employee['mar'] = 'NPD';

                $sal4 = Salarie::where('date', '>=', $year . '/04/01')->where('date', '<=', $year . '/04/31')->where('employee_id', $employee->id)->sum('total');
                $adv4 = Salarie::where('date', '>=', $year . '/04/01')->where('date', '<=', $year . '/04/31')->where('employee_id', $employee->id)->sum('advance');
                if ($sal4 != null)
                    $employee['apr'] = $sal4 + $adv4;
                else
                    $employee['apr'] = 'NPD';

                $sal5 = Salarie::where('date', '>=', $year . '/05/01')->where('date', '<=', $year . '/05/31')->where('employee_id', $employee->id)->sum('total');
                $adv5 = Salarie::where('date', '>=', $year . '/05/01')->where('date', '<=', $year . '/05/31')->where('employee_id', $employee->id)->sum('advance');
                if ($sal5 != null)
                    $employee['may'] = $sal5 + $adv5;
                else
                    $employee['may'] = 'NPD';

                $sal6 = Salarie::where('date', '>=', $year . '/06/01')->where('date', '<=', $year . '/06/31')->where('employee_id', $employee->id)->sum('total');
                $adv6 = Salarie::where('date', '>=', $year . '/06/01')->where('date', '<=', $year . '/06/31')->where('employee_id', $employee->id)->sum('advance');
                if ($sal6 != null)
                    $employee['jun'] = $sal6 + $adv6;
                else
                    $employee['jun'] = 'NPD';

                $sal7 = Salarie::where('date', '>=', $year . '/07/01')->where('date', '<=', $year . '/07/31')->where('employee_id', $employee->id)->sum('total');
                $adv7 = Salarie::where('date', '>=', $year . '/07/01')->where('date', '<=', $year . '/07/31')->where('employee_id', $employee->id)->sum('advance');
                if ($sal7 != null)
                    $employee['jul'] = $sal7 + $adv7;
                else
                    $employee['jul'] = 'NPD';

                $sal8 = Salarie::where('date', '>=', $year . '/08/01')->where('date', '<=', $year . '/08/31')->where('employee_id', $employee->id)->sum('total');
                $adv8 = Salarie::where('date', '>=', $year . '/08/01')->where('date', '<=', $year . '/08/31')->where('employee_id', $employee->id)->sum('advance');
                if ($sal8 != null)
                    $employee['aug'] = $sal8 + $adv8;
                else
                    $employee['aug'] = 'NPD';

                $sal9 = Salarie::where('date', '>=', $year . '/09/01')->where('date', '<=', $year . '/09/31')->where('employee_id', $employee->id)->sum('total');
                $adv9 = Salarie::where('date', '>=', $year . '/09/01')->where('date', '<=', $year . '/09/31')->where('employee_id', $employee->id)->sum('advance');
                if ($sal9 != null)
                    $employee['sep'] = $sal9 + $adv9;
                else
                    $employee['sep'] = 'NPD';

                $sal10 = Salarie::where('date', '>=', $year . '/10/01')->where('date', '<=', $year . '/10/31')->where('employee_id', $employee->id)->sum('total');
                $adv10 = Salarie::where('date', '>=', $year . '/10/01')->where('date', '<=', $year . '/10/31')->where('employee_id', $employee->id)->sum('advance');
                if ($sal10 != null)
                    $employee['oct'] = $sal10 + $adv10;
                else
                    $employee['oct'] = 'NPD';

                $sal11 = Salarie::where('date', '>=', $year . '/11/01')->where('date', '<=', $year . '/11/31')->where('employee_id', $employee->id)->sum('total');
                $adv11 = Salarie::where('date', '>=', $year . '/11/01')->where('date', '<=', $year . '/11/31')->where('employee_id', $employee->id)->sum('advance');
                if ($sal11 != null)
                    $employee['nov'] = $sal11 + $adv11;
                else
                    $employee['nov'] = 'NPD';

                $sal12 = Salarie::where('date', '>=', $year . '/12/01')->where('date', '<=', $year . '/12/31')->where('employee_id', $employee->id)->sum('total');
                $adv12 = Salarie::where('date', '>=', $year . '/12/01')->where('date', '<=', $year . '/12/31')->where('employee_id', $employee->id)->sum('advance');
                if ($sal12 != null)
                    $employee['dec'] = $sal12 + $adv12;
                else
                    $employee['dec'] = 'NPD';


                //unset($rents[$index]);
            }
        }

        $qry='SELECT DISTINCT YEAR(date) as year FROM salaries GROUP BY year DESC';
        $years=\Illuminate\Support\Facades\DB::select($qry);

        $emps=Employee::select('id','name')->get();
        //return $employees;
        return view('app.salary.salarychart',compact('employees','years','emps','selected','employee_id'));
    }

    public function excelExportSalary(Request $request){

        $fromDate=$request->fromDate;
        $toDate=$request->toDate;
        $employee_id=$request->employee_id;
        $toDate= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($toDate)));
        if($employee_id=='All')
            $salaries=Salarie::where('date','>=',$fromDate)->where('date','<=',$toDate)->orderBy('date')->get();
        else
            $salaries=Salarie::where('employee_id',$employee_id)->where('date','>=',$fromDate)->where('date','<=',$toDate)->orderBy('date')->get();

        if($salaries!=null) {
            $salariesArray = [];
            $i = 0;
            foreach ($salaries as $sal) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                if($sal->employee!=null)
                    $temp['Employee'] = $sal->employee->name;
                else
                    $temp['Employee'] = '';
                $temp['Date'] = $sal->date;
                $temp['Basic Pay'] = $sal->basic;
                $temp['Advance'] = $sal->advance;
                $temp['Allowance'] = $sal->allowance;
                $temp['Leave Salary'] = $sal->leavesalary;
                $temp['Total'] = $sal->total;
                $temp['Remark'] = $sal->remark;
                $temp['Created At'] = $sal->created_at;
                $temp['Updated At'] = $sal->updated_at;

                if ($sal->user != null)
                    $temp['Created By'] = $sal->user->name;
                else
                    $temp['Created By'] = '';
                $salariesArray[$i] = $temp;

                $i++;
            }

            Excel::create('salaries List', function ($excel) use ($salariesArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('Salary Advance   List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('Salaries List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($salariesArray) {
                    $sheet->fromArray($salariesArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xlsx');
        }
    }




}

<?php

namespace App\Http\Controllers\Auth;

use App\Employee;
use App\Ledgerposting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Salaryadvance;
use App\User;
use Illuminate\Support\Facades\DB;
use Validator;
use Response;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;

class SalaryadvanceController extends Controller
{

    /**
     * Validates given data for salaryadvance
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'date'       =>'required|date',
            // 'description'=>'required',
            'amount'=>'required|numeric',
            'employee_id'=>'required|numeric',
            //'user_id'=>'required|numeric',

        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $from=Carbon::now()->startOfMonth();
        $to=Carbon::now()->endOfMonth();
        return Salaryadvance::with('employee')->with('user')->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date','desc')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    public function  showMySalaryAdvance(Request $request){
        $employees=Employee::where('active',1)->select('id','name')->get();
        return view('app.salary.salaryadvance',compact('employees'));
    }

    public function SearchSalaryAdvance(Request $request){
        $from=new Carbon();$to=new Carbon();
        $employee_id="All";

        if($request->fromDate!=null)
            $from=$request->fromDate;
        if($request->toDate!=null)
            $to=$request->toDate;

        if($request->employee_id!=null)
            $employee_id=$request->employee_id;
        else
            $employee_id="All";

        $date=strtotime($from);
        $from=date('Y/m/d',$date);

        //$date=strtotime($to);
        //$to=date('Y/m/d',$date);
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));

        if($employee_id=='All') {
            $advances= Salaryadvance::orderBy('date','desc')->where('date', '>=', $from)->where('date', '<=', $to)->get();
        }else{
            $advances= Salaryadvance::where('employee_id', $employee_id)->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('date','desc')->get();
        }

        return $advances;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $salaryadv=new Salaryadvance($request->all());
        $salaryadv->user_id=Auth::id();
        if($salaryadv->save()) {


            //CREATING LEDGER POSTING ENTRIES
            $ledger=new Ledgerposting();
            $ledger->accounthead_id=1;
            $ledger->amount=$salaryadv->amount;
            $ledger->date=$salaryadv->date;
            $ledger->remark=$salaryadv->remark;
            $ledger->user_id=$salaryadv->user_id;
            $ledger->advance_id=$salaryadv->id;
            $ledger->save();



            return $salaryadv;
        }


        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        return Salaryadvance::find($id);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $salaryadv=Salaryadvance::findOrfail($id);

        //FOR ADJUSTMENT PREVIOUS ADVANCE AMOUNT FOR SALARY CALCULATION IN FUTURE
        $sum=Salaryadvance::where('employee_id',$request->employee_id)->sum('balance');
        if($sum==null)
            $sum=0;

        if($sum>0)
            $sum=$sum-$salaryadv->amount;
        else
            $sum=0;

        $salaryadv->fill($request->all());
        $salaryadv->user_id=Auth::id();


        //FOR REMOVING ALL balance TO ZERO
        DB::table('salaryadvances')->update(['balance'=>0]);

        //FOR ASSIGN CURRENT ADVANCE BALANCE
        $sum=$sum+$salaryadv->amount;
        $salaryadv->balance=$sum;

        if($salaryadv->update()) {


            //CREATING LEDGER POSTING ENTRIES
            Ledgerposting::where('advance_id',$salaryadv->id)->delete();

            $ledger=new Ledgerposting();
            $ledger->accounthead_id=1;
            $ledger->amount=$salaryadv->amount;
            $ledger->date=$salaryadv->date;
            $ledger->remark=$salaryadv->remark;
            $ledger->user_id=$salaryadv->user_id;
            $ledger->advance_id=$salaryadv->id;
            $ledger->save();


            return $salaryadv;
        }
        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Salaryadvance::destroy($id)){

            return Response::json(array('msg'=>'salary advance collection record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    }

    public function excelExportSalaryAdvance(Request $request){
        $fromDate=$request->fromDate;
        $toDate=$request->toDate;
        $employee_id=$request->employee_id;
        $toDate= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($toDate)));

        if($employee_id=='All')
            $saladv=Salaryadvance::where('date','>=',$fromDate)->where('date','<=',$toDate)->orderBy('date')->get();
        else
            $saladv=Salaryadvance::where('employee_id',$employee_id)->where('date','>=',$fromDate)->where('date','<=',$toDate)->orderBy('date')->get();

        if($saladv!=null) {
            $saladvArray = [];
            $i = 0;
            foreach ($saladv as $adv) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                if($adv->employee!=null)
                    $temp['Employee'] = $adv->employee->name;
                else
                    $temp['Employee'] ='';
                $temp['Date'] = $adv->date;
                $temp['Amount'] = $adv->amount;
                $temp['Remark'] = $adv->remark;
                $temp['Created At'] = $adv->created_at;
                $temp['Updated At'] = $adv->updated_at;

                if ($adv->user != null)
                    $temp['Created By'] = $adv->user->name;
                else
                    $temp['Created By'] = '';
                $saladvArray[$i] = $temp;

                $i++;
            }

            Excel::create('rentcollectionList', function ($excel) use ($saladvArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('Salary Advance   List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('Salary Advance List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($saladvArray) {
                    $sheet->fromArray($saladvArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xls');
        }
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Billbook;
use App\BillbookItem;
use App\Ledgerposting;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Response;

class BillbookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fromDate=$request->fromDate;
        $toDate=$request->toDate;
        $toDate= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($toDate)));
        if($fromDate==null || $toDate==null) {
            $fromDate=Carbon::now()->startOfMonth();
            $toDate=Carbon::now()->endOfMonth();
            return Billbook::where('date','>=',$fromDate)->where('date','<=',$toDate)->with('user')->with('items')->get();
        }else{
            return Billbook::where('date','>=',$fromDate)->where('date','<=',$toDate)->with('user')->with('items')->get();
        }
    }

    public function showMainAccountBillBook(){
        return view('app.main.billbook');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $items= $request->items;
        $newbill= $request->newbill;
        $total= $request->total;

        $bill=new Billbook();
        $bill->name=$newbill['name'];
        if(isset($newbill['address']))
            $bill->address=$newbill['address'];
        if(isset($newbill['phone']))
            $bill->phone=$newbill['phone'];
        $bill->amount=$total;
        $bill->date=$newbill['date'];
        $bill->user_id=Auth::id();
        if($bill->save()){
            foreach($items as $item){
                $itm=new BillbookItem();
                if(isset($item['accounthead_id']))
                    $itm->accounthead_id=$item['accounthead_id'];

                $itm->billbook_id=$bill->id;
                if(isset($item['amount']))
                     $itm->amount=$item['amount'];
                $itm->save();

                $ledger=new Ledgerposting();
                $ledger->accounthead_id=$itm->accounthead_id;
                $ledger->amount=$itm->amount;
                $ledger->date=$bill->date;
                $ledger->bill_id=$itm->id;
                $ledger->user_id=Auth::id();
                $ledger->save();
            }


            $pdf = \Illuminate\Support\Facades\App::make('dompdf.wrapper');

            $invoice=\App\Billbook::where('id',$bill->id)->with('items.accounthead')->get()->first();

            $pdf->loadView('pdf.invoice',compact('invoice'));
            $pdf->setPaper('a4', 'portrait');
            $pdf->output();

            return response($pdf->stream('bill'), 200)->header('Content-Type', 'application/pdf');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $items= $request->items;
        $newbill= $request->newbill;
        $total= $request->total;

        $bills=BillbookItem::where('billbook_id',$id)->get();
        foreach($bills as $bill){
            Ledgerposting::where('bill_id',$bill->id)->delete();
            $bill->delete();
        }

        $bill= Billbook::find($id);
        $bill->name=$newbill['name'];
        if(isset($newbill['address']))
            $bill->address=$newbill['address'];
        if(isset($newbill['phone']))
            $bill->phone=$newbill['phone'];
        $bill->amount=$total;
        $bill->date=$newbill['date'];
        $bill->user_id=Auth::id();
        if($bill->save()){
            foreach($items as $item){
                $itm=new BillbookItem();
                if(isset($item['accounthead_id']))
                    $itm->accounthead_id=$item['accounthead_id'];

                $itm->billbook_id=$bill->id;
                if(isset($item['amount']))
                    $itm->amount=$item['amount'];
                $itm->save();

                $ledger=new Ledgerposting();
                $ledger->accounthead_id=$itm->accounthead_id;
                $ledger->amount=$itm->amount;
                $ledger->date=$bill->date;
                $ledger->bill_id=$itm->id;
                $ledger->user_id=Auth::id();
                $ledger->save();
            }

            $pdf = \Illuminate\Support\Facades\App::make('dompdf.wrapper');

            $invoice=\App\Billbook::where('id',$bill->id)->with('items.accounthead')->get()->first();

            $pdf->loadView('pdf.invoice',compact('invoice'));
            $pdf->setPaper('a4', 'portrait');
            $pdf->output();

            return response($pdf->stream('bill'), 200)->header('Content-Type', 'application/pdf');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Billbook::destroy($id)){
            $bills=BillbookItem::where('billbook_id',$id)->get();
            foreach($bills as $bill){
                Ledgerposting::where('bill_id',$bill->id)->delete();
                $bill->delete();
            }
            return Response::json(array('msg'=>'rent collection record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    }
}

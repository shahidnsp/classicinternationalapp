<?php

namespace App\Http\Controllers\Auth;
use App\Rent;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use Response;
use Excel;

class RentController extends Controller
{
    /**
     * Validates given data for Rent
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'officename'       =>'required',
            //'area'      =>'required',
            'buildingrentamount'        =>'required',
            //'buildingrentname'        =>'required',
            //'buildingrentmobile'        =>'required|numeric',
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Rent::with('user')->orderBy('created_at','desc')->get();
    }

    public function showRent(Request $request){
        return view('app.rent.rent');
    }

    /**
     * CHANGE STATUS OF RENT
     */
    public function changeStatus($id){
        $rent=Rent::findOrfail($id);
        if($rent){
            if($rent->active==1)
                $rent->active=0;
            else
                $rent->active=1;
            if($rent->save()){
                return $rent;
            }

        }
        return "";
    }

    public function getRent(){
        $rents=Rent::where('active',1)->select('id','officename','buildingrentamount','buildingrentername')->orderBy('officename')->get();
        foreach($rents as $rent)
            $rent['value']=strtolower($rent->officename);
        return $rents;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $rent=new Rent($request->all());
        $rent->user_id=Auth::id();
        if($rent->save()){
            return $rent;
        }


        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Rent::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $rent=Rent::findOrfail($id);
        $rent->fill($request->all());
        $rent->user_id=Auth::id();
        if($rent->update()){
            return $rent;
        }
        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Rent::destroy($id)) {
            return Response::json(array('msg' => 'Rent record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    }

    public function exportRent(Request $request){
        $rents=Rent::orderBy('officename')->get();

        if($rents!=null) {
            $rentsArray = [];
            $i = 0;
            foreach ($rents as $rent) {
                $temp = [];
                $temp['SlNo'] = $i + 1;
                $temp['Office Name'] = $rent->officename;
                $temp['Area'] = $rent->area;
                $temp['Building Rent Amount'] = $rent->buildingrentamount;
                $temp['Remark'] = $rent->remark;
                $temp['Building Renter Name'] = $rent->buildingrentername;
                $temp['Building Renter Mobil No'] = $rent->buildingrentermobile;
                //if($rent->active==1)
                 //   $temp['Active'] = 'Active';
               // else
                 //   $temp['Active'] = 'In Active';
                $temp['Created At'] = $rent->created_at;
                $temp['Updated At'] = $rent->updated_at;

                if ($rent->user != null)
                    $temp['Created By'] = $rent->user->name;
                else
                    $temp['Created By'] = '';
                $rentsArray[$i] = $temp;
                $i++;
            }

            Excel::create(' Rent List', function ($excel) use ($rentsArray) {

                // Set the spreadsheet title, creator, and description
                $excel->setTitle('rents  List');
                $excel->setCreator('Shahid')->setCompany('Psybo Technologies');
                $excel->setDescription('rents List files');

                // Build the spreadsheet, passing in the payments array
                $excel->sheet('sheet1', function ($sheet) use ($rentsArray) {
                    $sheet->fromArray($rentsArray, null, 'A1', false);
                    $sheet->row(1, function ($row) {
                        //$row->setBackground('#000000');
                        $row->setFont(array(
                            'family' => 'Calibri',
                            'size' => '12',
                            'bold' => 'true'
                        ));
                        $row->setFontColor('#001158');
                    });
                });


            })->download('xls');
        }
    }
}

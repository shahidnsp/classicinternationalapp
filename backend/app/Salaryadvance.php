<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Salaryadvance extends Model
{
    protected  $fillable=['date','employee_id','amount','remark'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }


    public static function boot()
    {
        parent::boot();

        static::deleted(function($adv){
            //ADJUSTMENT FOR ADVANCE BALANCE
            $sum=$adv->balance-$adv->amount;
            $advance=Salaryadvance::where('employee_id',$adv->employee_id)->get()->last();
            if($advance!=null) {
                $advance->balance = $sum;
                $advance->save();
            }

            //FOR DELETING ADVANCE IN LEDGER POSTING
            Ledgerposting::where('advance_id',$adv->id)->delete();
        });

        static::created(function($advanc){
            //ADJUSTMENT FOR ADVANCE BALANCE
            $sum=Salaryadvance::where('employee_id',$advanc->employee_id)->sum('balance');
            if($sum==null)
                $sum=0;

            DB::table('salaryadvances')->update(['balance'=>0]);

            $advanc->balance=$sum+$advanc->amount;
            $advanc->save();



        });

        static::updating(function($advanc){


        });

        static::updated(function($advanc){

        });
    }
}

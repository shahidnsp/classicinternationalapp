<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    protected $table='batches';
    protected $fillable=[ 'name','description'];

    public function user(){
        return $this->belongsTo('App\User');
    }
}

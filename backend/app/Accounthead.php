<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accounthead extends Model
{
    protected $fillable=['name','group','type','description'];

    public function user(){
        return $this->belongsTo('App\User');
    }
}

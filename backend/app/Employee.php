<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable=[ 'name','mobile', 'whatsapp','email','addresskuwait','addressindia','mobileindia','designation_id','basicpay',
        'overtimepay','passportno','visano','drivinglicenseno','remark' ];

    public function designation(){
        return $this->belongsTo('App\Designation');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
}

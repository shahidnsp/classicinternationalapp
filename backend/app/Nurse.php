<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nurse extends Model
{
    protected $fillable=['name','passportno','mobilno1','mobileno2','email','hospital_id','remark','nursebatch_id'];

    public function batch(){
        return $this->belongsTo('App\Batch','nursebatch_id');
    }

    public function hospital(){
        return $this->belongsTo('App\Hospital');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
}

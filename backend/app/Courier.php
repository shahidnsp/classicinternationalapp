<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Courier extends Model
{
    protected $fillable=[ 'name','fromaddress','frommobile','toaddress','tomobile','courieramount','servicecharge','date','remark'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public static function boot()
    {
        parent::boot();

        static::deleted(function($cur){
            //FOR DELETING ADVANCE IN LEDGER POSTING
            Ledgerposting::where('courier_id',$cur->id)->delete();
        });

        static::created(function($cur){

        });

        static::updating(function($cur){


        });

        static::updated(function($cur){

        });
    }
}

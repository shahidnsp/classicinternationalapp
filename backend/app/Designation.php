<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $fillable=[ 'name','description','default'];

    public function user(){
        return $this->belongsTo('App\User');
    }
}



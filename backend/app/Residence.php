<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Residence extends Model
{
    protected  $fillable=['name','address','phone','remarks'];

    public function user(){
        return $this->belongsTo('App\User');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salarie extends Model
{
    protected  $fillable=['date','employee_id','basic','advance','allowance','leavesalary','total','remark'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }

    public static function boot()
    {
        parent::boot();

        static::deleted(function($sal){
            $sum=$sal->advance;
            $advance=Salaryadvance::where('employee_id',$sal->employee_id)->get()->last();
            if($advance!=null) {
                $advance->balance = $sum;
                $advance->save();
            }

            //FOR DELETING ADVANCE IN LEDGER POSTING
            Ledgerposting::where('salary_id',$sal->id)->delete();
        });

        static::created(function($sal){
            $sum=$sal->advance;
            $advance=Salaryadvance::where('employee_id',$sal->employee_id)->get()->last();
            if($advance!=null) {
                $advance->balance = $advance->balance-$sum;
                $advance->save();
            }
        });

        static::updating(function($advanc){


        });

        static::updated(function($advanc){


        });
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faircollection extends Model
{
    protected  $fillable=['date','candidate_id','employee_id','amount','remark'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function employee(){
        return $this->belongsTo('App\Employee');
    }

    public function transportation(){
        return $this->belongsTo('App\Transportation','candidate_id');
    }
}

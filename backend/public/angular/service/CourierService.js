/**
 * Created by Shahid Neermunda on 9/5/15.
 */

angular.module('CourierService',[]).factory('Courier',['$resource',
    function($resource){
        return $resource('/api/courier/:courierId',{
            courierId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
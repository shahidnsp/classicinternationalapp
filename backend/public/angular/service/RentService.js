/**
 * Created by Shahid Neermunda on 10/5/15.
 */

angular.module('RentService',[]).factory('Rent',['$resource',
    function($resource){
        return $resource('/api/rent/:rentId',{
            rentId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
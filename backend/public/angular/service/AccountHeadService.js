/**
 * Created by Shahid Neermunda on 2/5/15.
 */

angular.module('AccountHeadService',[]).factory('AccountHead',['$resource',
    function($resource){
        return $resource('/api/accounthead/:accountheadId',{
            accountheadId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
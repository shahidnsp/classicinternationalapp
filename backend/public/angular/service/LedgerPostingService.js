/**
 * Created by Shahid Neermunda on 3/5/15.
 */

angular.module('LedgerPostingService',[]).factory('LedgerPosting',['$resource',
    function($resource){
        return $resource('/api/ledgerposting/:ledgerpostingId',{
            ledgerpostingId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
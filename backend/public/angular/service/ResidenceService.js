/**
 * Created by Shahid Neermunda on 9/5/15.
 */

angular.module('ResidenceService',[]).factory('Residence',['$resource',
    function($resource){
        return $resource('/api/residence/:residenceId',{
            residenceId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
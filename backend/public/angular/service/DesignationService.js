/**
 * Created by Shahid Neermunda on 8/5/15.
 */

angular.module('DesignationService',[]).factory('Designation',['$resource',
    function($resource){
        return $resource('/api/designation/:designationId',{
            designationId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 8/5/15.
 */

angular.module('HospitalService',[]).factory('Hospital',['$resource',
    function($resource){
        return $resource('/api/hospital/:hospitalId',{
            hospitalId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
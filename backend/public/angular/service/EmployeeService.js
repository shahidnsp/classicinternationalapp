/**
 * Created by Shahid Neermunda on 8/5/15.
 */

angular.module('EmployeeService',[]).factory('Employee',['$resource',
    function($resource){
        return $resource('/api/employee/:employeeId',{
            employeeId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
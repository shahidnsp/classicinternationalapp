/**
 * Created by Shahid Neermunda on 8/5/15.
 */

angular.module('AdvanceSalaryService',[]).factory('AdvanceSalary',['$resource',
    function($resource){
        return $resource('/api/salaryadvance/:salaryadvanceId',{
            salaryadvanceId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
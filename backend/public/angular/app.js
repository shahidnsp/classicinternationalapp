

var app = angular.
    module('myApp', [
        'ngResource',
        'ngMaterial',
        'ui.bootstrap',
        'ngNotify',
        'mdPickers',
        'AccountHeadService',
        'LedgerPostingService',
        'DesignationService',
        'EmployeeService',
        'BatchService',
        'HospitalService',
        'NurseService',
        'ResidenceService',
        'CandidateService',
        'CourierService',
        'RentService',
        'UserInfoService',
        'FairCollectionService',
        'RentCollectionService',
        'AdvanceSalaryService',
        'SalaryService',
        'UserService',
        'BillBookService',
        'AttendanceService',
    ]);

app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

//app.config(['$locationProvider', function($locationProvider) {
//    $locationProvider.html5Mode({ enabled: true, requireBase: false }); }]);

app.filter('pagination', function() {
    return function(input, currentPage, pageSize) {
        if(angular.isArray(input)) {
            var start = (currentPage-1)*pageSize;
            var end = currentPage*pageSize;
            return input.slice(start, end);
        }
    };
});

app.filter('percentage', ['$filter', function ($filter) {
    return function (input, decimals) {
        return $filter('number')(input * 100, decimals) + '%';
    };
}]);
app.filter('sum', function(){
    return function(items, prop){
        return items.reduce(function(a, b){
            return a + b[prop];
        }, 0);
    };
});

app.config(function($mdDateLocaleProvider) {
    $mdDateLocaleProvider.formatDate = function(date) {
        return date ? moment(date).format('DD-MM-YYYY') : '';
    };

    $mdDateLocaleProvider.parseDate = function(dateString) {
        var m = moment(dateString, 'DD-MM-YYYY', true);
        return m.isValid() ? m.toDate() : new Date(NaN);
    };
});

app.directive('mdDatepicker', function ($parse) {

    return {
        require: 'ngModel',
        link: function (scope, elem, attr, ngModel) {
            $parse(attr.ngModel).assign(scope, new Date(_.get(scope, attr.ngModel)));
        }
    }
});

app.directive('imageReader', function ($q) {
    var slice = Array.prototype.slice;

    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            if (!ngModel) return;

            ngModel.$render = function () {};

            element.bind('change', function (e) {
                var element = e.target;

                $q.all(slice.call(element.files, 0).map(readFile))
                    .then(function (values) {
                        if (element.multiple) ngModel.$setViewValue(values);
                        else ngModel.$setViewValue(values.length ? values[0] : null);
                    });

                function readFile(file) {
                    var deferred = $q.defer();

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        deferred.resolve(e.target.result);
                    };
                    reader.onerror = function (e) {
                        deferred.reject(e);
                    };
                    reader.readAsDataURL(file);

                    return deferred.promise;
                }
            });
        }
    };
});

app.directive('ngFileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.ngFileModel);
            var isMultiple = attrs.multiple;
            var modelSetter = model.assign;
            element.bind("change", function (changeEvent) {
                var values = [];

                for (var i = 0; i < element[0].files.length; i++) {
                    var reader = new FileReader();

                    reader.onload = (function (i) {
                        return function(e) {
                            var value = {
                                lastModified: changeEvent.target.files[i].lastModified,
                                lastModifiedDate: changeEvent.target.files[i].lastModifiedDate,
                                name: changeEvent.target.files[i].name,
                                size: changeEvent.target.files[i].size,
                                type: changeEvent.target.files[i].type,
                                data: e.target.result
                            };
                            values.push(value);
                        }

                    })(i);

                    reader.readAsDataURL(changeEvent.target.files[i]);
                }


                scope.$apply(function () {
                    if (isMultiple) {
                        modelSetter(scope, values);
                    } else {
                        modelSetter(scope, values[0]);
                    }
                });
            });
        }
    }
}]);

/*app.directive('autoFocus',function($timeout){
   return{
       restrict: 'AC',
       link: function(_scope,_element){
           $timeout(function(){
               _element[0].focus();
           },0);
       }
   } ;
});*/

/*app.config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default')
        .dark();
});*/
/*app.config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default')
        .primaryPalette('pink')
        .accentPalette('orange');
});*/


/*(function ()
{
    'use strict';

    app.provider('msDatepickerFixConfig', msDatepickerFixConfigProvider);
    app.directive('msDatepickerFix', msDatepickerFix);

    *//** @ngInject *//*
    function msDatepickerFixConfigProvider()
    {
        var service = this;

        // Default configuration
        var defaultConfig = {
            // To view
            formatter: function (val)
            {
                if ( !val )
                {
                    return '';
                }

                return val === '' ? val : new Date(val);
            },
            // To model
            parser   : function (val)
            {
                if ( !val )
                {
                    return '';
                }

                return moment(val).add(moment(val).utcOffset(), 'm').toDate();
            }
        };

        // Methods
        service.config = config;

        //////////

        *//**
         * Extend default configuration with the given one
         *
         * @param configuration
         *//*
        function config(configuration)
        {
            defaultConfig = angular.extend({}, defaultConfig, configuration);
        }

        *//**
         * Service
         *//*
        service.$get = function ()
        {
            return defaultConfig;
        };
    }

    *//** @ngInject *//*
    function msDatepickerFix(msDatepickerFixConfig)
    {
        return {
            require : 'ngModel',
            priority: 1,
            link    : function (scope, elem, attrs, ngModel)
            {
                ngModel.$formatters.push(msDatepickerFixConfig.formatter); // to view
                ngModel.$parsers.push(msDatepickerFixConfig.parser); // to model
            }
        };
    }
})();*/

/**
 * Created by Shahid Neermunda on 2/5/15.
 */

angular.module('AccountHeadService',[]).factory('AccountHead',['$resource',
    function($resource){
        return $resource('/api/accounthead/:accountheadId',{
            accountheadId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 8/5/15.
 */

angular.module('AdvanceSalaryService',[]).factory('AdvanceSalary',['$resource',
    function($resource){
        return $resource('/api/salaryadvance/:salaryadvanceId',{
            salaryadvanceId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 5/7/15.
 */

angular.module('AttendanceService',[]).factory('Attendance',['$resource',
    function($resource){
        return $resource('/api/attendance/:attendanceId',{
            attendanceId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 8/5/15.
 */

angular.module('BatchService',[]).factory('Batch',['$resource',
    function($resource){
        return $resource('/api/batch/:batchId',{
            batchId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 8/5/15.
 */

angular.module('BillBookService',[]).factory('BillBook',['$resource',
    function($resource){
        return $resource('/api/bill/:billId',{
            billId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 9/5/15.
 */

angular.module('CandidateService',[]).factory('Candidate',['$resource',
    function($resource){
        return $resource('/api/transportation/:transportationId',{
            transportationId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 9/5/15.
 */

angular.module('CourierService',[]).factory('Courier',['$resource',
    function($resource){
        return $resource('/api/courier/:courierId',{
            courierId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 8/5/15.
 */

angular.module('DesignationService',[]).factory('Designation',['$resource',
    function($resource){
        return $resource('/api/designation/:designationId',{
            designationId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 8/5/15.
 */

angular.module('EmployeeService',[]).factory('Employee',['$resource',
    function($resource){
        return $resource('/api/employee/:employeeId',{
            employeeId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 12/05/15.
 */

angular.module('FairCollectionService',[]).factory('FairCollection',['$resource',
    function($resource){
        return $resource('/api/faircollection/:faircollectionId',{
            faircollectionId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 8/5/15.
 */

angular.module('HospitalService',[]).factory('Hospital',['$resource',
    function($resource){
        return $resource('/api/hospital/:hospitalId',{
            hospitalId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 3/5/15.
 */

angular.module('LedgerPostingService',[]).factory('LedgerPosting',['$resource',
    function($resource){
        return $resource('/api/ledgerposting/:ledgerpostingId',{
            ledgerpostingId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 8/5/15.
 */

angular.module('NurseService',[]).factory('Nurse',['$resource',
    function($resource){
        return $resource('/api/nurse/:nurseId',{
            nurseId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 12/05/15.
 */

angular.module('RentCollectionService',[]).factory('RentCollection',['$resource',
    function($resource){
        return $resource('/api/rentcollection/:rentcollectionId',{
            rentcollectionId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 10/5/15.
 */

angular.module('RentService',[]).factory('Rent',['$resource',
    function($resource){
        return $resource('/api/rent/:rentId',{
            rentId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 9/5/15.
 */

angular.module('ResidenceService',[]).factory('Residence',['$resource',
    function($resource){
        return $resource('/api/residence/:residenceId',{
            residenceId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 19/5/15.
 */

angular.module('SalaryService',[]).factory('Salary',['$resource',
    function($resource){
        return $resource('/api/salaries/:salaryId',{
            salaryId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 11/05/2017.
 */
angular.module('UserInfoService',[]).factory('UserInfo',['$http',function($http){
    var UserInfo = {};

    /*UserInfo.resetPassword=function(userId){
        $http.post('/api/resetPassword',{id:userId}).
            success(function(data,status,headers,config){
                alert('User password reset to admin');
            }).error(function(data,status,headers,config){
                console.log(data);
                alert('Sorry,user password reset failed');
            });
    };*/

    UserInfo.query=function(){
        return $http.get('/api/userinfo')
    };

    /*

    UserInfo.update=function(data){
        return $http.post('/api/userinfo',data);
    };

    UserInfo.changePassword=function(oldPassword,password){
        $http.post('/api/changePassword',
            {oldPassword:oldPassword,password:password})
            .success(function(data,status){
                //TODO redirect to logout
                alert('Password has been changed.');
            })
            .error(function(data){
                alert('Sorry, Password change failed.');
            });
    };





    */

    UserInfo.get = function(id){
        return $http.get('/api/user/'+id);
    };

    UserInfo.save = function(id,data){
        return $http.put('/api/userinfo/'+id,data);
    };

    UserInfo.getAllPage=function(id){
        return $http.get('/api/getAllPages/'+id);
    };

    UserInfo.getUserPermission=function(userId){
        return $http.get('/api/getPermission/?id='+userId);
    };

    UserInfo.setUserPermission = function(data){
        return $http.post('/api/changePermission',data);
    };

    return UserInfo;
}
]);
/**
 * Created by psybo on 5/8/15.
 */
angular.module('UserService',[]).factory('User',['$resource',
    function($resource){
        return $resource('/api/user/:userId',{
            userId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
app.controller('AttendanceController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,Employee,UserInfo,Attendance){
    $scope.headedit = false;
    $scope.newattendance = new Attendance();
    $scope.curAttendance = {};

    $scope.attendances=[];

    function loadAttendance() {
        Attendance.query(function (attendance) {
            $scope.attendances = attendance;
        });
    }loadAttendance();

    $scope.employees=[];
    $http.get('/api/getEmployees').
        success(function(data, status)
        {
            $scope.employees = data;
        });

    $scope.types = [
        { value: 'income', name: 'Income' },
        { value: 'expense', name: 'Expense' }
    ];

    // $scope.numPerPage=5;

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Attendance(Main Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });



    $scope.newAttendance = function () {
        $scope.headedit = true;
        $scope.newattendance = new Attendance();
        $scope.newattendance.date = new Date();
        $scope.curAttendance = {};
    };
    $scope.editAttendance = function (thisHead) {
        $scope.headedit = true;
        $scope.curAttendance =  thisHead;
        $scope.newattendance = new Attendance();
        $scope.newattendance = angular.copy(thisHead);
        $scope.newattendance.date = new Date(thisHead.date);
        $anchorScroll();
    };
    $scope.addAttendance = function () {
        $scope.newattendance.group='Main';
        $scope.newattendance.default=0;
        if ($scope.curAttendance.id) {
            $http.put('/api/attendance/'+$scope.curAttendance.id, $scope.newattendance).
                success(function(data, status)
                {
                    //$window.location.reload();
                    loadAttendance();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Attendance Head updated Successfully');
                });
        } else{
            $scope.newattendance.$save(function(attendance){
                //$window.location.reload();
                // $window.location.href=$window.location.href;
                $scope.attendances.push(attendance);
                loadAttendance();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Attendance Head Saved Successfully');

            });
        }
        $scope.headedit = false;
    };
    $scope.deleteAttendance = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/attendance/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    if(data.default===true) {
                        ngNotify.set('Attendance Head Deleted Successfully');
                    }else{
                        ngNotify.set(' Attendance Head Set has DEFAULT...Not DELETABLE');
                    }
                    loadAttendance();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelAttendance = function () {
        $scope.headedit = false;
        $scope.newattendance =  new Attendance();
    };
    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);
    $scope.employee_id='All';
    $scope.search=function(fromDate,toDate,employee_id){
        Attendance.query({fromDate:fromDate,toDate:toDate,employee_id:employee_id},function (attendance) {
            $scope.attendances = attendance;
        });
    };
    

});
app.controller('BatchController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,Batch,UserInfo){
    $scope.batchedit = false;
    $scope.newbatch = new Batch();
    $scope.curBatch = {};

    $scope.batches=[];
    function loadBatch() {
        Batch.query(function (head) {
            $scope.batches = head;
        });
    }loadBatch();

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Batch(Nurse Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

       // console.log(data);
    });




    $scope.newBatch = function () {
        $scope.batchedit = true;
        $scope.newbatch = new Batch();
        $scope.curBatch = {};
    };
    $scope.editBatch = function (thisHead) {
        $scope.batchedit = true;
        $scope.curBatch =  thisHead;
        $scope.newbatch = new Batch();
        $scope.newbatch = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addBatch = function () {
        $scope.newbatch.default=0;
        if ($scope.curBatch.id) {
            $http.put('/api/batch/'+$scope.curBatch.id, $scope.newbatch).
                success(function(data, status)
                {
                    loadBatch();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Batch updated Successfully');
                });
        } else{
            $scope.newbatch.$save(function(account){
                loadBatch();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Batch saved Successfully');
            });
        }
        $scope.batchedit = false;
    };
    $scope.deleteBatch = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/batch/'+item.id).
                success(function(data, status)
                {
                    loadBatch();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Batch removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelBatch = function () {
        $scope.batchedit = false;
        $scope.newbatch =  new Batch();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Batch Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeBatchStatus/'+id).
                success(function(data, status)
                {
                    loadBatch();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Batch Status Changed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.open=function(data,ev){
        $http.get('/api/batch/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/batch',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {

        $scope.batch=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

});
app.controller('CandidateController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,Candidate){
    $scope.candidateedit = false;
    $scope.newcandidate = new Candidate();
    $scope.curCandidate = {};
    $scope.basepath=$window.location.origin;
    $scope.candidates=[];
    function loadCandidate() {
        Candidate.query(function (head) {
            $scope.candidates = head;
        });
    }loadCandidate();

    $scope.search=function(driver_id,hostel_id,basedon){
        Candidate.query({driver_id:driver_id,hostel_id:hostel_id,basedon:basedon},function (head) {
            $scope.candidates = head;
        });
    };

    $scope.download=function(driver_id,hostel_id,basedon){
        $scope.showProgressExport=true;
        var req = {
            method: 'POST',
            url: '/api/pdfExportCandidate',
            responseType:"arraybuffer",
            data: {driver_id:driver_id,hostel_id:hostel_id,basedon:basedon}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='Candidates.pdf' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();

                // Remove anchor from body
                document.body.removeChild(a);
                $scope.showProgressExport=false;
            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };



    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Candidate Register(Transportation)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });

    $scope.residences=[];
    $scope.hospitals=[];
    $scope.drivers=[];
    $scope.basedon='Driver';
    $scope.driver_id='All';
    $scope.hostel_id='All';

    function loadResidence(){
        $http.get('/api/getResidence').
            success(function(data, status)
            {
                $scope.residences=data;
            });
    }loadResidence();

    function loadHospitals(){
        $http.get('/api/getHospital').
            success(function(data, status)
            {
                $scope.hospitals=data;
            });
    }loadHospitals();

    function loadDrivers(){
        $http.get('/api/getDriver').
            success(function(data, status)
            {
                $scope.drivers=data;
            });
    }loadDrivers();



    $scope.newCandidate = function () {
        $scope.candidateedit = true;
        $scope.newcandidate = new Candidate();
        $scope.curCandidate = {};
    };
    $scope.editCandidate = function (thisHead) {
        $scope.candidateedit = true;
        $scope.curCandidate =  thisHead;
        $scope.newcandidate = new Candidate();
        $scope.newcandidate = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addCandidate = function () {
        $scope.newcandidate.default=0;
        if ($scope.curCandidate.id) {
            $http.put('/api/transportation/'+$scope.curCandidate.id, $scope.newcandidate).
                success(function(data, status)
                {
                    loadCandidate();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Candidate updated Successfully');
                });
        } else{
            $scope.newcandidate.$save(function(account){
                loadCandidate();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Candidate saved Successfully');
            });
        }
        $scope.candidateedit = false;
    };
    $scope.deleteCandidate = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/transportation/'+item.id).
                success(function(data, status)
                {
                    loadCandidate();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Candidate removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelCandidate = function () {
        $scope.candidateedit = false;
        $scope.newcandidate =  new Candidate();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Candidate Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeCandidateStatus/'+id).
                success(function(data, status)
                {
                    loadCandidate();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Candidate Status Changed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.open=function(data,ev){
        $http.get('/api/employee/'+data.employee_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/employee',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    $scope.getProfile=function(item){
        $scope.showProgress=true;
        var req = {
            method: 'POST',
            url: '/api/getMyProfile',
            responseType:"arraybuffer",
            data: {id:item.id}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download =item.name+'.pdf' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);
                $scope.showProgress=false;
            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

    function DialogController($scope, $mdDialog,$window,data) {

        $scope.basepath=$window.location.origin;
        $scope.employee=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openResidence=function(data,ev){
        $http.get('/api/residence/'+data.residence_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogResidenceController,
                    templateUrl: $window.location.origin+'/template/residence',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogResidenceController($scope, $mdDialog,data) {

        $scope.residence=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openHospital=function(data,ev){
        $http.get('/api/hospital/'+data.hospital_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogHospitalController,
                    templateUrl: $window.location.origin+'/template/hospital',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogHospitalController($scope, $mdDialog,data) {

        $scope.hospital=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openCandidate=function(data,ev){
        $http.get('/api/transportation/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogCandidateController,
                    templateUrl: $window.location.origin+'/template/candidate',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogCandidateController($scope, $mdDialog,data) {

        $scope.candidate=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };

        $scope.openHospital=function(data,ev){
            $http.get('/api/hospital/'+data.hospital_id).
                success(function(data, status)
                {
                    $mdDialog.show({
                        controller: DialogHospitalController,
                        templateUrl: $window.location.origin+'/template/hospital',
                        parent: angular.element(document.querySelector('#popup')),
                        targetEvent: ev,
                        clickOutsideToClose:true,
                        locals: {
                            data: data
                        }
                    })
                        .then(function(answer) {
                            //$scope.status = 'You said the information was "' + answer + '".';
                        }, function() {
                            //$scope.status = 'You cancelled the dialog.';
                        });
                });

            // console.log($window.location.origin);
        };

        function DialogHospitalController($scope, $mdDialog,data) {

            $scope.hospital=data;
            $scope.hide = function() {
                $mdDialog.hide();
            };

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };
        }


    }

});
app.controller('CourierController', function($scope,$http,$filter,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,Courier){
    $scope.courieredit = false;
    $scope.newcourier = new Courier();
    $scope.curCourier = {};

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.couriers =[];
    function loadCourier() {
        Courier.query(function (head) {
            $scope.couriers = head;
            //console.log(head);
        });
    }loadCourier();

    $scope.search=function(fromDate,toDate){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_courier', {params:{fromDate:from,toDate:to}}).
            success(function(data, status)
            {
                $scope.couriers = data;
            });
    };



    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Courier Register(Courier Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });




    $scope.newCourier = function () {
        $scope.courieredit = true;
        $scope.newcourier = new Courier();
        $scope.newcourier.date=new Date();
        $scope.curCourier = {};
    };
    $scope.editCourier = function (thisHead) {
        $scope.courieredit = true;
        $scope.curCourier =  thisHead;
        $scope.newcourier = new Courier();
        $scope.newcourier = angular.copy(thisHead);
        $scope.newcourier.date=new Date(thisHead.date);
        $anchorScroll();
    };
    $scope.addCourier = function () {
        $scope.newcourier.date=new Date($scope.newcourier.date);
        if ($scope.curCourier.id) {
            $http.put('/api/courier/'+$scope.curCourier.id, $scope.newcourier).
                success(function(data, status)
                {
                    loadCourier();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                });
        } else{
            $scope.newcourier.$save(function(account){
                loadCourier();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Your Transaction updated Successfully');
            });
        }
        $scope.courieredit = false;
    };
    $scope.deleteCourier = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/courier/'+item.id).
                success(function(data, status)
                {
                    loadCourier();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelCourier = function () {
        $scope.courieredit = false;
        $scope.newcourier =  new Courier();
    };


    $scope.openCourier=function(data,user,ev){
        $mdDialog.show({
            controller: DialogCourierController,
            templateUrl: $window.location.origin+'/template/courier',
            parent: angular.element(document.querySelector('#popup')),
            targetEvent: ev,
            clickOutsideToClose:true,
            locals: {
                data: data,
                user:user
            }
        })
            .then(function(answer) {
                //$scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                //$scope.status = 'You cancelled the dialog.';
            });

        // console.log($window.location.origin);
    };

    function DialogCourierController($scope, $mdDialog,data,user) {

        $scope.courier=data;
        $scope.user=user;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }


    $scope.export=function(fromDate,toDate){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        var req = {
            method: 'POST',
            url: '/api/excelExportCourier',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='CourierReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

});
app.controller('CourierAccountAccountHeadController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,AccountHead){
    $scope.headedit = false;
    $scope.newaccount = new AccountHead();
    $scope.curAccount = {};

    function loadAccountHead() {
        AccountHead.query({group: 'Courier'}, function (head) {
            $scope.accountheads = head;
        });
    }loadAccountHead();

    //$scope.numPerPage=5;
    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Account Head(Courier Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });



    $scope.newAccountHead = function () {
        $scope.headedit = true;
        $scope.newaccount = new AccountHead();
        $scope.curAccount = {};
    };
    $scope.editAccountHeadt = function (thisHead) {
        $scope.headedit = true;
        $scope.curAccount =  thisHead;
        $scope.newaccount = new AccountHead();
        $scope.newaccount = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addAccountHead = function () {
        $scope.newaccount.group='Courier';
        $scope.newaccount.default=0;
        if ($scope.curAccount.id) {
            $http.put('/api/accounthead/'+$scope.curAccount.id, $scope.newaccount).
                success(function(data, status)
                {
                    loadAccountHead();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Account Head updated Successfully');
                });
        } else{
            $scope.newaccount.$save(function(account){
                $scope.accountheads.push(account);
                loadAccountHead();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Account Head Saved Successfully');
            });
        }
        $scope.headedit = false;
    };
    $scope.deleteAccountHead = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/accounthead/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });
                    if(data.default===true) {
                        ngNotify.set('Account Head Deleted Successfully');
                    }else{
                        ngNotify.set(' Account Head Set has DEFAULT...Not DELETABLE');
                    }
                    loadAccountHead();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelAccountHead = function () {
        $scope.headedit = false;
        $scope.newaccount =  new AccountHead();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Account Head Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeAccountHeadStatus/'+id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Account Head Status Changed Successfully');

                    loadAccountHead();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };


    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

});
app.controller('CourierAccountExpenseController', function($scope,$http,$filter,$window,$location,$log,$timeout,ngNotify,$mdDialog,$anchorScroll,UserInfo,LedgerPosting){
    $scope.expenseedit = false;
    $scope.newexpense = new LedgerPosting();
    $scope.curExpense = {};
    $scope.account_id = 'All';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.expenses =[];
    function loadLedgerPosting() {
        LedgerPosting.query({group:'Courier',type:'expense'}, function (head) {
            $scope.expenses = head;
            //console.log(head);
        });
    }loadLedgerPosting();

    $scope.search=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_courier_expense', {params:{fromDate:from,toDate:to,account_id:account_id}}).
            success(function(data, status)
            {
                $scope.expenses = data;
            });
    };

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Expense Register(Courier Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });






    $scope.newExpense = function () {
        $scope.expenseedit = true;
        $scope.newexpense = new LedgerPosting();
        $scope.newexpense.date=new Date();
        $scope.curExpense = {};

    };
    $scope.editExpense = function (thisHead) {
        $scope.expenseedit = true;
        getSelectedItem(thisHead.accounthead_id);
        $scope.curExpense =  thisHead;
        $scope.newexpense = new LedgerPosting();
        $scope.newexpense = angular.copy(thisHead);
        $scope.newexpense.date=new Date(thisHead.date);
        $anchorScroll();
    };
    function getSelectedItem(id){
        $http.get('/api/getAccountHead/'+id).
            success(function(data, status)
            {
                $scope.selectedItem=data;
            });
    }
    $scope.addExpense = function () {
        $scope.newexpense.accounthead_id=$scope.selectedItem.id;
        $scope.newexpense.date=new Date($scope.newexpense.date);
        if ($scope.curExpense.id) {
            $http.put('/api/ledgerposting/'+$scope.curExpense.id, $scope.newexpense).
                success(function(data, status)
                {
                    loadLedgerPosting();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                });
        } else{
            $scope.newexpense.$save(function(account){
                loadLedgerPosting();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Your Transaction saved Successfully');
            });
        }
        $scope.expenseedit = false;
    };
    $scope.deleteExpense = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/ledgerposting/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction Deleted Successfully');
                    loadLedgerPosting();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelExpense = function () {
        $scope.expenseedit = false;
        $scope.newexpense =  new LedgerPosting();
    };




    $scope.simulateQuery = false;
    $scope.isDisabled    = false;

    // list of `state` value/display objects
    $scope.states        = [];loadAll();
    $scope.querySearch   = querySearch;
    $scope.selectedItemChange = selectedItemChange;
    $scope.searchTextChange   = searchTextChange;

    $scope.newState = newState;

    function newState(state) {
        alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
        var results = query ? $scope.states.filter( createFilterFor(query) ) : $scope.states,
            deferred;
        if ($scope.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    function searchTextChange(text) {
        //$log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
        //$log.info('Item changed to ' + JSON.stringify(item));
    }


    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
        $scope.data1=[];
        $http.get('/api/accounthead_expense/',{params:{head:'Courier'}}).
            success(function(data, status)
            {
                $scope.states=data;
            });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {

        var lowercaseQuery =query; //angular.lowercase(query);

        return function filterFn(state) {
            return (state.value.indexOf(lowercaseQuery) === 0);
        };

    }

    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.accounthead_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.export=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        var req = {
            method: 'POST',
            url: '/api/excelExportLedgerPosting',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,type:'expense',group:'Courier'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='ExpenseReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };
});
app.controller('CourierAccountIncomeController', function($scope,$http,$window,$filter,$location,$log,$timeout,ngNotify,$mdDialog,$anchorScroll,UserInfo,LedgerPosting){
    $scope.incomeedit = false;
    $scope.newincome = new LedgerPosting();
    $scope.curIncome = {};
    $scope.account_id = 'All';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    //$scope.numPerPage=5;
    $scope.incomes =[];
    function loadLedgerPosting() {
        LedgerPosting.query({group:'Courier',type:'income'}, function (head) {
            $scope.incomes = head;
            //console.log(head);
        });
    }loadLedgerPosting();

    $scope.search=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_courier_income', {params:{fromDate:from,toDate:to,account_id:account_id}}).
            success(function(data, status)
            {
                $scope.incomes = data;
            });
    };

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Income Register(Courier Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });




    $scope.newIncome = function () {
        $scope.incomeedit = true;
        $scope.newincome = new LedgerPosting();
        $scope.newincome.date=new Date();
        $scope.curIncome = {};

    };
    $scope.editIncome = function (thisHead) {
        $scope.incomeedit = true;
        getSelectedItem(thisHead.accounthead_id);
        $scope.curIncome =  thisHead;
        $scope.newincome = new LedgerPosting();
        $scope.newincome = angular.copy(thisHead);
        $scope.newincome.date=new Date(thisHead.date);
        $anchorScroll();
    };
    function getSelectedItem(id){
        $http.get('/api/getAccountHead/'+id).
            success(function(data, status)
            {
                $scope.selectedItem=data;
            });
    }
    $scope.addIncome = function () {
        $scope.newincome.accounthead_id=$scope.selectedItem.id;
        $scope.newincome.date=new Date($scope.newincome.date);
        if ($scope.curIncome.id) {
            $http.put('/api/ledgerposting/'+$scope.curIncome.id, $scope.newincome).
                success(function(data, status)
                {
                    loadLedgerPosting();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                });
        } else{
            $scope.newincome.$save(function(account){
                loadLedgerPosting();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Your Transaction saved Successfully');
            });
        }
        $scope.incomeedit = false;
    };
    $scope.deleteIncome = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/ledgerposting/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction Deleted Successfully');
                    loadLedgerPosting();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelIncome = function () {
        $scope.incomeedit = false;
        $scope.newincome =  new LedgerPosting();
    };




    $scope.simulateQuery = false;
    $scope.isDisabled    = false;

    // list of `state` value/display objects
    $scope.states        = [];loadAll();
    $scope.querySearch   = querySearch;
    $scope.selectedItemChange = selectedItemChange;
    $scope.searchTextChange   = searchTextChange;

    $scope.newState = newState;

    function newState(state) {
        alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
        var results = query ? $scope.states.filter( createFilterFor(query) ) : $scope.states,
            deferred;
        if ($scope.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    function searchTextChange(text) {
        //$log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
        //$log.info('Item changed to ' + JSON.stringify(item));
    }


    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
        $scope.data1=[];
        $http.get('/api/accounthead_income/',{params:{head:'Courier'}}).
            success(function(data, status)
            {
                $scope.states=data;
            });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {

        var lowercaseQuery =query; //angular.lowercase(query);

        return function filterFn(state) {
            return (state.value.indexOf(lowercaseQuery) === 0);
        };

    }

    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.accounthead_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.export=function(fromDate,toDate,account_id){
        var req = {
            method: 'POST',
            url: '/api/excelExportLedgerPosting',
            responseType:"arraybuffer",
            data: {fromDate:fromDate,toDate:toDate,account_id:account_id,type:'income',group:'Courier'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='IncomeReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };
});
app.controller('CourierDaybookController', function($scope,$http,$filter,$window,$mdDialog,UserInfo,LedgerPosting){


    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.daybooks =[];
    function loadLedgerPosting() {
        $http.get('/api/getDaybookByGroup', {params:{fromDate:$scope.fromDate,toDate:$scope.toDate,group:'Courier'}}).
            success(function(data, status)
            {
                $scope.daybooks = data;
                getSum(data);
            });
    }loadLedgerPosting();

    $scope.search=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/getDaybookByGroup', {params:{fromDate:from,toDate:to,account_id:account_id,group:'Courier'}}).
            success(function(data, status)
            {
                $scope.daybooks = data;
                getSum(data);
            });
    };

    $scope.totalincome=0;
    $scope.totalexpense=0;
    function getSum(daybooks){
        var length=daybooks.length;
        var income=0;
        var expense=0;
        for(var i=0;i<length;i++){
            if(daybooks[i].accounthead.type==='income')
                income+=parseFloat(daybooks[i].amount);
            if(daybooks[i].accounthead.type==='expense')
                expense+=parseFloat(daybooks[i].amount);
        }
        $scope.totalincome=income;
        $scope.totalexpense=expense;
    }

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Day Book(Courier Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });

    $scope.account_id = 'All';


    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.accounthead_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.showProgress=false;
    $scope.export=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $scope.showProgress=true;
        var req = {
            method: 'POST',
            url: '/api/excelExportDaybookByGroup',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,group:'Courier'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='DaybookReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);
                $scope.showProgress=false;
            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

    $scope.exportPDF=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $scope.showProgress=true;
        var req = {
            method: 'POST',
            url: '/api/pdfExportDayBook',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,group:'Courier'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='DaybookReport.pdf' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();

                // Remove anchor from body
                document.body.removeChild(a);

                $scope.showProgress=false;

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    }

});
app.controller('DaybookController', function($scope,$http,$window,$filter,$mdDialog,UserInfo,LedgerPosting){


    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.daybooks =[];
    function loadLedgerPosting() {
        LedgerPosting.query(function (head) {
            $scope.daybooks = head;
            //console.log(head);
            getSum(head);
        });
    }loadLedgerPosting();

    $scope.search=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_overall_daybook', {params:{fromDate:from,toDate:to,account_id:account_id}}).
            success(function(data, status)
            {
                $scope.daybooks = data;
                getSum(data);
            });
    };

    $scope.totalincome=0;
    $scope.totalexpense=0;
    function getSum(daybooks){
        var length=daybooks.length;
        var income=0;
        var expense=0;
        for(var i=0;i<length;i++){
            if(daybooks[i].accounthead.type==='income')
                income+=parseFloat(daybooks[i].amount);
            if(daybooks[i].accounthead.type==='expense')
                expense+=parseFloat(daybooks[i].amount);
        }
        $scope.totalincome=income;
        $scope.totalexpense=expense;
    }

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Day Book(Day Book)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });

    $scope.account_id = 'All';


    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.accounthead_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.showProgress=false;
    $scope.export=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $scope.showProgress=true;
        var req = {
            method: 'POST',
            url: '/api/excelExportDaybook',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='DaybookReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);
                $scope.showProgress=false;
            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

    $scope.exportPDF=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $scope.showProgress=true;
        var req = {
            method: 'POST',
            url: '/api/pdfExportDayBook',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,group:'All'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='DaybookReport.pdf' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();

                // Remove anchor from body
                document.body.removeChild(a);

                $scope.showProgress=false;

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

});
app.controller('DesignationController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,Designation){
    $scope.designationedit = false;
    $scope.newdesignation = new Designation();
    $scope.curDesignation = {};
    $scope.designations=[];
    function loadDesignation() {
        Designation.query(function (head) {
            $scope.designations = head;
        });
    }loadDesignation();

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Designation(Main Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });




    $scope.newDesignation = function () {
        $scope.designationedit = true;
        $scope.newdesignation = new Designation();
        $scope.curDesignation = {};
    };
    $scope.editDesignation = function (thisHead) {
        $scope.designationedit = true;
        $scope.curDesignation =  thisHead;
        $scope.newdesignation = new Designation();
        $scope.newdesignation = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addDesignation = function () {
        $scope.newdesignation.default=0;
        if ($scope.curDesignation.id) {
            $http.put('/api/designation/'+$scope.curDesignation.id, $scope.newdesignation).
                success(function(data, status)
                {
                    loadDesignation();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Designation updated Successfully');
                });
        } else{
            $scope.newdesignation.$save(function(account){
                loadDesignation();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Designation saved Successfully');
            });
        }
        $scope.designationedit = false;
    };
    $scope.deleteDesignation = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/designation/'+item.id).
                success(function(data, status)
                {
                    loadDesignation();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });
                    if(data.default===true){
                        ngNotify.set('Designation deleted Successfully');
                    }else{
                        ngNotify.set('No Permission to delete This Item');
                    }

                    console.log(data.default);
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelDesignation = function () {
        $scope.designationedit = false;
        $scope.newdesignation =  new Designation();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Designation Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeDesignationStatus/'+id).
                success(function(data, status)
                {
                    loadDesignation();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Designation Status updated Successfully');


                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.open=function(data,ev){
        $http.get('/api/designation/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/designation',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {

        $scope.designation=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

});
app.controller('DriverController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,Employee){
    $scope.driveredit = false;
    $scope.newdriver = new Employee();
    $scope.curDriver = {};

    $scope.basepath=$window.location.origin;

    $scope.drivers=[];
    function loadEmployee() {
        Employee.query({designation_id:1},function (head) {
            $scope.drivers = head;
        });
    }loadEmployee();

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Drivers(Transportation)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });



    $scope.newDriver = function () {
        $scope.driveredit = true;
        $scope.newdriver = new Employee();
        $scope.curDriver = {};
    };
    $scope.editDriver = function (thisHead) {
        $scope.driveredit = true;
        $scope.curDriver =  thisHead;
        $scope.newdriver = new Employee();
        $scope.newdriver = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addDriver = function () {

        var photos=[];
        angular.forEach($scope.photos, function (value, key) {
            photos[key]=value;
        });

        if ($scope.curDriver.id) {
            /*$http.put('/api/employee/'+$scope.curDriver.id, $scope.newdriver).
                success(function(data, status)
                {
                    $window.location.reload();
                });*/
            $http.put('/api/employee/'+$scope.curDriver.id,{name:$scope.newdriver.name,
                mobile:$scope.newdriver.mobile,whatsapp:$scope.newdriver.whatsapp,
                email:$scope.newdriver.email,addresskuwait:$scope.newdriver.addresskuwait,
                addressindia:$scope.newdriver.addressindia,mobileindia:$scope.newdriver.mobileindia,
                designation_id:1,basicpay:$scope.newdriver.basicpay,
                overtimepay:$scope.newdriver.overtimepay,passportno:$scope.newdriver.passportno,
                visano:$scope.newdriver.visano,drivinglicenseno:$scope.newdriver.drivinglicenseno,
                remark:$scope.newdriver.remark, photos:photos}).
                success(function(data, status)
                {
                    loadEmployee();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Employee updated Successfully');
                });
        } else{
            /*$scope.newdriver.$save(function(account){
                $window.location.reload();
            });*/
            $http.post('/api/employee',{name:$scope.newdriver.name,
                mobile:$scope.newdriver.mobile,whatsapp:$scope.newdriver.whatsapp,
                email:$scope.newdriver.email,addresskuwait:$scope.newdriver.addresskuwait,
                addressindia:$scope.newdriver.addressindia,mobileindia:$scope.newdriver.mobileindia,
                designation_id:1,basicpay:$scope.newdriver.basicpay,
                overtimepay:$scope.newdriver.overtimepay,passportno:$scope.newdriver.passportno,
                visano:$scope.newdriver.visano,drivinglicenseno:$scope.newdriver.drivinglicenseno,
                remark:$scope.newdriver.remark, photos:photos}).
                success(function(data, status)
                {
                    loadEmployee();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Employee saved Successfully');
                });
        }
        $scope.driveredit = false;
    };
    $scope.deleteDriver = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/employee/'+item.id).
                success(function(data, status)
                {
                    loadEmployee();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Employee deleted Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelDriver = function () {
        $scope.driveredit = false;
        $scope.newdriver =  new Employee();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Driver Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeEmployeeStatus/'+id).
                success(function(data, status)
                {
                    loadEmployee();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Employee Status changed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.open=function(data,ev){
        $http.get('/api/employee/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/employee',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {

        $scope.basepath=$window.location.origin;
        $scope.employee=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

});
/**
 * Created by psybo on 18/5/17.
 */
app.controller('DutychartController', function($scope,$http,$window,$mdDialog,$location,UserInfo){

    $scope.driver_id='All';
    $scope.showProgress=false;

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Duty Chart(Transportation)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });

    $scope.account_id = 'All';

    $scope.duties=[];
    $scope.getDutyChart=function(){
        $scope.showProgress=true;
        $http.post('/api/dutychartjson',{year:$scope.year,driver_id:$scope.driver_id}).
            success(function(data, status)
            {
                $scope.duties=data;
                getSum(data);
                $scope.showProgress=false;
            });
    };

    $scope.jansum=0;
    $scope.febsum=0;
    $scope.marsum=0;
    $scope.aprsum=0;
    $scope.maysum=0;
    $scope.junsum=0;
    $scope.julsum=0;
    $scope.augsum=0;
    $scope.sepsum=0;
    $scope.octsum=0;
    $scope.novsum=0;
    $scope.decsum=0;

    function getSum(data){
        $scope.jansum=0;
        $scope.febsum=0;
        $scope.marsum=0;
        $scope.aprsum=0;
        $scope.maysum=0;
        $scope.junsum=0;
        $scope.julsum=0;
        $scope.augsum=0;
        $scope.sepsum=0;
        $scope.octsum=0;
        $scope.novsum=0;
        $scope.decsum=0;
        var length=data.length;
        for(var i=0;i<length;i++){
            if(data[i].jan!='NPD')
                $scope.jansum+=parseFloat(data[i].jan);

            if(data[i].feb!='NPD')
                $scope.febsum+=parseFloat(data[i].feb);

            if(data[i].mar!='NPD')
                $scope.marsum+=parseFloat(data[i].mar);

            if(data[i].apr!='NPD')
                $scope.aprsum+=parseFloat(data[i].apr);

            if(data[i].may!='NPD')
                $scope.maysum+=parseFloat(data[i].may);

            if(data[i].jun!='NPD')
                $scope.junsum+=parseFloat(data[i].jun);

            if(data[i].jul!='NPD')
                $scope.julsum+=parseFloat(data[i].jul);

            if(data[i].aug!='NPD')
                $scope.augsum+=parseFloat(data[i].aug);

            if(data[i].sep!='NPD')
                $scope.sepsum+=parseFloat(data[i].sep);

            if(data[i].oct!='NPD')
                $scope.octsum+=parseFloat(data[i].oct);

            if(data[i].nov!='NPD')
                $scope.novsum+=parseFloat(data[i].nov);

            if(data[i].dec!='NPD')
                $scope.decsum+=parseFloat(data[i].dec);
        }
    }



    $scope.getPaidAndUnpaidPDF=function(year,driver_id){

        $scope.showProgress=true;
        var req = {
            method: 'PUT',
            url: '/api/getPaidAndUnpaidPDF',
            responseType:"arraybuffer",
            data: {year: year, driver_id: driver_id}
        };

        $http(req)
            .success(function(data, status, headers){

                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='dutychart.pdf' ;//headers('filename');

                document.body.appendChild(a);
                a.click();


                document.body.removeChild(a);
                $scope.showProgress=false;
            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

    $scope.getUnpaidPDF=function(year,driver_id){
        $scope.showProgress=true;
        var req = {
            method: 'PUT',
            url: '/api/getUnpaidPDF',
            responseType:"arraybuffer",
            data: {year: year, driver_id: driver_id}
        };

        $http(req)
            .success(function(data, status, headers){

                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='unpaidPDF.pdf' ;//headers('filename');

                document.body.appendChild(a);
                a.click();


                document.body.removeChild(a);
                $scope.showProgress=false;
            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

    $scope.getPaidPDF=function(year,driver_id){
        $scope.showProgress=true;
        var req = {
            method: 'PUT',
            url: '/api/getPaidPDF',
            responseType:"arraybuffer",
            data: {year: year, driver_id: driver_id}
        };

        $http(req)
            .success(function(data, status, headers){

                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='paidPDF.pdf' ;//headers('filename');

                document.body.appendChild(a);
                a.click();


                document.body.removeChild(a);
                $scope.showProgress=false;
            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };





    $scope.openResidence=function(data,ev){
        $http.get('/api/residence/'+data.residence_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogResidenceController,
                    templateUrl: $window.location.origin+'/template/residence',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogResidenceController($scope, $mdDialog,data) {
        $scope.residence=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openHospital=function(data,ev){
        $http.get('/api/hospital/'+data.hospital_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogHospitalController,
                    templateUrl: $window.location.origin+'/template/hospital',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogHospitalController($scope, $mdDialog,data) {
        $scope.hospital=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openEmployee=function(data,ev){
        $http.get('/api/employee/'+data.employee_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/employee',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {

        $scope.basepath=$window.location.origin;
        $scope.employee=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openDutyChart=function(data,residence,hospital,driver,ev){
        $mdDialog.show({
            controller: DialogDutyController,
            templateUrl: $window.location.origin+'/template/dutychart',
            parent: angular.element(document.querySelector('#popup')),
            targetEvent: ev,
            clickOutsideToClose:true,
            locals: {
                data: data,
                residence:residence,
                hospital:hospital,
                driver:driver
            }
        })
            .then(function(answer) {
                //$scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                //$scope.status = 'You cancelled the dialog.';
            });

    };

    function DialogDutyController($scope, $mdDialog,$window,data,residence,hospital,driver) {

        $scope.dutychart=data;
        $scope.residence=residence;
        $scope.hospital=hospital;
        $scope.driver=driver;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.export=function(year,driver_id){
        var req = {
            method: 'POST',
            url: '/api/excelExportDutyChart',
            responseType:"arraybuffer",
            data: {year:year,driver_id:driver_id}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='DutyChartReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

});
app.controller('EmployeeController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,Employee){
    $scope.employeeedit = false;
    $scope.newemployee = new Employee();
    $scope.curEmployee = {};

    $scope.basepath=$window.location.origin;

    $scope.employees=[];
    function loadEmployee() {
        Employee.query(function (head) {
            $scope.employees = head;
        });
    }loadEmployee();

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Employee Register(Main Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });

    $scope.designations=[];

    function loadDesignation(){
        $http.get('/api/getDesignation').
            success(function(data, status)
            {
                $scope.designations=data;
            });
    }loadDesignation();



    $scope.newEmployee = function () {
        $scope.employeeedit = true;
        $scope.newemployee = new Employee();
        $scope.curEmployee = {};
    };
    $scope.editEmployee = function (thisHead) {
        $scope.employeeedit = true;
        $scope.curEmployee =  thisHead;
        $scope.newemployee = new Employee();
        $scope.newemployee = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addEmployee = function () {

        var photos=[];
        angular.forEach($scope.photos, function (value, key) {
            photos[key]=value;
        });

        if ($scope.curEmployee.id) {
            /*$http.put('/api/employee/'+$scope.curEmployee.id, $scope.newemployee).
                success(function(data, status)
                {
                    $window.location.reload();
                });*/
                $http.put('/api/employee/'+$scope.curEmployee.id,{name:$scope.newemployee.name,
                    mobile:$scope.newemployee.mobile,whatsapp:$scope.newemployee.whatsapp,
                    email:$scope.newemployee.email,addresskuwait:$scope.newemployee.addresskuwait,
                    addressindia:$scope.newemployee.addressindia,mobileindia:$scope.newemployee.mobileindia,
                    designation_id:$scope.newemployee.designation_id,basicpay:$scope.newemployee.basicpay,
                    overtimepay:$scope.newemployee.overtimepay,passportno:$scope.newemployee.passportno,
                    visano:$scope.newemployee.visano,drivinglicenseno:$scope.newemployee.drivinglicenseno,
                    remark:$scope.newemployee.remark, photos:photos}).
                    success(function(data, status)
                    {
                        loadEmployee();
                        ngNotify.config({
                            theme: 'pure',
                            position: 'top',
                            duration: 3000,
                            type: 'info',
                            sticky: false,
                            button: true,
                            html: false
                        });

                        ngNotify.set('Employee updated Successfully');
                    });
        } else{
            $http.post('/api/employee',{name:$scope.newemployee.name,
                    mobile:$scope.newemployee.mobile,whatsapp:$scope.newemployee.whatsapp,
                    email:$scope.newemployee.email,addresskuwait:$scope.newemployee.addresskuwait,
                    addressindia:$scope.newemployee.addressindia,mobileindia:$scope.newemployee.mobileindia,
                    designation_id:$scope.newemployee.designation_id,basicpay:$scope.newemployee.basicpay,
                    overtimepay:$scope.newemployee.overtimepay,passportno:$scope.newemployee.passportno,
                    visano:$scope.newemployee.visano,drivinglicenseno:$scope.newemployee.drivinglicenseno,
                    remark:$scope.newemployee.remark, photos:photos}).
                success(function(data, status)
                {
                    loadEmployee();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Employee saved Successfully');
                });
            /*$scope.newemployee.$save(function(account){
                $window.location.reload();
            });*/
        }
        $scope.employeeedit = false;
    };
    $scope.deleteEmployee = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/employee/'+item.id).
                success(function(data, status)
                {
                    loadEmployee();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Employee deleted Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelEmployee = function () {
        $scope.employeeedit = false;
        $scope.newemployee =  new Employee();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Employee Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeEmployeeStatus/'+id).
                success(function(data, status)
                {
                    loadEmployee();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Employee Status changed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.open=function(data,ev){
        $http.get('/api/employee/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/employee',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {

        $scope.basepath=$window.location.origin;
        $scope.employee=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

});
app.controller('FairController', function($scope,$http,$window,$location,$log,$timeout,$mdDialog,$anchorScroll,ngNotify,UserInfo,FairCollection){
    $scope.fairedit = false;
    $scope.newfair = new FairCollection();
    $scope.curFair = {};
    $scope.candidate_id = 'All';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);

    $scope.year=year;

    $scope.fairs =[];
    function loadFairCollection() {
        FairCollection.query(function (head) {
            $scope.fairs = head;
            //console.log(head);
        });
    }loadFairCollection();

    $scope.showProgress=false;
    $scope.showProgressSave=false;

    $scope.search=function(fromDate,toDate,candidate_id){
        $http.get('/api/search_transportation_fair', {params:{fromDate:$scope.fromDate,toDate:$scope.toDate,candidate_id:candidate_id}}).
            success(function(data, status)
            {
                $scope.fairs = data;
            });
    };

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Fair Register(Transportation)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });

    $scope.employees={};
    function loadEmployee(){
        $http.get('/api/getDriver').
            success(function(data, status)
            {
                $scope.employees=data;
            });
    }
    loadEmployee();


    $scope.newfairs=[];
    $scope.newFair = function () {
        $scope.fairedit = true;
        $scope.newfair = new FairCollection();
        $scope.month=new Date();
        $scope.curFair = {};
        $scope.newfairs={};
    };
    $scope.getFair=function(month){
        $scope.showProgress=true;
        $scope.newfairs=[];
        $http.post('/api/showoreditfair',{month:$scope.month}).
            success(function(data, status)
            {
                $scope.newfairs=data;
                $scope.showProgress=false;
            });
    };
    $scope.editFair = function (thisHead) {
        $scope.fairedit = true;
        $scope.curFair =  thisHead;
        $scope.month=new Date(thisHead.date);
        $scope.newfairs=[];
        $http.post('/api/showoreditfair',{month:$scope.month}).
            success(function(data, status)
            {
                $scope.newfairs=data;
            });
        $anchorScroll();
    };
    $scope.addFair = function (fairs) {
        $scope.showProgressSave=true;
        if ($scope.curFair.id) {
            $http.put('/api/faircollection/'+$scope.curFair.id, {fairs:fairs}).
                success(function(data, status)
                {
                    loadFairCollection();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                    $scope.showProgressSave=false;
                });
        } else{

            $http.post('/api/faircollection',{fairs:fairs}).
                success(function(data, status)
                {
                    loadFairCollection();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction saved Successfully');
                    console.log(data);
                    $scope.showProgressSave=false;
                });
        }

        $scope.fairedit = false;
    };
    $scope.deleteFair = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/faircollection/'+item.id).
                success(function(data, status)
                {
                    loadFairCollection();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelFair = function () {
        $scope.fairedit = false;
        $scope.newfair =  new FairCollection();
    };




    $scope.simulateQuery = false;
    $scope.isDisabled    = false;

    // list of `state` value/display objects
    $scope.states        = [];loadAll();
    $scope.querySearch   = querySearch;
    $scope.selectedItemChange = selectedItemChange;
    $scope.searchTextChange   = searchTextChange;

    $scope.newState = newState;

    function newState(state) {
        alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
        var results = query ? $scope.states.filter( createFilterFor(query) ) : $scope.states,
            deferred;
        if ($scope.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    function searchTextChange(text) {
        //$log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
        //$log.info('Item changed to ' + JSON.stringify(item));
        $scope.newfair.employee_id=item.employee_id;
    }


    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
        $scope.data1=[];
        $http.get('/api/getCandidate/').
            success(function(data, status)
            {
                $scope.states=data;
            });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {

        var lowercaseQuery =query; //angular.lowercase(query);

        return function filterFn(state) {
            return (state.value.indexOf(lowercaseQuery) === 0);
        };

    }

    $scope.openEmployee=function(data,ev){
        $http.get('/api/employee/'+data.employee_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/employee',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {

        $scope.basepath=$window.location.origin;
        $scope.employee=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openCandidate=function(data,ev){
        $http.get('/api/transportation/'+data.candidate_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogCandidateController,
                    templateUrl: $window.location.origin+'/template/candidate',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogCandidateController($scope, $mdDialog,data) {

        $scope.candidate=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };

        $scope.openHospital=function(data,ev){
            $http.get('/api/hospital/'+data.hospital_id).
                success(function(data, status)
                {
                    $mdDialog.show({
                        controller: DialogHospitalController,
                        templateUrl: $window.location.origin+'/template/hospital',
                        parent: angular.element(document.querySelector('#popup')),
                        targetEvent: ev,
                        clickOutsideToClose:true,
                        locals: {
                            data: data
                        }
                    })
                        .then(function(answer) {
                            //$scope.status = 'You said the information was "' + answer + '".';
                        }, function() {
                            //$scope.status = 'You cancelled the dialog.';
                        });
                });

            // console.log($window.location.origin);
        };

        function DialogHospitalController($scope, $mdDialog,data) {

            $scope.hospital=data;
            $scope.hide = function() {
                $mdDialog.hide();
            };

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };
        }


    }


    $scope.export=function(fromDate,toDate,candidate_id){
        var req = {
            method: 'POST',
            url: '/api/excelExportFair',
            responseType:"arraybuffer",
            data: {fromDate:fromDate,toDate:toDate,candidate_id:candidate_id}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='FairCollectionReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };
});
/**
 * Created by psybo on 18/5/17.
 */
app.controller('FairRegisterReport1Controller', function($scope,$http,$filter,$window,$mdDialog,$location,ngNotify,UserInfo){

    $scope.driver_id='All';
    var makeDate = new Date();
    makeDate = new Date(makeDate.setMonth(makeDate.getMonth() - 1));
    $scope.month=makeDate;
    $scope.showProgress=false;

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Fair Register(Transportation)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });

    $scope.account_id = 'All';

    $scope.duties=[];
    $scope.getDutyChart=function(){
        $scope.showProgress=true;
        $scope.reports=[];

        $http.post('/api/fairregisterreportjson1',{month:$scope.month,driver_id:$scope.driver_id}).
            success(function(data, status)
            {
                $scope.reports=data;
                $scope.showProgress=false;
            });
    };



    $scope.showMonth=function(date){
        var d = new Date(date);
        var month = new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";
        var curmonth=d.getMonth();
        var premonth=0;
        var nextmonth=0;
        $scope.curMonth=month[curmonth];
        if(curmonth==0)
            premonth=11;
        else
            premonth=curmonth-1;

        if(curmonth==11)
            nextmonth=0;
        else
            nextmonth=curmonth+1;

        $scope.preMonth=month[premonth];
        $scope.nextMonth=month[nextmonth];
    };

    $scope.showMonth($scope.month);



    $scope.saveFair=function(fairs,month,ev){

        var date = $filter("date")(Date.parse(month), 'yyyy-MM-dd');
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to Save These Fair ?')
            .textContent('Are you sure to Save These Fair.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $scope.showProgress=true;
            $http.post('/api/savefairregister',{fairs:fairs,date:date}).
                success(function(data, status)
                {
                    console.log(data);
                    $scope.showProgress=false;
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Fairs Saved Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.openResidence=function(data,ev){
        $http.get('/api/residence/'+data.residence_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogResidenceController,
                    templateUrl: $window.location.origin+'/template/residence',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogResidenceController($scope, $mdDialog,data) {
        $scope.residence=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openHospital=function(data,ev){
        $http.get('/api/hospital/'+data.hospital_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogHospitalController,
                    templateUrl: $window.location.origin+'/template/hospital',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogHospitalController($scope, $mdDialog,data) {
        $scope.hospital=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openEmployee=function(data,ev){
        $http.get('/api/employee/'+data.employee_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/employee',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {

        $scope.basepath=$window.location.origin;
        $scope.employee=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openDutyChart=function(data,residence,hospital,driver,ev){
        $mdDialog.show({
            controller: DialogDutyController,
            templateUrl: $window.location.origin+'/template/dutychart',
            parent: angular.element(document.querySelector('#popup')),
            targetEvent: ev,
            clickOutsideToClose:true,
            locals: {
                data: data,
                residence:residence,
                hospital:hospital,
                driver:driver
            }
        })
            .then(function(answer) {
                //$scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                //$scope.status = 'You cancelled the dialog.';
            });

    };

    function DialogDutyController($scope, $mdDialog,$window,data,residence,hospital,driver) {

        $scope.dutychart=data;
        $scope.residence=residence;
        $scope.hospital=hospital;
        $scope.driver=driver;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }


});
/**
 * Created by psybo on 18/5/17.
 */
app.controller('FairRegisterReport2Controller', function($scope,$http,$window,$mdDialog,$location,UserInfo){

    $scope.driver_id='All';
    $scope.hospital_id='All';
    var d=new Date();
    d.setDate(1);
    d.setDate(-2);
    $scope.month=d;
    $scope.showProgress=false;

    $scope.basedon='Driver';

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Fair Register Report 2(Transportation)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });

    $scope.account_id = 'All';
    $scope.status = 'All';
    $scope.totalIncome=0;

    $scope.duties=[];
    $scope.getDutyChart=function(){
        $scope.showProgress=true;
        $scope.reports=[];
        $http.post('/api/fairregisterreportjson2',{month:$scope.month,driver_id:$scope.driver_id,status: $scope.status,basedon:$scope.basedon,hospital_id:$scope.hospital_id}).
            success(function(data, status)
            {
                $scope.reports=data;
                getSum(data);
                $scope.showProgress=false;
            });
    };

    function getSum(data){
        var length=data.length;
        var sum=0;
        for(var i=0;i<length;i++){
            if(data[i].curMonth!='NPD')
                sum=sum+parseFloat(data[i].curMonth);
        }
        $scope.totalIncome=sum;
    }

    $scope.changeStatus=function(status){
        $scope.showProgress=true;
        $scope.reports=[];
        var reports=[];
        $http.post('/api/fairregisterreportjson2',{month:$scope.month,driver_id:$scope.driver_id,status:status}).
            success(function(data, jsonstatus)
            {
                $scope.reports=data;
                getSum(data);
                $scope.showProgress=false;
            });

    };

    $scope.showMonth=function(date){
        var d = new Date(date);
        var month = new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";
        var curmonth=d.getMonth();
        $scope.curMonth=month[curmonth];
    };

    $scope.showMonth($scope.month);

    $scope.showProgressExport=false;
    $scope.exportExcel=function(month,driver_id,status,basedon,hospital_id){
        $scope.showProgressExport=true;
        var req = {
            method: 'POST',
            url: '/api/excelExportFairRegisterReport2',
            responseType:"arraybuffer",
            data: {month:month,driver_id:driver_id,status:status,basedon:basedon,hospital_id:hospital_id}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='excelExportFairRegisterReport2.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();

                // Remove anchor from body
                document.body.removeChild(a);
                $scope.showProgressExport=false;
            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

    $scope.exportPDF=function(month,driver_id,status,basedon,hospital_id){
        $scope.showProgressExport=true;
        var req = {
            method: 'POST',
            url: '/api/pdfExportFairRegisterReport2',
            responseType:"arraybuffer",
            data: {month:month,driver_id:driver_id,status:status,basedon:basedon,hospital_id:hospital_id}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='pdfExportFairRegisterReport2.pdf' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();

                // Remove anchor from body
                document.body.removeChild(a);
                $scope.showProgressExport=false;
            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };


    $scope.openResidence=function(data,ev){
        $http.get('/api/residence/'+data.residence_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogResidenceController,
                    templateUrl: $window.location.origin+'/template/residence',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogResidenceController($scope, $mdDialog,data) {
        $scope.residence=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openHospital=function(data,ev){
        $http.get('/api/hospital/'+data.hospital_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogHospitalController,
                    templateUrl: $window.location.origin+'/template/hospital',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogHospitalController($scope, $mdDialog,data) {
        $scope.hospital=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openEmployee=function(data,ev){
        $http.get('/api/employee/'+data.employee_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/employee',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {

        $scope.basepath=$window.location.origin;
        $scope.employee=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openDutyChart=function(data,residence,hospital,driver,ev){
        $mdDialog.show({
            controller: DialogDutyController,
            templateUrl: $window.location.origin+'/template/dutychart',
            parent: angular.element(document.querySelector('#popup')),
            targetEvent: ev,
            clickOutsideToClose:true,
            locals: {
                data: data,
                residence:residence,
                hospital:hospital,
                driver:driver
            }
        })
            .then(function(answer) {
                //$scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                //$scope.status = 'You cancelled the dialog.';
            });

    };

    function DialogDutyController($scope, $mdDialog,$window,data,residence,hospital,driver) {

        $scope.dutychart=data;
        $scope.residence=residence;
        $scope.hospital=hospital;
        $scope.driver=driver;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }


});
app.controller('HomeController', function($scope,$http,$window,$anchorScroll,UserInfo){

    if (localStorage.getItem("itemPerPage") === null) {
        localStorage.setItem("itemPerPage", 10);
    }
    function getPerPage(){
        return parseInt(localStorage.itemPerPage);
    }

    $scope.changeNum = function (itemNum) {
        localStorage.itemPerPage = itemNum;
        $scope.numPerPage = getPerPage();

        //$window.location.href = refineUrl()+'?num='+itemNum;
    };

    $scope.moveToTop=function(){
        $anchorScroll();
    };

    /*function refineUrl()
    {
        //get full url
        var url = window.location.href;
        //get url after/
        var value = url.substring(url.lastIndexOf('/') + 1);
        //get the part after before ?
        value  = value.split("?")[0];
        return value;
    }*/



    //console.log(localStorage.itemPerPage);
    $scope.numsForPage = [5, 10, 25, 50, 100];
    $scope.currentPage = 1;
    $scope.numPerPage = getPerPage();
    $scope.maxSize = 5;

    $scope.user = {};

    UserInfo.query().success(function(data){
        $scope.user = data;
        console.log(data);
    });

});
app.controller('HospitalController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,Hospital){
    $scope.hospitaledit = false;
    $scope.newhospital = new Hospital();
    $scope.curHospital = {};

    $scope.hospitals=[];
    function loadHospital() {
        Hospital.query(function (head) {
            $scope.hospitals = head;
        });
    }loadHospital();

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Hospitals(Nurse Account)" || menu[i].name==="Hospitals(Transportation)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });




    $scope.newHospital = function () {
        $scope.hospitaledit = true;
        $scope.newhospital = new Hospital();
        $scope.curHospital = {};
    };
    $scope.editHospital = function (thisHead) {
        $scope.hospitaledit = true;
        $scope.curHospital =  thisHead;
        $scope.newhospital = new Hospital();
        $scope.newhospital = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addHospital = function () {
        $scope.newhospital.default=0;
        if ($scope.curHospital.id) {
            $http.put('/api/hospital/'+$scope.curHospital.id, $scope.newhospital).
                success(function(data, status)
                {
                    loadHospital();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Hospital updated Successfully');
                });
        } else{
            $scope.newhospital.$save(function(account){
                loadHospital();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Hospital saved Successfully');
            });
        }
        $scope.hospitaledit = false;
    };
    $scope.deleteHospital = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/hospital/'+item.id).
                success(function(data, status)
                {
                    loadHospital();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Hospital removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelHospital = function () {
        $scope.hospitaledit = false;
        $scope.newhospital =  new Hospital();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Hospital Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeHospitalStatus/'+id).
                success(function(data, status)
                {
                    loadHospital();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Hospital Status Changed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.openHospital=function(data,ev){
        $http.get('/api/hospital/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogHospitalController,
                    templateUrl: $window.location.origin+'/template/hospital',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogHospitalController($scope, $mdDialog,data) {
        $scope.hospital=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }



});
app.controller('MainAccountAccountHeadController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,AccountHead){
    $scope.headedit = false;
    $scope.newaccount = new AccountHead();
    $scope.curAccount = {};

    $scope.accountheads=[];

    function loadAccountHead() {
        AccountHead.query({group: 'Main'}, function (employee) {
            $scope.accountheads = employee;
        });
    }loadAccountHead();


    $scope.types = [
        { value: 'income', name: 'Income' },
        { value: 'expense', name: 'Expense' }
    ];

   // $scope.numPerPage=5;

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Account Head(Main Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });



    $scope.newAccountHead = function () {
        $scope.headedit = true;
        $scope.newaccount = new AccountHead();
        $scope.curAccount = {};
    };
    $scope.editAccountHeadt = function (thisHead) {
        $scope.headedit = true;
        $scope.curAccount =  thisHead;
        $scope.newaccount = new AccountHead();
        $scope.newaccount = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addAccountHead = function () {
        $scope.newaccount.group='Main';
        $scope.newaccount.default=0;
        if ($scope.curAccount.id) {
            $http.put('/api/accounthead/'+$scope.curAccount.id, $scope.newaccount).
                success(function(data, status)
                {
                    //$window.location.reload();
                    loadAccountHead();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Account Head updated Successfully');
                });
        } else{
            $scope.newaccount.$save(function(account){
                //$window.location.reload();
               // $window.location.href=$window.location.href;
                $scope.accountheads.push(account);
                loadAccountHead();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Account Head Saved Successfully');

            });
        }
        $scope.headedit = false;
    };
    $scope.deleteAccountHead = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/accounthead/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    if(data.default===true) {
                        ngNotify.set('Account Head Deleted Successfully');
                    }else{
                        ngNotify.set(' Account Head Set has DEFAULT...Not DELETABLE');
                    }
                    loadAccountHead();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelAccountHead = function () {
        $scope.headedit = false;
        $scope.newaccount =  new AccountHead();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Account Head Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeAccountHeadStatus/'+id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Account Head Status Changed Successfully');

                    loadAccountHead();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

});
app.controller('MainAccountBillBookController', function($scope,$http,$window,$filter,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,AccountHead,BillBook){
    $scope.billedit = false;
    $scope.newbill = new BillBook();
    $scope.curBill = {};

    $scope.billbooks=[];

    function loadBillBook() {
        BillBook.query(function (bill) {
            $scope.billbooks = bill;
        });
    }loadBillBook();



    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Bill Book(Main Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });

    $scope.editingData=[];
    $scope.newitems = [];
    $scope.updateitem = {};
    $scope.addItem = function () {
        var row=$scope.newitems.length;
        $scope.inserted = {
            id: row,
            amount: '1'
        };
        $scope.newitems.push($scope.inserted);
        $scope.editingData[row] = true;
        $scope.addTotal();
    };

    $scope.updateItem=function(item){
        if(item.accounthead_id!=null) {
            includeunit(item);
        }
    };

    function includeunit(item){
        $http.get('/api/accounthead/'+item.accounthead_id).
            success(function(data,status,headers,config){
                $scope.currow=data;

                updateRow(item);

                //console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }

    function updateRow(item){
        $scope.updateitem={
            id:item.id,
            amount:item.amount,
            accounthead_id:item.accounthead_id,
            name:$scope.currow.name
        };

        $scope.newitems[item.id]=$scope.updateitem;

        $scope.editingData[item.id] = false;
        $scope.addTotal();
    }

    $scope.editItem=function(item){
        $scope.newitems[item.id]=item;
        $scope.editingData[item.id] = true;
    };

    $scope.removeItem = function (item) {
        var confirmDelete = confirm("Do you really need to remove the item ?");
        if (confirmDelete) {
            var curIndex = $scope.newitems.indexOf(item);
            $scope.newitems.splice(curIndex, 1);
            $scope.addTotal();
        }
    };

    $scope.totalAmount=0;

     $scope.addTotal=function(){
        //$scope.totalAmount=parseFloat($scope.totalAmount)+parseFloat(amount);
        $scope.totalAmount=0;
        for(var i=0;i<$scope.newitems.length;i++)
            $scope.totalAmount=parseFloat($scope.totalAmount)+parseFloat($scope.newitems[i].amount)
    };


    AccountHead.query({group: 'Main'}, function (employee) {
        $scope.expenses = employee;
    });

    $scope.newBillBook = function () {
        $scope.billedit = true;
        $scope.newbill = new BillBook();
        $scope.newbill.date = new Date();
        $scope.curBill = {};
        $scope.newitems=[];
    };

    $scope.editBillBook = function (thisHead) {
        $scope.billedit = true;
        $scope.curBill =  thisHead;
        $scope.newbill = new BillBook();
        $scope.newbill = angular.copy(thisHead);
        $scope.newbill.date=new Date(thisHead.date);
        $scope.newitems=[];
        for(var i=0;i<thisHead.items.length;i++){
            $scope.updateItemEdit(thisHead.items[i]);
        }
        $anchorScroll();
    };
    $scope.updateItemEdit=function(item){
        if(item.accounthead_id!=null) {
            includeunitEdit(item);
        }
    };

    function includeunitEdit(item){
        $http.get('/api/accounthead/'+item.accounthead_id).
            success(function(data,status,headers,config){
                $scope.currow=data;

                updateRowEdit(item);

                //console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }

    function updateRowEdit(item){
        $scope.updateitem={
            id:item.id,
            amount:item.amount,
            accounthead_id:item.accounthead_id,
            name:$scope.currow.name
        };

        $scope.newitems.push($scope.updateitem);

        $scope.editingData[item.id] = false;
        $scope.addTotal();
    }


    $scope.addBillBook = function () {
        if ($scope.curBill.id) {
            if ($scope.newitems.length > 0) {
                var req = {
                    method: 'PUT',
                    url: '/api/bill/'+$scope.curBill.id,
                    responseType:"arraybuffer",
                    data: {newbill: $scope.newbill, items: $scope.newitems, total: $scope.totalAmount}
                };

                $http(req)
                    .success(function(data, status, headers){

                        loadBillBook();
                        ngNotify.config({
                            theme: 'pure',
                            position: 'top',
                            duration: 3000,
                            type: 'info',
                            sticky: false,
                            button: true,
                            html: false
                        });
                        ngNotify.set('Bill Saved Successfully');
                        $scope.billedit = false;

                        var arr = data;
                        var byteArray = new Uint8Array(arr);
                        var a = window.document.createElement('a');

                        a.href = window.URL.createObjectURL(
                            new Blob([byteArray], { type: 'application/octet-stream' })
                        );
                        a.download ='invoice.pdf' ;//headers('filename');

                        // Append anchor to body.
                        document.body.appendChild(a);
                        a.click();


                        // Remove anchor from body
                        document.body.removeChild(a);

                    }
                ).error(function(data, status, headers){
                        console.log(data);

                    }
                );
            }else{
                alert('No Item in List');
            }

        } else {
            if ($scope.newitems.length > 0) {
                var req = {
                    method: 'POST',
                    url: '/api/bill',
                    responseType:"arraybuffer",
                    data: {newbill: $scope.newbill, items: $scope.newitems, total: $scope.totalAmount}
                };

                $http(req)
                    .success(function(data, status, headers){

                        loadBillBook();
                        ngNotify.config({
                            theme: 'pure',
                            position: 'top',
                            duration: 3000,
                            type: 'info',
                            sticky: false,
                            button: true,
                            html: false
                        });
                        ngNotify.set('Bill Saved Successfully');
                        $scope.billedit = false;

                        var arr = data;
                        var byteArray = new Uint8Array(arr);
                        var a = window.document.createElement('a');

                        a.href = window.URL.createObjectURL(
                            new Blob([byteArray], { type: 'application/octet-stream' })
                        );
                        a.download ='invoice.pdf' ;//headers('filename');

                        // Append anchor to body.
                        document.body.appendChild(a);
                        a.click();


                        // Remove anchor from body
                        document.body.removeChild(a);

                    }
                ).error(function(data, status, headers){
                        console.log(data);

                    }
                );

            }else{
                alert('No Item in List');
            }
        }
    };

    $scope.deleteBillBook = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/bill/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Bill Deleted Successfully');
                    loadBillBook();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.cancelBillBook = function () {
        $scope.billedit = false;
        $scope.newbill =  new BillBook();
        $scope.editingData=[];
        $scope.newitems = [];
        $scope.totalAmount=0;
    };

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }
    $scope.employee_id='All';
    $scope.search=function(fromDate,toDate){
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        BillBook.query({fromDate:from,toDate:to},function (bill) {
            $scope.billbooks = bill;
        });
    };

});
app.controller('MainAccountExpenseController', function($scope,$http,$filter,$window,$location,$log,$timeout,$mdDialog,$anchorScroll,ngNotify,UserInfo,LedgerPosting){
    $scope.expenseedit = false;
    $scope.newexpense = new LedgerPosting();
    $scope.curExpense = {};
    $scope.account_id = 'All';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    //$scope.numPerPage=5;

    $scope.expenses =[];
    function loadLedgerPosting() {
        LedgerPosting.query({group:'Main',type:'expense'}, function (head) {
            $scope.expenses = head;
            //console.log(head);
        });
    }loadLedgerPosting();

    $scope.search=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_main_expense', {params:{fromDate:from,toDate:to,account_id:account_id}}).
            success(function(data, status)
            {
                $scope.expenses = data;
            });
    };

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Expense Register(Main Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });



    $scope.newExpense = function () {
        $scope.expenseedit = true;
        $scope.newexpense = new LedgerPosting();
        $scope.newexpense.date=new Date();
        $scope.curExpense = {};

    };
    $scope.editExpense = function (thisHead) {
        $scope.expenseedit = true;
        getSelectedItem(thisHead.accounthead_id);
        $scope.curExpense =  thisHead;
        $scope.newexpense = new LedgerPosting();
        $scope.newexpense = angular.copy(thisHead);
        $scope.newexpense.date=new Date(thisHead.date);
        $anchorScroll();
    };
    function getSelectedItem(id){
        $http.get('/api/getAccountHead/'+id).
            success(function(data, status)
            {
                $scope.selectedItem=data;
            });
    }
    $scope.addExpense = function () {
        $scope.newexpense.accounthead_id=$scope.selectedItem.id;
        $scope.newexpense.date=new Date($scope.newexpense.date);
        if ($scope.curExpense.id) {
            $http.put('/api/ledgerposting/'+$scope.curExpense.id, $scope.newexpense).
                success(function(data, status)
                {
                    loadLedgerPosting();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                });
        } else{
            $scope.newexpense.$save(function(account){
                loadLedgerPosting();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Your Transaction saved Successfully');
            });
        }
        $scope.expenseedit = false;
    };
    $scope.deleteExpense = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/ledgerposting/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction Deleted Successfully');
                    loadLedgerPosting();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelExpense = function () {
        $scope.expenseedit = false;
        $scope.newexpense =  new LedgerPosting();
    };




    $scope.simulateQuery = false;
    $scope.isDisabled    = false;

    // list of `state` value/display objects
    $scope.states        = [];loadAll();
    $scope.querySearch   = querySearch;
    $scope.selectedItemChange = selectedItemChange;
    $scope.searchTextChange   = searchTextChange;

    $scope.newState = newState;

    function newState(state) {
        alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
        var results = query ? $scope.states.filter( createFilterFor(query) ) : $scope.states,
            deferred;
        if ($scope.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    function searchTextChange(text) {
        //$log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
        //$log.info('Item changed to ' + JSON.stringify(item));
    }


    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
        $scope.data1=[];
        $http.get('/api/accounthead_expense/',{params:{head:'Main'}}).
            success(function(data, status)
            {
                $scope.states=data;
            });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {

        var lowercaseQuery =query; //angular.lowercase(query);

        return function filterFn(state) {
            return (state.value.indexOf(lowercaseQuery) === 0);
        };

    }

    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.accounthead_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.export=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        var req = {
            method: 'POST',
            url: '/api/excelExportLedgerPosting',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,type:'expense',group:'Main'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='ExpenseReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };
});
app.controller('MainAccountIncomeController', function($scope,$http,$window,$filter,$location,$log,$timeout,$mdDialog,$anchorScroll,ngNotify,UserInfo,LedgerPosting){
    $scope.incomeedit = false;
    $scope.newincome = new LedgerPosting();
    $scope.curIncome = {};
    $scope.account_id = 'All';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    //$scope.numPerPage=5;
    $scope.incomes =[];
    function loadLedgerPosting() {
        LedgerPosting.query({group:'Main',type:'income'}, function (head) {
            $scope.incomes = head;
            //console.log(head);
        });
    }loadLedgerPosting();

    $scope.search=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_main_income', {params:{fromDate:from,toDate:to,account_id:account_id}}).
            success(function(data, status)
            {
                $scope.incomes = data;
            });
    };

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Income Register(Main Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });


    $scope.newIncome = function () {
        $scope.incomeedit = true;
        $scope.newincome = new LedgerPosting();
        $scope.newincome.date=new Date();
        $scope.curIncome = {};

    };
    $scope.editIncome = function (thisHead) {
        $scope.incomeedit = true;
        getSelectedItem(thisHead.accounthead_id);
        $scope.curIncome =  thisHead;
        $scope.newincome = new LedgerPosting();
        $scope.newincome = angular.copy(thisHead);
        $scope.newincome.date=new Date(thisHead.date);
        $anchorScroll();
    };

    function getSelectedItem(id){
        $http.get('/api/getAccountHead/'+id).
            success(function(data, status)
            {
                $scope.selectedItem=data;
            });
    }

    $scope.addIncome = function () {
        $scope.newincome.accounthead_id=$scope.selectedItem.id;
        $scope.newincome.date=new Date($scope.newincome.date);
        if ($scope.curIncome.id) {
            $http.put('/api/ledgerposting/'+$scope.curIncome.id, $scope.newincome).
                success(function(data, status)
                {
                    loadLedgerPosting();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                });
        } else{
            $scope.newincome.$save(function(account){
                loadLedgerPosting();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Your Transaction saved Successfully');
            });
        }
        $scope.incomeedit = false;
    };
    $scope.deleteIncome = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/ledgerposting/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction Deleted Successfully');
                    loadLedgerPosting();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelIncome = function () {
        $scope.incomeedit = false;
        $scope.newincome =  new LedgerPosting();
    };




    $scope.simulateQuery = false;
    $scope.isDisabled    = false;

    // list of `state` value/display objects
    $scope.states        = [];loadAll();
    $scope.querySearch   = querySearch;
    $scope.selectedItemChange = selectedItemChange;
    $scope.searchTextChange   = searchTextChange;

    $scope.newState = newState;

    function newState(state) {
        alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
        var results = query ? $scope.states.filter( createFilterFor(query) ) : $scope.states,
            deferred;
        if ($scope.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    function searchTextChange(text) {
        //$log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
        //$log.info('Item changed to ' + JSON.stringify(item));
    }


    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
        $scope.data1=[];
        $http.get('/api/accounthead_income/',{params:{head:'Main'}}).
            success(function(data, status)
            {
                $scope.states=data;
            });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {

        var lowercaseQuery =query; //angular.lowercase(query);

        return function filterFn(state) {
            return (state.value.indexOf(lowercaseQuery) === 0);
        };

    }

    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.accounthead_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

       // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }


    $scope.export=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        var req = {
            method: 'POST',
            url: '/api/excelExportLedgerPosting',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,type:'income',group:'Main'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='IncomeReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };
});
app.controller('MainDaybookController', function($scope,$http,$filter,$window,$mdDialog,UserInfo,LedgerPosting){


    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.daybooks =[];
    function loadLedgerPosting() {
        var from = $filter("date")(Date.parse($scope.fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse($scope.toDate), 'yyyy-MM-dd');
        $http.get('/api/getDaybookByGroup', {params:{fromDate:from,toDate:to,group:'Main'}}).
            success(function(data, status)
            {
                $scope.daybooks = data;
                getSum(data);
            });
    }loadLedgerPosting();

    $scope.search=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');
        $http.get('/api/getDaybookByGroup', {params:{fromDate:from,toDate:to,account_id:account_id,group:'Main'}}).
            success(function(data, status)
            {
                $scope.daybooks = data;
                getSum(data);
            });
    };

    $scope.totalincome=0;
    $scope.totalexpense=0;
    function getSum(daybooks){
        var length=daybooks.length;
        var income=0;
        var expense=0;
        for(var i=0;i<length;i++){
            if(daybooks[i].accounthead.type==='income')
                income+=parseFloat(daybooks[i].amount);
            if(daybooks[i].accounthead.type==='expense')
                expense+=parseFloat(daybooks[i].amount);
        }
        $scope.totalincome=income;
        $scope.totalexpense=expense;
    }

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Day Book(Main Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });

    $scope.account_id = 'All';


    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.accounthead_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.showProgress=false;

    $scope.export=function(fromDate,toDate,account_id){
        $scope.showProgress=true;

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        var req = {
            method: 'POST',
            url: '/api/excelExportDaybookByGroup',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,group:'Main'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='DaybookReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

                $scope.showProgress=false;

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };



    $scope.exportPDF=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $scope.showProgress=true;
        var req = {
            method: 'POST',
            url: '/api/pdfExportDayBook',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,group:'Main'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='DaybookReport.pdf' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();

                // Remove anchor from body
                document.body.removeChild(a);

                $scope.showProgress=false;

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

});
app.controller('NurseController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,Nurse){
    $scope.nurseedit = false;
    $scope.newnurse = new Nurse();
    $scope.curNurse = {};

    $scope.nurses=[];
    function loadNurse() {
        Nurse.query(function (head) {
            $scope.nurses = head;
        });
    }loadNurse();

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Nurse Register(Nurse Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });

    $scope.batches=[];
    $scope.hospitals=[];

    function loadBatch(){
        $http.get('/api/getBatch').
            success(function(data, status)
            {
                $scope.batches=data;
            });
    }loadBatch();

    function loadHospital(){
        $http.get('/api/getHospital').
            success(function(data, status)
            {
                $scope.hospitals=data;
            });
    }loadHospital();



    $scope.newNurse = function () {
        $scope.nurseedit = true;
        $scope.newnurse = new Nurse();
        $scope.curNurse = {};
    };
    $scope.editNurse = function (thisHead) {
        $scope.nurseedit = true;
        $scope.curNurse =  thisHead;
        $scope.newnurse = new Nurse();
        $scope.newnurse = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addNurse = function () {
        $scope.newnurse.default=0;
        if ($scope.curNurse.id) {
            $http.put('/api/nurse/'+$scope.curNurse.id, $scope.newnurse).
                success(function(data, status)
                {
                    loadNurse();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Nurse updated Successfully');
                });
        } else{
            $scope.newnurse.$save(function(account){
                loadNurse();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Nurse saved Successfully');
            });
        }
        $scope.nurseedit = false;
    };
    $scope.deleteNurse = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/nurse/'+item.id).
                success(function(data, status)
                {
                    loadNurse();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Nurse removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelNurse = function () {
        $scope.nurseedit = false;
        $scope.newnurse =  new Nurse();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Nurse Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeNurseStatus/'+id).
                success(function(data, status)
                {
                    loadNurse();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Nurse Status Changed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };


    $scope.openHospital=function(data,ev){
        $http.get('/api/hospital/'+data.hospital_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogHospitalController,
                    templateUrl: $window.location.origin+'/template/hospital',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogHospitalController($scope, $mdDialog,data) {
        $scope.hospital=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openBatch=function(data,ev){
        $http.get('/api/batch/'+data.nursebatch_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/batch',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {

        $scope.batch=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openNurse=function(data,ev){
        $http.get('/api/nurse/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogNurseController,
                    templateUrl: $window.location.origin+'/template/nurse',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogNurseController($scope, $mdDialog,$window,data) {

        $scope.nurse=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

});
app.controller('NurseAccountAccountHeadController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,AccountHead){
    $scope.headedit = false;
    $scope.newaccount = new AccountHead();
    $scope.curAccount = {};

    //$scope.numPerPage=5;

    function loadAccountHead() {
        AccountHead.query({group: 'Nurse'}, function (head) {
            $scope.accountheads = head;
        });
    }loadAccountHead();

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Account Head(Nurse Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });




    $scope.newAccountHead = function () {
        $scope.headedit = true;
        $scope.newaccount = new AccountHead();
        $scope.curAccount = {};
    };
    $scope.editAccountHeadt = function (thisHead) {
        $scope.headedit = true;
        $scope.curAccount =  thisHead;
        $scope.newaccount = new AccountHead();
        $scope.newaccount = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addAccountHead = function () {
        $scope.newaccount.group='Nurse';
        $scope.newaccount.default=0;
        if ($scope.curAccount.id) {
            $http.put('/api/accounthead/'+$scope.curAccount.id, $scope.newaccount).
                success(function(data, status)
                {
                    loadAccountHead();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Account Head updated Successfully');
                });
        } else{
            $scope.newaccount.$save(function(account){
                loadAccountHead();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Account Head Saved Successfully');
            });
        }
        $scope.headedit = false;
    };
    $scope.deleteAccountHead = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/accounthead/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    if(data.default===true) {
                        ngNotify.set('Account Head Deleted Successfully');
                    }else{
                        ngNotify.set(' Account Head Set has DEFAULT...Not DELETABLE');
                    }
                    loadAccountHead();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelAccountHead = function () {
        $scope.headedit = false;
        $scope.newaccount =  new AccountHead();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Account Head Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeAccountHeadStatus/'+id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Account Head Status Changed Successfully');

                    loadAccountHead();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

});
app.controller('NurseAccountExpenseController', function($scope,$http,$filter,$window,$location,$log,$timeout,ngNotify,$mdDialog,$anchorScroll,UserInfo,LedgerPosting){
    $scope.expenseedit = false;
    $scope.newexpense = new LedgerPosting();
    $scope.curExpense = {};
    $scope.account_id = 'All';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.expenses =[];
    function loadLedgerPosting() {
        LedgerPosting.query({group:'Nurse',type:'expense'}, function (head) {
            $scope.expenses = head;
            //console.log(head);
        });
    }loadLedgerPosting();

    $scope.search=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_nurse_expense', {params:{fromDate:from,toDate:to,account_id:account_id}}).
            success(function(data, status)
            {
                $scope.expenses = data;
            });
    };

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Expense Register(Nurse Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });




    $scope.newExpense = function () {
        $scope.expenseedit = true;
        $scope.newexpense = new LedgerPosting();
        $scope.newexpense.date=new Date();
        $scope.curExpense = {};

    };
    $scope.editExpense = function (thisHead) {
        $scope.expenseedit = true;
        getSelectedItem(thisHead.accounthead_id);
        $scope.curExpense =  thisHead;
        $scope.newexpense = new LedgerPosting();
        $scope.newexpense = angular.copy(thisHead);
        $scope.newexpense.date=new Date(thisHead.date);
        $anchorScroll();
    };
    function getSelectedItem(id){
        $http.get('/api/getAccountHead/'+id).
            success(function(data, status)
            {
                $scope.selectedItem=data;
            });
    }
    $scope.addExpense = function () {
        $scope.newexpense.accounthead_id=$scope.selectedItem.id;
        $scope.newexpense.date=new Date($scope.newexpense.date);
        if ($scope.curExpense.id) {
            $http.put('/api/ledgerposting/'+$scope.curExpense.id, $scope.newexpense).
                success(function(data, status)
                {
                    loadLedgerPosting();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                });
        } else{
            $scope.newexpense.$save(function(account){
                loadLedgerPosting();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Your Transaction saved Successfully');
            });
        }
        $scope.expenseedit = false;
    };
    $scope.deleteExpense = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/ledgerposting/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction Deleted Successfully');
                    loadLedgerPosting();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelExpense = function () {
        $scope.expenseedit = false;
        $scope.newexpense =  new LedgerPosting();
    };




    $scope.simulateQuery = false;
    $scope.isDisabled    = false;

    // list of `state` value/display objects
    $scope.states        = [];loadAll();
    $scope.querySearch   = querySearch;
    $scope.selectedItemChange = selectedItemChange;
    $scope.searchTextChange   = searchTextChange;

    $scope.newState = newState;

    function newState(state) {
        alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
        var results = query ? $scope.states.filter( createFilterFor(query) ) : $scope.states,
            deferred;
        if ($scope.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    function searchTextChange(text) {
        //$log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
        //$log.info('Item changed to ' + JSON.stringify(item));
    }


    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
        $scope.data1=[];
        $http.get('/api/accounthead_expense/',{params:{head:'Nurse'}}).
            success(function(data, status)
            {
                $scope.states=data;
            });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {

        var lowercaseQuery =query; //angular.lowercase(query);

        return function filterFn(state) {
            return (state.value.indexOf(lowercaseQuery) === 0);
        };

    }

    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.accounthead_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.export=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        var req = {
            method: 'POST',
            url: '/api/excelExportLedgerPosting',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,type:'expense',group:'Nurse'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='ExpenseReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };
});
app.controller('NurseAccountIncomeController', function($scope,$http,$window,$filter,$location,$log,$timeout,ngNotify,$mdDialog,$anchorScroll,UserInfo,LedgerPosting){
    $scope.incomeedit = false;
    $scope.newincome = new LedgerPosting();
    $scope.curIncome = {};
    $scope.account_id = 'All';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.incomes =[];
    function loadLedgerPosting() {
        LedgerPosting.query({group:'Nurse',type:'income'}, function (head) {
            $scope.incomes = head;
            //console.log(head);
        });
    }loadLedgerPosting();

    $scope.search=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_nurse_income', {params:{fromDate:from,toDate:to,account_id:account_id}}).
            success(function(data, status)
            {
                $scope.incomes = data;
            });
    };

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Income Register(Nurse Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });




    $scope.newIncome = function () {
        $scope.incomeedit = true;
        $scope.newincome = new LedgerPosting();
        $scope.newincome.date=new Date();
        $scope.curIncome = {};

    };
    $scope.editIncome = function (thisHead) {
        $scope.incomeedit = true;
        getSelectedItem(thisHead.accounthead_id);
        $scope.curIncome =  thisHead;
        $scope.newincome = new LedgerPosting();
        $scope.newincome = angular.copy(thisHead);
        $scope.newincome.date=new Date(thisHead.date);
        $anchorScroll();
    };

    function getSelectedItem(id){
        $http.get('/api/getAccountHead/'+id).
            success(function(data, status)
            {
                $scope.selectedItem=data;
            });
    }
    $scope.addIncome = function () {
        $scope.newincome.accounthead_id=$scope.selectedItem.id;
        $scope.newincome.date=new Date($scope.newincome.date);
        if ($scope.curIncome.id) {
            $http.put('/api/ledgerposting/'+$scope.curIncome.id, $scope.newincome).
                success(function(data, status)
                {
                    loadLedgerPosting();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                });
        } else{
            $scope.newincome.$save(function(account){
                loadLedgerPosting();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Your Transaction saved Successfully');
            });
        }
        $scope.incomeedit = false;
    };
    $scope.deleteIncome = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/ledgerposting/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction Deleted Successfully');
                    loadLedgerPosting();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelIncome = function () {
        $scope.incomeedit = false;
        $scope.newincome =  new LedgerPosting();
    };




    $scope.simulateQuery = false;
    $scope.isDisabled    = false;

    // list of `state` value/display objects
    $scope.states        = [];loadAll();
    $scope.querySearch   = querySearch;
    $scope.selectedItemChange = selectedItemChange;
    $scope.searchTextChange   = searchTextChange;

    $scope.newState = newState;

    function newState(state) {
        alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
        var results = query ? $scope.states.filter( createFilterFor(query) ) : $scope.states,
            deferred;
        if ($scope.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    function searchTextChange(text) {
        //$log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
        //$log.info('Item changed to ' + JSON.stringify(item));
    }


    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
        $scope.data1=[];
        $http.get('/api/accounthead_income/',{params:{head:'Nurse'}}).
            success(function(data, status)
            {
                $scope.states=data;
            });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {

        var lowercaseQuery =query; //angular.lowercase(query);

        return function filterFn(state) {
            return (state.value.indexOf(lowercaseQuery) === 0);
        };

    }

    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.accounthead_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.export=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        var req = {
            method: 'POST',
            url: '/api/excelExportLedgerPosting',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,type:'income',group:'Nurse'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='IncomeReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };
});
app.controller('NurseDaybookController', function($scope,$http,$filter,$window,$mdDialog,UserInfo,LedgerPosting){


    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.daybooks =[];
    function loadLedgerPosting() {
        $http.get('/api/getDaybookByGroup', {params:{fromDate:$scope.fromDate,toDate:$scope.toDate,group:'Nurse'}}).
            success(function(data, status)
            {
                $scope.daybooks = data;
                getSum(data);
            });
    }loadLedgerPosting();

    $scope.search=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/getDaybookByGroup', {params:{fromDate:from,toDate:to,account_id:account_id,group:'Nurse'}}).
            success(function(data, status)
            {
                $scope.daybooks = data;
                getSum(data);
            });
    };

    $scope.totalincome=0;
    $scope.totalexpense=0;
    function getSum(daybooks){
        var length=daybooks.length;
        var income=0;
        var expense=0;
        for(var i=0;i<length;i++){
            if(daybooks[i].accounthead.type==='income')
                income+=parseFloat(daybooks[i].amount);
            if(daybooks[i].accounthead.type==='expense')
                expense+=parseFloat(daybooks[i].amount);
        }
        $scope.totalincome=income;
        $scope.totalexpense=expense;
    }

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Day Book(Nurse Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });

    $scope.account_id = 'All';


    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.accounthead_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }
    $scope.showProgress=false;

    $scope.export=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $scope.showProgress=true;
        var req = {
            method: 'POST',
            url: '/api/excelExportDaybookByGroup',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,group:'Nurse'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='DaybookReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);
                $scope.showProgress=false;

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

    $scope.exportPDF=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $scope.showProgress=true;
        var req = {
            method: 'POST',
            url: '/api/pdfExportDayBook',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,group:'Nurse'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='DaybookReport.pdf' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();

                // Remove anchor from body
                document.body.removeChild(a);

                $scope.showProgress=false;

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

});
app.controller('OverallExpenseController', function($scope,$http,$filter,$window,$mdDialog,UserInfo,LedgerPosting){

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.expenses =[];
    function loadLedgerPosting() {
        LedgerPosting.query({type:'expense'},function (head) {
            $scope.expenses = head;
            //console.log(head);
            getSum(head);
        });
    }loadLedgerPosting();

    $scope.search=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_overall_expense', {params:{fromDate:from,toDate:to,account_id:account_id}}).
            success(function(data, status)
            {
                $scope.expenses = data;
                getSum(data);
            });
    };

    $scope.totalexpense=0;
    function getSum(daybooks){
        var length=daybooks.length;
        var expense=0;
        for(var i=0;i<length;i++){
            expense+=parseFloat(daybooks[i].amount);
        }
        $scope.totalexpense=expense;
    }

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Expense Register(Day Book)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });

    $scope.account_id = 'All';


    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.accounthead_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.export=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        var req = {
            method: 'POST',
            url: '/api/excelExportOverallIncome',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,type:'expense'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='IncomeReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

});
app.controller('OverallIncomeController', function($scope,$http,$filter,$window,$mdDialog,UserInfo,LedgerPosting){

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.incomes =[];
    function loadLedgerPosting() {
        LedgerPosting.query({type:'income'},function (head) {
            $scope.incomes = head;
            //console.log(head);
            getSum(head);
        });
    }loadLedgerPosting();

    $scope.search=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_overall_income', {params:{fromDate:from,toDate:to,account_id:account_id}}).
            success(function(data, status)
            {
                $scope.incomes = data;
                getSum(data);
            });
    };

    $scope.totalincome=0;
    function getSum(daybooks){
        var length=daybooks.length;
        var income=0;
        var expense=0;
        for(var i=0;i<length;i++){
            income+=parseFloat(daybooks[i].amount);
        }
        $scope.totalincome=income;
    }

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Income Register(Day Book)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });

    $scope.account_id = 'All';


    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.accounthead_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.export=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');
        
        var req = {
            method: 'POST',
            url: '/api/excelExportOverallIncome',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,type:'income'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='IncomeReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

});
app.controller('RentController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,Rent){
    $scope.rentedit = false;
    $scope.newrent = new Rent();
    $scope.curRent = {};

    $scope.rents=[];
    function loadRent() {
        Rent.query(function (head) {
            $scope.rents = head;
        });
    }loadRent();

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Rent Register(Rent Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });



    $scope.newRent = function () {
        $scope.rentedit = true;
        $scope.newrent = new Rent();
        $scope.curRent = {};
    };
    $scope.editRent = function (thisHead) {
        $scope.rentedit = true;
        $scope.curRent =  thisHead;
        $scope.newrent = new Rent();
        $scope.newrent = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addRent = function () {
        $scope.newrent.default=0;
        if ($scope.curRent.id) {
            $http.put('/api/rent/'+$scope.curRent.id, $scope.newrent).
                success(function(data, status)
                {
                    loadRent();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Rent updated Successfully');
                });
        } else{
            $scope.newrent.$save(function(account){
                loadRent();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Rent saved Successfully');
            });
        }
        $scope.rentedit = false;
    };
    $scope.deleteRent = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/rent/'+item.id).
                success(function(data, status)
                {
                    loadRent();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Rent removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelRent = function () {
        $scope.rentedit = false;
        $scope.newrent =  new Rent();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Rent Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeRentStatus/'+id).
                success(function(data, status)
                {
                    loadRent();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Rent Status Changed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.openRent=function(data,ev){
        $http.get('/api/rent/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogRentController,
                    templateUrl: $window.location.origin+'/template/rent',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogRentController($scope, $mdDialog,data) {

        $scope.rent=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

});
app.controller('RentAccountAccountHeadController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,AccountHead){
    $scope.headedit = false;
    $scope.newaccount = new AccountHead();
    $scope.curAccount = {};

    //$scope.numPerPage=5;
    $scope.accountheads=[];
    function loadAccountHead() {
        AccountHead.query({group: 'Rent'}, function (head) {
            $scope.accountheads = head;
        });
    }loadAccountHead();

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Account Head(Rent Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });



    $scope.newAccountHead = function () {
        $scope.headedit = true;
        $scope.newaccount = new AccountHead();
        $scope.curAccount = {};
    };
    $scope.editAccountHeadt = function (thisHead) {
        $scope.headedit = true;
        $scope.curAccount =  thisHead;
        $scope.newaccount = new AccountHead();
        $scope.newaccount = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addAccountHead = function () {
        $scope.newaccount.group='Rent';
        $scope.newaccount.default=0;
        if ($scope.curAccount.id) {
            $http.put('/api/accounthead/'+$scope.curAccount.id, $scope.newaccount).
                success(function(data, status)
                {
                    loadAccountHead();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Account Head updated Successfully');
                });
        } else{
            $scope.newaccount.$save(function(account){
                loadAccountHead();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Account Head Saved Successfully');
            });
        }
        $scope.headedit = false;
    };
    $scope.deleteAccountHead = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/accounthead/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    if(data.default===true) {
                        ngNotify.set('Account Head Deleted Successfully');
                    }else{
                        ngNotify.set(' Account Head Set has DEFAULT...Not DELETABLE');
                    }
                    loadAccountHead();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelAccountHead = function () {
        $scope.headedit = false;
        $scope.newaccount =  new AccountHead();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Account Head Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeAccountHeadStatus/'+id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Account Head Status Changed Successfully');

                    loadAccountHead();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

});
app.controller('RentAccountExpenseController', function($scope,$http,$window,$filter,$location,$log,$timeout,ngNotify,$mdDialog,$anchorScroll,UserInfo,LedgerPosting){
    $scope.expenseedit = false;
    $scope.newexpense = new LedgerPosting();
    $scope.curExpense = {};
    $scope.account_id = 'All';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.expenses =[];
    function loadLedgerPosting() {
        LedgerPosting.query({group:'Rent',type:'expense'}, function (head) {
            $scope.expenses = head;
            //console.log(head);
        });
    }loadLedgerPosting();

    $scope.search=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_rent_expense', {params:{fromDate:from,toDate:to,account_id:account_id}}).
            success(function(data, status)
            {
                $scope.expenses = data;
            });
    };

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Expense Register(Rent Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });




    $scope.newExpense = function () {
        $scope.expenseedit = true;
        $scope.newexpense = new LedgerPosting();
        $scope.newexpense.date=new Date();
        $scope.curExpense = {};

    };
    $scope.editExpense = function (thisHead) {
        $scope.expenseedit = true;
        getSelectedItem(thisHead.accounthead_id);
        $scope.curExpense =  thisHead;
        $scope.newexpense = new LedgerPosting();
        $scope.newexpense = angular.copy(thisHead);
        $scope.newexpense.date=new Date(thisHead.date);
        $anchorScroll();
    };

    function getSelectedItem(id){
        $http.get('/api/getAccountHead/'+id).
            success(function(data, status)
            {
                $scope.selectedItem=data;
            });
    }

    $scope.addExpense = function () {
        $scope.newexpense.accounthead_id=$scope.selectedItem.id;
        $scope.newexpense.date=new Date($scope.newexpense.date);
        if ($scope.curExpense.id) {
            $http.put('/api/ledgerposting/'+$scope.curExpense.id, $scope.newexpense).
                success(function(data, status)
                {
                    loadLedgerPosting();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                });
        } else{
            $scope.newexpense.$save(function(account){
                loadLedgerPosting();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Your Transaction saved Successfully');
            });
        }
        $scope.expenseedit = false;
    };
    $scope.deleteExpense = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/ledgerposting/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction Deleted Successfully');
                    loadLedgerPosting();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelExpense = function () {
        $scope.expenseedit = false;
        $scope.newexpense =  new LedgerPosting();
    };




    $scope.simulateQuery = false;
    $scope.isDisabled    = false;

    // list of `state` value/display objects
    $scope.states        = [];loadAll();
    $scope.querySearch   = querySearch;
    $scope.selectedItemChange = selectedItemChange;
    $scope.searchTextChange   = searchTextChange;

    $scope.newState = newState;

    function newState(state) {
        alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
        var results = query ? $scope.states.filter( createFilterFor(query) ) : $scope.states,
            deferred;
        if ($scope.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    function searchTextChange(text) {
        //$log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
        //$log.info('Item changed to ' + JSON.stringify(item));
    }


    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
        $scope.data1=[];
        $http.get('/api/accounthead_expense/',{params:{head:'Rent'}}).
            success(function(data, status)
            {
                $scope.states=data;
            });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {

        var lowercaseQuery =query; //angular.lowercase(query);

        return function filterFn(state) {
            return (state.value.indexOf(lowercaseQuery) === 0);
        };

    }

    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.accounthead_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.export=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        var req = {
            method: 'POST',
            url: '/api/excelExportLedgerPosting',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,type:'expense',group:'Rent'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='ExpenseReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };
});
app.controller('RentAccountIncomeController', function($scope,$http,$filter,$window,$location,$log,$timeout,$mdDialog,ngNotify,$anchorScroll,UserInfo,LedgerPosting){
    $scope.incomeedit = false;
    $scope.newincome = new LedgerPosting();
    $scope.curIncome = {};
    $scope.account_id = 'All';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.incomes =[];
    function loadLedgerPosting() {
        LedgerPosting.query({group:'Rent',type:'income'}, function (head) {
            $scope.incomes = head;
            //console.log(head);
        });
    }loadLedgerPosting();

    $scope.search=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_rent_income', {params:{fromDate:from,toDate:to,account_id:account_id}}).
            success(function(data, status)
            {
                $scope.incomes = data;
            });
    };

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Income Register(Rent Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });


    $scope.newIncome = function () {
        $scope.incomeedit = true;
        $scope.newincome = new LedgerPosting();
        $scope.newincome.date=new Date();
        $scope.curIncome = {};

    };
    $scope.editIncome = function (thisHead) {
        $scope.incomeedit = true;
        getSelectedItem(thisHead.accounthead_id);
        $scope.curIncome =  thisHead;
        $scope.newincome = new LedgerPosting();
        $scope.newincome = angular.copy(thisHead);
        $scope.newincome.date=new Date(thisHead.date);
        $anchorScroll();
    };

    function getSelectedItem(id){
        $http.get('/api/getAccountHead/'+id).
            success(function(data, status)
            {
                $scope.selectedItem=data;
            });
    }

    $scope.addIncome = function () {
        $scope.newincome.accounthead_id=$scope.selectedItem.id;
        $scope.newincome.date=new Date($scope.newincome.date);
        if ($scope.curIncome.id) {
            $http.put('/api/ledgerposting/'+$scope.curIncome.id, $scope.newincome).
                success(function(data, status)
                {
                    loadLedgerPosting();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                });
        } else{
            $scope.newincome.$save(function(account){
                loadLedgerPosting();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Your Transaction saved Successfully');
            });
        }
        $scope.incomeedit = false;
    };
    $scope.deleteIncome = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/ledgerposting/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction Deleted Successfully');
                    loadLedgerPosting();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelIncome = function () {
        $scope.incomeedit = false;
        $scope.newincome =  new LedgerPosting();
    };




    $scope.simulateQuery = false;
    $scope.isDisabled    = false;

    // list of `state` value/display objects
    $scope.states        = [];loadAll();
    $scope.querySearch   = querySearch;
    $scope.selectedItemChange = selectedItemChange;
    $scope.searchTextChange   = searchTextChange;

    $scope.newState = newState;

    function newState(state) {
        alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
        var results = query ? $scope.states.filter( createFilterFor(query) ) : $scope.states,
            deferred;
        if ($scope.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    function searchTextChange(text) {
        //$log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
        //$log.info('Item changed to ' + JSON.stringify(item));
    }


    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
        $scope.data1=[];
        $http.get('/api/accounthead_income/',{params:{head:'Rent'}}).
            success(function(data, status)
            {
                $scope.states=data;
            });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {

        var lowercaseQuery =query; //angular.lowercase(query);

        return function filterFn(state) {
            return (state.value.indexOf(lowercaseQuery) === 0);
        };

    }

    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.accounthead_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.export=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        var req = {
            method: 'POST',
            url: '/api/excelExportLedgerPosting',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,type:'income',group:'Rent'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='IncomeReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };
});
/**
 * Created by psybo on 18/5/17.
 */
app.controller('RentchartController', function($scope,$http,$window,$mdDialog,UserInfo){

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Rent Chart(Rent Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });


    $scope.openRentChart=function(data,ev){
        $mdDialog.show({
            controller: DialogRentController,
            templateUrl: $window.location.origin+'/template/rentchart',
            parent: angular.element(document.querySelector('#popup')),
            targetEvent: ev,
            clickOutsideToClose:true,
            locals: {
                data: data
            }
        })
            .then(function(answer) {
                //$scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                //$scope.status = 'You cancelled the dialog.';
            });

    };

    function DialogRentController($scope, $mdDialog,data) {

        $scope.rentchart=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.export=function(year){

        var req = {
            method: 'POST',
            url: '/api/excelExportRentChart',
            responseType:"arraybuffer",
            data: {year:year}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='RentChartReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

});
app.controller('RentCollectionController', function($scope,$http,$filter,$window,$location,$log,$timeout,$mdDialog,$anchorScroll,ngNotify,UserInfo,RentCollection){
    $scope.rentedit = false;
    $scope.newrent = new RentCollection();
    $scope.curRentCollection = {};
    $scope.rent_id = 'All';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.rentcollections =[];
    function loadRentCollection() {
        RentCollection.query(function (head) {
            $scope.rentcollections = head;
            calculateTotal(head);
            //console.log(head);
        });
    }loadRentCollection();

    $scope.search=function(fromDate,toDate,rent_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_rent_collection', {params:{fromDate:from,toDate:to,rent_id:rent_id}}).
            success(function(data, status)
            {
                $scope.rentcollections = data;
                calculateTotal(data);
            });
    };
    $scope.total=0;
    function calculateTotal(rents){
        var length=rents.length;
        var sum=0;
        for(var i=0;i<length;i++){
            sum+=parseFloat(rents[i].amount);
        }
        $scope.total=sum;
    }

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Rent Payment(Rent Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });



    $scope.newRentCollection = function () {
        $scope.rentedit = true;
        $scope.newrent = new RentCollection();
        $scope.newrent.date=new Date();
        $scope.curRentCollection = {};

    };
    $scope.editRentCollection = function (thisHead) {
        $scope.rentedit = true;
        $scope.curRentCollection =  thisHead;
        $scope.newrent = new RentCollection();
        $scope.newrent = angular.copy(thisHead);
        $scope.newrent.date=new Date(thisHead.date);
        $anchorScroll();
    };
    $scope.addRentCollection = function () {
        $scope.newrent.rent_id=$scope.selectedItem.id;
        $scope.newrent.date=new Date($scope.newrent.date);
        if ($scope.curRentCollection.id) {
            $http.put('/api/rentcollection/'+$scope.curRentCollection.id, $scope.newrent).
                success(function(data, status)
                {
                    loadRentCollection();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                });
        } else{
            $scope.newrent.$save(function(account){
                loadRentCollection();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Your Transaction Saved Successfully');
            });
        }
        $scope.rentedit = false;
    };
    $scope.deleteRentCollection = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/rentcollection/'+item.id).
                success(function(data, status)
                {
                    loadRentCollection();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelRentCollection = function () {
        $scope.rentedit = false;
        $scope.newrent =  new RentCollection();
    };




    $scope.simulateQuery = false;
    $scope.isDisabled    = false;

    // list of `state` value/display objects
    $scope.states        = [];loadAll();
    $scope.querySearch   = querySearch;
    $scope.selectedItemChange = selectedItemChange;
    $scope.searchTextChange   = searchTextChange;

    $scope.newState = newState;

    function newState(state) {
        alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
        var results = query ? $scope.states.filter( createFilterFor(query) ) : $scope.states,
            deferred;
        if ($scope.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    function searchTextChange(text) {
        //$log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
        //$log.info('Item changed to ' + JSON.stringify(item));

    }


    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
        $scope.data1=[];
        $http.get('/api/getRent/').
            success(function(data, status)
            {
                $scope.states=data;
            });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {

        var lowercaseQuery =query; //angular.lowercase(query);

        return function filterFn(state) {
            return (state.value.indexOf(lowercaseQuery) === 0);
        };

    }

    $scope.openRent=function(data,ev){
        $http.get('/api/rent/'+data.rent_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogRentController,
                    templateUrl: $window.location.origin+'/template/rent',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogRentController($scope, $mdDialog,data) {

        $scope.rent=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }


    $scope.export=function(fromDate,toDate,rent_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        var req = {
            method: 'POST',
            url: '/api/excelExportRentCollection',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,rent_id:rent_id}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='RentCollectionReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };
});
app.controller('RentDaybookController', function($scope,$http,$filter,$window,$mdDialog,UserInfo,LedgerPosting){


    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.daybooks =[];
    function loadLedgerPosting() {
        $http.get('/api/getDaybookByGroup', {params:{fromDate:$scope.fromDate,toDate:$scope.toDate,group:'Rent'}}).
            success(function(data, status)
            {
                $scope.daybooks = data;
                getSum(data);
            });
    }loadLedgerPosting();

    $scope.search=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/getDaybookByGroup', {params:{fromDate:from,toDate:to,account_id:account_id,group:'Rent'}}).
            success(function(data, status)
            {
                $scope.daybooks = data;
                getSum(data);
            });
    };

    $scope.totalincome=0;
    $scope.totalexpense=0;
    function getSum(daybooks){
        var length=daybooks.length;
        var income=0;
        var expense=0;
        for(var i=0;i<length;i++){
            if(daybooks[i].accounthead.type==='income')
                income+=parseFloat(daybooks[i].amount);
            if(daybooks[i].accounthead.type==='expense')
                expense+=parseFloat(daybooks[i].amount);
        }
        $scope.totalincome=income;
        $scope.totalexpense=expense;
    }

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Day Book(Rent Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });

    $scope.account_id = 'All';


    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.accounthead_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }
    $scope.showProgress=false;
    $scope.export=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $scope.showProgress=true;
        var req = {
            method: 'POST',
            url: '/api/excelExportDaybookByGroup',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,group:'Rent'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='DaybookReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);
                $scope.showProgress=false;

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

    $scope.exportPDF=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $scope.showProgress=true;
        var req = {
            method: 'POST',
            url: '/api/pdfExportDayBook',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,group:'Rent'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='DaybookReport.pdf' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();

                // Remove anchor from body
                document.body.removeChild(a);

                $scope.showProgress=false;

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

});
app.controller('ResidenceController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,Residence){
    $scope.residenceedit = false;
    $scope.newresidence = new Residence();
    $scope.curResidence = {};

    $scope.residences=[];
    function loadResidence() {
        Residence.query(function (head) {
            $scope.residences = head;
        });
    }loadResidence();

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Residence Area(Transportation)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });




    $scope.newResidence = function () {
        $scope.residenceedit = true;
        $scope.newresidence = new Residence();
        $scope.curResidence = {};
    };
    $scope.editResidence = function (thisHead) {
        $scope.residenceedit = true;
        $scope.curResidence =  thisHead;
        $scope.newresidence = new Residence();
        $scope.newresidence = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addResidence = function () {
        $scope.newresidence.default=0;
        if ($scope.curResidence.id) {
            $http.put('/api/residence/'+$scope.curResidence.id, $scope.newresidence).
                success(function(data, status)
                {
                    loadResidence();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Residence updated Successfully');
                });
        } else{
            $scope.newresidence.$save(function(account){
                loadResidence();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Residence saved Successfully');
            });
        }
        $scope.residenceedit = false;
    };
    $scope.deleteResidence = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/residence/'+item.id).
                success(function(data, status)
                {
                    loadResidence();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Residence removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelResidence = function () {
        $scope.residenceedit = false;
        $scope.newresidence =  new Residence();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Residence Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeResidenceStatus/'+id).
                success(function(data, status)
                {
                    loadResidence();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Residence Status Changed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.openResidence=function(data,ev){
        $http.get('/api/residence/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogResidenceController,
                    templateUrl: $window.location.origin+'/template/residence',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogResidenceController($scope, $mdDialog,data) {
        $scope.residence=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

});
app.controller('SalaryController', function($scope,$http,$filter,$window,$location,$log,$timeout,$mdDialog,$anchorScroll,ngNotify,UserInfo,Salary){
    $scope.salaryedit = false;
    $scope.newsalary = new Salary();
    $scope.curSalary = {};
    $scope.employee_id = 'All';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.salaries =[];
    function loadSalary() {
        Salary.query(function (head) {
            $scope.salaries = head;
            getSum(head);
            //console.log(head);
        });
    }loadSalary();

    $scope.search=function(fromDate,toDate,employee_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_salary', {params:{fromDate:from,toDate:to,employee_id:employee_id}}).
            success(function(data, status)
            {
                $scope.salaries = data;
                getSum(data);
            });
    };
    $scope.total=0;
    function getSum(salaries){
        var length=salaries.length;
        var sum=0;
        for(var i=0;i<length;i++){
            sum+=parseFloat(salaries[i].total);
        }
        $scope.total=sum;
    }

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Salary Register(Salary)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });




    $scope.newSalary = function () {
        $scope.salaryedit = true;
        $scope.newsalary = new Salary();
        $scope.newsalary.date=new Date();
        $scope.curSalary = {};
        $scope.leave='';
        $scope.half='';
    };
    $scope.editSalary = function (thisHead) {
        $scope.salaryedit = true;
        $scope.curSalary =  thisHead;
        $scope.newsalary = new Salary();
        //$scope.newsalary = angular.copy(thisHead);
        $scope.newsalary.employee_id=thisHead.employee_id;
        $scope.newsalary.basic=parseFloat(thisHead.basic);
        $scope.newsalary.advance=parseFloat(thisHead.advance);
        $scope.newsalary.allowance=parseFloat(thisHead.allowance);
        $scope.newsalary.leavesalary=parseFloat(thisHead.leavesalary);
        $scope.newsalary.total=parseFloat(thisHead.total);
        $scope.newsalary.date=new Date(thisHead.date);
        $scope.newsalary.remark=thisHead.remark;
        $anchorScroll();
    };
    $scope.addSalary = function () {
        $scope.newsalary.date=new Date($scope.newsalary.date);
        if ($scope.curSalary.id) {
            $http.put('/api/salaries/'+$scope.curSalary.id, $scope.newsalary).
                success(function(data, status)
                {
                    loadSalary();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                });
        } else{
            $scope.newsalary.$save(function(account){
                loadSalary();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Your Transaction saved Successfully');
            });
        }
        $scope.salaryedit = false;
    };
    $scope.deleteSalary = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/salaries/'+item.id).
                success(function(data, status)
                {
                    loadSalary();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelSalary = function () {
        $scope.salaryedit = false;
        $scope.newsalary =  new Salary();
    };
    $scope.leave='';
    $scope.getBasicPayAndAdvance = function (employee_id) {
        $http.post('/api/getBasicpay',{employee_id:employee_id}).
            success(function(data, status)
            {
                $scope.newsalary.basic=parseFloat(data.basic);
                $scope.newsalary.advance=parseFloat(data.advance);
                $scope.newsalary.allowance=0;
                $scope.newsalary.leavesalary=0;
                calculateSalary();
                console.log(data);
            });

        $http.post('/api/getLeave',{employee_id:employee_id,date:$scope.newsalary.date}).
            success(function(data, status)
            {
                $scope.leave=data.leave;
                $scope.half=data.half;
            });
    };

    function calculateSalary(){
        var basic= 0,advance= 0,allowance= 0,leavesalary= 0,total=0;
        try{
            basic=parseFloat($scope.newsalary.basic);
        }catch(err){
            basic= 0;
            console.log(err);
        }

        try{
            advance=parseFloat($scope.newsalary.advance);
        }catch(err){
            advance= 0;
            console.log(err);
        }

        try{
            allowance=parseFloat($scope.newsalary.allowance);
        }catch(err){
            allowance= 0;
            console.log(err);
        }

        try{
            leavesalary=parseFloat($scope.newsalary.leavesalary);
        }catch(err){
            leavesalary= 0;
            console.log(err);
        }


        total=((basic-advance)+allowance)-leavesalary;
        console.log(total);
        $scope.newsalary.total=total;

    }


    $scope.changeSalary=function(){
        calculateSalary();
    };



    $scope.open=function(data,ev){
        $http.get('/api/employee/'+data.employee_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/employee',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {
        $scope.employee=data;
        $scope.basepath=$window.location.origin;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.export=function(fromDate,toDate,employee_id){
        var req = {
            method: 'POST',
            url: '/api/excelExportSalary',
            responseType:"arraybuffer",
            data: {fromDate:fromDate,toDate:toDate,employee_id:employee_id}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='SalaryReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };
});
app.controller('SalaryAdvanceController', function($scope,$http,$window,$filter,$location,$log,$timeout,$mdDialog,ngNotify,$anchorScroll,UserInfo,AdvanceSalary){
    $scope.advanceedit = false;
    $scope.newadvance = new AdvanceSalary();
    $scope.curAdvance = {};
    $scope.employee_id = 'All';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.advances =[];
    function loadAdvanceSalary() {
        AdvanceSalary.query(function (head) {
            $scope.advances = head;

            //console.log(head);
        });
    }loadAdvanceSalary();

    $scope.search=function(fromDate,toDate,employee_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_salary_advance', {params:{fromDate:from,toDate:to,employee_id:employee_id}}).
            success(function(data, status)
            {
                $scope.advances = data;

            });
    };

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Salary Advance(Salary)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });




    $scope.newAdvance = function () {
        $scope.advanceedit = true;
        $scope.newadvance = new AdvanceSalary();
        $scope.newadvance.date=new Date();
        $scope.curAdvance = {};

    };
    $scope.editAdvance = function (thisHead) {
        $scope.advanceedit = true;
        $scope.curAdvance =  thisHead;
        $scope.newadvance = new AdvanceSalary();
        $scope.newadvance = angular.copy(thisHead);
        $scope.newadvance.date=new Date(thisHead.date);
        $anchorScroll();
    };
    $scope.addAdvance = function () {
        $scope.newadvance.date=new Date($scope.newadvance.date);
        if ($scope.curAdvance.id) {
            $http.put('/api/salaryadvance/'+$scope.curAdvance.id, $scope.newadvance).
                success(function(data, status)
                {
                    loadAdvanceSalary();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                });
        } else{
            $scope.newadvance.$save(function(account){
                loadAdvanceSalary();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Your Transaction saved Successfully');
            });
        }
        $scope.advanceedit = false;
    };
    $scope.deleteAdvance = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/salaryadvance/'+item.id).
                success(function(data, status)
                {
                    loadAdvanceSalary();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelAdvance = function () {
        $scope.advanceedit = false;
        $scope.newadvance =  new AdvanceSalary();
    };



    $scope.open=function(data,ev){
        $http.get('/api/employee/'+data.employee_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/employee',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {
        $scope.employee=data;
        $scope.basepath=$window.location.origin;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.export=function(fromDate,toDate,employee_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        var req = {
            method: 'POST',
            url: '/api/excelExportSalaryAdvance',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,employee_id:employee_id}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='SalaryAdvanceReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };
});
/**
 * Created by psybo on 18/5/17.
 */
app.controller('SalarychartController', function($scope,$http,$window,$mdDialog,UserInfo){

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Salary Chart(Salary)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });


    $scope.openSalaryChart=function(data,ev){
        $mdDialog.show({
            controller: DialogDutyController,
            templateUrl: $window.location.origin+'/template/salarychart',
            parent: angular.element(document.querySelector('#popup')),
            targetEvent: ev,
            clickOutsideToClose:true,
            locals: {
                data: data
            }
        })
            .then(function(answer) {
                //$scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                //$scope.status = 'You cancelled the dialog.';
            });

    };

    function DialogDutyController($scope, $mdDialog,$window,data) {

        $scope.basepath=$window.location.origin;
        $scope.salarychart=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }



});
app.controller('ThemeController', function($scope,$http,$window){
    $scope.newtheme={toggle:true,theme:1};

    $scope.changeTheme=function(theme){
        $scope.newtheme.theme=theme;
    };

    $scope.updateTheme=function(){
        $http.post('/api/changetheme', $scope.newtheme).
            success(function(data, status)
            {
                //$window.location.reload();
                $window.location.href=$window.location.href;
            });
        console.log($scope.newtheme);
    };

});
app.controller('TransportationAccountAccountHeadController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,AccountHead){
    $scope.headedit = false;
    $scope.newaccount = new AccountHead();
    $scope.curAccount = {};

    //$scope.numPerPage=5;

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Account Head(Transportation)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });

    $scope.accountheads=[];
    function loadAccountHead() {
        AccountHead.query({group: 'Transportation'}, function (head) {
            $scope.accountheads = head;
        });
    }loadAccountHead();

    $scope.newAccountHead = function () {
        $scope.headedit = true;
        $scope.newaccount = new AccountHead();
        $scope.curAccount = {};
    };
    $scope.editAccountHeadt = function (thisHead) {
        $scope.headedit = true;
        $scope.curAccount =  thisHead;
        $scope.newaccount = new AccountHead();
        $scope.newaccount = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addAccountHead = function () {
        $scope.newaccount.group='Transportation';
        $scope.newaccount.default=0;
        if ($scope.curAccount.id) {
            $http.put('/api/accounthead/'+$scope.curAccount.id, $scope.newaccount).
                success(function(data, status)
                {

                    loadAccountHead();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Account Head updated Successfully');
                });
        } else{
            $scope.newaccount.$save(function(account){
                loadAccountHead();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Account Head Saved Successfully');
            });
        }
        $scope.headedit = false;
    };
    $scope.deleteAccountHead = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/accounthead/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    if(data.default===true) {
                        ngNotify.set('Account Head Deleted Successfully');
                    }else{
                        ngNotify.set(' Account Head Set has DEFAULT...Not DELETABLE');
                    }
                    loadAccountHead();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelAccountHead = function () {
        $scope.headedit = false;
        $scope.newaccount =  new AccountHead();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Account Head Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeAccountHeadStatus/'+id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Account Head Status Changed Successfully');

                    loadAccountHead();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }



});
app.controller('TransportationAccountExpenseController', function($scope,$http,$filter,$window,$location,$log,$timeout,ngNotify,$mdDialog,$anchorScroll,UserInfo,LedgerPosting){
    $scope.expenseedit = false;
    $scope.newexpense = new LedgerPosting();
    $scope.curExpense = {};
    $scope.account_id = 'All';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.expenses =[];
    function loadLedgerPosting() {
        LedgerPosting.query({group:'Transportation',type:'expense'}, function (head) {
            $scope.expenses = head;
            //console.log(head);
        });
    }loadLedgerPosting();

    $scope.search=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_transportation_expense', {params:{fromDate:from,toDate:to,account_id:account_id}}).
            success(function(data, status)
            {
                $scope.expenses = data;
            });
    };

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Expense Register(Transportation)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });



    $scope.newExpense = function () {
        $scope.expenseedit = true;
        $scope.newexpense = new LedgerPosting();
        $scope.newexpense.date=new Date();
        $scope.curExpense = {};

    };
    $scope.editExpense = function (thisHead) {
        $scope.expenseedit = true;
        getSelectedItem(thisHead.accounthead_id);
        $scope.curExpense =  thisHead;
        $scope.newexpense = new LedgerPosting();
        $scope.newexpense = angular.copy(thisHead);
        $scope.newexpense.date=new Date(thisHead.date);
        $anchorScroll();
    };

    function getSelectedItem(id){
        $http.get('/api/getAccountHead/'+id).
            success(function(data, status)
            {
                $scope.selectedItem=data;
            });
    }

    $scope.addExpense = function () {
        $scope.newexpense.accounthead_id=$scope.selectedItem.id;
        $scope.newexpense.date=new Date($scope.newexpense.date);
        if ($scope.curExpense.id) {
            $http.put('/api/ledgerposting/'+$scope.curExpense.id, $scope.newexpense).
                success(function(data, status)
                {
                    loadLedgerPosting();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                });
        } else{
            $scope.newexpense.$save(function(account){
                loadLedgerPosting();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Your Transaction saved Successfully');
            });
        }
        $scope.expenseedit = false;
    };
    $scope.deleteExpense = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/ledgerposting/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction Deleted Successfully');
                    loadLedgerPosting();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelExpense = function () {
        $scope.expenseedit = false;
        $scope.newexpense =  new LedgerPosting();
    };




    $scope.simulateQuery = false;
    $scope.isDisabled    = false;

    // list of `state` value/display objects
    $scope.states        = [];loadAll();
    $scope.querySearch   = querySearch;
    $scope.selectedItemChange = selectedItemChange;
    $scope.searchTextChange   = searchTextChange;

    $scope.newState = newState;

    function newState(state) {
        alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
        var results = query ? $scope.states.filter( createFilterFor(query) ) : $scope.states,
            deferred;
        if ($scope.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    function searchTextChange(text) {
        //$log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
        //$log.info('Item changed to ' + JSON.stringify(item));
    }


    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
        $scope.data1=[];
        $http.get('/api/accounthead_expense/',{params:{head:'Transportation'}}).
            success(function(data, status)
            {
                $scope.states=data;
            });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {

        var lowercaseQuery =query; //angular.lowercase(query);

        return function filterFn(state) {
            return (state.value.indexOf(lowercaseQuery) === 0);
        };

    }

    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.accounthead_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.export=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        var req = {
            method: 'POST',
            url: '/api/excelExportLedgerPosting',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,type:'expense',group:'Transportation'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='ExpenseReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };
});
app.controller('TransportationAccountIncomeController', function($scope,$http,$filter,$window,$location,$log,$timeout,ngNotify,$mdDialog,$anchorScroll,UserInfo,LedgerPosting){
    $scope.incomeedit = false;
    $scope.newincome = new LedgerPosting();
    $scope.curIncome = {};
    $scope.account_id = 'All';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.incomes =[];
    function loadLedgerPosting() {
        LedgerPosting.query({group:'Transportation',type:'income'}, function (head) {
            $scope.incomes = head;
            //console.log(head);
        });
    }loadLedgerPosting();

    $scope.search=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_transportation_income', {params:{fromDate:from,toDate:to,account_id:account_id}}).
            success(function(data, status)
            {
                $scope.incomes = data;
            });
    };

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Income Register(Transportation)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });



    $scope.newIncome = function () {
        $scope.incomeedit = true;
        $scope.newincome = new LedgerPosting();
        $scope.newincome.date=new Date();
        $scope.curIncome = {};

    };
    $scope.editIncome = function (thisHead) {
        $scope.incomeedit = true;
        getSelectedItem(thisHead.accounthead_id);
        $scope.curIncome =  thisHead;
        $scope.newincome = new LedgerPosting();
        $scope.newincome = angular.copy(thisHead);
        $scope.newincome.date=new Date(thisHead.date);
        $anchorScroll();
    };

    function getSelectedItem(id){
        $http.get('/api/getAccountHead/'+id).
            success(function(data, status)
            {
                $scope.selectedItem=data;
            });
    }

    $scope.addIncome = function () {
        $scope.newincome.accounthead_id=$scope.selectedItem.id;
        $scope.newincome.date=new Date($scope.newincome.date);
        if ($scope.curIncome.id) {
            $http.put('/api/ledgerposting/'+$scope.curIncome.id, $scope.newincome).
                success(function(data, status)
                {
                    loadLedgerPosting();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                });
        } else{
            $scope.newincome.$save(function(account){
                loadLedgerPosting();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Your Transaction saved Successfully');
            });
        }
        $scope.incomeedit = false;
    };
    $scope.deleteIncome = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/ledgerposting/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction Deleted Successfully');
                    loadLedgerPosting();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelIncome = function () {
        $scope.incomeedit = false;
        $scope.newincome =  new LedgerPosting();
    };




    $scope.simulateQuery = false;
    $scope.isDisabled    = false;

    // list of `state` value/display objects
    $scope.states        = [];loadAll();
    $scope.querySearch   = querySearch;
    $scope.selectedItemChange = selectedItemChange;
    $scope.searchTextChange   = searchTextChange;

    $scope.newState = newState;

    function newState(state) {
        alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
        var results = query ? $scope.states.filter( createFilterFor(query) ) : $scope.states,
            deferred;
        if ($scope.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    function searchTextChange(text) {
        //$log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
        //$log.info('Item changed to ' + JSON.stringify(item));
    }


    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
        $scope.data1=[];
        $http.get('/api/accounthead_income/',{params:{head:'Transportation'}}).
            success(function(data, status)
            {
                $scope.states=data;
            });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {

        var lowercaseQuery =query; //angular.lowercase(query);

        return function filterFn(state) {
            return (state.value.indexOf(lowercaseQuery) === 0);
        };

    }

    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.accounthead_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }


    $scope.export=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        var req = {
            method: 'POST',
            url: '/api/excelExportLedgerPosting',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,type:'income',group:'Transportation'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='IncomeReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };
});
app.controller('TransportationDaybookController', function($scope,$http,$filter,$window,$mdDialog,UserInfo,LedgerPosting){


    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.daybooks =[];
    function loadLedgerPosting() {
        $http.get('/api/getDaybookByGroup', {params:{fromDate:$scope.fromDate,toDate:$scope.toDate,group:'Transportation'}}).
            success(function(data, status)
            {
                $scope.daybooks = data;
                getSum(data);
            });
    }loadLedgerPosting();

    $scope.search=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/getDaybookByGroup', {params:{fromDate:from,toDate:to,account_id:account_id,group:'Transportation'}}).
            success(function(data, status)
            {
                $scope.daybooks = data;
                getSum(data);
            });
    };

    $scope.totalincome=0;
    $scope.totalexpense=0;
    function getSum(daybooks){
        var length=daybooks.length;
        var income=0;
        var expense=0;
        for(var i=0;i<length;i++){
            if(daybooks[i].accounthead.type==='income')
                income+=parseFloat(daybooks[i].amount);
            if(daybooks[i].accounthead.type==='expense')
                expense+=parseFloat(daybooks[i].amount);
        }
        $scope.totalincome=income;
        $scope.totalexpense=expense;
    }

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Day Book(Transportation)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });

    $scope.account_id = 'All';


    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.accounthead_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }
    $scope.showProgress=false;
    $scope.export=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $scope.showProgress=true;
        var req = {
            method: 'POST',
            url: '/api/excelExportDaybookByGroup',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,group:'Transportation'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='DaybookReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);
                $scope.showProgress=false;
            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

    $scope.exportPDF=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $scope.showProgress=true;
        var req = {
            method: 'POST',
            url: '/api/pdfExportDayBook',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,group:'Transportation'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='DaybookReport.pdf' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();

                // Remove anchor from body
                document.body.removeChild(a);

                $scope.showProgress=false;

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

});
app.controller('UserController', function($scope,$http,$window){

    function loadProfile(){
        $http.get('/api/getprofile').
            success(function(data, status)
            {
                $scope.name=data.name;
                $scope.email=data.email;
            });
    }
    loadProfile();

    $scope.updateProfile=function(){
        var photos=[];
        angular.forEach($scope.photo, function (value, key) {
            photos[key]=value;
        });

        $http.post('/api/updateprofile',{name:$scope.name,email:$scope.email,photos:photos}).
            success(function(data, status)
            {
                //$window.location.reload();
                $window.location.href=$window.location.href;
            });
    };

});
app.controller('UserInfoController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,User,UserInfo){
    $scope.headedit = false;
    $scope.newuser = {};
    $scope.curUser = {};
    $scope.showpassword = true;

    //Only for admin load all users
    /*if($scope.user.isAdmin==true){
        User.query(function(users){
            $scope.users =users;
            console.log(users);
        });
    }*/
    $scope.users=[];
    function loadUser() {
        User.query(function (users) {
            $scope.users = users;
            console.log(users);
        });
    }
    loadUser();



    $scope.newUser = function () {
        $scope.headedit = true;
        $scope.newuser = {};
        $scope.curUser = {};
        $scope.showpassword = true;
    };
    $scope.editUser = function (thisHead) {
        $scope.headedit = true;
        $scope.curUser =  thisHead;
        $scope.newuser = {};
        $scope.newuser = angular.copy(thisHead);
        $scope.showpassword = false;
        $anchorScroll();
    };
    $scope.addUser = function () {
        if ($scope.curUser.id) {
            $http.put('/api/user/'+$scope.curUser.id, $scope.newuser).
                success(function(data, status)
                {
                    loadUser();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('User updated Successfully');
                });
        } else{
            $http.post('/api/user', $scope.newuser).
                success(function(data, status)
                {
                    loadUser();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('User Created Successfully');
                });
        }
        $scope.headedit = false;
    };
    $scope.deleteUser = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/user/'+item.id).
                success(function(data, status)
                {
                    loadUser();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('User Removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelUser = function () {
        $scope.headedit = false;
        $scope.newuser =  {};
    };

    $scope.available='0';

    $scope.checkUser = function (username) {
        $http.post('/api/checkuser/', {username:username}).
            success(function(data, status)
            {
               if(data.found==='Found')
                   $scope.available='1';
                else
                   $scope.available='0';
            });
    };
    $scope.resetPassword = function (user,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to Reset Password ?')
            .textContent('Are you sure to reset '+user.name+' \'s Password')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.post('/api/resetPassword',{id:user.id}).
                success(function(data,status,headers,config){
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popup')))
                            .clickOutsideToClose(true)
                            .title('Success')
                            .textContent(user.name+' \'s Password changed to "admin".')
                            .ariaLabel('Alert Dialog Demo')
                            .ok('Got it!')
                            .targetEvent(ev)
                    );
                }).error(function(data,status,headers,config){
                    console.log(data);
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popup')))
                            .clickOutsideToClose(true)
                            .title('Error')
                            .textContent(' Password reset failed....')
                            .ariaLabel('Alert Dialog Demo')
                            .ok('Got it!')
                            .targetEvent(ev)
                    );
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };


    $scope.pages = [];
    $scope.mypermisions = [];
    $scope.permissionMode = function (thisUser) {
        $scope.curUser = thisUser;
        $scope.name = thisUser.name;

        UserInfo.getAllPage(thisUser.id).success(function(data){
            $scope.pages = data
        });

        UserInfo.getUserPermission(thisUser.id).success(function(data){
            $scope.mypermisions = data;
        });

        $scope.resetPermission = true;
        //$scope.breadCrumbs.push("home", $scope.curUser.name);
    };

    $scope.backToUsers = function () {
        $scope.pages=[];
        $scope.mypermisions = [];
        $scope.resetPermission = false;
    };

    $scope.savePermission = function (ev){
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to Reset Permission ?')
            .textContent('Are you sure to reset '+$scope.curUser.name+' \'s Permission')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {

            UserInfo.setUserPermission({id:$scope.curUser.id,permission:$scope.mypermisions})
                .success(function(data){
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popup')))
                            .clickOutsideToClose(true)
                            .title('Success')
                            .textContent($scope.curUser.name+' \'s Permission changed Successfully.')
                            .ariaLabel('Alert Dialog Demo')
                            .ok('Got it!')
                            .targetEvent(ev)
                    );
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });


    };

    $scope.editPage = false;
    $scope.writePage= true;
    $scope.deletePage = false;

    $scope.toggleWrite = function(page){
        page.write = page.write =='true'?'fasle':'true';
    };

    $scope.toggleEdit = function(page){
        page.edit = page.edit=='true'?'false':'true';
    };

    $scope.toggleDelete = function(page){
        page.delete = page.delete=='true'?'false':'true';
    };

    $scope.addToList = function (thisPage) {
        thisPage.write=0;
        thisPage.edit=0;
        thisPage.delete=0;
        // add to premission list
        $scope.mypermisions.push(thisPage);
        // delete from list
        var curIndex = $scope.pages.indexOf(thisPage);
        $scope.pages.splice(curIndex,1);
    };

    $scope.removeToList = function (thisItem) {
        // add to premission list
        $scope.pages.push(thisItem);
        // delete from list
        var curIndex = $scope.mypermisions.indexOf(thisItem);
        $scope.mypermisions.splice(curIndex, 1);
    };



});

//# sourceMappingURL=app.js.map

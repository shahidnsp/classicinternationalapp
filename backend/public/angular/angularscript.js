

var app = angular.
    module('myApp', [
        'ngResource',
        'ngMaterial',
        'ui.bootstrap',
        'ngNotify',
        'mdPickers',
        'AccountHeadService',
        'LedgerPostingService',
        'DesignationService',
        'EmployeeService',
        'BatchService',
        'HospitalService',
        'NurseService',
        'ResidenceService',
        'CandidateService',
        'CourierService',
        'RentService',
        'UserInfoService',
        'FairCollectionService',
        'RentCollectionService',
        'AdvanceSalaryService',
        'SalaryService',
        'UserService',
        'BillBookService',
        'AttendanceService',
    ]);

app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

//app.config(['$locationProvider', function($locationProvider) {
//    $locationProvider.html5Mode({ enabled: true, requireBase: false }); }]);

app.filter('pagination', function() {
    return function(input, currentPage, pageSize) {
        if(angular.isArray(input)) {
            var start = (currentPage-1)*pageSize;
            var end = currentPage*pageSize;
            return input.slice(start, end);
        }
    };
});

app.filter('percentage', ['$filter', function ($filter) {
    return function (input, decimals) {
        return $filter('number')(input * 100, decimals) + '%';
    };
}]);
app.filter('sum', function(){
    return function(items, prop){
        return items.reduce(function(a, b){
            return a + b[prop];
        }, 0);
    };
});

app.config(function($mdDateLocaleProvider) {
    $mdDateLocaleProvider.formatDate = function(date) {
        return date ? moment(date).format('DD-MM-YYYY') : '';
    };

    $mdDateLocaleProvider.parseDate = function(dateString) {
        var m = moment(dateString, 'DD-MM-YYYY', true);
        return m.isValid() ? m.toDate() : new Date(NaN);
    };
});

app.directive('mdDatepicker', function ($parse) {

    return {
        require: 'ngModel',
        link: function (scope, elem, attr, ngModel) {
            $parse(attr.ngModel).assign(scope, new Date(_.get(scope, attr.ngModel)));
        }
    }
});

app.directive('imageReader', function ($q) {
    var slice = Array.prototype.slice;

    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            if (!ngModel) return;

            ngModel.$render = function () {};

            element.bind('change', function (e) {
                var element = e.target;

                $q.all(slice.call(element.files, 0).map(readFile))
                    .then(function (values) {
                        if (element.multiple) ngModel.$setViewValue(values);
                        else ngModel.$setViewValue(values.length ? values[0] : null);
                    });

                function readFile(file) {
                    var deferred = $q.defer();

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        deferred.resolve(e.target.result);
                    };
                    reader.onerror = function (e) {
                        deferred.reject(e);
                    };
                    reader.readAsDataURL(file);

                    return deferred.promise;
                }
            });
        }
    };
});

app.directive('ngFileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.ngFileModel);
            var isMultiple = attrs.multiple;
            var modelSetter = model.assign;
            element.bind("change", function (changeEvent) {
                var values = [];

                for (var i = 0; i < element[0].files.length; i++) {
                    var reader = new FileReader();

                    reader.onload = (function (i) {
                        return function(e) {
                            var value = {
                                lastModified: changeEvent.target.files[i].lastModified,
                                lastModifiedDate: changeEvent.target.files[i].lastModifiedDate,
                                name: changeEvent.target.files[i].name,
                                size: changeEvent.target.files[i].size,
                                type: changeEvent.target.files[i].type,
                                data: e.target.result
                            };
                            values.push(value);
                        }

                    })(i);

                    reader.readAsDataURL(changeEvent.target.files[i]);
                }


                scope.$apply(function () {
                    if (isMultiple) {
                        modelSetter(scope, values);
                    } else {
                        modelSetter(scope, values[0]);
                    }
                });
            });
        }
    }
}]);

/*app.directive('autoFocus',function($timeout){
   return{
       restrict: 'AC',
       link: function(_scope,_element){
           $timeout(function(){
               _element[0].focus();
           },0);
       }
   } ;
});*/

/*app.config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default')
        .dark();
});*/
/*app.config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default')
        .primaryPalette('pink')
        .accentPalette('orange');
});*/


/*(function ()
{
    'use strict';

    app.provider('msDatepickerFixConfig', msDatepickerFixConfigProvider);
    app.directive('msDatepickerFix', msDatepickerFix);

    *//** @ngInject *//*
    function msDatepickerFixConfigProvider()
    {
        var service = this;

        // Default configuration
        var defaultConfig = {
            // To view
            formatter: function (val)
            {
                if ( !val )
                {
                    return '';
                }

                return val === '' ? val : new Date(val);
            },
            // To model
            parser   : function (val)
            {
                if ( !val )
                {
                    return '';
                }

                return moment(val).add(moment(val).utcOffset(), 'm').toDate();
            }
        };

        // Methods
        service.config = config;

        //////////

        *//**
         * Extend default configuration with the given one
         *
         * @param configuration
         *//*
        function config(configuration)
        {
            defaultConfig = angular.extend({}, defaultConfig, configuration);
        }

        *//**
         * Service
         *//*
        service.$get = function ()
        {
            return defaultConfig;
        };
    }

    *//** @ngInject *//*
    function msDatepickerFix(msDatepickerFixConfig)
    {
        return {
            require : 'ngModel',
            priority: 1,
            link    : function (scope, elem, attrs, ngModel)
            {
                ngModel.$formatters.push(msDatepickerFixConfig.formatter); // to view
                ngModel.$parsers.push(msDatepickerFixConfig.parser); // to model
            }
        };
    }
})();*/

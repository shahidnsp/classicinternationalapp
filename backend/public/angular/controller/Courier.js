app.controller('CourierController', function($scope,$http,$filter,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,Courier){
    $scope.courieredit = false;
    $scope.newcourier = new Courier();
    $scope.curCourier = {};

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.couriers =[];
    function loadCourier() {
        Courier.query(function (head) {
            $scope.couriers = head;
            //console.log(head);
        });
    }loadCourier();

    $scope.search=function(fromDate,toDate){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_courier', {params:{fromDate:from,toDate:to}}).
            success(function(data, status)
            {
                $scope.couriers = data;
            });
    };



    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Courier Register(Courier Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });




    $scope.newCourier = function () {
        $scope.courieredit = true;
        $scope.newcourier = new Courier();
        $scope.newcourier.date=new Date();
        $scope.curCourier = {};
    };
    $scope.editCourier = function (thisHead) {
        $scope.courieredit = true;
        $scope.curCourier =  thisHead;
        $scope.newcourier = new Courier();
        $scope.newcourier = angular.copy(thisHead);
        $scope.newcourier.date=new Date(thisHead.date);
        $anchorScroll();
    };
    $scope.addCourier = function () {
        $scope.newcourier.date=new Date($scope.newcourier.date);
        if ($scope.curCourier.id) {
            $http.put('/api/courier/'+$scope.curCourier.id, $scope.newcourier).
                success(function(data, status)
                {
                    loadCourier();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                });
        } else{
            $scope.newcourier.$save(function(account){
                loadCourier();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Your Transaction updated Successfully');
            });
        }
        $scope.courieredit = false;
    };
    $scope.deleteCourier = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/courier/'+item.id).
                success(function(data, status)
                {
                    loadCourier();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelCourier = function () {
        $scope.courieredit = false;
        $scope.newcourier =  new Courier();
    };


    $scope.openCourier=function(data,user,ev){
        $mdDialog.show({
            controller: DialogCourierController,
            templateUrl: $window.location.origin+'/template/courier',
            parent: angular.element(document.querySelector('#popup')),
            targetEvent: ev,
            clickOutsideToClose:true,
            locals: {
                data: data,
                user:user
            }
        })
            .then(function(answer) {
                //$scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                //$scope.status = 'You cancelled the dialog.';
            });

        // console.log($window.location.origin);
    };

    function DialogCourierController($scope, $mdDialog,data,user) {

        $scope.courier=data;
        $scope.user=user;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }


    $scope.export=function(fromDate,toDate){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        var req = {
            method: 'POST',
            url: '/api/excelExportCourier',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='CourierReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

});
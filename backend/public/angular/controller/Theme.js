app.controller('ThemeController', function($scope,$http,$window){
    $scope.newtheme={toggle:true,theme:1};

    $scope.changeTheme=function(theme){
        $scope.newtheme.theme=theme;
    };

    $scope.updateTheme=function(){
        $http.post('/api/changetheme', $scope.newtheme).
            success(function(data, status)
            {
                //$window.location.reload();
                $window.location.href=$window.location.href;
            });
        console.log($scope.newtheme);
    };

});
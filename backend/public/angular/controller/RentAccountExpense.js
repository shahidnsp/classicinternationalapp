app.controller('RentAccountExpenseController', function($scope,$http,$window,$filter,$location,$log,$timeout,ngNotify,$mdDialog,$anchorScroll,UserInfo,LedgerPosting){
    $scope.expenseedit = false;
    $scope.newexpense = new LedgerPosting();
    $scope.curExpense = {};
    $scope.account_id = 'All';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.expenses =[];
    function loadLedgerPosting() {
        LedgerPosting.query({group:'Rent',type:'expense'}, function (head) {
            $scope.expenses = head;
            //console.log(head);
        });
    }loadLedgerPosting();

    $scope.search=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_rent_expense', {params:{fromDate:from,toDate:to,account_id:account_id}}).
            success(function(data, status)
            {
                $scope.expenses = data;
            });
    };

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Expense Register(Rent Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });




    $scope.newExpense = function () {
        $scope.expenseedit = true;
        $scope.newexpense = new LedgerPosting();
        $scope.newexpense.date=new Date();
        $scope.curExpense = {};

    };
    $scope.editExpense = function (thisHead) {
        $scope.expenseedit = true;
        getSelectedItem(thisHead.accounthead_id);
        $scope.curExpense =  thisHead;
        $scope.newexpense = new LedgerPosting();
        $scope.newexpense = angular.copy(thisHead);
        $scope.newexpense.date=new Date(thisHead.date);
        $anchorScroll();
    };

    function getSelectedItem(id){
        $http.get('/api/getAccountHead/'+id).
            success(function(data, status)
            {
                $scope.selectedItem=data;
            });
    }

    $scope.addExpense = function () {
        $scope.newexpense.accounthead_id=$scope.selectedItem.id;
        $scope.newexpense.date=new Date($scope.newexpense.date);
        if ($scope.curExpense.id) {
            $http.put('/api/ledgerposting/'+$scope.curExpense.id, $scope.newexpense).
                success(function(data, status)
                {
                    loadLedgerPosting();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                });
        } else{
            $scope.newexpense.$save(function(account){
                loadLedgerPosting();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Your Transaction saved Successfully');
            });
        }
        $scope.expenseedit = false;
    };
    $scope.deleteExpense = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/ledgerposting/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction Deleted Successfully');
                    loadLedgerPosting();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelExpense = function () {
        $scope.expenseedit = false;
        $scope.newexpense =  new LedgerPosting();
    };




    $scope.simulateQuery = false;
    $scope.isDisabled    = false;

    // list of `state` value/display objects
    $scope.states        = [];loadAll();
    $scope.querySearch   = querySearch;
    $scope.selectedItemChange = selectedItemChange;
    $scope.searchTextChange   = searchTextChange;

    $scope.newState = newState;

    function newState(state) {
        alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
        var results = query ? $scope.states.filter( createFilterFor(query) ) : $scope.states,
            deferred;
        if ($scope.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    function searchTextChange(text) {
        //$log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
        //$log.info('Item changed to ' + JSON.stringify(item));
    }


    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
        $scope.data1=[];
        $http.get('/api/accounthead_expense/',{params:{head:'Rent'}}).
            success(function(data, status)
            {
                $scope.states=data;
            });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {

        var lowercaseQuery =query; //angular.lowercase(query);

        return function filterFn(state) {
            return (state.value.indexOf(lowercaseQuery) === 0);
        };

    }

    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.accounthead_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.export=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        var req = {
            method: 'POST',
            url: '/api/excelExportLedgerPosting',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,type:'expense',group:'Rent'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='ExpenseReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };
});
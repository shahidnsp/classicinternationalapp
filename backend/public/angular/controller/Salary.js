app.controller('SalaryController', function($scope,$http,$filter,$window,$location,$log,$timeout,$mdDialog,$anchorScroll,ngNotify,UserInfo,Salary){
    $scope.salaryedit = false;
    $scope.newsalary = new Salary();
    $scope.curSalary = {};
    $scope.employee_id = 'All';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.salaries =[];
    function loadSalary() {
        Salary.query(function (head) {
            $scope.salaries = head;
            getSum(head);
            //console.log(head);
        });
    }loadSalary();

    $scope.search=function(fromDate,toDate,employee_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_salary', {params:{fromDate:from,toDate:to,employee_id:employee_id}}).
            success(function(data, status)
            {
                $scope.salaries = data;
                getSum(data);
            });
    };
    $scope.total=0;
    function getSum(salaries){
        var length=salaries.length;
        var sum=0;
        for(var i=0;i<length;i++){
            sum+=parseFloat(salaries[i].total);
        }
        $scope.total=sum;
    }

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Salary Register(Salary)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });




    $scope.newSalary = function () {
        $scope.salaryedit = true;
        $scope.newsalary = new Salary();
        $scope.newsalary.date=new Date();
        $scope.curSalary = {};
        $scope.leave='';
        $scope.half='';
    };
    $scope.editSalary = function (thisHead) {
        $scope.salaryedit = true;
        $scope.curSalary =  thisHead;
        $scope.newsalary = new Salary();
        //$scope.newsalary = angular.copy(thisHead);
        $scope.newsalary.employee_id=thisHead.employee_id;
        $scope.newsalary.basic=parseFloat(thisHead.basic);
        $scope.newsalary.advance=parseFloat(thisHead.advance);
        $scope.newsalary.allowance=parseFloat(thisHead.allowance);
        $scope.newsalary.leavesalary=parseFloat(thisHead.leavesalary);
        $scope.newsalary.total=parseFloat(thisHead.total);
        $scope.newsalary.date=new Date(thisHead.date);
        $scope.newsalary.remark=thisHead.remark;
        $anchorScroll();
    };
    $scope.addSalary = function () {
        $scope.newsalary.date=new Date($scope.newsalary.date);
        if ($scope.curSalary.id) {
            $http.put('/api/salaries/'+$scope.curSalary.id, $scope.newsalary).
                success(function(data, status)
                {
                    loadSalary();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                });
        } else{
            $scope.newsalary.$save(function(account){
                loadSalary();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Your Transaction saved Successfully');
            });
        }
        $scope.salaryedit = false;
    };
    $scope.deleteSalary = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/salaries/'+item.id).
                success(function(data, status)
                {
                    loadSalary();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelSalary = function () {
        $scope.salaryedit = false;
        $scope.newsalary =  new Salary();
    };
    $scope.leave='';
    $scope.getBasicPayAndAdvance = function (employee_id) {
        $http.post('/api/getBasicpay',{employee_id:employee_id}).
            success(function(data, status)
            {
                $scope.newsalary.basic=parseFloat(data.basic);
                $scope.newsalary.advance=parseFloat(data.advance);
                $scope.newsalary.allowance=0;
                $scope.newsalary.leavesalary=0;
                calculateSalary();
                console.log(data);
            });

        $http.post('/api/getLeave',{employee_id:employee_id,date:$scope.newsalary.date}).
            success(function(data, status)
            {
                $scope.leave=data.leave;
                $scope.half=data.half;
            });
    };

    function calculateSalary(){
        var basic= 0,advance= 0,allowance= 0,leavesalary= 0,total=0;
        try{
            basic=parseFloat($scope.newsalary.basic);
        }catch(err){
            basic= 0;
            console.log(err);
        }

        try{
            advance=parseFloat($scope.newsalary.advance);
        }catch(err){
            advance= 0;
            console.log(err);
        }

        try{
            allowance=parseFloat($scope.newsalary.allowance);
        }catch(err){
            allowance= 0;
            console.log(err);
        }

        try{
            leavesalary=parseFloat($scope.newsalary.leavesalary);
        }catch(err){
            leavesalary= 0;
            console.log(err);
        }


        total=((basic-advance)+allowance)-leavesalary;
        console.log(total);
        $scope.newsalary.total=total;

    }


    $scope.changeSalary=function(){
        calculateSalary();
    };



    $scope.open=function(data,ev){
        $http.get('/api/employee/'+data.employee_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/employee',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {
        $scope.employee=data;
        $scope.basepath=$window.location.origin;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.export=function(fromDate,toDate,employee_id){
        var req = {
            method: 'POST',
            url: '/api/excelExportSalary',
            responseType:"arraybuffer",
            data: {fromDate:fromDate,toDate:toDate,employee_id:employee_id}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='SalaryReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };
});
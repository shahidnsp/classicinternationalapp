app.controller('UserInfoController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,User,UserInfo){
    $scope.headedit = false;
    $scope.newuser = {};
    $scope.curUser = {};
    $scope.showpassword = true;

    //Only for admin load all users
    /*if($scope.user.isAdmin==true){
        User.query(function(users){
            $scope.users =users;
            console.log(users);
        });
    }*/
    $scope.users=[];
    function loadUser() {
        User.query(function (users) {
            $scope.users = users;
            console.log(users);
        });
    }
    loadUser();



    $scope.newUser = function () {
        $scope.headedit = true;
        $scope.newuser = {};
        $scope.curUser = {};
        $scope.showpassword = true;
    };
    $scope.editUser = function (thisHead) {
        $scope.headedit = true;
        $scope.curUser =  thisHead;
        $scope.newuser = {};
        $scope.newuser = angular.copy(thisHead);
        $scope.showpassword = false;
        $anchorScroll();
    };
    $scope.addUser = function () {
        if ($scope.curUser.id) {
            $http.put('/api/user/'+$scope.curUser.id, $scope.newuser).
                success(function(data, status)
                {
                    loadUser();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('User updated Successfully');
                });
        } else{
            $http.post('/api/user', $scope.newuser).
                success(function(data, status)
                {
                    loadUser();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('User Created Successfully');
                });
        }
        $scope.headedit = false;
    };
    $scope.deleteUser = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/user/'+item.id).
                success(function(data, status)
                {
                    loadUser();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('User Removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelUser = function () {
        $scope.headedit = false;
        $scope.newuser =  {};
    };

    $scope.available='0';

    $scope.checkUser = function (username) {
        $http.post('/api/checkuser/', {username:username}).
            success(function(data, status)
            {
               if(data.found==='Found')
                   $scope.available='1';
                else
                   $scope.available='0';
            });
    };
    $scope.resetPassword = function (user,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to Reset Password ?')
            .textContent('Are you sure to reset '+user.name+' \'s Password')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.post('/api/resetPassword',{id:user.id}).
                success(function(data,status,headers,config){
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popup')))
                            .clickOutsideToClose(true)
                            .title('Success')
                            .textContent(user.name+' \'s Password changed to "admin".')
                            .ariaLabel('Alert Dialog Demo')
                            .ok('Got it!')
                            .targetEvent(ev)
                    );
                }).error(function(data,status,headers,config){
                    console.log(data);
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popup')))
                            .clickOutsideToClose(true)
                            .title('Error')
                            .textContent(' Password reset failed....')
                            .ariaLabel('Alert Dialog Demo')
                            .ok('Got it!')
                            .targetEvent(ev)
                    );
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };


    $scope.pages = [];
    $scope.mypermisions = [];
    $scope.permissionMode = function (thisUser) {
        $scope.curUser = thisUser;
        $scope.name = thisUser.name;

        UserInfo.getAllPage(thisUser.id).success(function(data){
            $scope.pages = data
        });

        UserInfo.getUserPermission(thisUser.id).success(function(data){
            $scope.mypermisions = data;
        });

        $scope.resetPermission = true;
        //$scope.breadCrumbs.push("home", $scope.curUser.name);
    };

    $scope.backToUsers = function () {
        $scope.pages=[];
        $scope.mypermisions = [];
        $scope.resetPermission = false;
    };

    $scope.savePermission = function (ev){
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to Reset Permission ?')
            .textContent('Are you sure to reset '+$scope.curUser.name+' \'s Permission')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {

            UserInfo.setUserPermission({id:$scope.curUser.id,permission:$scope.mypermisions})
                .success(function(data){
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popup')))
                            .clickOutsideToClose(true)
                            .title('Success')
                            .textContent($scope.curUser.name+' \'s Permission changed Successfully.')
                            .ariaLabel('Alert Dialog Demo')
                            .ok('Got it!')
                            .targetEvent(ev)
                    );
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });


    };

    $scope.editPage = false;
    $scope.writePage= true;
    $scope.deletePage = false;

    $scope.toggleWrite = function(page){
        page.write = page.write =='true'?'fasle':'true';
    };

    $scope.toggleEdit = function(page){
        page.edit = page.edit=='true'?'false':'true';
    };

    $scope.toggleDelete = function(page){
        page.delete = page.delete=='true'?'false':'true';
    };

    $scope.addToList = function (thisPage) {
        thisPage.write=0;
        thisPage.edit=0;
        thisPage.delete=0;
        // add to premission list
        $scope.mypermisions.push(thisPage);
        // delete from list
        var curIndex = $scope.pages.indexOf(thisPage);
        $scope.pages.splice(curIndex,1);
    };

    $scope.removeToList = function (thisItem) {
        // add to premission list
        $scope.pages.push(thisItem);
        // delete from list
        var curIndex = $scope.mypermisions.indexOf(thisItem);
        $scope.mypermisions.splice(curIndex, 1);
    };



});

/**
 * Created by psybo on 18/5/17.
 */
app.controller('RentchartController', function($scope,$http,$window,$mdDialog,UserInfo){

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Rent Chart(Rent Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });


    $scope.openRentChart=function(data,ev){
        $mdDialog.show({
            controller: DialogRentController,
            templateUrl: $window.location.origin+'/template/rentchart',
            parent: angular.element(document.querySelector('#popup')),
            targetEvent: ev,
            clickOutsideToClose:true,
            locals: {
                data: data
            }
        })
            .then(function(answer) {
                //$scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                //$scope.status = 'You cancelled the dialog.';
            });

    };

    function DialogRentController($scope, $mdDialog,data) {

        $scope.rentchart=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.export=function(year){

        var req = {
            method: 'POST',
            url: '/api/excelExportRentChart',
            responseType:"arraybuffer",
            data: {year:year}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='RentChartReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

});
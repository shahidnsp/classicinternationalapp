/**
 * Created by psybo on 18/5/17.
 */
app.controller('FairRegisterReport2Controller', function($scope,$http,$window,$mdDialog,$location,UserInfo){

    $scope.driver_id='All';
    $scope.hospital_id='All';
    var d=new Date();
    d.setDate(1);
    d.setDate(-2);
    $scope.month=d;
    $scope.showProgress=false;

    $scope.basedon='Driver';

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Fair Register Report 2(Transportation)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });

    $scope.account_id = 'All';
    $scope.status = 'All';
    $scope.totalIncome=0;

    $scope.duties=[];
    $scope.getDutyChart=function(){
        $scope.showProgress=true;
        $scope.reports=[];
        $http.post('/api/fairregisterreportjson2',{month:$scope.month,driver_id:$scope.driver_id,status: $scope.status,basedon:$scope.basedon,hospital_id:$scope.hospital_id}).
            success(function(data, status)
            {
                $scope.reports=data;
                getSum(data);
                $scope.showProgress=false;
            });
    };

    function getSum(data){
        var length=data.length;
        var sum=0;
        for(var i=0;i<length;i++){
            if(data[i].curMonth!='NPD')
                sum=sum+parseFloat(data[i].curMonth);
        }
        $scope.totalIncome=sum;
    }

    $scope.changeStatus=function(status){
        $scope.showProgress=true;
        $scope.reports=[];
        var reports=[];
        $http.post('/api/fairregisterreportjson2',{month:$scope.month,driver_id:$scope.driver_id,status:status}).
            success(function(data, jsonstatus)
            {
                $scope.reports=data;
                getSum(data);
                $scope.showProgress=false;
            });

    };

    $scope.showMonth=function(date){
        var d = new Date(date);
        var month = new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";
        var curmonth=d.getMonth();
        $scope.curMonth=month[curmonth];
    };

    $scope.showMonth($scope.month);

    $scope.showProgressExport=false;
    $scope.exportExcel=function(month,driver_id,status,basedon,hospital_id){
        $scope.showProgressExport=true;
        var req = {
            method: 'POST',
            url: '/api/excelExportFairRegisterReport2',
            responseType:"arraybuffer",
            data: {month:month,driver_id:driver_id,status:status,basedon:basedon,hospital_id:hospital_id}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='excelExportFairRegisterReport2.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();

                // Remove anchor from body
                document.body.removeChild(a);
                $scope.showProgressExport=false;
            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

    $scope.exportPDF=function(month,driver_id,status,basedon,hospital_id){
        $scope.showProgressExport=true;
        var req = {
            method: 'POST',
            url: '/api/pdfExportFairRegisterReport2',
            responseType:"arraybuffer",
            data: {month:month,driver_id:driver_id,status:status,basedon:basedon,hospital_id:hospital_id}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='pdfExportFairRegisterReport2.pdf' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();

                // Remove anchor from body
                document.body.removeChild(a);
                $scope.showProgressExport=false;
            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };


    $scope.openResidence=function(data,ev){
        $http.get('/api/residence/'+data.residence_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogResidenceController,
                    templateUrl: $window.location.origin+'/template/residence',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogResidenceController($scope, $mdDialog,data) {
        $scope.residence=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openHospital=function(data,ev){
        $http.get('/api/hospital/'+data.hospital_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogHospitalController,
                    templateUrl: $window.location.origin+'/template/hospital',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogHospitalController($scope, $mdDialog,data) {
        $scope.hospital=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openEmployee=function(data,ev){
        $http.get('/api/employee/'+data.employee_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/employee',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {

        $scope.basepath=$window.location.origin;
        $scope.employee=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openDutyChart=function(data,residence,hospital,driver,ev){
        $mdDialog.show({
            controller: DialogDutyController,
            templateUrl: $window.location.origin+'/template/dutychart',
            parent: angular.element(document.querySelector('#popup')),
            targetEvent: ev,
            clickOutsideToClose:true,
            locals: {
                data: data,
                residence:residence,
                hospital:hospital,
                driver:driver
            }
        })
            .then(function(answer) {
                //$scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                //$scope.status = 'You cancelled the dialog.';
            });

    };

    function DialogDutyController($scope, $mdDialog,$window,data,residence,hospital,driver) {

        $scope.dutychart=data;
        $scope.residence=residence;
        $scope.hospital=hospital;
        $scope.driver=driver;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }


});
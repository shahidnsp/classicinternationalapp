/**
 * Created by psybo on 18/5/17.
 */
app.controller('DutychartController', function($scope,$http,$window,$mdDialog,$location,UserInfo){

    $scope.driver_id='All';
    $scope.showProgress=false;

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Duty Chart(Transportation)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });

    $scope.account_id = 'All';

    $scope.duties=[];
    $scope.getDutyChart=function(){
        $scope.showProgress=true;
        $http.post('/api/dutychartjson',{year:$scope.year,driver_id:$scope.driver_id}).
            success(function(data, status)
            {
                $scope.duties=data;
                getSum(data);
                $scope.showProgress=false;
            });
    };

    $scope.jansum=0;
    $scope.febsum=0;
    $scope.marsum=0;
    $scope.aprsum=0;
    $scope.maysum=0;
    $scope.junsum=0;
    $scope.julsum=0;
    $scope.augsum=0;
    $scope.sepsum=0;
    $scope.octsum=0;
    $scope.novsum=0;
    $scope.decsum=0;

    function getSum(data){
        $scope.jansum=0;
        $scope.febsum=0;
        $scope.marsum=0;
        $scope.aprsum=0;
        $scope.maysum=0;
        $scope.junsum=0;
        $scope.julsum=0;
        $scope.augsum=0;
        $scope.sepsum=0;
        $scope.octsum=0;
        $scope.novsum=0;
        $scope.decsum=0;
        var length=data.length;
        for(var i=0;i<length;i++){
            if(data[i].jan!='NPD')
                $scope.jansum+=parseFloat(data[i].jan);

            if(data[i].feb!='NPD')
                $scope.febsum+=parseFloat(data[i].feb);

            if(data[i].mar!='NPD')
                $scope.marsum+=parseFloat(data[i].mar);

            if(data[i].apr!='NPD')
                $scope.aprsum+=parseFloat(data[i].apr);

            if(data[i].may!='NPD')
                $scope.maysum+=parseFloat(data[i].may);

            if(data[i].jun!='NPD')
                $scope.junsum+=parseFloat(data[i].jun);

            if(data[i].jul!='NPD')
                $scope.julsum+=parseFloat(data[i].jul);

            if(data[i].aug!='NPD')
                $scope.augsum+=parseFloat(data[i].aug);

            if(data[i].sep!='NPD')
                $scope.sepsum+=parseFloat(data[i].sep);

            if(data[i].oct!='NPD')
                $scope.octsum+=parseFloat(data[i].oct);

            if(data[i].nov!='NPD')
                $scope.novsum+=parseFloat(data[i].nov);

            if(data[i].dec!='NPD')
                $scope.decsum+=parseFloat(data[i].dec);
        }
    }



    $scope.getPaidAndUnpaidPDF=function(year,driver_id){

        $scope.showProgress=true;
        var req = {
            method: 'PUT',
            url: '/api/getPaidAndUnpaidPDF',
            responseType:"arraybuffer",
            data: {year: year, driver_id: driver_id}
        };

        $http(req)
            .success(function(data, status, headers){

                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='dutychart.pdf' ;//headers('filename');

                document.body.appendChild(a);
                a.click();


                document.body.removeChild(a);
                $scope.showProgress=false;
            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

    $scope.getUnpaidPDF=function(year,driver_id){
        $scope.showProgress=true;
        var req = {
            method: 'PUT',
            url: '/api/getUnpaidPDF',
            responseType:"arraybuffer",
            data: {year: year, driver_id: driver_id}
        };

        $http(req)
            .success(function(data, status, headers){

                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='unpaidPDF.pdf' ;//headers('filename');

                document.body.appendChild(a);
                a.click();


                document.body.removeChild(a);
                $scope.showProgress=false;
            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

    $scope.getPaidPDF=function(year,driver_id){
        $scope.showProgress=true;
        var req = {
            method: 'PUT',
            url: '/api/getPaidPDF',
            responseType:"arraybuffer",
            data: {year: year, driver_id: driver_id}
        };

        $http(req)
            .success(function(data, status, headers){

                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='paidPDF.pdf' ;//headers('filename');

                document.body.appendChild(a);
                a.click();


                document.body.removeChild(a);
                $scope.showProgress=false;
            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };





    $scope.openResidence=function(data,ev){
        $http.get('/api/residence/'+data.residence_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogResidenceController,
                    templateUrl: $window.location.origin+'/template/residence',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogResidenceController($scope, $mdDialog,data) {
        $scope.residence=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openHospital=function(data,ev){
        $http.get('/api/hospital/'+data.hospital_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogHospitalController,
                    templateUrl: $window.location.origin+'/template/hospital',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogHospitalController($scope, $mdDialog,data) {
        $scope.hospital=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openEmployee=function(data,ev){
        $http.get('/api/employee/'+data.employee_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/employee',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {

        $scope.basepath=$window.location.origin;
        $scope.employee=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openDutyChart=function(data,residence,hospital,driver,ev){
        $mdDialog.show({
            controller: DialogDutyController,
            templateUrl: $window.location.origin+'/template/dutychart',
            parent: angular.element(document.querySelector('#popup')),
            targetEvent: ev,
            clickOutsideToClose:true,
            locals: {
                data: data,
                residence:residence,
                hospital:hospital,
                driver:driver
            }
        })
            .then(function(answer) {
                //$scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                //$scope.status = 'You cancelled the dialog.';
            });

    };

    function DialogDutyController($scope, $mdDialog,$window,data,residence,hospital,driver) {

        $scope.dutychart=data;
        $scope.residence=residence;
        $scope.hospital=hospital;
        $scope.driver=driver;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.export=function(year,driver_id){
        var req = {
            method: 'POST',
            url: '/api/excelExportDutyChart',
            responseType:"arraybuffer",
            data: {year:year,driver_id:driver_id}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='DutyChartReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

});
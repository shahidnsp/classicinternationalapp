app.controller('DesignationController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,Designation){
    $scope.designationedit = false;
    $scope.newdesignation = new Designation();
    $scope.curDesignation = {};
    $scope.designations=[];
    function loadDesignation() {
        Designation.query(function (head) {
            $scope.designations = head;
        });
    }loadDesignation();

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Designation(Main Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });




    $scope.newDesignation = function () {
        $scope.designationedit = true;
        $scope.newdesignation = new Designation();
        $scope.curDesignation = {};
    };
    $scope.editDesignation = function (thisHead) {
        $scope.designationedit = true;
        $scope.curDesignation =  thisHead;
        $scope.newdesignation = new Designation();
        $scope.newdesignation = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addDesignation = function () {
        $scope.newdesignation.default=0;
        if ($scope.curDesignation.id) {
            $http.put('/api/designation/'+$scope.curDesignation.id, $scope.newdesignation).
                success(function(data, status)
                {
                    loadDesignation();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Designation updated Successfully');
                });
        } else{
            $scope.newdesignation.$save(function(account){
                loadDesignation();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Designation saved Successfully');
            });
        }
        $scope.designationedit = false;
    };
    $scope.deleteDesignation = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/designation/'+item.id).
                success(function(data, status)
                {
                    loadDesignation();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });
                    if(data.default===true){
                        ngNotify.set('Designation deleted Successfully');
                    }else{
                        ngNotify.set('No Permission to delete This Item');
                    }

                    console.log(data.default);
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelDesignation = function () {
        $scope.designationedit = false;
        $scope.newdesignation =  new Designation();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Designation Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeDesignationStatus/'+id).
                success(function(data, status)
                {
                    loadDesignation();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Designation Status updated Successfully');


                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.open=function(data,ev){
        $http.get('/api/designation/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/designation',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {

        $scope.designation=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

});
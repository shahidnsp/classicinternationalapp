app.controller('EmployeeController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,Employee){
    $scope.employeeedit = false;
    $scope.newemployee = new Employee();
    $scope.curEmployee = {};

    $scope.basepath=$window.location.origin;

    $scope.employees=[];
    function loadEmployee() {
        Employee.query(function (head) {
            $scope.employees = head;
        });
    }loadEmployee();

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Employee Register(Main Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });

    $scope.designations=[];

    function loadDesignation(){
        $http.get('/api/getDesignation').
            success(function(data, status)
            {
                $scope.designations=data;
            });
    }loadDesignation();



    $scope.newEmployee = function () {
        $scope.employeeedit = true;
        $scope.newemployee = new Employee();
        $scope.curEmployee = {};
    };
    $scope.editEmployee = function (thisHead) {
        $scope.employeeedit = true;
        $scope.curEmployee =  thisHead;
        $scope.newemployee = new Employee();
        $scope.newemployee = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addEmployee = function () {

        var photos=[];
        angular.forEach($scope.photos, function (value, key) {
            photos[key]=value;
        });

        if ($scope.curEmployee.id) {
            /*$http.put('/api/employee/'+$scope.curEmployee.id, $scope.newemployee).
                success(function(data, status)
                {
                    $window.location.reload();
                });*/
                $http.put('/api/employee/'+$scope.curEmployee.id,{name:$scope.newemployee.name,
                    mobile:$scope.newemployee.mobile,whatsapp:$scope.newemployee.whatsapp,
                    email:$scope.newemployee.email,addresskuwait:$scope.newemployee.addresskuwait,
                    addressindia:$scope.newemployee.addressindia,mobileindia:$scope.newemployee.mobileindia,
                    designation_id:$scope.newemployee.designation_id,basicpay:$scope.newemployee.basicpay,
                    overtimepay:$scope.newemployee.overtimepay,passportno:$scope.newemployee.passportno,
                    visano:$scope.newemployee.visano,drivinglicenseno:$scope.newemployee.drivinglicenseno,
                    remark:$scope.newemployee.remark, photos:photos}).
                    success(function(data, status)
                    {
                        loadEmployee();
                        ngNotify.config({
                            theme: 'pure',
                            position: 'top',
                            duration: 3000,
                            type: 'info',
                            sticky: false,
                            button: true,
                            html: false
                        });

                        ngNotify.set('Employee updated Successfully');
                    });
        } else{
            $http.post('/api/employee',{name:$scope.newemployee.name,
                    mobile:$scope.newemployee.mobile,whatsapp:$scope.newemployee.whatsapp,
                    email:$scope.newemployee.email,addresskuwait:$scope.newemployee.addresskuwait,
                    addressindia:$scope.newemployee.addressindia,mobileindia:$scope.newemployee.mobileindia,
                    designation_id:$scope.newemployee.designation_id,basicpay:$scope.newemployee.basicpay,
                    overtimepay:$scope.newemployee.overtimepay,passportno:$scope.newemployee.passportno,
                    visano:$scope.newemployee.visano,drivinglicenseno:$scope.newemployee.drivinglicenseno,
                    remark:$scope.newemployee.remark, photos:photos}).
                success(function(data, status)
                {
                    loadEmployee();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Employee saved Successfully');
                });
            /*$scope.newemployee.$save(function(account){
                $window.location.reload();
            });*/
        }
        $scope.employeeedit = false;
    };
    $scope.deleteEmployee = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/employee/'+item.id).
                success(function(data, status)
                {
                    loadEmployee();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Employee deleted Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelEmployee = function () {
        $scope.employeeedit = false;
        $scope.newemployee =  new Employee();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Employee Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeEmployeeStatus/'+id).
                success(function(data, status)
                {
                    loadEmployee();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Employee Status changed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.open=function(data,ev){
        $http.get('/api/employee/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/employee',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {

        $scope.basepath=$window.location.origin;
        $scope.employee=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

});
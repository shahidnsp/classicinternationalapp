app.controller('HomeController', function($scope,$http,$window,$anchorScroll,UserInfo){

    if (localStorage.getItem("itemPerPage") === null) {
        localStorage.setItem("itemPerPage", 10);
    }
    function getPerPage(){
        return parseInt(localStorage.itemPerPage);
    }

    $scope.changeNum = function (itemNum) {
        localStorage.itemPerPage = itemNum;
        $scope.numPerPage = getPerPage();

        //$window.location.href = refineUrl()+'?num='+itemNum;
    };

    $scope.moveToTop=function(){
        $anchorScroll();
    };

    /*function refineUrl()
    {
        //get full url
        var url = window.location.href;
        //get url after/
        var value = url.substring(url.lastIndexOf('/') + 1);
        //get the part after before ?
        value  = value.split("?")[0];
        return value;
    }*/



    //console.log(localStorage.itemPerPage);
    $scope.numsForPage = [5, 10, 25, 50, 100];
    $scope.currentPage = 1;
    $scope.numPerPage = getPerPage();
    $scope.maxSize = 5;

    $scope.user = {};

    UserInfo.query().success(function(data){
        $scope.user = data;
        console.log(data);
    });

});
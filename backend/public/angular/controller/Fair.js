app.controller('FairController', function($scope,$http,$window,$location,$log,$timeout,$mdDialog,$anchorScroll,ngNotify,UserInfo,FairCollection){
    $scope.fairedit = false;
    $scope.newfair = new FairCollection();
    $scope.curFair = {};
    $scope.candidate_id = 'All';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);

    $scope.year=year;

    $scope.fairs =[];
    function loadFairCollection() {
        FairCollection.query(function (head) {
            $scope.fairs = head;
            //console.log(head);
        });
    }loadFairCollection();

    $scope.showProgress=false;
    $scope.showProgressSave=false;

    $scope.search=function(fromDate,toDate,candidate_id){
        $http.get('/api/search_transportation_fair', {params:{fromDate:$scope.fromDate,toDate:$scope.toDate,candidate_id:candidate_id}}).
            success(function(data, status)
            {
                $scope.fairs = data;
            });
    };

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Fair Register(Transportation)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });

    $scope.employees={};
    function loadEmployee(){
        $http.get('/api/getDriver').
            success(function(data, status)
            {
                $scope.employees=data;
            });
    }
    loadEmployee();


    $scope.newfairs=[];
    $scope.newFair = function () {
        $scope.fairedit = true;
        $scope.newfair = new FairCollection();
        $scope.month=new Date();
        $scope.curFair = {};
        $scope.newfairs={};
    };
    $scope.getFair=function(month){
        $scope.showProgress=true;
        $scope.newfairs=[];
        $http.post('/api/showoreditfair',{month:$scope.month}).
            success(function(data, status)
            {
                $scope.newfairs=data;
                $scope.showProgress=false;
            });
    };
    $scope.editFair = function (thisHead) {
        $scope.fairedit = true;
        $scope.curFair =  thisHead;
        $scope.month=new Date(thisHead.date);
        $scope.newfairs=[];
        $http.post('/api/showoreditfair',{month:$scope.month}).
            success(function(data, status)
            {
                $scope.newfairs=data;
            });
        $anchorScroll();
    };
    $scope.addFair = function (fairs) {
        $scope.showProgressSave=true;
        if ($scope.curFair.id) {
            $http.put('/api/faircollection/'+$scope.curFair.id, {fairs:fairs}).
                success(function(data, status)
                {
                    loadFairCollection();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                    $scope.showProgressSave=false;
                });
        } else{

            $http.post('/api/faircollection',{fairs:fairs}).
                success(function(data, status)
                {
                    loadFairCollection();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction saved Successfully');
                    console.log(data);
                    $scope.showProgressSave=false;
                });
        }

        $scope.fairedit = false;
    };
    $scope.deleteFair = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/faircollection/'+item.id).
                success(function(data, status)
                {
                    loadFairCollection();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelFair = function () {
        $scope.fairedit = false;
        $scope.newfair =  new FairCollection();
    };




    $scope.simulateQuery = false;
    $scope.isDisabled    = false;

    // list of `state` value/display objects
    $scope.states        = [];loadAll();
    $scope.querySearch   = querySearch;
    $scope.selectedItemChange = selectedItemChange;
    $scope.searchTextChange   = searchTextChange;

    $scope.newState = newState;

    function newState(state) {
        alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
        var results = query ? $scope.states.filter( createFilterFor(query) ) : $scope.states,
            deferred;
        if ($scope.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    function searchTextChange(text) {
        //$log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
        //$log.info('Item changed to ' + JSON.stringify(item));
        $scope.newfair.employee_id=item.employee_id;
    }


    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
        $scope.data1=[];
        $http.get('/api/getCandidate/').
            success(function(data, status)
            {
                $scope.states=data;
            });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {

        var lowercaseQuery =query; //angular.lowercase(query);

        return function filterFn(state) {
            return (state.value.indexOf(lowercaseQuery) === 0);
        };

    }

    $scope.openEmployee=function(data,ev){
        $http.get('/api/employee/'+data.employee_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/employee',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {

        $scope.basepath=$window.location.origin;
        $scope.employee=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openCandidate=function(data,ev){
        $http.get('/api/transportation/'+data.candidate_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogCandidateController,
                    templateUrl: $window.location.origin+'/template/candidate',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogCandidateController($scope, $mdDialog,data) {

        $scope.candidate=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };

        $scope.openHospital=function(data,ev){
            $http.get('/api/hospital/'+data.hospital_id).
                success(function(data, status)
                {
                    $mdDialog.show({
                        controller: DialogHospitalController,
                        templateUrl: $window.location.origin+'/template/hospital',
                        parent: angular.element(document.querySelector('#popup')),
                        targetEvent: ev,
                        clickOutsideToClose:true,
                        locals: {
                            data: data
                        }
                    })
                        .then(function(answer) {
                            //$scope.status = 'You said the information was "' + answer + '".';
                        }, function() {
                            //$scope.status = 'You cancelled the dialog.';
                        });
                });

            // console.log($window.location.origin);
        };

        function DialogHospitalController($scope, $mdDialog,data) {

            $scope.hospital=data;
            $scope.hide = function() {
                $mdDialog.hide();
            };

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };
        }


    }


    $scope.export=function(fromDate,toDate,candidate_id){
        var req = {
            method: 'POST',
            url: '/api/excelExportFair',
            responseType:"arraybuffer",
            data: {fromDate:fromDate,toDate:toDate,candidate_id:candidate_id}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='FairCollectionReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };
});
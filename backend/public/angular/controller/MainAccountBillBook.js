app.controller('MainAccountBillBookController', function($scope,$http,$window,$filter,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,AccountHead,BillBook){
    $scope.billedit = false;
    $scope.newbill = new BillBook();
    $scope.curBill = {};

    $scope.billbooks=[];

    function loadBillBook() {
        BillBook.query(function (bill) {
            $scope.billbooks = bill;
        });
    }loadBillBook();



    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Bill Book(Main Account)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });

    $scope.editingData=[];
    $scope.newitems = [];
    $scope.updateitem = {};
    $scope.addItem = function () {
        var row=$scope.newitems.length;
        $scope.inserted = {
            id: row,
            amount: '1'
        };
        $scope.newitems.push($scope.inserted);
        $scope.editingData[row] = true;
        $scope.addTotal();
    };

    $scope.updateItem=function(item){
        if(item.accounthead_id!=null) {
            includeunit(item);
        }
    };

    function includeunit(item){
        $http.get('/api/accounthead/'+item.accounthead_id).
            success(function(data,status,headers,config){
                $scope.currow=data;

                updateRow(item);

                //console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }

    function updateRow(item){
        $scope.updateitem={
            id:item.id,
            amount:item.amount,
            accounthead_id:item.accounthead_id,
            name:$scope.currow.name
        };

        $scope.newitems[item.id]=$scope.updateitem;

        $scope.editingData[item.id] = false;
        $scope.addTotal();
    }

    $scope.editItem=function(item){
        $scope.newitems[item.id]=item;
        $scope.editingData[item.id] = true;
    };

    $scope.removeItem = function (item) {
        var confirmDelete = confirm("Do you really need to remove the item ?");
        if (confirmDelete) {
            var curIndex = $scope.newitems.indexOf(item);
            $scope.newitems.splice(curIndex, 1);
            $scope.addTotal();
        }
    };

    $scope.totalAmount=0;

     $scope.addTotal=function(){
        //$scope.totalAmount=parseFloat($scope.totalAmount)+parseFloat(amount);
        $scope.totalAmount=0;
        for(var i=0;i<$scope.newitems.length;i++)
            $scope.totalAmount=parseFloat($scope.totalAmount)+parseFloat($scope.newitems[i].amount)
    };


    AccountHead.query({group: 'Main'}, function (employee) {
        $scope.expenses = employee;
    });

    $scope.newBillBook = function () {
        $scope.billedit = true;
        $scope.newbill = new BillBook();
        $scope.newbill.date = new Date();
        $scope.curBill = {};
        $scope.newitems=[];
    };

    $scope.editBillBook = function (thisHead) {
        $scope.billedit = true;
        $scope.curBill =  thisHead;
        $scope.newbill = new BillBook();
        $scope.newbill = angular.copy(thisHead);
        $scope.newbill.date=new Date(thisHead.date);
        $scope.newitems=[];
        for(var i=0;i<thisHead.items.length;i++){
            $scope.updateItemEdit(thisHead.items[i]);
        }
        $anchorScroll();
    };
    $scope.updateItemEdit=function(item){
        if(item.accounthead_id!=null) {
            includeunitEdit(item);
        }
    };

    function includeunitEdit(item){
        $http.get('/api/accounthead/'+item.accounthead_id).
            success(function(data,status,headers,config){
                $scope.currow=data;

                updateRowEdit(item);

                //console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }

    function updateRowEdit(item){
        $scope.updateitem={
            id:item.id,
            amount:item.amount,
            accounthead_id:item.accounthead_id,
            name:$scope.currow.name
        };

        $scope.newitems.push($scope.updateitem);

        $scope.editingData[item.id] = false;
        $scope.addTotal();
    }


    $scope.addBillBook = function () {
        if ($scope.curBill.id) {
            if ($scope.newitems.length > 0) {
                var req = {
                    method: 'PUT',
                    url: '/api/bill/'+$scope.curBill.id,
                    responseType:"arraybuffer",
                    data: {newbill: $scope.newbill, items: $scope.newitems, total: $scope.totalAmount}
                };

                $http(req)
                    .success(function(data, status, headers){

                        loadBillBook();
                        ngNotify.config({
                            theme: 'pure',
                            position: 'top',
                            duration: 3000,
                            type: 'info',
                            sticky: false,
                            button: true,
                            html: false
                        });
                        ngNotify.set('Bill Saved Successfully');
                        $scope.billedit = false;

                        var arr = data;
                        var byteArray = new Uint8Array(arr);
                        var a = window.document.createElement('a');

                        a.href = window.URL.createObjectURL(
                            new Blob([byteArray], { type: 'application/octet-stream' })
                        );
                        a.download ='invoice.pdf' ;//headers('filename');

                        // Append anchor to body.
                        document.body.appendChild(a);
                        a.click();


                        // Remove anchor from body
                        document.body.removeChild(a);

                    }
                ).error(function(data, status, headers){
                        console.log(data);

                    }
                );
            }else{
                alert('No Item in List');
            }

        } else {
            if ($scope.newitems.length > 0) {
                var req = {
                    method: 'POST',
                    url: '/api/bill',
                    responseType:"arraybuffer",
                    data: {newbill: $scope.newbill, items: $scope.newitems, total: $scope.totalAmount}
                };

                $http(req)
                    .success(function(data, status, headers){

                        loadBillBook();
                        ngNotify.config({
                            theme: 'pure',
                            position: 'top',
                            duration: 3000,
                            type: 'info',
                            sticky: false,
                            button: true,
                            html: false
                        });
                        ngNotify.set('Bill Saved Successfully');
                        $scope.billedit = false;

                        var arr = data;
                        var byteArray = new Uint8Array(arr);
                        var a = window.document.createElement('a');

                        a.href = window.URL.createObjectURL(
                            new Blob([byteArray], { type: 'application/octet-stream' })
                        );
                        a.download ='invoice.pdf' ;//headers('filename');

                        // Append anchor to body.
                        document.body.appendChild(a);
                        a.click();


                        // Remove anchor from body
                        document.body.removeChild(a);

                    }
                ).error(function(data, status, headers){
                        console.log(data);

                    }
                );

            }else{
                alert('No Item in List');
            }
        }
    };

    $scope.deleteBillBook = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/bill/'+item.id).
                success(function(data, status)
                {
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Bill Deleted Successfully');
                    loadBillBook();
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.cancelBillBook = function () {
        $scope.billedit = false;
        $scope.newbill =  new BillBook();
        $scope.editingData=[];
        $scope.newitems = [];
        $scope.totalAmount=0;
    };

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }
    $scope.employee_id='All';
    $scope.search=function(fromDate,toDate){
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        BillBook.query({fromDate:from,toDate:to},function (bill) {
            $scope.billbooks = bill;
        });
    };

});
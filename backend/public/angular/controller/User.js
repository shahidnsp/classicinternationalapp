app.controller('UserController', function($scope,$http,$window){

    function loadProfile(){
        $http.get('/api/getprofile').
            success(function(data, status)
            {
                $scope.name=data.name;
                $scope.email=data.email;
            });
    }
    loadProfile();

    $scope.updateProfile=function(){
        var photos=[];
        angular.forEach($scope.photo, function (value, key) {
            photos[key]=value;
        });

        $http.post('/api/updateprofile',{name:$scope.name,email:$scope.email,photos:photos}).
            success(function(data, status)
            {
                //$window.location.reload();
                $window.location.href=$window.location.href;
            });
    };

});
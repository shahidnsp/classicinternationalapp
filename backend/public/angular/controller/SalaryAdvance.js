app.controller('SalaryAdvanceController', function($scope,$http,$window,$filter,$location,$log,$timeout,$mdDialog,ngNotify,$anchorScroll,UserInfo,AdvanceSalary){
    $scope.advanceedit = false;
    $scope.newadvance = new AdvanceSalary();
    $scope.curAdvance = {};
    $scope.employee_id = 'All';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.advances =[];
    function loadAdvanceSalary() {
        AdvanceSalary.query(function (head) {
            $scope.advances = head;

            //console.log(head);
        });
    }loadAdvanceSalary();

    $scope.search=function(fromDate,toDate,employee_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_salary_advance', {params:{fromDate:from,toDate:to,employee_id:employee_id}}).
            success(function(data, status)
            {
                $scope.advances = data;

            });
    };

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Salary Advance(Salary)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });




    $scope.newAdvance = function () {
        $scope.advanceedit = true;
        $scope.newadvance = new AdvanceSalary();
        $scope.newadvance.date=new Date();
        $scope.curAdvance = {};

    };
    $scope.editAdvance = function (thisHead) {
        $scope.advanceedit = true;
        $scope.curAdvance =  thisHead;
        $scope.newadvance = new AdvanceSalary();
        $scope.newadvance = angular.copy(thisHead);
        $scope.newadvance.date=new Date(thisHead.date);
        $anchorScroll();
    };
    $scope.addAdvance = function () {
        $scope.newadvance.date=new Date($scope.newadvance.date);
        if ($scope.curAdvance.id) {
            $http.put('/api/salaryadvance/'+$scope.curAdvance.id, $scope.newadvance).
                success(function(data, status)
                {
                    loadAdvanceSalary();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction updated Successfully');
                });
        } else{
            $scope.newadvance.$save(function(account){
                loadAdvanceSalary();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Your Transaction saved Successfully');
            });
        }
        $scope.advanceedit = false;
    };
    $scope.deleteAdvance = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/salaryadvance/'+item.id).
                success(function(data, status)
                {
                    loadAdvanceSalary();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Your Transaction removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelAdvance = function () {
        $scope.advanceedit = false;
        $scope.newadvance =  new AdvanceSalary();
    };



    $scope.open=function(data,ev){
        $http.get('/api/employee/'+data.employee_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/employee',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {
        $scope.employee=data;
        $scope.basepath=$window.location.origin;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.export=function(fromDate,toDate,employee_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        var req = {
            method: 'POST',
            url: '/api/excelExportSalaryAdvance',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,employee_id:employee_id}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='SalaryAdvanceReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };
});
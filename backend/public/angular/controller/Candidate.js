app.controller('CandidateController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,Candidate){
    $scope.candidateedit = false;
    $scope.newcandidate = new Candidate();
    $scope.curCandidate = {};
    $scope.basepath=$window.location.origin;
    $scope.candidates=[];
    function loadCandidate() {
        Candidate.query(function (head) {
            $scope.candidates = head;
        });
    }loadCandidate();

    $scope.search=function(driver_id,hostel_id,basedon){
        Candidate.query({driver_id:driver_id,hostel_id:hostel_id,basedon:basedon},function (head) {
            $scope.candidates = head;
        });
    };

    $scope.download=function(driver_id,hostel_id,basedon){
        $scope.showProgressExport=true;
        var req = {
            method: 'POST',
            url: '/api/pdfExportCandidate',
            responseType:"arraybuffer",
            data: {driver_id:driver_id,hostel_id:hostel_id,basedon:basedon}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='Candidates.pdf' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();

                // Remove anchor from body
                document.body.removeChild(a);
                $scope.showProgressExport=false;
            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };



    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Candidate Register(Transportation)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });

    $scope.residences=[];
    $scope.hospitals=[];
    $scope.drivers=[];
    $scope.basedon='Driver';
    $scope.driver_id='All';
    $scope.hostel_id='All';

    function loadResidence(){
        $http.get('/api/getResidence').
            success(function(data, status)
            {
                $scope.residences=data;
            });
    }loadResidence();

    function loadHospitals(){
        $http.get('/api/getHospital').
            success(function(data, status)
            {
                $scope.hospitals=data;
            });
    }loadHospitals();

    function loadDrivers(){
        $http.get('/api/getDriver').
            success(function(data, status)
            {
                $scope.drivers=data;
            });
    }loadDrivers();



    $scope.newCandidate = function () {
        $scope.candidateedit = true;
        $scope.newcandidate = new Candidate();
        $scope.curCandidate = {};
    };
    $scope.editCandidate = function (thisHead) {
        $scope.candidateedit = true;
        $scope.curCandidate =  thisHead;
        $scope.newcandidate = new Candidate();
        $scope.newcandidate = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addCandidate = function () {
        $scope.newcandidate.default=0;
        if ($scope.curCandidate.id) {
            $http.put('/api/transportation/'+$scope.curCandidate.id, $scope.newcandidate).
                success(function(data, status)
                {
                    loadCandidate();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Candidate updated Successfully');
                });
        } else{
            $scope.newcandidate.$save(function(account){
                loadCandidate();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Candidate saved Successfully');
            });
        }
        $scope.candidateedit = false;
    };
    $scope.deleteCandidate = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/transportation/'+item.id).
                success(function(data, status)
                {
                    loadCandidate();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Candidate removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelCandidate = function () {
        $scope.candidateedit = false;
        $scope.newcandidate =  new Candidate();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Candidate Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeCandidateStatus/'+id).
                success(function(data, status)
                {
                    loadCandidate();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Candidate Status Changed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.open=function(data,ev){
        $http.get('/api/employee/'+data.employee_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/employee',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    $scope.getProfile=function(item){
        $scope.showProgress=true;
        var req = {
            method: 'POST',
            url: '/api/getMyProfile',
            responseType:"arraybuffer",
            data: {id:item.id}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download =item.name+'.pdf' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);
                $scope.showProgress=false;
            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

    function DialogController($scope, $mdDialog,$window,data) {

        $scope.basepath=$window.location.origin;
        $scope.employee=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openResidence=function(data,ev){
        $http.get('/api/residence/'+data.residence_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogResidenceController,
                    templateUrl: $window.location.origin+'/template/residence',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogResidenceController($scope, $mdDialog,data) {

        $scope.residence=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openHospital=function(data,ev){
        $http.get('/api/hospital/'+data.hospital_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogHospitalController,
                    templateUrl: $window.location.origin+'/template/hospital',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogHospitalController($scope, $mdDialog,data) {

        $scope.hospital=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.openCandidate=function(data,ev){
        $http.get('/api/transportation/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogCandidateController,
                    templateUrl: $window.location.origin+'/template/candidate',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogCandidateController($scope, $mdDialog,data) {

        $scope.candidate=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };

        $scope.openHospital=function(data,ev){
            $http.get('/api/hospital/'+data.hospital_id).
                success(function(data, status)
                {
                    $mdDialog.show({
                        controller: DialogHospitalController,
                        templateUrl: $window.location.origin+'/template/hospital',
                        parent: angular.element(document.querySelector('#popup')),
                        targetEvent: ev,
                        clickOutsideToClose:true,
                        locals: {
                            data: data
                        }
                    })
                        .then(function(answer) {
                            //$scope.status = 'You said the information was "' + answer + '".';
                        }, function() {
                            //$scope.status = 'You cancelled the dialog.';
                        });
                });

            // console.log($window.location.origin);
        };

        function DialogHospitalController($scope, $mdDialog,data) {

            $scope.hospital=data;
            $scope.hide = function() {
                $mdDialog.hide();
            };

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };
        }


    }

});
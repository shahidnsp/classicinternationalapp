app.controller('DaybookController', function($scope,$http,$window,$filter,$mdDialog,UserInfo,LedgerPosting){


    var date=new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate();
    if(day>11) {
        $scope.fromDate = new Date(year, month, 11);
        $scope.toDate = new Date(year, month + 1, 10);
    }else{
        $scope.fromDate = new Date(year, month-1, 11);
        $scope.toDate = new Date(year, month, 10);
    }

    $scope.daybooks =[];
    function loadLedgerPosting() {
        LedgerPosting.query(function (head) {
            $scope.daybooks = head;
            //console.log(head);
            getSum(head);
        });
    }loadLedgerPosting();

    $scope.search=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/search_overall_daybook', {params:{fromDate:from,toDate:to,account_id:account_id}}).
            success(function(data, status)
            {
                $scope.daybooks = data;
                getSum(data);
            });
    };

    $scope.totalincome=0;
    $scope.totalexpense=0;
    function getSum(daybooks){
        var length=daybooks.length;
        var income=0;
        var expense=0;
        for(var i=0;i<length;i++){
            if(daybooks[i].accounthead.type==='income')
                income+=parseFloat(daybooks[i].amount);
            if(daybooks[i].accounthead.type==='expense')
                expense+=parseFloat(daybooks[i].amount);
        }
        $scope.totalincome=income;
        $scope.totalexpense=expense;
    }

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Day Book(Day Book)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }
        // console.log(data);
    });

    $scope.account_id = 'All';


    $scope.open=function(data,ev){
        $http.get('/api/accounthead/'+data.accounthead_id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/accounthead',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,data) {
        $scope.account=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    $scope.showProgress=false;
    $scope.export=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $scope.showProgress=true;
        var req = {
            method: 'POST',
            url: '/api/excelExportDaybook',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='DaybookReport.xlsx' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);
                $scope.showProgress=false;
            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

    $scope.exportPDF=function(fromDate,toDate,account_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $scope.showProgress=true;
        var req = {
            method: 'POST',
            url: '/api/pdfExportDayBook',
            responseType:"arraybuffer",
            data: {fromDate:from,toDate:to,account_id:account_id,group:'All'}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='DaybookReport.pdf' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();

                // Remove anchor from body
                document.body.removeChild(a);

                $scope.showProgress=false;

            }
        ).error(function(data, status, headers){
                console.log(data);

            }
        );
    };

});
app.controller('DriverController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,Employee){
    $scope.driveredit = false;
    $scope.newdriver = new Employee();
    $scope.curDriver = {};

    $scope.basepath=$window.location.origin;

    $scope.drivers=[];
    function loadEmployee() {
        Employee.query({designation_id:1},function (head) {
            $scope.drivers = head;
        });
    }loadEmployee();

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Drivers(Transportation)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });



    $scope.newDriver = function () {
        $scope.driveredit = true;
        $scope.newdriver = new Employee();
        $scope.curDriver = {};
    };
    $scope.editDriver = function (thisHead) {
        $scope.driveredit = true;
        $scope.curDriver =  thisHead;
        $scope.newdriver = new Employee();
        $scope.newdriver = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addDriver = function () {

        var photos=[];
        angular.forEach($scope.photos, function (value, key) {
            photos[key]=value;
        });

        if ($scope.curDriver.id) {
            /*$http.put('/api/employee/'+$scope.curDriver.id, $scope.newdriver).
                success(function(data, status)
                {
                    $window.location.reload();
                });*/
            $http.put('/api/employee/'+$scope.curDriver.id,{name:$scope.newdriver.name,
                mobile:$scope.newdriver.mobile,whatsapp:$scope.newdriver.whatsapp,
                email:$scope.newdriver.email,addresskuwait:$scope.newdriver.addresskuwait,
                addressindia:$scope.newdriver.addressindia,mobileindia:$scope.newdriver.mobileindia,
                designation_id:1,basicpay:$scope.newdriver.basicpay,
                overtimepay:$scope.newdriver.overtimepay,passportno:$scope.newdriver.passportno,
                visano:$scope.newdriver.visano,drivinglicenseno:$scope.newdriver.drivinglicenseno,
                remark:$scope.newdriver.remark, photos:photos}).
                success(function(data, status)
                {
                    loadEmployee();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Employee updated Successfully');
                });
        } else{
            /*$scope.newdriver.$save(function(account){
                $window.location.reload();
            });*/
            $http.post('/api/employee',{name:$scope.newdriver.name,
                mobile:$scope.newdriver.mobile,whatsapp:$scope.newdriver.whatsapp,
                email:$scope.newdriver.email,addresskuwait:$scope.newdriver.addresskuwait,
                addressindia:$scope.newdriver.addressindia,mobileindia:$scope.newdriver.mobileindia,
                designation_id:1,basicpay:$scope.newdriver.basicpay,
                overtimepay:$scope.newdriver.overtimepay,passportno:$scope.newdriver.passportno,
                visano:$scope.newdriver.visano,drivinglicenseno:$scope.newdriver.drivinglicenseno,
                remark:$scope.newdriver.remark, photos:photos}).
                success(function(data, status)
                {
                    loadEmployee();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Employee saved Successfully');
                });
        }
        $scope.driveredit = false;
    };
    $scope.deleteDriver = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/employee/'+item.id).
                success(function(data, status)
                {
                    loadEmployee();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Employee deleted Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelDriver = function () {
        $scope.driveredit = false;
        $scope.newdriver =  new Employee();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Driver Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeEmployeeStatus/'+id).
                success(function(data, status)
                {
                    loadEmployee();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Employee Status changed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.open=function(data,ev){
        $http.get('/api/employee/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: $window.location.origin+'/template/employee',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogController($scope, $mdDialog,$window,data) {

        $scope.basepath=$window.location.origin;
        $scope.employee=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

});
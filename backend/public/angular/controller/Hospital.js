app.controller('HospitalController', function($scope,$http,$window,$location,$mdDialog,$anchorScroll,ngNotify,UserInfo,Hospital){
    $scope.hospitaledit = false;
    $scope.newhospital = new Hospital();
    $scope.curHospital = {};

    $scope.hospitals=[];
    function loadHospital() {
        Hospital.query(function (head) {
            $scope.hospitals = head;
        });
    }loadHospital();

    $scope.permission=true;
    UserInfo.query().success(function(data){
        var menu=data.menu;
        var length=menu.length;
        for(var i=0;i<length;i++){
            if(menu[i].name==="Hospitals(Nurse Account)" || menu[i].name==="Hospitals(Transportation)"){
                $scope.permission=true;
                break;
            }else{
                $scope.permission=false;
            }
        }

        // console.log(data);
    });




    $scope.newHospital = function () {
        $scope.hospitaledit = true;
        $scope.newhospital = new Hospital();
        $scope.curHospital = {};
    };
    $scope.editHospital = function (thisHead) {
        $scope.hospitaledit = true;
        $scope.curHospital =  thisHead;
        $scope.newhospital = new Hospital();
        $scope.newhospital = angular.copy(thisHead);
        $anchorScroll();
    };
    $scope.addHospital = function () {
        $scope.newhospital.default=0;
        if ($scope.curHospital.id) {
            $http.put('/api/hospital/'+$scope.curHospital.id, $scope.newhospital).
                success(function(data, status)
                {
                    loadHospital();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Hospital updated Successfully');
                });
        } else{
            $scope.newhospital.$save(function(account){
                loadHospital();
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Hospital saved Successfully');
            });
        }
        $scope.hospitaledit = false;
    };
    $scope.deleteHospital = function (item,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to delete ?')
            .textContent('Are you sure to delete this content.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.delete('/api/hospital/'+item.id).
                success(function(data, status)
                {
                    loadHospital();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Hospital removed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };
    $scope.cancelHospital = function () {
        $scope.hospitaledit = false;
        $scope.newhospital =  new Hospital();
    };

    $scope.changeStaus = function (id,ev) {
        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.querySelector('#popup')))
            .title('Would you like to change Status ?')
            .textContent('Are you sure to Change Hospital Status.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Sure!')
            .cancel('No');

        $mdDialog.show(confirm).then(function() {
            $http.put('/api/changeHospitalStatus/'+id).
                success(function(data, status)
                {
                    loadHospital();
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Hospital Status Changed Successfully');
                });
        }, function() {
            //ANY ACTION WHEN CLICK IN NO
        });
    };

    $scope.openHospital=function(data,ev){
        $http.get('/api/hospital/'+data.id).
            success(function(data, status)
            {
                $mdDialog.show({
                    controller: DialogHospitalController,
                    templateUrl: $window.location.origin+'/template/hospital',
                    parent: angular.element(document.querySelector('#popup')),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    locals: {
                        data: data
                    }
                })
                    .then(function(answer) {
                        //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        //$scope.status = 'You cancelled the dialog.';
                    });
            });

        // console.log($window.location.origin);
    };

    function DialogHospitalController($scope, $mdDialog,data) {
        $scope.hospital=data;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }



});